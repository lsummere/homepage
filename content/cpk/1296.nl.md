---
cpk_affected: vele verbindingen van servers van FNWI
cpk_begin: &id001 2022-09-04 16:00:00
cpk_end: 2022-09-05 11:00:00
cpk_number: 1296
date: *id001
title: Netwerk storing datacenter
url: cpk/1296
---
Nog onbekende oorzaak, diverse verbindingen zijn sterk gedegradeerd.
Update: Een defecte switch is vervangen.
