---
cpk_affected: fnwi login server gebruikers
cpk_begin: &id001 2016-01-11 06:30:00
cpk_end: 2016-01-11 07:30:00
cpk_number: 1154
date: *id001
tags:
- medewerkers
- studenten
title: Server lilo3 en lilo4 maandag ochtend reboot gefaald
url: cpk/1154
---
De server lilo3/4 hing bij down gaan tijdens de maandag ochtend reboot.
Oplossing: power cycle. Oorzaak nog onbekend.
