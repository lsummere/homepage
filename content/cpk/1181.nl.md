---
cpk_affected: Gebruikers van licenties, zie hieronder
cpk_begin: &id001 2016-05-30 06:30:00
cpk_end: 2016-05-30 09:23:00
cpk_number: 1181
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: License server reboot probleem
url: cpk/1181
---
Door een probleem met een nieuwe licentie ging de license server niet
goed down voor de wekelijkse reboot. Toen dit geconstateerd werd, is de
server is gereboot en het probleem is opgelost.

Getroffen licenties:

`Altera Ansys Accelrys Genesys Hdlworks Idapro`
`Xilinx IntelCompiler Ingr Maple Mathematica Matlab MentorGraphics Mestrelab`
`Topspin Orcad Originlab StateaseRlm`
