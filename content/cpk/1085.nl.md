---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-05-06 12:14:00
cpk_end: 2014-05-06 12:35:00
cpk_number: 1085
date: *id001
tags:
- medewerkers
- studenten
title: En weer Print/phpMyAdmin server problemen
url: cpk/1085
---
Net als enkele weken geleden, was er een herstart nodig om de server tot
leven te brengen. Als lapmiddel wordt deze server nu elke nacht
herstart.
