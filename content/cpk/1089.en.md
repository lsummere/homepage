---
cpk_affected: Users of the printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-05-08 16:24:00
cpk_end: 2014-05-08 16:33:00
cpk_number: 1089
date: *id001
tags:
- medewerkers
- studenten
title: And yet again Print/phpMyAdmin server problem
url: cpk/1089
---
Just like yesterday, a reboot of the machine was necessary to restore
the functionality. Tomorrow morning, the server will restart with a new
kernel. It this doesn’t solve the problem, we will look into a new
version of Samba.
