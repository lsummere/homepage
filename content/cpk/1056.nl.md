---
cpk_affected: Gebruikers van Blackboard / Onderwijs in beeld
cpk_begin: &id001 2013-11-15 10:35:00
cpk_end: 2013-11-15 10:55:00
cpk_number: 1056
date: *id001
title: Onderwijs in beeld was 20 minuten niet beschikbaar
url: cpk/1056
---
Een van de servers kreeg om onduidelijke redenen een hoge load en
verleende geen diensten meer aan gebruikers. Na een herstart was het
probleem verdwenen.
