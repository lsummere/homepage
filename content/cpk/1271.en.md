---
cpk_affected: Science mail users wanting to send mail
cpk_begin: &id001 2021-01-22 10:00:00
cpk_end: 2021-01-22 10:30:00
cpk_number: 1271
date: *id001
title: Science smtp service temporarily not usable
url: cpk/1271
---
<itemTags>medewerkers, studenten</itemTags>


A configuration change unwantedly made the smtp service unusable. When
we noticed this, it was repaired immediately.
