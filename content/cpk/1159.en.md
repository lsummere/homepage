---
cpk_affected: 6 laptops of staff departments of the Faculty of Science
cpk_begin: &id001 2016-01-21 11:44:00
cpk_end: 2016-01-21 16:23:00
cpk_number: 1159
date: *id001
title: No free IP numbers in Vlan136
url: cpk/1159
---
The DHCP server didn’t have free leases on Vlan136, so new devices on
Vlan136 didn’t get an IP address, so didn’t have a working network
connection. After we freed some unused numbers for PCs with a fixed
address, we were able to drastically enlarge the number of leases on
Vlan136.
