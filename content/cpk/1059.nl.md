---
cpk_affected: alle RU/UMCN gebruikers
cpk_begin: &id001 2013-12-03 09:36:00
cpk_end: 2013-12-03 10:00:00
cpk_number: 1059
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Stroomstoring 3 december
url: cpk/1059
---
Vrijdagochtend rond half tien was er een kortdurende stroomstoring voor
RU/UMCN, waarschijnlijk veroorzaakt door een schakelfout bij Liander.
Deze storing, die niet vermeld staat op de [stroomstoringen
website](http://www.liander.nl/liander/storingen_onderbrekingen/actueel_storingsoverzicht.htm;jsessionid=8BVgSdwF4SnwgtF62Qhcvp9PZPDSPyVQLfrRsbVGph0MW28zy2Z6!1492477250!2138117550?postcode=6525ED&page=1&ordering=SortDatumVan&skipFlip=&sortPlaats=inactive&sortDatumVan=down&sortDatumTot=inactive&sortTypeOnderbreking=inactive&sortUitgevallenComponenten=inactive&sortOorzaak=inactive&sortEnergiesoort=inactive&sortGetroffenAfnemers=inactive&toonAlles=false&invoerDatumStart=02-12-2013&invoerDatumEind=03-01-2014&energiesoort=E&energietype=option3),
zorgde ervoor dat alle systemen die niet op een UPS aangesloten waren,
herstarten. Omdat de netwerkswitches alleen in vleugel 1 en 7 op een UPS
zitten, verloren ook veel gebruikers gedurende ca. 20 minuten de toegang
tot het netwerk, waaronder draadloos en IP-telefoons. Apparaten die
sneller herstart waren dan het netwerk, kunnen nog een latere herstart
nodig gehad hebben om weer toegang tot het netwerk te krijgen.
