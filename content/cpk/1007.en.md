---
cpk_begin: &id001 2013-01-28 08:00:00
cpk_end: 2013-01-28 09:00:00
cpk_number: 1007
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: New Radius server for ru-wlan and eduroam (wireless)
url: cpk/1007
---
On Monday, January 28th 2013 at 8:00 am, one of the servers that is
being used by the wireless network of the RU, will be replaced. This
replacement will affect you as a user of the wireless networks ru-wlan
and eduroam: There will appear a new certificate when connecting. You
can just accept this, after which the connection should work. If this
appears not to be the case, then it’s best that you remove your old
Eduroam- respectively your old RU-WLAN settings first to activate the
new connection .

Specifically for iPhone / iPad users: We recommend that you first remove
your old Eduroam- respectively your old RU-WLAN profile before
activating the new connection without a profile. If that unexpectedly
fails, please review the information on
[www.ru.nl/wireless](http://www.ru.nl/wireless) for iPhone/iPads. If
necessary, you can also download a new profile from that site.
