---
cpk_affected: Radboudumc gebouwen
cpk_begin: &id001 2015-09-30 16:00:00
cpk_end: 2015-09-30 16:45:00
cpk_number: 1145
date: *id001
tags:
- medewerkers
title: Radboudumc-terrein spanningsloos voor test
url: cpk/1145
---
Op woensdag 30 september rond 16:00 uur zal het Radboudumc een [test
uitvoeren door vrijwel het gehele UMC-terrein spanningsloos te
schakelen](http://www.radboudnet.nl/actueel/nieuws/@1012772/noodstroomtest/).
Voor de gebouwen van de RU heeft deze test geen directe gevolgen.
[Liander](http://www.liander.nl/) geeft aan dat zowel het uitschakelen
als het inschakelen geen merkbare gevolgen heeft voor de rest van het
stroomnet in Nijmegen inclusief de RU. Het blijft echter altijd mogelijk
dat het schakelen van grote vermogens een storing elders kan
veroorzaken. Als onderzoek, onderwijs of andere activiteiten ernstig
hinder kunnen ondervinden van een storing in de stroomvoorziening, kan
men overwegen deze activiteit op 30 september ’s middags bijtijds te
beëindigen of maatregelen te nemen om de continuïteit te garanderen met
noodstroom en UPS.
