---
title: Extra disks aan wnhome stekkeren
author: petervc
cpk_number: 0
cpk_begin: 1997-12-01 14:55:00
cpk_end: 1997-12-01 14:56:00
cpk_affected: veel B-fac gebruikers (voor 10 seconden)
date: 1997-12-01
url: cpk/0
---
Er worden 2 fast-wide 9GB scsi-disks aan de
wide-scsi-controller van wnhome gestekkerd:
- 1 voor NetPC's (overname `/vol/netpc` van `wn0`)
- 1 als staging disk voor Amanda (nieuwe backup-software)

{{< notice info >}}

Dit is één van de allereerste CPK-tjes die we in ons [mailman](/wiki/howto/mailman) archief 
konden terugvinden.

We hebben niet de moeite genomen om dit archief te converteren tot individuele 
markdown bestandjes waar deze website mee wordt gevoed.

[Dit](/downloads/2012/mailman-archive.txt) zijn de ~1k missende CPK-tjes.


{{< /notice >}}
