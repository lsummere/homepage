---
cpk_affected: gitlab users
cpk_begin: &id001 2016-02-26 09:00:00
cpk_end: 2016-02-26 10:45:00
cpk_number: 1168
tags:
- medewerkers
- studenten
date: *id001
title: Gitlab upgrade
url: cpk/1168
---

Because of an upgrade of gitlab to version 8.5.1, the
[GitLab](/en/howto/gitlab/) service will be offline for a while.
