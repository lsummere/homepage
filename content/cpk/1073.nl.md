---
cpk_affected: Gebruikers van gedeelde schijven.
cpk_begin: &id001 2014-03-10 06:30:00
cpk_end: 2014-03-10 10:10:00
cpk_number: 1073
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver heap in de problemen
url: cpk/1073
---
De fileserver had na de maandagochtend reboot problemen met fouten in
een filesysteem (LVmd2-7). Na handmatig reparatie kon de machine
opstarten.
