---
cpk_affected: Radboudumc buildings
cpk_begin: &id001 2015-09-30 16:00:00
cpk_end: 2015-09-30 16:45:00
cpk_number: 1145
date: *id001
tags:
- medewerkers
title: Radboudumc buildings power switched off for test
url: cpk/1145
---
Wednesday September 30, around 4:00 p.m., Radboudumc will carry out a
[test by switching off the power to almost all their
buildings](http://www.radboudnet.nl/actueel/nieuws/@1012772/noodstroomtest/).
For the RU buildings this test has no direct effect.
[Liander](http://www.liander.nl/) states that switching off and on again
will have no noticeable effect on the rest of the power net in Nijmegen
including Radboud University. However, switching high power always has
the possibility of introducing a power problem elsewhere. If research,
education or any activity will be seriously hindered by a power problem,
you might consider ending this activity in time or taking measures to
guarantee continuity with emergency power and UPS.
