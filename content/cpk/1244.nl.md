---
cpk_affected: Gebruikers van de posterprinter "kamerbreed"
cpk_begin: &id001 2019-03-26 00:00:00
cpk_end: 2019-03-27 16:00:00
cpk_number: 1244
date: *id001
tags:
- medewerkers
- studenten
title: Posterprinter "kamerbreed" defect
url: cpk/1244
---
De [posterprinter](/nl/howto/kamerbreed/) is defect, maar het is niet
makkelijk te repareren. We hopen dat de printer over niet te lange tijd
weer beschikbaar zal zijn. Wijk tot die tijd evt. uit naar de [RU
copyshop](http://www.ru.nl/facilitairbedrijf/winkels/copyshop/).
