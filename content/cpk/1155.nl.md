---
cpk_affected: Diverse virtuele machines
cpk_begin: &id001 2016-01-13 07:16:00
cpk_end: 2016-01-13 09:14:00
cpk_number: 1155
date: *id001
tags:
- medewerkers
- studenten
title: Virtuele servers netwerk stukke interfaces na updates
url: cpk/1155
---
De virtuele servers fret, wolf en vos kregen updates voor libvirt,
waardoor de netwerk interfaces van de virtuele machines die op deze
servers draaien verdwenen. Opgelost na een reboot van de server (fret)
of herstart van libvirt daemons en de virtuele machines.

Getroffen virtuele machines:

`bfacdc03 bfacdc04 drukker est2 gitlab3 ms2`
`nwidc01 redmine2 smtp1 smtp2 ts2`
