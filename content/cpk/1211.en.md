---
cpk_affected: GitLab users
cpk_begin: &id001 2017-06-30 09:00:00
cpk_end: 2017-06-30 11:00:00
cpk_number: 1211
date: *id001
tags:
- medewerkers
- studenten
title: GitLab temporarily unavailable during upgrade
url: cpk/1211
---
Because of a planned upgrade, gitlab.science.ru.nl and
gitlab.pep.cs.ru.nl will not be available.
