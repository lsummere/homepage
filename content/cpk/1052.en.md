---
cpk_affected: 'all wireless @ru.nl users '
cpk_begin: &id001 2013-10-23 14:30:00
cpk_end: 2013-10-23 16:15:00
cpk_number: 1052
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Wireless @ru.nl authentication problem
url: cpk/1052
---
Yesterday afternoon around 14:30 the [ISC](http://www.ru.nl/isc)
conducted a seemingly innocent maintenance on the LDAP-server, but
immediately after that auth-requests from Radius were no longer
serviced. This made it impossible for wireless users to authenticate
with their u/s/e number. Users in the realm @science.ru.nl were not
affected by this.
