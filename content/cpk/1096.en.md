---
cpk_affected: 'all users wanting to read Science mail '
cpk_begin: &id001 2014-06-05 14:05:00
cpk_end: 2014-06-05 14:15:00
cpk_number: 1096
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mail server problem
url: cpk/1096
---
The IMAP server got overloaded again. Restarting the IMAP service was
required to bring it back in operation. We think it is related to
snapshots and therefore regularly remove invalid snapshots from now on.
