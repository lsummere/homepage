---
cpk_affected: Users of the goudsmit
cpk_begin: &id001 2014-05-12 13:00:00
cpk_end: 2014-05-12 14:21:00
cpk_number: 1091
date: *id001
tags:
- medewerkers
title: Maintenance Goudsmit
url: cpk/1091
---
One of the systemdisks reported SMART-errors and will be replaced.
