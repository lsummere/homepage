---
cpk_affected: wifi users at the locations mentioned below
cpk_begin: &id001 2013-02-18 18:00:00
cpk_end: 2013-02-18 18:30:00
cpk_number: 1009
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Short interval in wireless network service
url: cpk/1009
---
On Monday feb 18 at 6:00 pm there will be some maintenance at the
wireless network which will effect the following locations at
Toernooiveld:

`FNWI cellar A1` `FEL`
`Huygens: Library of Science, terrace behind Huygens, cantine, room HG-1.132`
`KDV1 en KDV2` `Linnaeus building` `Logistic Centre` `Mercator I`
`Mercator II, ground floor and 7nd floor` `Mercator III, 2nd floor`
`Transitorium FNWI (ACSW and FELIX)` `UBC`

We expect the service will be completely available again within 30
minutes.
