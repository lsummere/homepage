---
cpk_affected: Gebruikers van de posterprinter "kamerbreed"
cpk_begin: &id001 2016-12-02 00:00:00
cpk_end: 2016-12-22 00:00:00
cpk_number: 1191
date: *id001
tags:
- medewerkers
- studenten
title: Posterprinter "kamerbreed" defect
url: cpk/1191
---
De printer is defect. Er zijn onderdelen besteld. We hopen dat de
printer deze maand weer beschikbaar zal zijn.
