---
cpk_affected: 6 laptops van stafafdelingen FNWI
cpk_begin: &id001 2016-01-21 11:44:00
cpk_end: 2016-01-21 16:23:00
cpk_number: 1159
date: *id001
title: Geen vrije IP-nummers in Vlan136
url: cpk/1159
---
De IP-leases (IP-nummers voor PC’s zonder vast IP-adres) van Vlan136
waren volledig in gebruik, daardoor kregen nieuwe apparaten op Vlan136
geen IP-adres en hadden ze dus geen netwerk-connectiviteit. Na melding
hiervan zijn ongebruikte maar bezette nummers vrijgemaakt en is het
aantal IP-leases flink uitgebreid.
