---
cpk_affected: Users with homedirectory server "pile" (as can be seen on `<http://DIY.science.ru.nl>`)
cpk_begin: &id001 2012-10-12 07:00:00
cpk_end: 2012-10-12 09:00:00
cpk_number: 1000
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Announced downtime: home server "pile" down for reboot'
url: cpk/1000
---
Next Friday morning, the home server “pile” will be rebooted. There are
problems with the [snapshots](/en/howto/backup/), which could make a
reboot take more time. Therefore we schedule the reboot for early next
Friday.
