---
cpk_affected: Users of cluster node cn56 and Sun Grid Engine on Ubuntu 12.04 systems
cpk_begin: &id001 2014-07-07 08:00:00
cpk_end: 2014-07-07 11:00:00
cpk_number: 1099
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Failure cn56 disk
url: cpk/1099
---
One of the mirrored disks failed. We didn’t manage to rebuild the raid
set with a new disk. The system is reinstalled with two new disks. After
that, data from /scratch could be recovered from one of the old disks.
