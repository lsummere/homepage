---
cpk_affected: All eduroam users at the RU
cpk_begin: &id001 2015-11-16 08:00:00
cpk_end: 2015-11-16 09:30:00
cpk_number: 1150
date: *id001
tags:
- medewerkers
title: Problems availability wireless network eduroam
url: cpk/1150
---
As a result of a short network service interruption last night, there
were some problems with obtaining ip addresses on the wireless network
eduroam.
