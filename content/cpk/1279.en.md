---
cpk_affected: 'users of the virtual servers: Roundcube, websites with databases on
  this server, ...'
cpk_begin: &id001 2021-03-05 07:45:00
cpk_end: 2021-03-05 09:40:00
cpk_number: 1279
date: *id001
tags:
- medewerkers
- studenten
title: 'Host of several virtual servers broken: Roundcube, websites and others'
url: cpk/1279
---
Yesterday the SSD bootdisk of this VM host reported the first problems.
This morning this had the effect of stopping all VMs running on this
host. By moving the VMs to a different VM host, the problem has been
solved. We will investigate how to best prevent this problem in the
future or lessen its impact.
