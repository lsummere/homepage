---
cpk_affected: Gebruikers van imappsecr, imappdir en astropresentie mappen
cpk_begin: &id001 2018-03-06 08:40:00
cpk_end: 2018-03-06 14:00:00
cpk_number: 1227
date: *id001
tags:
- medewerkers
title: Ransomware on IMAPP shares
url: cpk/1227
---
Door het per ongeluk openen van een e-mail bijlage (genaamd Resume.doc),
waar een macrovirus in zat, zijn alle bestanden en submappen op de
genoemde mappen verwijderd of versleuteld en een bericht achtergelaten
in elke map om naar een bepaalde URL te gaan om ze weer terug te kunnen
krijgen. Wij hebben de files teruggehaald uit de backup en de PC is
opnieuw geinstalleerd. NB. De f-secure software heeft dit document niet
herkent als foute software toen het geopend werd.
