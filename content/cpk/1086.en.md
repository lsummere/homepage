---
cpk_affected: Employees using Uxxxxxx@ru.nl for Eduroam authentication, primarily
  within FNWI
cpk_begin: &id001 2014-04-30 13:00:00
cpk_end: 2014-05-01 16:25:00
cpk_number: 1086
date: *id001
tags:
- medewerkers
title: U-number authentication problem Eduroam
url: cpk/1086
---
The new [RU IdM system](http://www.ru.nl/idm/) erroneously rewrote the
[RU password
hash](http://en.wikipedia.org/wiki/Cryptographic_hash_function#Password_verification)
when data about a person changed. This primariliy affected employees of
FNWI, because for this group the Science email address had been
incorporated in IdM. This will make it possible to use the Science
address as an external mail address when resetting RU passwords. When
the problem was recognized, it was fixed swiftly by restoring the RU
password hashes from backup.
