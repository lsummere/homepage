---
cpk_affected: all network users at the Science Faculty, Mercator, UBC, Childcare Heyendael
cpk_begin: &id001 2017-11-27 23:30:00
cpk_end: 2017-11-28 02:00:00
cpk_number: 1220
date: *id001
tags:
- medewerkers
- studenten
title: 'Network maintenance at Science Faculty: upgrade router'
url: cpk/1220
---
New firmware will be loaded into the Heyendaal-East distribution router.
During this period the wired and wireless network will be unavailable
for approx. 30 minutes.
