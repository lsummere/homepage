---
cpk_affected: Windows users with homedirectory on home2 (check yours in diy.science.ru.nl)
cpk_begin: &id001 2018-03-06 12:00:00
cpk_end: 2018-03-06 12:50:00
cpk_number: 1228
date: *id001
tags:
- medewerkers
title: Homedirectories onbereikbaar op windows
url: cpk/1228
---
During the process of restoring the homedirectory of the affected user
of the ransomware, the samba server was restarted, however it stopped,
but not start. This was noticed only 45 minutes later. We have ajusted
our monitoring to help us notice this more quickly.
