---
cpk_affected: Users of dual boot PC's (the dual-boot menu is served
cpk_begin: &id001 2020-06-03 06:30:00
cpk_end: 2020-06-03 10:12:00
cpk_number: 1262
date: *id001
title: Webserver 'havik' offline
url: cpk/1262
---
by a website) and various websites.

The server couldn't be reached after the scheduled weekly reboot, not
even on its management interface. Because also C&CZ employees work from
home and the interruption didn't get enough urgency fast enough, the
interruption lasted too long, apologies for that. The support partner
has been contacted and the server has been updated, but the origin of
the problem is still unclear. We will also look at making these services
more redundant or more easily movable to a different server.
