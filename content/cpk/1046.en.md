---
cpk_affected: Users of sound in Linux on dual boot PC's
cpk_begin: &id001 2013-09-23 00:00:00
cpk_end: 2017-09-01 00:00:00
cpk_number: 1046
date: *id001
tags:
- medewerkers
- studenten
title: Sound on Linux dual-boot PC's disabled
url: cpk/1046
---
The availability of sound in Windows appeared to be depending on the
state in which Linux had left it. Therefore the sound in Linux on dual
boot PC’s has been disabled, in order to have it available on Windows.
Sound under Linux will be available again once the HP Compaq Elite 8300
AiO harware has been phased out.
