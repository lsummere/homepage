---
cpk_affected: Gebruikers in Mercator1 4de verdieping
cpk_begin: &id001 2014-08-14 16:09:00
cpk_end: 2014-08-15 11:46:00
cpk_number: 1100
date: *id001
tags:
- medewerkers
title: M1.04 geen netwerk
url: cpk/1100
---
Bij de aanpassing van het netwerk voor de verhuizing van ISIS naar
Mercator1 verdieping 0 t/m 3, is abusievelijk door ISC netwerkbeheer ook
de configuratie van de netwerk-switchpoorten van de vierde verdieping
aangepast. Toen vanochtend het probleem gemeld werd, kon het snel
opgelost worden.
