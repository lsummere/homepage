---
cpk_affected: Employees of DCC
cpk_begin: &id001 2015-06-15 06:30:00
cpk_end: 2015-06-15 15:30:00
cpk_number: 1135
date: *id001
tags:
- medewerkers
title: Server homedcc had old homes after reboot
url: cpk/1135
---
After the server reboot on Monday morning, the changes to the home
directories of the last two weeks had disappeared. It is still not clear
how this could have happened. The backups of Saturday June 13 22:55 have
been restored.
