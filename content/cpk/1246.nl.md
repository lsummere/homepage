---
cpk_affected: Gebruikers in Huygens vleugel 4
cpk_begin: &id001 2019-04-13 05:15:00
cpk_end: 2019-04-15 14:00:00
cpk_number: 1246
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkstoring Huygens vleugel 4
url: cpk/1246
---
Door een defect geraakte netwerkswitch zijn ca 40 netwerk-aansluitpunten
in Huygens vleugel 4 tijdelijk buiten bedrijf geraakt. De switch is
inmiddels vervangen.
