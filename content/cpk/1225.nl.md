---
cpk_affected: Science gebruikers met netwerkschijven op deze server
cpk_begin: &id001 2018-01-22 08:30:00
cpk_end: 2018-01-22 09:45:00
cpk_number: 1225
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver onderhoud i.v.m. gerapporteerd schijfprobleem
url: cpk/1225
---
De fileserver rapporteerde vanochtend een schijfprobleem. Om dit te
onderzoeken moet de firmware een update krijgen, hiervoor moet de server
down. Dit hebben we gepland voor a.s. maandagmorgen vanwege het Next
Business Day onderhoudscontract.

Getroffen schijven:

`acfiles aerochem amsbackup amscommon animal astropresentie avance500`
`avance600 beevee b-fac-backup bioniccell bio-orgchem boltje carisma`
`celbio1 celbio2 chemprac cncz coastoffon csgi-archief desda ds ehef1`
`ehef2 ehef3 ehef4 femtospin geminstr2 giphouse gissig global gmi goudsmit1`
`hfml-backup hfml-backup2 hfml-backup3 hfml-backup-inst highres imappdir`
`imappsecr impuls introcie isis janvanhest kaartenbak kangoeroe khaj`
`linova500 linova800 marcurkasco mathsolcie mbaudit1 mbscon mbsl molbiol`
`mwstudiereis neuroinf neuroinf2 nfstest olympus puc randomwalkmodel`
`ruversatest secres2 secrmolbiol sigma sigmacies spectra spectra-rw`
`spm spmdata3 spmdata4 spmdata5 spmdata6 spmdata7 spmmagstm spmnanolab`
`spmstaff ssi ssiguest staff-hfml stroom thalia thaliacies ucm vsc vsc1`
`vsc10 vsc11 vsc12 vsc13 vsc14 vsc15 vsc2 vsc3 vsc4 vsc5 vsc6 vsc7 vsc8`
`vsc9 vscadmin wiskalg wiskunde wkru`
