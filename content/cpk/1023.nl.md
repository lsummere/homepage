---
cpk_affected: alle FNWI-gebruikers die mail wilden lezen via de IMAP-server
cpk_begin: &id001 2013-06-12 13:45:00
cpk_end: 2013-06-12 14:27:00
cpk_number: 1023
date: *id001
tags:
- medewerkers
title: Mailprobleem (IMAP-server)
url: cpk/1023
---
De IMAP-server had een probleem, waardoor een reboot noodzakelijk was.
