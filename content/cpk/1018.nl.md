---
cpk_affected: alle FNWI gebruikers met homedirectories op de bundle
cpk_begin: &id001 2013-04-29 06:30:00
cpk_end: 2013-04-29 11:30:00
cpk_number: 1018
date: *id001
tags:
- medewerkers
- studenten
title: Homeserver bundle herstart probleem
url: cpk/1018
---
De fileserver is niet goed opgekomen na de wekelijkse
maandagochtend-herstart. Pas na volledig stroomloos maken van de machine
kon het console benaderd worden. Middels de reddingsboot zijn alle
snapshots verwijderd maar lukte de herstart niet vanwege een corrupt
filesysteem. Nadat alle filesystemen gecontroleerd waren lukte het pas
de machine correct op te starten. Deze acties hadden uiteindelijk een
uitzonderlijk lange downtijd tot gevolg.
