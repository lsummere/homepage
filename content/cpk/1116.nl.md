---
cpk_affected: vrijwel alle computers in FNWI
cpk_begin: &id001 2014-11-15 11:29:00
cpk_end: 2014-11-17 09:34:00
cpk_number: 1116
date: *id001
tags:
- medewerkers
- studenten
title: DNS resolver probleem
url: cpk/1116
---
De server die binnen FNWI als eerste [DNS
resolver](http://en.wikipedia.org/wiki/Domain_Name_System#DNS_resolvers)
genoemd staat, crashte, waarschijnlijk door een defect rakende harde
schijf. Hierdoor werd voor veel computers in FNWI het netwerk stroperig
of zelfs vrijwel onbruikbaar. Pas na een reboot van deze server was het
probleem1 verholpen. Maandagochtend, bij de wekelijkse reboot van deze
server, bleef deze wachten op een handmatige akkoord op opstarten met
een defecte schijf (probleem2). Allerlei andere servers die daarna om
06:30 rebooten, hadden problemen vanwege de ontbrekende DNS resolver. Er
zijn weer meer maatregelen getroffen om de overlast een volgende keer te
verminderen. Verhuizing naar nieuwe DNS-servers is in voorbereiding.
