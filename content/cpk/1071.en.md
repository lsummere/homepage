---
cpk_affected: Users of a KM MFP C364e as a scanner through e-mail
cpk_begin: &id001 2013-10-12 00:00:00
cpk_end: 2014-02-26 00:00:00
cpk_number: 1071
date: *id001
tags:
- medewerkers
- studenten
title: Scan to email sometimes doesn't work for Konica Minolta MFPs
url: cpk/1071
---
Update: we changed the configuration of the network switchports of the
KM’s, which resolves the problem. This was necessary, because the KM’s
do not try hard enough to make a connection with the SMTP-server.

Update: an alternative is scanning to a USB stick. Log in at the MFP
with the scan-pin, put a document in the feeder or on the document
glass, select `Scan` and then plug the USB stick into the USB port on
the right side of the MFP , on the top side. After a few seconds the MFP
recognizes the USB stick and presents the choice “Save a document to
external memory”. Choose `OK` and press `Start`. See also [RU
<http://www.ru.nl/publish/pages/687597/uitrol-mf-scan.pdf> manual] on
the [RU-page about the
MFP’s](http://www.ru.nl/ictservicecentrum/medewerkers/werkplek-campus/uitrol/).

Users report to us that the scanning to e-mail sometimes doesn’t work,
with an error like “Server not found. Scan deleted”. The problem has
been reported to KM. As far as we know, the problem occurs only seldom.
It can temporarily be resolved by switching the machine off and on
again.
