---
cpk_affected: Users of 75 shared network disks
cpk_begin: &id001 2013-09-17 16:25:00
cpk_end: 2013-09-17 17:15:00
cpk_number: 1043
date: *id001
tags:
- medewerkers
- studenten
title: File server heap problems
url: cpk/1043
---
The fileserver failed to see its disks. A reboot of the machine solved
the problem.
