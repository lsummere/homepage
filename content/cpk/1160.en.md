---
cpk_affected: websites of this server (wielewaal)
cpk_begin: &id001 2016-02-01 06:30:00
cpk_end: 2016-02-01 09:20:00
cpk_number: 1160
date: *id001
tags:
- medewerkers
- studenten
title: 'Webserver down: tftp & egw'
url: cpk/1160
---
Quite a number of websites of ICIS were not available amongst which was
eGroupWare. Furthermore, the boot process of workstations was impaired
(because of the missing tftp). A reboot of the server seems to be
sufficient to solve the issue.

Affected websites:

`blackgem brainhealth croatia14 croatia15 cs eduspec eduspecorg egw`
`ethergids fbkr kickstart mhealth oni snn`
