---
cpk_affected: 'all users wanting to read Science mail '
cpk_begin: &id001 2014-12-15 06:30:00
cpk_end: 2014-12-15 11:15:00
cpk_number: 1121
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mail server problem
url: cpk/1121
---
There was a problem with a failed harddisk of the IMAP server. This
slowed the machine down so that it was practically unusable. The problem
was solved by replacing the disk.
