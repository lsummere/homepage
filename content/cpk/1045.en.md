---
cpk_affected: all FNWI users using the Port Forwarder to connect to BASS RU.
cpk_begin: &id001 2013-06-03 00:00:00
cpk_end: 2013-09-26 08:00:00
cpk_number: 1045
date: *id001
tags:
- medewerkers
- studenten
title: BASS RU problem
url: cpk/1045
---
The central BASS RU environment has been updated last weekend. Part of
this upgrade was a change of the web address of the second logon screen,
as announced on our [BASS](/en/howto/bass/) page. This morning it became
clear that access to the second logon screen via the Port Forwarder
didn’t work anymore. Therefore the UCI rolled back the change of the
second logon screen. This means that AFTER LOGGING ON TO THE PROXY at
<https://admin.ru.nl/> BASS is available again via
<https://bassruap01.uci.ru.nl:8010/OA_HTML/AppsLocalLogin.jsp>

This problem will be finally resolved when the Port Forwarder will be
stopped on October 1, 2013.
