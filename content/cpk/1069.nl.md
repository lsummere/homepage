---
cpk_affected: Gebruikers in Huygens vleugel 5
cpk_begin: &id001 2014-02-03 19:00:00
cpk_end: 2014-02-03 23:59:00
cpk_number: 1069
date: *id001
tags:
- medewerkers
title: Netwerkonderbreking in Huygens vleugel 5
url: cpk/1069
---
Op maandagavond 3 februari tussen 19:00 - 24:00 uur vinden
onderhoudswerkzaamheden plaats aan de netwerkapparatuur in Huygens
vleugel 5. Zowel het bedrade als draadloze netwerk zullen daardoor op
enkele momenten niet beschikbaar zijn op alle locaties in deze vleugel,
op alle verdiepingen.
