---
cpk_affected: FNWI-studenten
cpk_begin: &id001 2014-04-02 11:32:00
cpk_end: 2014-04-04 10:27:00
cpk_number: 1077
date: *id001
tags:
- studenten
title: Printer pr-hg-00-002 printte niet vanuit Windows/Mac
url: cpk/1077
---
Waarschijnlijk door een te grote printjob naar de printer pr-hg-00-002,
raakte woensdagochtend de printqueue voor deze printer op de server
(printto/printsmb/ooievaar) verstopt. Dit probleem werd pas aan het eind
van de donderdag bij C&CZ gerapporteerd. Vrijdagochtend is het
gerepareerd door de printqueue met oude jobs te legen.
