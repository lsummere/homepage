---
cpk_affected: Gebruikers van Science mail
cpk_begin: &id001 2013-09-03 13:12:00
cpk_end: 2013-09-03 13:30:00
cpk_number: 1038
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Herhaling: mailproblemen na weggeven wachtwoord aan phishers'
url: cpk/1038
---
Zojuist bleek dat weer een Science gebruiker het Science-wachtwoord aan
phishers gegeven had. Daarna worden dan steeds de Science mailservers
(horde webmail, smtp) door Internet-criminelen misbruikt om spam te
versturen. In zo’n geval moeten we tijdelijk mailservers stoppen,
terwijl we de spam aan het opruimen zijn. Ook lopen we het risico dat
onze mailservers op zwarte lijsten komen, waardoor alle gebruikers
problemen kunnen hebben met het versturen van mail naar buiten de RU.

SVP: niet zomaar klikken op een link in een e-mail !!!
