---
cpk_affected: All network traffic in and outbound the RU.
cpk_begin: &id001 2014-09-08 22:00:00
cpk_end: 2014-09-09 00:03:00
cpk_number: 1104
date: *id001
tags:
- medewerkers
- studenten
title: Surfnet uplink offline
url: cpk/1104
---
Due to an [error during the planned maintenance by
Surfnet](http://www.ru.nl/ictservicecentrum/actueel/storingsberichten/storing/)
of the RU network interface, the uplink of Radboud University to Surfnet
was down.
