---
cpk_affected: Users of this data/file/vol server
cpk_begin: &id001 2014-05-26 06:45:00
cpk_end: 2014-05-26 13:45:00
cpk_number: 1094
date: *id001
tags:
- medewerkers
- studenten
title: Data/file/vol server (heap) problem
url: cpk/1094
---
The server had a problem, which had started at the Monday morning
reboot. A reboot of the machine solved the problem. Soon all data will
be moved to a new server. This will not cause much problems for users,
because everybody uses the aliases something-srv.science.ru.nl.
