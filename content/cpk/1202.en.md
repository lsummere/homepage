---
cpk_affected: Users of the poster printer "kamerbreed"
cpk_begin: &id001 2017-04-03 00:00:00
cpk_end: 2017-05-01 00:00:00
cpk_number: 1202
date: *id001
tags:
- medewerkers
- studenten
title: Poster printer "kamerbreed" broken
url: cpk/1202
---
The printer has to be repaired, but the engineer can’t easily fix it. We
hope to have the printer operational again in the near future. A new one
with service contract has been ordered, in order to make the poster
service more reliable. In the meantime you might use the [RU
copyshop](http://www.ru.nl/facilitairbedrijf/winkels/copyshop/).
