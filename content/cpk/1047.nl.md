---
cpk_affected: Gebruikers van phpmyadmin en de printto en printsmb printerservers
cpk_begin: &id001 2013-09-27 11:02:00
cpk_end: 2013-09-27 11:10:00
cpk_number: 1047
date: *id001
tags:
- medewerkers
- studenten
title: Printto/printsmb/phpmyadmin server in de problemen
url: cpk/1047
---
De printserver was gecrasht. Na een herstart was het probleem verdwenen.
