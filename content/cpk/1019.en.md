---
cpk_affected: Users of disk volumes/network shares on file server Stack.
cpk_begin: &id001 2013-04-29 04:08:00
cpk_end: 2013-05-01 09:15:00
cpk_number: 1019
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver stack crashed due to failed hard disk
url: cpk/1019
---
Problem: Crash due to defective disk Solution: Deactivated disk using
hot spare and reboot
