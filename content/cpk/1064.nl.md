---
cpk_affected: Gebruikers van netwerkschijven
cpk_begin: &id001 2013-12-23 00:00:00
cpk_end: 2013-12-24 00:00:00
cpk_number: 1064
date: *id001
tags:
- medewerkers
- studenten
title: Verhuizing netwerkschijven naar nieuwe servers
url: cpk/1064
---
In de loop van deze twee dagen zullen bovenstaande shares naar nieuwe
hardware worden verhuisd. Tijdens de verhuizing zullen de shares
tijdelijk niet beschikbaar zijn. Mocht u problemen ondervinden met het
aankoppelen van een netwerkschijf onder windows, koppel altijd aan met
\\\\sharenaam-srv.science.ru.nl\\sharenaam. Zie ook
[Diskruimte\#Naming](/nl/howto/diskruimte#naming/).

Getroffen netwerkschijven:

`acfiles2 botany botany-general carta comsol digicd encapson encapson2`
`exoarchief felix gi2 gi3 hfml-45t hfml-data hfml-engineering ifl iris`
`mailmanincludes mbaudit1 mbaudit2 mbbioel mbcns mbcortex mbdata mbread`
`mbwrite mestrelab mi2 mi3 milkun4 milkun4rw molchem molchem2 molphtec`
`mol-secr multimedia neuropi ns3 nwi-backup onlyme owc pcb planthgl`
`plantkunde-hgl sdisk/software share snn sofie spmdata1 tdisk/cursus tece teceleiding`
`temp tracegastemp wallpaper xpcursus xpsoftware`
