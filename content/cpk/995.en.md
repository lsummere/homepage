---
cpk_affected: Users of disk volumes on file server Plenty. The S and T disks that
  are used in the PC rooms.
cpk_begin: &id001 2012-09-24 06:30:00
cpk_end: 2012-09-24 09:00:00
cpk_number: 995
date: *id001
title: Disk server "Plenty" offline
url: cpk/995
---
During the weekly reboot (monday mornings), the server got stuck in the
BIOS.
