---
author: remcoa
date: '2022-10-06T15:12:45Z'
keywords: []
lang: nl
tags:
- medewerkers
- studenten
- storage
title: Netwerkschijf
wiki_id: '685'
---
### Informatie voor alle netwerkschijven

-   In het algemeen is voor een verbinding met een netwerkschijf een
    servernaam en sharenaam nodig.
-   Gewoonlijk zijn netwerkschijven direct beschikbaar vanuit het RU
    interne netwerk, bedraad of draadloos. Van buiten de RU is een
    [VPN](/nl/howto/vpn/)-tunnel noodzakelijk om een netwerkschijf te
    kunnen koppelen.
-   Een aantal netwerkschijven of -shares zijn beschikbaar voor alle
    Science gebruikers:
    -   De U-schijf: de eigen science home directory: de server- en
        sharenaam zijn te vinden op
        [dhz.science.ru.nl](https://dhz.science.ru.nl/) achter ‘Eigen
        schijf op’. De servernaam is ‘home1.science.ru.nl’ of
        ‘home2.science.ru.nl’. De sharenaam is in elk geval hetzelfde
        als de science loginnaam.
    -   De [S-schijf](/nl/howto/s-schijf/) met veel standaard, direct
        uitvoerbare software - server: software-srv.science.ru.nl,
        share: software.
    -   De [T-schijf](/nl/howto/t-schijf/) met cursus-software - server:
        cursus-srv.science.ru.nl, share: cursus.
    -   De [install disk](/nl/howto/install-share/) met vrijwel alle
        installers voor software met campus-licenties (voor
        licentie-informatie contacteer C&CZ) - server:
        install-srv.science.ru.nl, share: install.
-   Afdelingen van FNWI kunnen ook [schijfruimte
    huren](/nl/howto/diskruimte/).

### Netwerkschijf onder Windows 10

-   Open de bestandsverkenner en ga naar “Deze Computer”
-   Selecteer in de ribbon -\> computer -\> Netwerkverbinding maken
-   Kies een drive-letter uit en vul de netwerklocatie in
-   vink beide keuzes aan en klik op ok
-   Vul als gebruikersnaam b-fac[science gebruikersnaam] in met het
    bijbehorende wachtwoord
-   Vink onthoud mijn gegevens aan en klik op ok.

De netwerk locatie is nu toegevoegd.

### Netwerkschijf onder Windows xp,7,8

Een netwerkschijf aankoppelen:

-   Ga naar de juiste plaats:
    -   Windows 8
        -   Open “Windows verkenner”
            -   Bijvoorbeeld via de zoekfunctie: plaats de muis in de
                uiterste rechterbovenhoek waardoor rechts een verticale
                knoppenbalk (toolbar) uitschuift. Klik op de optie
                “Zoeken” en zoek vervolgens op “verkenner”.
        -   Kies in de verkenner “Deze pc” (Linker verticale menubalk,
            klik erop met de rechtermuisknop)
    -   Windows 7
        -   Rechtsklik op ‘Start’
        -   Kies ‘Windows Verkenner’
        -   Selecteer ‘Mijn Computer’
    -   Windows XP
        -   Rechtsklik op ‘Start’
        -   Kies ‘Windows Verkenner’
        -   Ga naar ‘Tools’ of ‘Extra’ in de menubalk
-   Kies vervolgens “Netwerkverbinding maken…”
-   Kies bij ‘Drive:’ een van de niet gebruikte Drive-letters (b.v. de
    door Windows gesuggereerde letter).
-   Geef als ‘Map:’ op *\\\\servernaam\\sharenaam*.
-   Indien uw PC niet door C&CZ beheerd wordt (bv. door uzelf of door
    het ISC) en u daarop een andere loginnaam heeft dan uw Science
    loginnaam, kies dan ‘Verbinding maken via een andere gebruikersnaam’
    en geef uw Science loginnaam op. Bij gelijke loginnamen wordt
    automatisch de goede loginnaam genomen. U moet dan alleen nog het
    Science wachtwoord invullen.
-   Klik op ‘OK’.

Bijvoorbeeld de S-disk met veel standaard software kan gekoppeld worden
middels ‘\\\\software-srv.science.ru.nl\\software’.

### Netwerkschijf op een Mac

Een netwerkschijf aankoppelen op een Mac:

-   Activeer de Finder b.v. door ergens op de Desktop te klikken.
-   Kies in menu ‘Go’ voor ‘Connect to Server…’ (of typ Command-K).
-   Vul de share-naam in als *<smb://servernaam/sharenaam>* en klik
    ‘Verbind’.
-   Authenticeer met een geldige science
    loginnaam/wachtwoord-combinatie.

Bijvoorbeeld de S-disk met veel standaard software kan gekoppeld worden
middels ‘<smb://software-srv.science.ru.nl/software>’.

### Netwerkschijf op Android

Een netwerkschijf aankoppelen op Android kan door een app met
SMB-ondersteuning te installeren. Er zijn diverse alternatieven:

-   [Astro File
    Browser](https://play.google.com/store/apps/details?id=com.metago.astro&hl=nl)
    met de [Astro SMB
    module](https://play.google.com/store/apps/details?id=com.metago.astro.smb&hl=nl).
    Koop de Ad Free-versie als je geen irritante reclame wil zien. De
    naamgeving van schijven is hetzelfde als in Windows,
    `\\someshare-srv.science.ru.nl\someshare`, zie evt. de pagina over
    [Diskruimte](/nl/howto/diskruimte/).
-   [FolderSync
    Lite](https://play.google.com/store/apps/developer?id=Tacit%20Dynamics).
    Hiermee kan een netwerkschijf gesynchroniseerd worden van en/of naar
    de lokale SD-kaart. Koop de FolderSync niet-Light versie als je geen
    irritante reclame wil zien, de enkele keer dat je deze app echt
    opent. De naamgeving van schijven is hetzelfde als in iOS/Linux,
    <smb://someshare-srv.science.ru.nl/someshare>, zie evt. de pagina
    over [Diskruimte](/nl/howto/diskruimte/).
-   [SyncMe
    Wireless](https://play.google.com/store/apps/details?id=com.bv.wifisync).
    Ook hiermee kan een netwerkschijf gesynchroniseerd worden van en/of
    naar de lokale SD-kaart.
-   [SolidExplorer](https://play.google.com/store/apps/details?id=pl.solidexplorer2).
    De ingebouwde storage manager maakt het mogelijk allerlei soorten
    netwerkschijven te koppelen.

### Netwerkschijf op iPad/iPhone

Zie hiervoor de pagina over [het gebruik van een iPad](/nl/howto/ipad/).

### Netwerkschijf op C&CZ-beheerde Linux

Op de [diskruimte pagina](/nl/howto/diskruimte/) staan details over de
netwerkschijven, die automatisch beschikbaar zijn. Ze zijn te vinden
onder /home voor de home-directory, onder /vol voor de volletjes
(data-volumes) en onder /www voor de bestanden van websites.

### Netwerkschijf op zelfbeheerde Linux

Een netwerkschijf aankoppelen op Linux (dit werkt voor xubuntu 10.4):

-   Installeer smbclient en cifs-utils. *sudo apt-get install smbclient
    cifs-utils*
-   Maak het bestand */etc/samba/user* aan en zet hier

` username=USER`\
` password=PASSWORD`

Neer, waar USER uw science gebruikersnaam en PASSWORD uw science
wachtwoord is. Beveilig dit bestand met *chmod 400 /etc/samba/user*.

-   Maak de map */media/USER* aan.
-   Zet de volgende zin in */etc/fstab*

` //FILESERVER.science.ru.nl/USER /media/USER cifs credentials=/etc/samba/user,noexec,users,uid=1000,gid=100 0 0`

Hier is USER uw gebruikersnaam.

FILESERVER moet vervangen worden door de host die aangegeven wordt bij
[DHZ](https://dhz.science.ru.nl/) onder “Eigen schijf op”.

-   *sudo mount /media/USER* of herstart uw computer.
