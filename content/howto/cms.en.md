---
author: petervc
date: '2021-06-03T09:18:55Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
- contactpersonen
title: CMS
wiki_id: '911'
---
The Radboud University makes use of a Content Management System (CMS)
for websites. All CMS based RU websites are hosted by the ISC. The
university’s Communication department (part of Radboud Services) is the
owner of the service and responsible for its proper operation. C&CZ also
offers [WWW\_Services](/en/howto/www-service/). Which type of service is
best suited depends on the functional requirements.
[C&CZ](/en/howto/contact/) happily advises on this matter.

## Functional Management and Support

All faculties and clusters have one or more facultary CMS functional
managers. The main tasks of the functional managers are:

-   User and access management for CMS users i.e. web site maintainers.
-   Creating new web sites for (new) CMS users, removing obsolete sites,
    and reordering the overall structure of web sites on behalf of the
    owner(s) if necessary.
-   Technical support and advice for CMS users, including the
    coordination of new bug notifications between CMS users and the
    Communication department.
-   Coordination of information about the CMS between users and the
    Communication department.
-   Limited and occasional content management of general facultary web
    pages (seldomly whole web sites).

Contact information for the functional managers can be found on the
[Contact](/en/howto/contact/) page.

## Web Site Maintainers

There are many CMS based web sites at our faculty and thus many site
maintainers. There is a [mailing
list](http://mailman.science.ru.nl/mailman/listinfo/nwiweb) one can
subscribe to, in order to receive information about the CMS, maintenance
issues, bug notifications, etc.
