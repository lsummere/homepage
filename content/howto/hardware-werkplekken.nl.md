---
author: petervc
date: '2012-08-17T14:33:34Z'
keywords: []
lang: nl
tags:
- hardware
title: Hardware werkplekken
wiki_id: '580'
---
## PC werkplek

Iedereen die een PC aan wil schaffen, zal willen weten welke afspraken
er zijn met de [huisleverancier](/nl/howto/huisleverancier-pc's/). C&CZ
kan helpen bij het [installeren](/nl/howto/installeren-werkplek/) of deze
zelfs volledig verzorgen. Daarbij kan men dan kiezen tussen
[MS-Windows](/nl/howto/windows-beheerde-werkplek/) en [Ubuntu
Linux](/nl/howto/unix/). Er is ook informatie over de
[reparatieprocedure](/nl/howto/reparaties/).
