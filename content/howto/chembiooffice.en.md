---
author: polman
date: '2022-04-05T11:52:47Z'
keywords: []
lang: en
tags:
- software
title: ChemBioOffice
wiki_id: '690'
---
### There is a problem with the extension of the license, the supplier has been notified.

The license was supposed to be extended till the end of April while the
negotiations for a new license period are still on going. Unfortunately
this turns out to be not true. We are waiting for an update form the
supplier

ChemBioOffice is a multifunctional software package for searching and
drawing of molecular structures, 3-D visualisation and molecular
modeling, drawing of biological pathways and management of biological
data.

Most important elements of ChemBioOffice:

\#\* ChemDraw: drawing of chemical structures (i.a. name-to-structure
tool) and biological components, NMR and MS fragmentation prediction,
prediction of physical and chemical properties like ClogP and polar
surface area, importing of structures based on name.

\#\* Chem3D: 3-D visualisation of molecules, rotation, zooming,
conformational analysis, molecular modeling and dynamics, both empiric
(MM2) and ab initio (GAMESS), orbital visualisation.

\#\* ChemFinder: searching of molecular structures on the WWW

\#\* ChemACX: database of commercially available structures

\#\* BioDraw: application for drawing, sharing, and presenting
biological pathways. Common pathway elements (membranes, DNA, enzymes,
receptors, reaction arrows, etc.) are built in, and other elements may
be imported.

\#\* BioAssay: manages data from biological experiments. Designed for
chemists and biologists working on pharmaceutical, drug or gene research
and is of particular value for researchers performing in vivo
experiments with complex models, with integration of chemical and
biological data.

\#\* BioViz: transforms the numbers in your database into graphics on
your screen. Retrieve or search for a set of compounds, choose the data
you want to see, whether it is biological test results in Oracle tables,
physical property values calculated automatically or prices in a
catalog, and BioViz will generate an interactive window showing a
scatterplot, histogram, or other useful data graphic.

The software is actively in use in multiple workgroups within the
Science Faculty and in increasing amounts also in the curriculum.
Because of the user-friendliness and intuitive use of the software it is
highly appropriate for student use. **Everyone with an e-mail account
ending in @ru.nl or @student.ru.nl is eligible to download the software
and install it 3 times maximum.**

In order to download ChemBioOffice, registration at the PerkinElmer
website is required:

<https://informatics.perkinelmer.com/sitesubscription/#R>` (See Radboud Universiteit)`

Then go to:

<https://perkinelmer.flexnetoperations.com/control/prkl/login>` `

and download the software. A serial number can be found after login
under “order history”.
