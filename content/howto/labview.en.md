---
author: petervc
date: '2022-03-18T13:43:09Z'
keywords: []
lang: en
tags:
- software
title: LabVIEW
wiki_id: '1084'
---
C&CZ administers the licenses for the use of
[LabVIEW](https://en.wikipedia.org/wiki/LabVIEW) within FNWI. LabVIEW by
[NI](https://www.ni.com/) is a graphical programming environment
engineers use to develop automated research, validation, and production
test systems.
