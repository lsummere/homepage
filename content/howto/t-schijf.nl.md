---
author: petervc
date: '2014-11-14T11:44:26Z'
keywords: []
lang: nl
tags:
- software
title: T-schijf
wiki_id: '533'
---
## T-schijf

Op door [C&CZ beheerde pc’s](/nl/howto/hardware-bwpc/) in terminalkamers
is standaard, naast de [S-schijf](/nl/howto/s-schijf/), een
[netwerkschijf](/nl/howto/netwerkschijf/) gekoppeld als T-schijf met
vooral cursussoftware. Daarop staat een groot aantal programma’s voor
een groot aantal cursussen. Docenten kunnen zelf de cursussoftware
aanpassen. De programma’s kunnen vanaf die netwerkschijf gestart worden
(geen lokale installatie op de pc nodig). Ook op niet-beheerde pc’s kan
men hiervan gebruikmaken.

[Koppel de netwerk share](/nl/howto/netwerkschijf/) aan als
**\\\\cursus-srv.science.ru.nl\\cursus**, gebruik station: T omdat een
deel van de software alleen werkt als de stationsletter T is.

Vink eventueel aan “Opnieuw verbinding maken bij inloggen”.
