---
author: petervc
date: '2017-06-16T16:04:42Z'
keywords: []
lang: nl
tags:
- hardware
title: KM
wiki_id: '912'
---
## Konica Minolta Multifunctionals (Printers/Copiers/Scanners)

De [RU](http://www.ru.nl) heeft in 2013 het printen/kopiëren/scannen
Europees aanbesteed. Op grond van een grote reeks aan eisen is deze
aanbesteding gewonnen door [Konica
Minolta](http://www.konicaminolta.nl). De machines die ingezet worden,
zijn zowel [kopieerapparaat](/nl/howto/copiers/) als
[netwerkprinter](/nl/howto/printers-en-printen/) als
[scanner](/nl/howto/scanners/).

### Documentatie

Alle documentatie is te vinden op de [Konica Minolta
website](http://www.konicaminolta.nl/nl/business-solutions/customer-support/downloads.html).
Posters die het gebruik uitleggen zijn via deze wiki beschikbaar voor de
aanwezige modellen: [Bizhub C364e, C554 en
C554e](/download/old/bizhub-c554-c454-c364-c284-c224_quick-reference_en_1-0-0.pdf)
en voor de hoogvolume [Bizhub
C654](/download/old/bizhub-c754-c654_poster_en_1-0-0.pdf).

### Plaatsing van de apparaten

Vanaf 1 juli 2013 worden in de FNWI-gebouwen [op allerlei
plaatsen](/nl/howto/copiers/) nieuwe multifunctionele
printers/copiers/scanners (MFP’s) van het merk [Konica
Minolta](http://www.konicaminolta.nl/) (KM) aangesloten. Deze apparaten
vervangen de Ricoh apparaten, die enkele weken later zullen verdwijnen.

We hebben besloten om op meer plaatsen MFP’s neer te zetten dan waar nu
Ricoh’s staan. Dit is in lijn met het RU-beleid om de loopafstand vanaf
een willekeurige werkplek tot de dichtstbijzijnde MFP te beperken.
Hierdoor zal het voor afdelingen aantrekkelijker worden om eigen
voorzieningen, met eigen voorraad papier, toner en onderhoud, op te
heffen. Voor de nieuwe apparaten wordt dat alles namelijk door KM
verzorgd tegen een vaste, uniforme tikprijs voor de hele Radboud
Universiteit. Ook melden de machines zelf storingen aan KM (via GSM),
zodat problemen soms al opgelost worden zonder dat ze door een
medewerker of student gemeld zijn. Daarom is de verwachting dat e.e.a.
voor de RU zowel efficiënter als goedkoper zal zijn.

De plaatsing van individuele apparaten kan nog aangepast worden. Op
plaatsen waar veel gebruik gemaakt wordt, kan een grotere/snellere
machine neergezet worden, op plaatsen waar het gebruik te gering is, kan
een machine verwijderd worden.

Neem voor alle vragen over de nieuwe apparaten [contact op met
C&CZ](/nl/howto/contact/).

### Peage

De MFP’s zullen op den duur via [Peage](/nl/howto/peage/) aangestuurd
worden. Omdat FNWI gedeelde voorzieningen heeft voor medewerkers en
studenten, wordt Peage bij FNWI pas ingevoerd als het operationeel is
voor medewerkers. De verwachting is dat dit tegen het einde van 2013 zal
zijn. Met Peage wordt het mogelijk om ook gevoelige documenten naar een
gedeelde printer te sturen, omdat men dan pas bij de printer, na
authenticatie met de campuskaart en persoonlijke pincode, kiest welke
printopdracht uitgevoerd moet worden.

### Printen

Zolang Peage nog niet beschikbaar is voor medewerkers, wordt de
aansturing door C&CZ gedaan. De KM’s worden dus voorlopig ingezet als
[gebudgetteerde C&CZ-printers](/nl/howto/printers-en-printen/) en op
dezelfde manier aangestuurd en afgerekend, al zal men de nieuwe printers
wel op de eigen werkplek moeten definieren.

### Kopiëren

Zolang Peage nog niet beschikbaar is voor medewerkers, krijgt elke KM
een unieke pincode. FNWI-medewerkers kunnen die pincode(s) van C&CZ
krijgen (*bel 56666*), waardoor kopiëren voor hen gratis is: de
faculteit betaalt. Wanneer men klaar is met kopiëren, kan men rechts op
het deurtje met een sleutelgat en twee pijltjes drukken om uit te
loggen, anders doet de machine dat pas na vijf minuten. Bij het
vermoeden van misbruik zal de pincode uit voorzorg door C&CZ gewijzigd
worden.

Studenten kunnen kopiëren op de Peage-MFP bij het restaurant, of ze
kunnen scannen en daarna printen. De Ricoh’s met Xafax-paal zullen op
korte termijn verwijderd worden. Studenten kunnen Xafax-kopieerkaartjes
bij C&CZ laten omzetten in printbudget.

### Scannen

Met de pincode ‘1111’ wordt een machine vrij gegeven om te scannen.
Scannen werkt effectief hetzelfde als met de Ricoh’s; scans worden
gemaild naar een te specificeren adres. De KM’s laten alle adressen zien
uit het [LDAP-adresboek van FNWI](/nl/howto/ldap-adresboek/), waardoor
het invoeren van adressen niet meer nodig is en minder
adresseringsfouten gemaakt zullen worden.

Er zijn diverse redenen waarom een scan naar email-adres fout kan gaan.
De meest voorkomende is een tikfout in het mail-adres, de andere
mogelijkheid is een scan die te groot is voor de centrale
RU-mailservers/Gmail/…, deze hanteren vaak een limiet van ca. 25 MB. In
al die gevallen krijgt C&CZ als afzender van die mail
(KMscan\@science.ru.nl) hiervan bericht. C&CZ probeert deze mails aan
het juiste mail-adres door te sturen of de gebruiker in te lichten dat
de scan te groot was voor de mailprovider. Alternatieven zijn scannen
naar je @science.ru.nl Inbox of scannen naar een USB-stick. Log hiervoor
in op de MFP met de scan-pincode, leg een origineel in de papierinvoer
of op de glasplaat, kies `Scan` en stop daarna de USB-stick in de
USB-poort aan de rechterzijkant van een MFP, bovenaan. Na een paar
seconden herkent de MFP de USB-stick en presenteert de keus “Save a
document to external memory”. Kies `OK` en druk op `Start`. Zie ook de
[RU-handleiding](http://www.ru.nl/publish/pages/687597/uitrol-mf-scan.pdf)
op de [RU-pagina over de
MFP’s](http://www.ru.nl/ict/medewerkers/printen-kopieren/).

### Service

Voor hardware- en papierstoringen kan de Konica Minolta helpdesk bereikt
worden op telefoonnummer 55955, optie 4. Zij zullen als eerste vragen
naar het serienummer of machine id. Deze is op de machine te vinden,
maar ook in de [lijst van alle multifunctionals](/nl/howto/copiers/). Er
is een Konica Minolta onderhoudsmonteur op de campus aanwezig. Daardoor
kunnen gemelde storingen in de regel vrij snel opgelost worden.
