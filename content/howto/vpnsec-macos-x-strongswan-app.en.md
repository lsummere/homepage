---
author: wim
date: '2016-03-10T13:10:03Z'
keywords: []
lang: en
tags: []
title: VpnSec MacOS X strongSwan App
wiki_id: '1016'
---
Borrowed from
<https://caedm.et.byu.edu/wiki/index.php5/VPN_Instructions_for_Mac_OS_X>

### Setup the IKEv2 VPN on Mac OS X

{{< notice note >}}
The following instructions are based on OS X 10.9 (Mavericks) and 10.10 (Yosemite). Other versions
of OS X may vary.
{{< /notice >}}

1.  Download the **strongSwan** client zip file
    from <http://download.strongswan.org/osx/>
2.  Double click the **strongswan-xxx.app.zip**
    file you just downloaded. This will extract the archive in the
    current folder.
3.  Drag the **strongSwan** application to your
    **Applications** folder
4.  Double click the **strongSwan** application
5.  Confirm that you want to open an application downloaded from the
    Internet by clicking **Open**
    -   *NOTE*: You will now have a light-gray
        swan icon in your menu bar. This will be the only indication
        strongSwan is running.
6.  Click on the light-gray **strongSwan** icon in
    the menu bar, and click **Add Connection…**
7.  In the **Connection name** field, enter
    `VpnSec`
8.  In the **Authentication** selector, choose
    *IKEv2 EAP*
9.  In the **Server address** field, enter
    `vpnsec.science.ru.nl`
10. In the **Username** field, enter your science
    username
11. Open up the **System Preferences** application
12. Select **Network**
13. Unlock the settings by clicking on the padlock icon on the left side
14. In the **Authenticate** dialog, enter your
    password for your computer, and click
    **Unlock**
15. In the **Location** field, select
    *Edit Locations*
16. Click the **+** button, and in the new entry
    type `VpnSec     Location`
17. Click **Done**. The
    **Location** field should now say
    **VpnSec Location**
18. Click the **Advanced…** button at the bottom of
    the **Network** settings window
19. Click the **DNS** tab
20. Under the **DNS Servers** box, click the
    **+** button
21. In the new entry, type `131.174.224.4`
22. Under the **DNS Servers** box, click the
    **+** button again
23. In the new entry, type `131.174.16.133`
24. Click **OK**
25. Click **Apply** at the bottom of the
    **Network** window
26. Close the **Network** window by clicking on the
    red dot in the upper left corner
27. Click the **Apple** icon in the menu bar
28. Point to **Location**, and click
    **Automatic**

### To connect to the IKEv2 VPN

1.  Start the **strongSwan** app from the Spotlight
    (if it isn’t already running)
2.  Click on the light-gray **strongSwan** icon in
    the menu bar
3.  Point to **VpnSec**, and click on
    **Connect**
4.  In the **Password required** dialog that pops
    up, enter your science password, and click
    **OK**
    -   *NOTE*: When the light-gray strongSwan
        icon in the menu bar changes to black, you are connected to the
        VPN. But, you must change the Network Location, or you will be
        unable to connect to other computers.
5.  Click the **Apple** icon in the menu bar
6.  Point to **Location**, and click
    **VpnSec Location**

You are now connected to the IKEv2 VPN.

### To disconnect from the IKEv2 VPN

1.  Click on the **strongSwan** icon in the menu
    bar
2.  Point to **VpnSec**, and click
    **Disconnect**
    -   *NOTE*: When the black strongSwan icon in
        the menu bar changes to light-gray, you are disconnect from the
        VPN. But you must change the Network Location back to Automatic,
        or you will be unable to connect to other computers.
3.  Click the **Apple** icon in the menu bar
4.  Point to **Location**, and click
    **Automatic**

You are now disconnected from the IKEv2 VPN
