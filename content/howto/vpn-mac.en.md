---
author: remcoa
date: '2022-10-06T15:12:05Z'
keywords: []
lang: en
tags:
- internet
title: Vpn mac
wiki_id: '805'
---
# PPTP VPN is now turned off!

as of december 1st, pptp vpn is no longer available.

## OLD: Configuring a PPTP VPN connection in OS X

Information on the [new Science VPN](/en/howto/vpn/) can be found
elsewhere on this wiki, below you only find how to configure and setup a
VPN connection to the \*\*OLD\*\* Science VPN server on a Mac running OS
X 10.6 (Snow Leopard). In OS X 10.5 (Leopard) the procedure is
practically the same. In OS X 10.4 (Tiger) a separate VPN application in
the Applications folder is used.

-   Open System Preferences;
-   Choose “Network” configuration;
-   Click on the + in the lower left corner to define a new interface;

{{< figure src="/img/old/vpn-2-define-interface.png" title="VPN-2-Define-Interface.png" >}}

-   Choose Interface: VPN, VPN Type: PPTP;
-   Choose a suitable name for the new interface, e.g. “Science VPN”;
-   Press Create to return to the Network Preferences pane;
-   A new VPN interface is added to the list on the left, select it;

{{< figure src="/img/old/vpn-3-configure.png" title="VPN-3-Configure.png" >}}

-   Specify the VPN server name: vpn.science.ru.nl;
-   Enter your Science login name;
-   Select “Show VPN status in menu bar”;
-   Press Apply to save the changes;

{{< figure src="/img/old/vpn-4-connect.png" title="VPN-4-Connect.png" >}}

-   Connect VPN from the VPN icon in the top menu bar;

{{< figure src="/img/old/vpn-5-authenticate.png" title="VPN-5-Authenticate.png" >}}

-   Enter your password in the authentication pop-up;

{{< figure src="/img/old/vpn-6-connected.png" title="VPN-6-Connected.png" >}}

-   The Network Preferences pane will show the connected status.

## Troubleshooting

Possible causes for the VPN connection to fail:

-   The Internet-connection is not available, check if you can browse
    the Internet.
-   The VPN connection was not properly configured, verify all
    configuration parameters.
-   The Science login name and password combination is not valid, try to
    login on [dhz.science.ru.nl](https://dhz.science.ru.nl/) to verify.
-   The VPN server is tempoprarily unavailable, ask C&CZ.
-   Your Internet provider does not allow GRE traffic (unlikely).
-   Your broadband router does not support GRE traffic (unlikely).
