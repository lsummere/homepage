---
author: wim
date: '2010-04-09T08:34:05Z'
keywords: []
lang: en
tags:
- internet
title: Vpn windowsxp
wiki_id: '75'
---
## Het opzetten van een PPTP VPN in Windows XP

Om een PPTP VPN op te zetten in Windows XP zijn een aantal onderdelen
nodig. U moet minimaal beschikken over:

-   Een internet connectie
-   Een provider met GRE ondersteuning (standaard)
-   Een kabel/ADSL router (mits van toepassing) met GRE ondersteuning
    (standaard)
-   Een gebruikersnaam en wachtwoord

Deze Procedure is onder te verdelen in 3 onderdelen:

-   [Installatie](#installatie)
-   [Configuratie](#configuratie)
-   [Vragen en problemen](#vragen_en_problemen)

### Installatie

Eerst opent u het configuratiescherm. Bij een standaard installatie is
dit te vinden onder:

**Start -\> Control Panel / Configuratiescherm**

Er zijn 2 soorten indelingen voor het configuratiescherm. De standaard
indeling is de **[Categorie](#category_view_/_categorie_indeling)**
weergave. Er bestaat echter ook een
**[Klassieke](#classic_view_/_klassieke_indeling)** weergave.

#### Category view / Categorie indeling

Dubbelklik op **Network and Internet Connections / Netwerk en Internet
verbindingen**.

Klik **Create a connection to the network at your workplace / Verbinding
maken met bedrijfsnetwerk**.

Vervolgens kiest u **Virtual Private Network connection /
VPN-verbinding**.

{{< figure src="/img/old/pptp\_du\_xp\_cat-1.jpg" title="pptp\_du\_xp\_cat-1.jpg" >}}

Klik **Next / Volgende**. Geef de connectie een naam , Bijvoorbeeld
**B-FAC vpn connectie**.

{{< figure src="/img/old/pptp\_du\_xp\_cat-2.jpg" title="pptp\_du\_xp\_cat-2.jpg" >}}

Klik **Next / Volgende**. Selecteer eventueele verbindingen die nodig
zijn om verbinding te krijgen met het internet.

{{< figure src="/img/old/pptp\_du\_xp\_cat-3.jpg" title="pptp\_du\_xp\_cat-3.jpg" >}}

Klik **Next / Volgende**. Vul de hostnaam of het IP adres van de VPN
server in.

{{< figure src="/img/old/pptp\_du\_xp\_cat-4.jpg" title="pptp\_du\_xp\_cat-4.jpg" >}}

Klik **Next / Volgende**. Kies of de verbinding beschikbaar moet zijn
voor u alleen of voor iedere login op de computer.

{{< figure src="/img/old/pptp\_du\_xp\_cat-5.jpg" title="pptp\_du\_xp\_cat-5.jpg" >}}

Klik **Next / Volgende**. Vink de optie **Add a shortcut to this
connection to my desktop / Snelkoppeling aan het bureaublad toevoegen**
aan.

{{< figure src="/img/old/pptp\_du\_xp\_cat-6.jpg" title="pptp\_du\_xp\_cat-6.jpg" >}}\
Klik **Finish / Voltooien**.

Nu kunt u [hier](#configuratie) verder gaan met de volgende stappen.

Hieronder volgen dezelfde installatiestappen, maar dan voor de klassieke
configuratieschermweergave.

#### Classic View / Klassieke indeling

Dubbelklik op **Network Connections / Netwerkverbindingen**.

Klik op **Create a new connection / Een nieuwe verbinding maken** aan de
linker kant.

{{< figure src="/img/old/pptp\_du\_xp\_kla-1.jpg" title="pptp\_du\_xp\_kla-1.jpg" >}}

Klik **Next / Volgende**. Kies optie 2 **Connect to the network at my
workplace / Verbinding met het netwerk op mijn werk maken**.

{{< figure src="/img/old/pptp\_du\_xp\_kla-2.jpg" title="pptp\_du\_xp\_kla-2.jpg" >}}

Klik **Next / Volgende**. Selecteer **Virtual Private Network connection
/ VPN-verbinding**.

{{< figure src="/img/old/pptp\_du\_xp\_kla-3.jpg" title="pptp\_du\_xp\_kla-3.jpg" >}}

Klik **Next / Volgende**. Geef de verbinding een toepasselijke naam,
bijvoorbeeld “B-FAC vpn connectie”.

{{< figure src="/img/old/pptp\_du\_xp\_kla-4.jpg" title="pptp\_du\_xp\_kla-4.jpg" >}}

Klik **Next / Volgende**. Selecteer eventueele verbindingen die nodig
zijn om verbinding te krijgen met het internet.

{{< figure src="/img/old/pptp\_du\_xp\_kla-5.jpg" title="pptp\_du\_xp\_kla-5.jpg" >}}

Klik **Next / Volgende**. Vul de hostnaam of IP adres van de VPN server
in.

{{< figure src="/img/old/pptp\_du\_xp\_kla-6.jpg" title="pptp\_du\_xp\_kla-6.jpg" >}}

Klik **Next / Volgende**. Kies of de verbinding beschikbaar moet zijn
voor u alleen of voor iedere login op de computer.

{{< figure src="/img/old/pptp\_du\_xp\_cat-5.jpg" title="pptp\_du\_xp\_cat-5.jpg" >}}

Klik **Next / Volgende**. Vink de optie **Add a shortcut to this
connection to my desktop / Snelkoppeling aan het bureaublad toevoegen**
aan.

{{< figure src="/img/old/pptp\_du\_xp\_kla-7.jpg" title="pptp\_du\_xp\_kla-7.jpg" >}}

Klik **Finish / Voltooien**.

### Configuratie

Als de installatie van de connectie voltooid is moeten er nog enkele
dingen aangepast worden. Klik op de snelkoppeling op het bureaublad die
naar de verbinding verwijst.

{{< figure src="/img/old/pptp\_du\_xp\_all-1.jpg" title="pptp\_du\_xp\_all-1.jpg" >}}

klik op **Properties / Eigenschappen**. Ga naar het tabblad **Networking
/ Netwerk** Zet het type VPN op **PPTP**.

Selecteer vervolgens **Internet-protocol (TCP/IP)**

{{< figure src="/img/old/pptp\_du\_xp\_all-2.jpg" title="pptp\_du\_xp\_all-2.jpg" >}}

klik op **Properties / Eigenschappen**… en daarna op **Advanced /
Geavanceerd**.

{{< figure src="/img/old/pptp\_du\_xp\_all-3.jpg" title="pptp\_du\_xp\_all-3.jpg" >}}

In het tabblad **General / Algemeen** moet u de optie **Use default
gateway on remote network / Standaard-gateway in het externe netwerk
gebruiken** uitvinken wanneer u alleen verbinding maakt naar de
universiteit. Maakt u echter verbinding naar niet universitaire sites,
waarvoor het belangrijk is dat de verbinding vanuit het universitaire
netwerk lijkt te komen, dan moet deze optie juist aan staan!

{{< figure src="/img/old/pptp\_du\_xp\_all-4.jpg" title="pptp\_du\_xp\_all-4.jpg" >}}

Vervolgens alle openstaande schermen sluiten met **OK** totdat u weer
bij het volgende scherm komt.

{{< figure src="/img/old/pptp\_du\_xp\_all-1.jpg" title="pptp\_du\_xp\_all-1.jpg" >}}

Nu moet u de gebruikersnaam en het wachtwoord invoeren en op **Connect /
Verbinden** klikken.

In het geval dat alles correct is zou de verbinding binnen een minuut
opgebouwd moeten zijn.

### Vragen en problemen

**Vraag**: Ik krijg een foutmelding over “Your credentials have failed
remote network authentication”.

**Antwoord**:

-   U gebruikersnaam of wachtwoord is onjuist. Neem contact op met
    systeembeheer.
-   U bevind zich in een domein en het domein wordt toegevoegd aan u
    gebruikersnaam. Kijk of u het domein kunt verwijderen of zonodig
    neem contact op met systeembeheer voor een aanpassing.

**Vraag**: Error 734: The PPP link control protocol was terminated.

**Antwoord**: U gebruikt mogelijk een te lage of geen encryptie op de
verbinding. Zie de installatieprocedure voor de correcte instellingen.

Vraag: Error 769: The specified destination is not reachable. Antwoord:
De hostname of het IP adres van de machine waar nu naar connect zijn
onjuist. Controleer de instellingen en indien nodig neem contact op met
het systeembeheer.

**Vraag**: Error 678: There was no answer.

**Antwoord**: de hostname of het IP addres zijn onjuist, Controleer de
instellingen.

**Vraag**: Error 619: the specified port is not connected.

**Antwoord**:

1.  Er is iets fout gegaan met de connectie, herstart windows en probeer
    het opnieuw.
2.  U draait een firewall die GRE pakketten blokkeert.
3.  Uw Service provider blokkeert GRE pakketten.
4.  Uw ADSL/Kabel router blokkeert GRE pakketten.

**Vraag**: Error 741: The local computer does not support the required
data encryption type.

**Antwoord**: Er wordt gebruikt gemaakt van een andere methode als dat
de server toestaat. Controleer instelling.

**Vraag**: Als ik de VPN connectie start werkt mijn internet niet meer.

**Antwoord**: U heeft de gateway naar het remote netwerk aan laten
staan. Controleer de instelling.

**Vraag**: Soms wordt mijn verbinding verbroken.

**Antwoord**: Raadpleeg systeembeheer, het is mogelijk dat iemand anders
gebruik maakt van u account.

**Vraag**: Werkt PPTP ook over een NAT verbinding ?

**Antwoord**: Ja, echter in sommige gevallen kan het zo zijn dat de
router geen GRE pakketten doorlaat. Er zijn situaties waarbij het niet
mogelijk is met meer dan een PC te connected als NAT wordt gebruikt.

**Vraag**: Werkt Xwin32 over de VPN verbinding ?

**Antwoord**: Het is mogelijk om Xwin32 te gebruiken. Het is te
gebruiken alsof het op het normale B-FAC netwerk is. Echter moet wel
rekening worden gehouden met de snelheid van de verbinding die vaak vele
malen langzamer is.

**Vraag**: Er gebeuren vreemde dingen op mijn computer, zijn er logs van
connecties ?

*’Antwoord*: Ja, er worden logs bijgehouden van welke connecties er naar
de VPN tunnels gaan. Mocht er iets vreemd aan de hand zijn dan kunt u
contact opnemen met systeembeheer. Zorg wel dat u over voldoende
gegevens beschikt zoals : tijdstip, gebruikersnaam, IP adres van u
verbinding. Tevens worden er logs bijgehouden van de connecties van de
VPN naar het KUN netwerk. Het is niet mogelijk om vanuit buiten het KUN
Netwerk de VPN verbinding te benaderen en visaversa.

**Vraag**: Kan ik vanuit het KUN netwerk mijn computer thuis benaderen ?

**Antwoord**: Ja, mits u het IP adres of domeinnaam weet die u is
toegewezen.

**Vraag**: Ik krijg een foutmelding over “Your credentials have failed
remote network authentication”.\
**Antwoord**:\
**Vraag**: Hoe F-Secure configureren onder Windows XP

**Antwoord**: U dient GRE in de F-Secure Firewall te enablen. Zie
[Firewall configuratie aanpassen](/en/howto/vpn-f-secure/) een
<http://forum.f-secure.com/topic.asp?TOPIC_ID=8461>
