---
author: petervc
date: '2022-06-28T14:52:51Z'
keywords: []
lang: nl
tags:
- software
title: Unix
wiki_id: '156'
---
## Linux software voor medewerkers en studenten van FNWI

### Beschikbare software op door C&CZ beheerde Linux-computers

`<include src="/var/www1/wiki/live/htdocs/cncz/include/software-cfengine.wiki" nopre wikitext />`{=html}

-   Allerlei software in /usr/local, /opt/

### Om zelf te installeren op een zelfbeheerde Linux PC

Op de [Install netwerkschijf](/nl/howto/install-share/) is de
onderstaande software voor Linux te vinden:

-   Mathematische programmatuur:
    -   [Maple](/nl/howto/maple/)
    -   [Matlab](/nl/howto/matlab/)
    -   [Mathematica](/nl/howto/mathematica/)
    -   IBM - [SPSS](http://www.ibm.com/spss)
        (Statistiek)
-   Office software:
    -   Konica Minolta [printerdrivers](/nl/howto/printers-en-printen/)
    -   TeX: [TeX Live](http://www.tug.org/texlive)
-   Data acquisitie programmeeromgeving:
    -   National Instruments [LabVIEW](/nl/howto/nieuws/)

### Te leen bij C&CZ

Op dit moment is bij C&CZ op kamer HG00.051 de volgende Linux software
te lenen door medewerkers en studenten van de B-faculteit:

-   Operating system:
    -   [Ubuntu](http://www.ubuntu.com) Linux
