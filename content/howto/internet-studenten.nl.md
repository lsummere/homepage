---
author: petervc
date: '2019-09-06T16:16:30Z'
keywords: []
lang: nl
tags:
- internet
title: Internet studenten
wiki_id: '69'
---
## studenten

## WWW door studenten

Alle zes studieverenigingen van de B-faculteit hebben een WWW service.
Dit zijn

-   [Thalia](http://www.thalia.nu/) (Informatica), *[1](mailto:www-thalia@science.ru.nl)*
-   [Desda](https://www.desda.org/) (Wiskunde),
    *[2](mailto:www-desda@science.ru.nl)*
-   [Sigma](https://www.vcmw-sigma.nl/) (Scheikunde),
    *[3](mailto:www-sigma@science.ru.nl)*
-   [Marie Curie](http://www.marie.science.ru.nl/)
    (Natuurkunde), *[4](mailto:www-marie@science.ru.nl)*
-   [BeeVee](http://www.beevee.science.ru.nl/)
    (Biologie), *[5](mailto:www-beevee@science.ru.nl)*
-   [Leonardo](http://www.leonardo.science.ru.nl/)
    (Natuurwetenschappen),
    *[6](mailto:www-leonardo@science.ru.nl)*

Studenten die met plannen rondlopen voor een eigen WWW service, wordt
aangeraden om contact op te nemen met de desbetreffende vereniging. Dit
kan via het Web, maar ook rechtstreeks door middel van een mailtje naar
de hierboven vermelde adressen.

Lukt het niet om de betreffende studievereniging warm te krijgen voor je
ideeën, maar heb je al een afdeling of vakgroep gekozen voor je
afstudeerwerk, dan kun je misschien daar terecht. Als zij nog geen
WWW-service aanbieden, dan is het misschien een idee om het initiatief
te nemen en een [nieuwe WWW-service](/nl/howto/internet-wwwservice/) voor
de afdeling of vakgroep te beginnen. Lukt dat allemaal niet, kom dan
eens langs bij C&CZ, afdeling
[systeembeheer](/nl/howto/overcncz-wieiswie-systeembeheer/) om te kijken
of je een WWW service in eigen beheer kunt opzetten.
