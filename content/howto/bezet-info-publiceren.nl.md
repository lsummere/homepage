---
author: petervc
date: '2011-08-18T14:34:37Z'
keywords: []
lang: nl
tags: []
title: Bezet-info publiceren
wiki_id: '723'
---
### Bezet-informatie publiceren uit agenda

Om het mogelijk te maken dat anderen, i.h.b. het Faculteitsbureau,
toegang hebben tot de bezet-informatie (blokkeertijden) uit de door u
gebruikte elektronische agenda, kunt u de volgende aanwijzingen volgen.
Als meer mensen deze dienst gebruiken, is men minder tijd kwijt aan
agendabeheer. N.B.: de bezet-informatie bevat alleen wanneer u bezet
bent maar niet waar, waarmee of met wie. Dit is een tijdelijke
oplossing: wanneer (in 2011?) een nieuw RU-mail/agendasysteem volledig
in bedrijf is genomen, kan men agenda’s eenvoudig en goed delen.

De oplossing bestaat uit URL’s van de vorm:
<https://calendar.science.ru.nl/freebusy/V.Achternaam.vfb> (let op
hoofdletters V en A, alles natuurlijk aangepast aan de eigen naam), waar
de bezet-informatie in Internet-standaard [iCalendar
VFREEBUSY-formaat](http://en.wikipedia.org/wiki/ICalendar#Free.2Fbusy_time_.28VFREEBUSY.29)
in gepubliceerd wordt. Deze URL’s kunnen in andere agenda-systemen
gebruikt worden.

# Calendar.science.ru.nl E-Groupware

Geen gebruikersactie nodig. De beheerders (C&CZ) publiceren deze info.

# Egw.cs.ru.nl E-Groupware

Geen gebruikersactie nodig. De beheerders (C&CZ) publiceren deze info.

# Outlook

1.  Ga in Outlook naar “Extra -\> Opties -\> Agenda opties”.
2.  Klik onder “Geavanceerd” op “Opties voor beschikbaarheidsinfo”.
3.  Zet een vinkje voor “Publiceren op mijn locatie” en vul als locatie
    in:
4.  <https://calendar.science.ru.nl/freebusy/V.Achternaam.vfb>
5.  Met natuurlijk de eigen Voorletter en Achternaam ingevuld op de
    plaats van V.Achternaam.
6.  Verander het aantal maanden wat gepubliceerd moet worden van 2 (de
    default) naar 12.
7.  Klik OK en herstart Outlook. Na ca. 15 minuten zal Outlook de
    beschikbaarheidsinfo gepubliceerd hebben.
8.  Stuur een mail naar postmaster van science.ru.nl met de mededeling
    dat de bezet-informatie gedeeld is.

# Google Calendar

1.  Log in op <http://www.google.com/calendar>
2.  Klik in de linkerbalk onder “Mijn Agenda’s” op het menu-vinkje naast
    de agenda die u wil delen en klik op “Agenda instellingen”.
3.  Kies tabblad: “Deze agenda delen”.
4.  Zet vinkjes bij “Deze agenda openbaar maken” en bij “Alleen mijn
    status (beschikbaar/bezet) delen (details verbergen)”.
5.  Stuur een mail naar postmaster van science.ru.nl met daarin
    1.  de Google gebruikersnaam
    2.  de mededeling dat de free/busy info gedeeld is.
