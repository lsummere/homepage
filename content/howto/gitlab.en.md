---
author: bram
date: '2021-12-03T16:33:36Z'
keywords: []
tags: [ science login, gitlab ]
title: GitLab
wiki_id: '945'
cover:
  image: img/2021/gitlab.png
---
## About GitLab

Quote from the [GitLab website](https://about.gitlab.com/features/)

> GitLab is an incredibly powerful open source code collaboration
> platform, git repository manager, issue tracker and code reviewer.

Our GitLab service is facing the public internet. Meaning you don’t need
to make use of a [VPN connection](/en/howto/vpn) to use it.

## Logging in

    Navigate to:

<http://gitlab.science.ru.nl>

You’ll see two login options:

- Science login - for students and employees of the Science Faculty.  Use your [Science login](/en/howto/login/).
- Standard - for external users.

## Science login

Although anyone with a [Science Login](/en/howto/login/) can login to
GitLab, only people that have actually logged in to GitLab will be
visible for other GitLab users. Keep that in mind when adding people to
projects or groups.

## External users

Since projects are usually not restricted to employees or students of
the Science Faculty, our GitLab service allows external users as well.
If you wish to add external users to your GitLab project, please send a
request to postmaster\@science.ru.nl, mentioning the following details:

| item         | description                                                                                                     |
| :----------- | --------------------------------------------------------------------------------------------------------------- |
| login name   | We’ll prefix it with an underscore by means of avoiding collisions with our existing and future Science logins. |
| full name    | This is how you’ll find the user in the GitLab system. For example, when adding the account to your project.    |
| mail address | GitLab will use this mail address to send an automatically generated mail with instructions to set a password.  |


As soon as [Postmaster](/en/howto/contact) created the account, you should be able to add the
external login as a member your project. We’re not yet sure whether
external users should have the option to create projects, so the project
limit for external users is set to 0 initially. Depending on the kind of
work flow (gitlab merge requests, etc), it might be necessary to give
external users the option to create one or more projects.

## Git and managing large files

As of July 18th 2016, LFS (Large File Storage) support is enabled on the
GitLab server. Git LFS allows you to manage version control on large
files without storing them in the git repository. Please refer to the
[Git LFS project website](https://git-lfs.github.com/) for more
information.

## Documentation

- [Git documentation](http://git-scm.com/documentation)
- [General GitLab documentation](http://doc.gitlab.com/)

## Changes
- 2022-10-17 enabled the feature-flags for the [Debian API](https://docs.gitlab.com/ee/user/packages/debian_repository/#enable-the-debian-api).
- 2021-12-03 Enabled [GitLab pages](/nl/news/2021-12-07_gitlab-pages-installed).
- 2021-10-21 enabled the Docker Container Registry service. See the [gitlab
 documentation](https://gitlab.science.ru.nl/help/user/packages/container_registry/index.md) for more info.
- 2020-06-26 gitlab.pep.cs.ru.nl moved to a new Ubuntu 20.04 server. These are the ssh fingerprints for `gitlab.pep.cs.ru.nl`:

``` text
256 SHA256:bVYa+loFqV0Hdv3sPukp40zf1LdQd4PNBOSJa6oRnyE root@gitlab9 (ECDSA)
256 SHA256:j4pDiX2SSKGHjoZ1OQXgrKpIvLxrCnckZCtYWapHFcM root@gitlab9 (ED25519)
3072 SHA256:QIdVcGdaEhyYXcUVtc2FIa1R9F/1KUekR+7IV23c0LI root@gitlab9 (RSA)
```

- 2019-01-25 GitLab updated to version 11.7.0 and moved to a new Ubuntu 18.04 server. You might encounter an SSH prompt when pushing or pulling code. The current ssh fingerprints for `gitlab.science.ru.nl` are:

``` text
SHA256:7G/4WD/tUE8H0k9MkBznlcx+ZWgUxnn3KazKm1XQwRk root@gitlab (ED25519)
SHA256:NgZ9HciXlwG5JNPSFbem7bBbDbhqaNAO7JHag8rwi/I root@gitlab (RSA)
SHA256:BSSXi19WgCSpZu5AQkUTtwm+5zAMioQJpFE3oxBrIMQ root@gitlab (ECDSA)
```
- 2016-09-13 Configured reply by email. You can now simply reply by email to GitLab mail notifications.
- 2016-07-18 Setup [GitLab LFS](/en/howto/gitlab#git-and-managing-large-files/) (Large File Storage).

## Maintenance window

Updates to GitLab are usually done on Friday mornings. During the upgrade, GitLab may be unavailable for a while.
