---
author: petervc
date: '2021-06-04T09:29:21Z'
keywords: []
lang: en
tags:
- email
title: Email antivirus
wiki_id: '10'
---
## Filtering of viruses in E-mail

### When and how does the filter work?

The C&CZ mailservers, running [`sendmail`](http://www.sendmail.org/),
invoke [`MIMEDefang`](http://www.mimedefang.org/) for each mail they
receive, to scan for a virus and filter for spam.

This means that the virus- and spamfilter works for **all** mail these
servers process, whether incoming, outgoing or forwarded.

If a mail contains an attachment with dangerous content, this mail is
put into quarantine. The mail without the dangerous attachment, but with
an attachment that explains what has happened, is forwarded to the
addressee. If the mail is delivered to a user on a C&CZ mailserver, then
the mail is delivered in the Virus folder. Mail older than 14 days is
automatically removed from this folder.

An attachment is considered dangerous if it is an executable program,
e.g. `.exe`, `.bat`, `.dll`. Since MS-Windows considers `zip`-files to
be directories, and therefore viruses in `zip`-files started to appear,
we also consider an executable in a `zip`-file to be dangerous. In order
not to make the list too long (a zip-file in a zip-file in …, the Dutch
chocolate Droste-effect), we also consider a zip-file in a zip-file to
be dangerous. As of February 20, 2017, after a few cases of ransomware,
we also consider a html/htm-file within a zip-file to be dangerous.

### Other antivirus measures

Since viruses can spread in more ways than mail (network shares,
USB-sticks, www, …) it is good practice for (MS-Windows) users to use a
PC virusscanner with a recent virusdatabase. The RU has a campus license
for the use of F-Secure. See our [Windows
software-page](/en/howto/microsoft-windows/) for more information about
the different versions.
