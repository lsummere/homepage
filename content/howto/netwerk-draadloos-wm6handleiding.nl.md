---
author: mkup
date: '2013-10-09T12:52:40Z'
keywords: []
lang: nl
tags: []
title: Netwerk draadloos WM6handleiding
wiki_id: '643'
---
## Draadloze instellingen van Windows Mobile

1.  **Windows instellingen**:

    \* Als het erg lang geleden is dat u uw wachtwoord gewijzigd hebt,
    doe dat eerst. Voor “Science” kan dat op [de Doe-Het-Zelf
    website](http://dhz.science.ru.nl), voor “ru-wlan” op [de
    RU-wachtwoord website](http://www.ru.nl/wachtwoord).

    -   Klik op “Start” → “Instellingen”.

        {{< figure src="/img/old/wmwifi-002.jpg" title="Start-Instellingen" >}}

    -   Er verschijnt een venster met de naam “Instellingen”. Klik
        hierin op het tabblad “Verbindingen” onderaan en daarbinnen op
        het ikoon “Wi-Fi”.

        {{< figure src="/img/old/wmwifi-004.jpg" title="Instellingen" >}}

    -   Binnen het venster “Instellingen” kan men dan bovenaan lezen dat
        de draadloze netwerken geconfigureerd kunnen worden.

    -   Als u zich binnen het bereik van het draadlozen netwerk
        “Science” of “ru-wlan” bevindt, klik dan lang op het “Science”
        of “ru-wlan” netwerk en zet Wi-Fi uit.

    -   Kies “Nieuw toevoegen …”.

        {{< figure src="/img/old/wmwifi-005.jpg" title="Draadloze netwerken configureren 1" >}}

    -   Vul als netwerknaam “Science” of “ru-wlan” in, en klik
        rechtsonder op “Volgende”.

        {{< figure src="/img/old/wmwifi-006.jpg" title="Draadloze netwerken configureren 2" >}}

    -   Kies binnen het scherm “Netwerkverificatie configureren” voor
        “WPA” en “TKIP” en klik rechtsonder op “Volgende”.

        {{< figure src="/img/old/wmwifi-007.jpg" title="Netwerkverificatie configureren 1" >}}

    -   Kies als EAP-Type voor “PEAP”. Meestal kunnen de
        PEAP-eigenschappen niet aangepast worden, dat is prima. Als die
        wel aangepast kunnen worden zou er PEAPv0/MSCHAPv2 gekozen
        moeten worden. Klik daarna rechtsonder op “Voltooien”.

        {{< figure src="/img/old/wmwifi-008.jpg" title=" Netwerkverificatie configureren 2" >}}

    -   Nadat evt. Wi-Fi weer aan gezet is en na invullen van
        loginnaam/wachtwoord (voor “Science”) of
        U-nummer/S-nummer/RU-wachtwoord (voor “ru-wlan”) en linksonder
        op “OK” klikken zal de verbinding met het draadloze netwerk tot
        stand komen.

        {{< figure src="/img/old/wmwifi-009.jpg" title="Gebruiker aanmelden" >}}
