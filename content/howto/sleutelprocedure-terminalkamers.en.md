---
author: wim
date: '2022-03-09T11:59:07Z'
keywords: []
lang: en
tags:
- medewerkers
- studenten
title: Sleutelprocedure terminalkamers
wiki_id: '496'
---
## Keys for the computerlabs

### General

-   This procedure for the keys of the C&CZ managed [computer
    labs](/en/howto/terminalkamers/) is valid all year, so also during
    holidays.
-   All keys are managed by IHZ reception (HG00.040).

### Authorised users

-   All students of the faculty of Science with a valid student pass.
-   Teachers and employees of the faculty of Science, e.g. employees of
    C&CZ and cleaning staff.
-   External students taking a course within the faculty of Science. The
    teacher takes care of IHZ reception having a list of these students.

### Opening/closing procedure

-   IHZ reception opens and closes according to the schedule below.

  ---------------------------------------- ---------------------------------- --------------------- ----------------------- --------------------------
                                                                              Friday   Saturday
                                                                              donderdag][Monday -                         
                                                                              Thursday]                                    

  TK023                                    open/close   8.30-21.30            8.30-21.30              9.30-16.00

  TK029, TK075, TK206, TK053, TK625        open/close   8.30-18.00            8.30-18.00              

  Studielandschap   open/close   8.30-22.00            8.30-20.00              9.30-16.00

  Library of         [openen/sluiten[open/close]   8.30-22.00            8.30-20.00              9.30-16.00
  Science]                                                                                                                 
  ---------------------------------------- ---------------------------------- --------------------- ----------------------- --------------------------

### Other issues

-   C&CZ makes sure that there are signs in the computer labs stating:
    -   that reservations have precedence;
    -   what the opening hours are;
    -   that only authorised persons are allowed;
    -   that no smoking/eating is allowed.
-   External courses are reserved through
    [IHZ](http://www.ru.nl/fnwi/ihz/item_264016/item_775144/). These can
    be viewed in the room reservation system. The name of the teacher
    cannot be seen in this way. Therefore the IHZ room reservation will
    have to give the name of the teacher to IHZ reception.
-   Almost all computer labs can be reserved for courses. In that case
    the teacher/reception has the right to ask all persons not taking
    the course to leave the room.
-   Because of environmental considerations all PCs in the computer labs
    are switched off automatically just after closing time, except for
    the computer labs that are at that moment used as a compute cluster
    during the night. For reservations of the computer labs outside
    opening hours it is therefore necessary to contact C&CZ beforehand,
    to make sure that the PC’s are available and up and running.

