---
author: petervc
date: '2022-06-30T10:41:54Z'
keywords: []
lang: en
tags:
- software
- medewerkers
title: BASS
wiki_id: '656'
---
The Radboud University uses a concern-application [BASS-FinLog
BASS-finlog](http://www.radboudnet.nl/bass/bass-finlog/handleidingen/).
This document explains how Science staff-members can use it.

### Login details and authorization

To be able to use BASS for electronic cost declarations (i-Expense), one
needs:

-   a U-number (personnel number).\
    This can be requested from
    [P&O](http://www.ru.nl/fnwi/po/afdeling_personeel/wie_wat_waar/).
    The corresponding RU-password can be reset by C&CZ for Science
    employees.
-   a budget number (Dutch: kostenplaats).\
    This is available through your department or can be requested from
    [P&C of the Financial
    Department](http://www.radboudnet.nl/fnwi/fez/planning_en_control/).

To place orders (i-Procurement) or approve requisitions, one
additionally needs:

-   authorization in BASS/FinLog/Oracle, can be requested with a
    [request
    form](http://www.radboudnet.nl/bass/bass-finlog/aanvragen/).

### Access to BASS

-   Access to BASS is certified only from certain combinations of the
    various Windows operating systems, versions of Internet Explorer,
    Java versions, etc. In practice many alternative configurations
    (Mac, Linux, other browsers) seem to work fine.
-   BASS functions properly from any standard RU workstation and also
    from any Windows workstation managed by C&CZ.
-   BASS can only be accessed *directly* from the campus network or
    through VPN (from any given Internet location). *Note* however that
    from a small part of the subnets at the Faculty of Science, direct
    access to BASS is not possible. These subnets are the ones with a
    lot of servers that can be accessed from the Internet.
-   BASS cannot be accessed from Server subnets, this includes our
    **lilo** servers.
-   BASS functions such as Time and Electronic Declaration, and
    iProcurement only use the web browser and usually work with any
    browser from any platform (Chrome, IE, Firefox, Safari, on PC, Mac,
    Linux, or tablet). So electronic declarations from an iPad via VPN
    works fine.
-   Other BASS functions (“forms”) depend on Java. Therefore Java must
    be installed on the PC (Mac, Linux, etc.) to work with forms. Here
    the matter becomes more complex.
    -   On an iPad Java is not available, so in principle iPads cannot
        be used to work with forms.
    -   The Java version on the client (PC, Mac, Linux) may not differ
        “too much” from the version on the BASS server. A lower version
        on the client isn’t a problem (for BASS, however it *is* a big
        security problem for the PC!), a higher version can be a problem
        for BASS.
    -   Other characteristics of the client (e.g. 32 or 64 bit version
        of the operating system) can play a role too.
-   The functions in BASS which require Java are mostly used by the
    employees at CIF, CFA, DPO, budget holders and employees placing
    orders. If such a user needs to be able to work with BASS from home,
    it is advised to use the “remote desktop” feature (i.e. to connect
    to the desktop of the PC at work).

### Preparing your PC

This manual describes how to use BASS from Windows 7, but things are
essentially the same for Linux and the Mac.

The use of this web application requires configuration changes of your
computer. The configuration change depends on your network settings, see
next step.

Note: BASS can only be accessed from the campus or through a
[VPN](/en/howto/vpn/) connection to the campus network. These limitations
are for the sake of security.

Some parts of BASS require Java 6.x or 7.x to be installed on the PC.
All C&CZ managed PC’s have the Java Runtime Environment (JRE) installed.

### Logon to BASS

This is a direct link to the login page of BASS.

<http://bass.ru.nl>

{{< figure src="/img/old/bass\_ru\_app.png" title="bass\_ru\_app.png" >}}

If this fails, you don’t have direct access to bass.ru.nl. You will have
to use the [vpn](/en/howto/vpn/).

## Who can answer my questions about the functionality of BASS-finlog?

The Radboudnet website shows the contact details of the [BASS
helpdesk](http://www.radboudnet.nl/bass).

## Where should I request authorizations for BASS?

To request authorizations for BASS one has to hand in signed
authorization documents. Users cannot request authorizations themselves
at the BASS-finlog helpdesk.

Information about this procedure can be found [at the Radboudnet
website](http://www.radboudnet.nl/bass/bass-finlog/aanvragen/).
Authorization request have to be processed and confirmed by the
financial department FEZ of the Faculty of Science.

## How can I set an initial BASS password or reset it later?

As of June 1, 2013 there is no separate BASS password anymore: use your
U number and RU password to logon to BASS.

## Which PC IP addresses do not yet have direct access to BASS?

Only the following FNWI desktop IP adresses will not have direct access
to BASS, but users can connect to BASS e.g. through
[VPN](/en/howto/vpn/):

    131.174.12.0 - 131.174.12.127
    131.174.15.33
    131.174.15.35
    131.174.15.36
    131.174.142.0 - 131.174.142.31
    131.174.192.0 - 131.174.192.255

## I can’t manage to get BASS running on my PC, are there any alternatives?

BASS can be reached and used with any browser on any device.
