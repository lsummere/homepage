---
author: wim
date: '2021-05-10T19:13:27Z'
keywords: []
lang: en
tags:
- hardware
- software
title: Printers en printen
wiki_id: '47'
---
# Printers

## Peage and Konica Minolta multifunctionals

Throughout the campus you can find multifunctionals that use the
[Peage](/en/howto/peage/) system, which is managed by the
[ISC](http://www.ru.nl/isc). These are Konica Minolta Bizhub
multifunctionals which are also used as copiers and color scanners.

## Binding, cutting, stapling, booklets, …

In the [Faculty
Library](http://www.ru.nl/library/library/library-locations/library-of-science/)
you can buy binders for your reports, etc. Also there is a cutting tool,
a 23 hole punch and several staplers. All Konica Minolta printers can
staple automatically.

In the MFP at HG00.002 a punch unit and a saddle-unit that can fold are
available. This makes it possible to produce complete booklets. One can
punch 2 or 4 holes. At maximum 20 sheets can be saddle-stitched and
folded, so one can produce an A4 booklet from A3 sheets or an A5 booklet
from A4 sheets.

Near HG03.089 one can also find a cutting tool, a 23 hole punch and a
big stapler.

## C&CZ poster printer

C&CZ runs a [A0 poster printer kamerbreed](/en/howto/kamerbreed/) to
print posters and such on high quality photo paper, canvas or adhesive
paper and the 3D printers.

## C&CZ 3D printers

C&CZ operates a 3D print service for staff and students of the Faculty
of Science, especially meant for the production of multicolor objects to
be used in education.

The first 3D printer is a [BCN3D
Sigma](https://www.bcn3dtechnologies.com/en/catalog/bcn3d-sigma/), a
professional open source FFF (Fused Filament Fabrication) 3D printer
with independent dual extruders, which allows multimaterial and
multicolor printing and support printing, which lessens geometric
limitations.

Per September 2017, we also have a [Prusa i3
MK2](https://www.3dhubs.com/3d-printers/original-prusa-i3-mk2) 3D
Printer, that can print 4 colors from 1 extruder with the
[multi-material
upgrade](https://all3dp.com/prusa-i3-multi-material-upgrade/).

If you want to start developing, it is advised to follow a few [video
courses FreeCAD
Tutorial](https://www.google.nl/search?q=freecad+tutorial#q=freecad+tutorial+site:youtube.com).

The price for a printed object: 10 cent per gram.

## Departmental printers (owned by the department)

There are also a few small printers that are still owned by a
department. In general these printers are meant to be used only by staff
of that department. The department has purchased the printer and is
responsible for paper, supplies and maintenance. When replacement is
due, in general the department switches to a Peage printer.

### Departmental printers (owned by the department): Printing under Windows

#### ISC managed PC (SRW)

End users on PCs managed by the [ISC](http://www.ru.nl/isc) are advised
to follow the procedure for a self managed Windows 7 PC below. If you
run into problems please contact ISC for assistance.

#### Windows 8, 8.1, 10 or higher

You will have to install a local printer driver. It is advisable to
download the printer driver and store it in a known location on your PC
in advance so it can be used later in step 2.

Start with connecting a network drive from the printer server, this way
you will see error messages if something went wrong.

-   Do this by visiting your **Desktop**.
-   Connecting a network drive can be accomplished by starting a
    **Windows Explorer** and selecting the tab **Computer** and then
    clicking **Map network drive**.
-   Select at **Drive:** e.g. **P:** and enter at **Folder:**
    **`\\print.science.ru.nl\printtest`**. Also select **Reconnect at
    sign-in** and **Connect using different credentials**. Click
    **Finish**.
-   Enter your science account and password in the Login dialog window.
    Also select **Remember my credentials** and click **OK**.
-   There should appear a **Windows Explorer** screen with a few files.

Now install the printer using a local port.

-   Do this by right clicking on **Start** -\> **Control Panel** -\>
    **Devices and Printers**,
-   Choose **Add a printer** and select **The printer that I want isn’t listed**,
-   Choose **Add a local printer or netwerk printer with manual
    settings**. Click **Next**,
-   Choose **Use an existing port:** **LPT1: (Printer Port)**. Click **Next**,
-   Install the correct printer driver,
-   Choose a **Printer name:**, e.g. **`ourownprinter`**. Click
    **Next**,
-   Choose **Do not share this printer**. Click **Next** and then **Finish**.

The printer is now in your list of printers. We have to adapt the
printer and change the port from **`LPT1:`** to the the network printer.

-   Right click on the newly created printer and choose **Printer
    properties**.
-   Visit the tab **Ports** and click op **Add Port…**.
-   Choose **Local Port** and click on **New Port…**.
-   Enter at **Enter a port name:**
    **`\\print.science.ru.nl\<printernaam>`** and click **OK**,
    **Close** and again **Close**.

Verify before printing that you can see and read the files at the
network drive **`\\print.science.ru.nl\printtest`**. The printer
connection **`\\print.science.ru.nl\`** in **Add Port…** and as well in
the the network drive has to be exactly the same. This is needed to
create a reliable connection to the printer server.

#### Windows 7: Fast installation on a managed PC

-   Open **Windows Explorer** (windows key + e).
-   Type **`\\print.science.ru.nl\`** in the address bar and hit the
    return key.
-   Use your science account and password if an authentication window
    pops up.
-   Double click the printer you’d like to install.

#### Windows 7: For a self maintained PC

First connect to a network drive from the printer-server, this will show
errors if there is something wrong with your login or password.
Connecting this network drive can be accomplished in a **Windows
Explorer** window like **My Documents**. Click on **Tools** -\> **Map
Network Drive…** and choose the **Folder:**
**`\\print.science.ru.nl\printtest`**.

-   Click **Start**, **Devices and Printers**, **Add a Printer** en
    select **Network printer, wireless printer or Bluetooth printer**.
-   Click **Select a shared printer by name** and enter
    **`\\print.science.ru.nl\``<printername>`** at **Select a
    shared printer by name**.
-   Click **Next**, wait a little and then click **Next** again.
-   Check if Preferences / Properties are for printing on A4 size paper
    instead of US letter size paper.

#### Windows2000/XP

First connect to a network drive from the printer-server, this will show
errors if there is something wrong with your login or password.
Connecting this network drive can be accomplished in a **Windows
Explorer** window like **My Documents**. Click on **Tools** -\> **Map
Network Drive…** and choose for the **Folder:**
**`\\print.science.ru.nl\printtest`**.

-   Then you can add a new printer using **Start** -\> **Settings** -\>
    **Printers and Faxes**, select the printer task **Add a printer**.
-   Choose **A network printer …**,
-   choose **Connect to this printer …** and type
    **`\\print.science.ru.nl\``<printername>`** in the **Name:**
    field, for example **`\\print.science.ru.nl\ourownprinter`**.
-   Now you can select to make it the default printer.
-   Finally click **Finish**.

If you manage your PC or laptop yourself, the simplest solution is to
create an account name and password identical to your Science account.
Then Windows will automatically use the correct account name and
password. An other solution would be to connect to the printer using
**Connect using a different user name**.

### Departmental printers: Printing on a Managed Linux PC

With **system-config-printer** you can choose your default printer (“Set
as Default”).

To print to a budgetted printer, you need a Kerberos ticket. If your
home directory comes from a server which demands NFS4 with Kerberos
(e.g. *home1*, *home2*, *pile* and *bundle*), you should already have a
valid Kerberos ticket. The command **klist** shows your Kerberos
tickets, with **kinit** you can get or renew a Kerberos ticket and
finally **kdestroy** allows you to start all over again.

To show the printer queue, use **lpq -P`<printername>`**. All
other ways, like the graphical application, always show an empty queue.

### Departmental printers: Printing on other Linux machines

In Linux you can choose to print in the same way Windows-machines do,
through SMB to a Samba-server: print.science.ru.nl.

Choose as device URI
**<smb://print.science.ru.nl/>`<printername>`**.

-   As device URI, type
    **<smb://print.science.ru.nl/>`<printername>`**
    or
    **<smb://username:password@print.science.ru.nl/>`<printername>`**,
    where you have to fill in your own username and password and must
    use the correct name of the printer in printername. You might be
    unable to put your password directly in the URI if your password
    contains special signs. To overcome this problem you can replace the
    special sign by a ‘%’ (percent sign) followed by it’s hexidecimal
    code. For example, if your password contains an ‘!’ (exclamation
    mark) you can replace it with ‘%21’.
-   An alternative method to create a cups printer is the command
    **lpadmin -p PrinterName -v
    <smb://Username:Password@Server/Printer> -P
    /path/to/yourprintermodel.ppd**.

#### Detailed instructions for OpenSuSE 11

-   Ask C&CZ to give your PC a permanent IP addres and name.
-   Ask the owner of the printbudget to add your “login” name to the
    list of users that is allowed to use the printer. This can be done
    by the printbudget owner using <http://dhz.science.ru.nl>, choose
    “printing”. However: only C&CZ can give permission to print from a
    computer that is not in the list of “C&CZ trusted” machines. For
    this purpose C&CZ has to give the command: “printbudget -g
    budgetname -m +user\@host”.
-   Start the SuSE setup tool YAST, give root password.
-   Choose: “Hardware”, “Printer”, “Print via network”.
-   Click “connection wizard”, “Line printer daemon (LPD) protocol”.
-   In “Connection settings, IP Address or Host name” enter
    “printsmb.science.ru.nl”.
-   Enter queue name: “hertz” (or any other network printer).
-   Select manufacturer: “Konica Minolta” (check brand of printer).
-   Click on “Test connection”, the reply should be “Test OK” after a
    few seconds.
-   Click “OK”.
-   At “Find and assign driver; search for:” enter “Generic”.
-   Choose default paper size “A4”.
-   At “Set arbitrary name” enter name of queue, e.g. “pr-hg-03-038”.
-   To select a driver, scroll to bottom of the list for "Generic
    PostScript level 2 Printer Foomatic/PostScript.
-   Click “OK”.
-   Wait for “Creating New Printer Setup” to finish.
-   You will go back to the printer configuration menu. Click on “Edit”
    to modify the queue that was just created.
-   Click on “options”, select “Duplex/double sided printing” and choose
    “DuplexNoTumble” and “OK”.
-   Enter “printer location”, make sure “Accept Print Jobs” and “Enable
    Printing” are selected.
-   Optionally, select “Use as Default”.
-   Back in the “Printer Configurations” menu click “Print Test Page”
    (select two pages to test double sided printing). Warning: if you
    are still “root” the printqueue may not accept these test pages.

#### Detailed instructions for Ubuntu

-   Make sure you install
    -   system-config-printer-gnome : graphical setup tool for printers
    -   python-smbc python3-smbc smbclient : needed for
        system-config-printer to let it connect to samba printers; when
        not installed the samba printer option is not available in
        system-config-printer
    -   gnome-keyring : needed for storing password
    -   seahorse : optional needed for editing credentials stored in
        gnome-keyring
    -   **Install all with command:** sudo apt-get install
        system-config-printer-gnome python-smbc python3-smbc smbclient
        gnome-keyring seahorse
-   Note: credentials are stored in gnome-keyring and not in KDE’s
    kwallet.
-   From a terminal window execute as normal user the command:
    `sudo     system-config-printer`\
    On Ubuntu linux you are allowed to continu if you are an admin user,
    otherwise you have to supply in a popup window the credentials for a
    valid admin user.
-   In the started gui choose add new printer.
-   In the upcoming dialog window choose:
    -   Device: Network Printer - Windows printer via Samba
    -   URI Printer:
        <smb://printsmb.science.ru.nl/>`<printername>`
    -   Authenthication: you have choice from two options, where latter
        is preferred :
        -   user (**ru\\u-number** + password =\> password saved
            plain-text in /etc/cups/printers.conf
        -   ‘Prompt user if authentication required’ : user must supply
            password when printing. When asked for credentials you can
            choose the “save” option which causes the credentials to be
            stored in the gnome-keyring. Credentials in the
            gnome-keyring can always be edited/deleted using the
            seahorse program.
        -   **TIP**: in case of problems with authentication look at the
            terminal from which you started `system-config-printer` if
            there are any errors printed. Sometimes this can give an
            indication why the authentication fails.
-   Use as driver a specific driver for the printer which you can
    download online at printer manufacturer website. (select the ppd
    file)\
    However sometimes “generic postscript” also works.
-   Printing the first time can be best done by printing a webpage from
    the firefox browser program. Because this gives you an
    authentication window with the option to store the credentials which
    makes sure your credentials are stored safely in the gnome keyring.
    By storing it won’t ask you to specify them the next time. The
    reason to use firefox is that other linux programs may have slighty
    different authentication dialog window which don’t give you the
    storage option forcing you to supply credentials on each print!

### Departmental printers: Printing on an OS X machine

#### OS X 10.6 (Snow Leopard) or later

-   Open **System Preferences**, select **Print & Fax**
-   Press the **+** plus button, it will open the **Printer Browser**
-   Click in the Toolbar on ’’’Advanced" (if unavailable: right click on
    the menu en choose **Customize Toolbar…**, drag the **Advanced**
    icon into the Toolbar)
-   It may happen that you can’t enter data in this dialog box, in that
    case you may have printer sharing turned on. If you turn off System
    Preferences -\> Sharing -\> Printer Sharing the advanced tab becomes
    usable again.
-   Enter requested data:
    -   Type: Windows
    -   URI: **<smb://printsmb/>`<printername>`**\
        If using eduroam, add the next line to your **`/etc/hosts`**
        file:\
        **131.174.30.41 printsmb**\
        Often, it just works to use
        **<smb://printsmb.science.ru.nl/>`<printernaam>`**.
    -   Name and location: make your own choice. Select the (precise)
        printer model.
-   Click **Add**.
-   When printing your first job the printer will show some error
    message. Click on **Resume**. A dialog requesting credentials will
    appear. Provide your science account and password. You could add
    these credentials to your **Key Ring**.
-   and you’re done!

Note: It may be necessary to supply your username and password in the
URI, as explained below for OS X 10.5. If there is a ; (semicolon) in
your password, this will lead to problems.

#### OS X 10.5 (Leopard)

-   Open “System Preferences”, select “Print & Fax”
-   Press the ”+” button, it will open the “Printer Browser”
-   Click in the Toolbar on “Advanced” (if unavailable: right click on
    the menu en choose “Customize Toolbar…”, drag the “Advanced” icon
    into the Toolbar)
-   Enter requested data:
    -   Type: Windows
    -   URI:
        **<smb://loginnaam:wachtwoord@printsmb/>`<printername>`**\
        If using eduroam, add the next line to your **`/etc/hosts`**
        file:\
        **131.174.30.41 printsmb**
    -   Name and location: make your own choice. Select, if you desire,
        the precise printer model.
-   Click “Add”, and you’re done!

#### OS X 10.4 (Tiger)

-   Open “System Preferences”, select “Print & Fax”
-   Press the ”+” button, it will open the “Printer Browser”
-   Now, press ALT and then click (with ALT still down) “More Printers”,
    and a dialog box appears
-   The top selector should be on “Advanced”, and the second selector
    should be on “Windows Printer via SAMBA”
-   As device name you can choose the name which you prefer to refer to
    the printer
-   As device URI, type <smb://username:password@printsmb/printername>,
    where you have to fill in your own username and password and must
    use the offical name of the printer in printername
-   Select, if you desire, the precise printer model (for example HP
    Laserjet 4200 series printer with 500-sheet input tray and duplex
    unit and 64 MB memory)
-   Click “Add”, and you’re done!
