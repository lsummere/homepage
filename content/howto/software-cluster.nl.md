---
author: petervc
date: '2015-10-14T13:11:38Z'
keywords: []
lang: nl
tags: []
title: Software cluster
wiki_id: '512'
---
## Cluster software

{{< notice warning >}}
All clusternodes will be moved to
[SLURM](/nl/howto/slurm/). The text below deals with the older GridEngine
clustersoftware. !!
{{< /notice >}}

## Oude Clustersoftware

Op het cnXX-cluster is als clustersoftware [Oracle
GridEngine](http://www.oracle.com/technetwork/oem/grid-engine-166852.html)
geïnstalleerd.

Gebruik:

-   Je mag alleen maar een shell-script submitten via qsub, hier als
    voorbeeld het shell-script ‘\~/date’:

        #! /bin/sh
        /bin/date

Om dit script te submitten tik je: `qsub -cwd ~/date`. De output en error wordt geschreven naar
bestanden
`~/date.[oe]$jobnumber`. Vanwege de -cwd staan die niet in de
homedirectory, maar in de huidige directory.

Als je deze job op een bepaalde host (hier als voorbeeld ‘cn00’ wil
laten lopen, gebruik: `qsub -q '*@cn00' ~/date`.

We hebben hostgroups gemaakt:

    qconf -shgrpl

laat zien welke er bestaan,

    qconf -shgrp <hostgroep>

laat zien welke subhostgroep of hosts er in die hostgroep
zitten.
Dus je kunt gebruiken:

       qsub -q '*@@mlfhosts,*@@tcmhosts' ~/date

Wanneer het geen ‘harde’ eis is, maar alleen een ‘soft’ voorkeur om
een bepaalde host te gebruiken:

    qsub -soft -q '*@@mlfhosts' ~/date

Een aardige optie van ‘qsub’ is: `-p priority`. Die optie is alleen
aanwezig bij qsub, qsh, qrsh, qlogin en qalter. Het (her)definieert de
prioriteit van een job ten opzichte van andere jobs. Dit gaat normaal
alleen over de volgorde waarin Grid Engine jobs laat starten. Grid
Engine rommelt normaal niet met lopende jobs. Priority is een geheel
getal van -1023 tot en met 1024. Default is de priority 0. Gebruikers
mogen alleen de prioriteit van hun jobs verlagen. Als een job hogere
prioriteit heeft, dan kan die eerder gekozen worden door Grid Engine om
te starten.

Natuurlijk kan ook het ‘nice’ commando gebruikt worden wanneer de hosts
van andere groepen gebruikt worden:

        nice - run a program with modified scheduling priority
               -n, --adjustment=N
               add integer N to the niceness (default 10)

-   Voor een MPI job:

Maak eerste een ’smpd passphrase" file:

        touch ~/.smpd
        chmod 600 ~/.smpd
        echo "phrase=MyOwnPassword" > ~/.smpd

Kies *svp* je eigen *MyOwnPassword* !!!
Dit wachtwoord wordt alleen voor MPI gebruikt, niet je
loginwachtwoord!

\~/mpich2.sh:

        #!/bin/sh -x
        #
        #$ -S /bin/sh
        #
        # sample mpich2 job
        # you will need to adjust the $PATH to your mpich2 installation
        # be sure to get the correct mpiexec for mpich2_smpd!!!
        export PATH=/usr/local/mpich2_smpd/bin:$PATH
        port=$((JOB_ID % 5000 + 20000))
        echo "Got $NSLOTS slots."
        mpiexec -n $NSLOTS -machinefile $TMPDIR/machines -port $port ~/mpihello
        exit 0

die de gecompileerde source runt van \~/mpihello.c:

        #include <stdio.h>
        #include "mpi.h"
        main(int argc, char** argv)
        {
         int noprocs, nid;
         MPI_Init(&argc, &argv);
         MPI_Comm_size(MPI_COMM_WORLD, &noprocs);
         MPI_Comm_rank(MPI_COMM_WORLD, &nid);
         if (nid == 0)
          printf("Hello world! I'm node %i of %i \n", nid, noprocs);
         MPI_Finalize();
        }

die gecompileerd is met:

`   /usr/local/mpich2_smpd/bin/mpicc mpihello.c -lmpich -o mpihello`

Je moet de parallel environment
‘mpich2\_smpd’ en het aantal slots kiezen:

`   qsub -pe mpich2_smpd 2 ~/mpich2.sh`

-   Andere interessante commando’s:

    qstat - show the status of Grid Engine jobs and queues
    qmod - modify a Grid Engine queue
    -cj    Clears the error state of the specified jobs(s).
    -cq    Clears the error state of the specified queue(s).

Als een job faalt waardoor de queue op een bepaalde machine in error (E)
komt, dan kan een systeembeheerder, te mailen via
postmaster\@science.ru.nl, dit oplossen door iets te tikken als:

`qmod -c all.q@cn16`

## qmon - X-Windows OSF/Motif graphical user’s interface for Grid Engine

Dit grafische user-interface voor grid engine kan gestart worden op cn99
met:

    ssh -X cn99 qmon

[Categorie: Software](/nl/tags/_software)
