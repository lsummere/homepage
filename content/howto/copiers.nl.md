---
author: bram
date: '2022-09-19T16:27:50Z'
keywords: []
lang: nl
tags: []
title: Copiers
wiki_id: '36'
---
## Kopieermachines

### Nieuw systeem: Peage

Alle Konica Minolta MFP’s zijn overgezet op [Peage](/nl/howto/peage/):
het RU-brede accountingsysteem om gebudgetteerd printen, kopiëren en
scannen over de gehele campus te uniformeren.
