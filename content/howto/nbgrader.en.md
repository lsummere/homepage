---
author: mroesner
date: '2022-08-30T07:17:33Z'
keywords: []
lang: en
tags: []
title: Nbgrader
wiki_id: '1092'
---
NBGrader is a system for assigning and grading Jupyter notebooks.

## Student Interface

Using the assignment list extension within the JupyterHub, students may
conveniently view, fetch, submit, and validate their assignments.

For details see:
<https://nbgrader.readthedocs.io/en/stable/user_guide/highlights.html#student-interface>

## Instructor Interface

The NBGrader services guide the instructor and teaching assistants
through assignment and grading tasks using the familiar JupyterHub
environment.

For details see:
<https://nbgrader.readthedocs.io/en/stable/user_guide/highlights.html#instructor-interface>
