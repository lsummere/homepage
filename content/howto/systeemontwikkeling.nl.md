---
author: petervc
date: '2022-11-02T09:49:45+01:00'
keywords: []
lang: nl
tags:
- overcncz
title: Systeemontwikkeling
wiki_id: '132'
---
## Systeemontwikkeling en -beheer

Systeemontwikkeling en -beheer is te vinden in kamer HG03.055.

## Wie zijn zij?

-   {{< author "remcoa" >}}\
    {{< author "petervc" >}}\
    {{< author "bram" >}}\
    {{< author "wim" >}}\
    {{< author "ericl" >}}\
    {{< author "fmelssen" >}}\
    {{< author "simon" >}}\
    {{< author "polman" >}}\
    {{< author "visser" >}}\
    {{< author "miek" >}}\
    {{< author "alexander" >}}

## Wat doen zij?

Systeembeheer zorgt o.a. voor

-   Beheer van de [facultaire- en afdelings-servers](/nl/howto/hardware-servers/).
-   Back-up en restore van de data op de servers.
-   Beheer van werkplekken bij de vakgroepen en in de [terminalkamers en openbare werkplekken](/nl/howto/terminalkamers/): Beheerde Werkplek
    PC’s met MS-Windows en/of Ubuntu Linux.

Voor alle andere werkplekken (honderden MS-Windows PC’s, tientallen
Linux PCs en MACs die niet door C&CZ beheerd worden) wordt er van
uitgegaan dat de eigenaar (afdeling) het beheer doet. C&CZ Systeembeheer
fungeert hiervoor voornamelijk als vraagbaak.

-   Ondersteuning bij onderwijsvernieuwing en digitale
    informatievoorziening.
-   Internet-nummerbeheer (*nameserver*, DNS/DHCP) voor de B-faculteit.
