---
author: petervc
date: '2017-08-11T15:24:10Z'
keywords: []
lang: nl
tags:
- studenten
title: Login
wiki_id: '163'
---
Elke medewerker en elke student van de B-faculteit (FNWI) heeft recht op
een (gratis) *persoonlijke* **Science** login. Met deze login en het
bijbehorende wachtwoord kunnen alle diensten worden gebruikt van de
computers die door [C&CZ](/nl/tags/overcncz) worden beheerd. De
loginnaam is gebaseerd op de eigen naam (voorletters, roepnaam,
achternaam), bijvoorbeeld *jdevries*. Merk ook op dat de loginnaam
(meestal minder dan 16 kleine letters, zonder **.**(punt) of **@…**)
niet hetzelfde is als je emailadres. Er is een [lijst van
Science-diensten](/nl/howto/nieuwe-studenten/) die met deze Science login
te gebruiken zijn.

Zogenaamde *functionele logins*, dat zijn logins die niet bij één
persoon horen, worden in principe niet gemaakt, C&CZ doet de uiterste
best om ervoor te zorgen dat men van alle diensten gebruik kan maken
zonder dat men meer logins nodig heeft.

Nieuwe medewerkers krijgen een Science-login [op verzoek van de
contactpersoon van de afdeling](/nl/howto/login-aanvragen/). Ook [gast
logins](/nl/howto/gastlogin/) zijn mogelijk.

Studenten krijgen na inschrijving automatisch een login via het
[Onderwijscentrum](http://www.ru.nl/fnwi/onderwijs/onderwijscentrum/),
meer specifiek de Student Service Desk,
{{< figure src="/img/old/telephone.gif" title="telefoon" >}} 52200.
Mocht de Student Service Desk niet over de laatste gegevens beschikken,
dan wordt er tijdelijk een login verstrekt via de [contactpersoon voor
de betreffende
studierichting](http://www.ru.nl/fnwi/onderwijs/studieadviseurs/).

Neem voor speciale wensen contact op met de contactpersoon van de
studierichting of afdeling. Als je de Science login nog even wil
aanhouden terwijl je hier niet meer werkt of studeert, dan zal C&CZ dit
alleen doen op verzoek van de contactpersoon van je studierichting of
afdeling.
