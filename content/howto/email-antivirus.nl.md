---
author: petervc
date: '2021-06-04T09:29:21Z'
keywords: []
lang: nl
tags:
- email
title: Email antivirus
wiki_id: '10'
---
## Virusfiltering in elektronische post

### Wanneer en hoe werkt het mail-virusfilter?

Op de door C&CZ beheerde mailservers wordt bij binnenkomst door de
smtp-server vanuit [`sendmail`](http://www.sendmail.org/) voor elk
binnenkomend mailtje [`MIMEDefang`](http://www.mimedefang.org/)
aangeroepen voor virusscanning en spamfiltering.

Dit betekent dat het virus- en spamfilter werkt voor **alle** mail die
door deze mailservers gaat, zowel binnenkomende, doorgestuurde als
uitgaande.

Wanneer een mail een bijlage met gevaarlijke inhoud bevat, dan wordt
deze mail in quarantaine geplaatst. De mail wordt daarna, zonder de
gevaarlijke bijlage, en met een bijlage die uitlegt wat er gebeurd is,
doorgestuurd naar de geadresseerde. Als deze mail afgeleverd wordt in de
mailbox van een gebruiker op een C&CZ-mailserver, dan wordt deze in de
Virus-map geplaatst. Mails ouder dan 14 dagen worden automatisch
verwijderd uit deze map.

Een bijlage wordt als gevaarlijk beschouwd als het een uitvoerbaar
programma is, bijvoorbeeld `.exe`, `.bat`, `.dll`. Omdat MS-Windows een
`zip`-bestand tegenwoordig als een map ziet, en er dus virussen in
zip-bijlagen bestaan, beschouwen we ook bijlagen met zip-bestanden met
daarin een uitvoerbaar programma als gevaarlijk. Om de lijst niet
oneindig lang te maken (een zip-bestand in een zip-bestand in …, het
Droste-effect), beschouwen we ook een bijlage met een zip-bestand waarin
weer een zip-bestand zit, als gevaarlijk. Sinds 20 februari 2017, na
enkele gevallen van ransomware, wordt ook een html/htm-bestand in een
zipfile als gevaarlijk beschouwd.

### Andere antivirus-maatregelen

Omdat virussen zich op allerlei andere manieren dan via e-mail kunnen
verspreiden (netwerk shares, floppies, CD-ROMs, www, …), blijft het voor
veel (MS-Windows) gebruikers nog steeds verstandig om op de PC een
antivirus-programma te blijven gebruiken, met een recente virusdatabase.
De RU heeft een campuslicentie voor het gebruik van F-Secure. Zie de
[Windows software-pagina](/nl/howto/microsoft-windows/) voor informatie
over welke versies er beschikbaar zijn.
