---
author: bram
date: '2022-09-19T16:33:11Z'
keywords: []
lang: nl
tags:
- netwerk
title: NetwerkInstellingen
wiki_id: '1089'
---
## Netwerk instellingen

Verreweg de meeste systemen die aangesloten worden op het netwerk doen
standaard **DHCP**. Daarmee worden de volgende instellingen gezet:

-   IP adres en netwerk masker
-   Default Gateway voor het subnet waar het IP adres in zit.
-   Nameservers om namen naar ip adressen te kunnen vertalen

Het IP adres, netwerk masker en default gateway kun je moeilijk anders
achterhalen. In specifieke gevallen kan het nodig zijn om dit handmatig
in te stellen, bijvoorbeeld als het apparaat in kwestie geen DHCP
ondersteunt.

**NB** De **nameserver** instellingen voor handmatig ingestelde machines
zijn:

-   **131.174.30.40**
-   **131.174.16.131**

Het gebruik van externe nameservers, zoals *8.8.8.8* of *9.9.9.9* wordt
afgeraden, omdat dit servers zijn die niet onder beheer van de
universiteit zijn en daarmee niet onder het security beleid van de RU
vallen.
