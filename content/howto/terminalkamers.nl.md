---
author: bram
date: '2022-09-19T16:12:50Z'
keywords: []
lang: nl
tags:
- hardware
- onderwijs
- studenten
- medewerkers
title: Terminalkamers
wiki_id: '52'
---
C&CZ beheert een groot aantal werkplekken waar in principe iedereen met
een [Science-login](/nl/howto/studenten-login/) gebruik van kan maken. Op
de PC’s in de PC-cursuszalen (terminalkamers), het studielandschap en de
bibliotheek kunnen studenten ook inloggen met S-nummer en RU-wachtwoord
in het RU-domein op MS-Windows. Alle PC-cursuszalen zijn dual boot:
tijdens het opstarten van de PC kan men kiezen tussen Linux (Ubuntu) en
Windows.

### Beschikbare PC-cursuszalen

De B-faculteit beschikt over de volgende PC-cursuszalen:

-   TK023, (HG00.023)\
    41 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (AIO, Core
    i5-6500, 3.2GHz, 8GB, SSD, 24" breedbeeld LCD monitor, geluid). Dual
    boot met Windows en Linux (Ubuntu).\
    De docent-PC is aangesloten op een beamer, waardoor het
    computerbeeld op de wand geprojecteerd kan worden.
-   TK029, (HG00.029)\
    61 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (AIO, Core
    i5-6500, 3.2GHz, 8GB, SSD, 24" breedbeeld LCD monitor, geluid). Dual
    boot met Windows en Linux (Ubuntu).\
    De docent-PC is aangesloten op een beamer, waardoor het
    computerbeeld op de wand geprojecteerd kan worden.
-   TK075 (HG00.075)\
    65 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (AIO, Core
    i5-8500, 3.0Ghz, 8GB, SSD, 24" breedbeeld LCD monitor, geluid). Dual
    boot met Windows en Linux (Ubuntu).\
    Een PC is aangesloten op een beamer, waardoor het computerbeeld op
    de wand geprojecteerd kan worden.
-   TK206, (HG00.206)\
    37 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (AIO, Core
    i5-6500, 3.2GHz, 8GB, SSD, 24" breedbeeld LCD monitor, geluid). Dual
    boot met Windows en Linux (Ubuntu).\
    Een PC is aangesloten op een beamer, waardoor het computerbeeld op
    de wand geprojecteerd kan worden.
-   TK625, (HG00.625)\
    43 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (AIO, Core
    i5-7500, 3.4Ghz, 8GB, SSD, 24" breedbeeld LCD monitor, geluid). Dual
    boot met Windows en Linux (Ubuntu).\
    Een PC is aangesloten op een beamer, waardoor het computerbeeld op
    de wand geprojecteerd kan worden.
-   TK053, (HG02.053)\
    21 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (AIO, Core
    i5-6500, 3.2GHz, 8GB, SSD, 24" breedbeeld LCD monitor, geluid). Dual
    boot met Windows en Linux (Ubuntu).\
    Een PC is aangesloten op een beamer, waardoor het computerbeeld op
    de wand geprojecteerd kan worden.

### Studielandschap & bibliotheek

Sinds de zomer van 2021 worden de computers in het studielandschap
(HG00.201) en de bibliotheek (HG00.011) beheerd door het [ILS)](/nl/howto/contact/#ils-helpdesk).
Dat maakt alle Windows-computers in alle RU-studielandschappen en
bibliotheeklocaties uniform.
Informatie over o.a. het reserveren van een werkplek en openingstijden
vind je op de website van de [Library of Science](https://www.ru.nl/ubn/bibliotheek/locaties/library-of-science/).

Naast de door het ILS beheerde Windows computers, zijn er nog 8 door
C&CZ beheerde Linux computers aanwezig, vooraan in het
studielandschap (HG00.201)

### Laptops

De faculteit heeft een [laptop pool](/nl/howto/laptop-pool/):

-   Bibliotheek, (HG00.011)
    60 [beheerde werkplek PC’s](/nl/howto/hardware-bwpc/) (HP EliteBook
    850 G6 - Core i7-8665U, 16GB, 512GB SSD, 15.6" Full HD) met Windows
    10. 

### Docenten-PC’s met beamer in collegezalen en colloquiumkamers

In de volgende reserveerbare collegezalen en colloquiumkamers is een
docenten-PC aanwezig, aangesloten op een beamer.

-   HG00.062
-   HG00.065
-   HG00.068
-   HG00.071
-   HG00.086
-   HG00.303
-   HG00.304
-   HG00.307
-   HG00.308
-   HG00.310

### Reserveren

Net als collegezalen en colloquiumkamers kunnen PC-cursuszalen
gereserveerd worden voor cursussen of colleges. Men dient hierbij te
denken aan:

-   Reserveren van de ruimte.

Neem contact op met *Zaalreservering*, afdeling IHZ (Interne en
Huishoudelijke Zaken), kamer HG00.040,
{{< figure src="/img/old/telephone.gif" title="telefoon" >}} 52010.
N.B.: Dit betreft alleen het reserveren van de ruimte!

-   De toegang tot de PC-cursuszalen is geregeld via een
    [sleutelprocedure](/nl/howto/medewerkers-sleutel/).
-   Alle cursisten moeten een [login](/nl/howto/studenten-login/) hebben.
-   Geïnstalleerde software. Op de PC’s in de PC-cursuszalen is naast MS
    Windows met MS Office of Linux met Open Office standaard ook veel
    meer programmatuur beschikbaar. Zie: [Windows
    applicaties](/nl/howto/hardware-bwpc/), [Linux
    software](/nl/howto/unix/).
-   Additionele software voor cursussen **moet** door het systeembeheer
    van C&CZ geïnstalleerd worden.\
    Het is **niet** mogelijk om Administrator of Power User privileges
    te krijgen om additionele software te installeren danwel
    problematische programma’s uit te voeren.
-   Met name nieuwe software voor de PC kamers dient u **tijdig**
    (liefst enkele weken van te voren) aan te leveren.\
    Dit is nodig omdat systeembeheer moet uitzoeken of de software gaat
    werken indien deze met privileges van een gewone gebruiker wordt
    uitgevoerd, te packagen en tenslotte te distribueren naar de
    gewenste werkplekken. Het is voorgekomen dat bepaalde software niet
    kon worden gebruikt vanwege de beveiligings eisen, er moest
    uitgeweken worden naar alternatieve programmatuur.

Als een PC-cursuszaal niet gereserveerd is voor een cursus, kan iedereen
met een [login](/nl/howto/studenten-login/) er gebruik van maken.

### Bezettingsgraad

De bezetting van de PC-cursuszalen wordt bijgehouden op
[terminalkamerbezetting](http://welke.tk).

### Gedragsregels

In het algemeen is het verboden in de PC-cursuszalen te roken, eten en
drinken, o.a. ter bescherming van de apparatuur. Ook het op andere
manieren (geluid, langdurig scherm locken, …) veroorzaken van overlast
voor andere gebruikers is natuurlijk niet toegestaan. Verder is er een
prioriteit voor het gebruik: de hoogste prioriteit hebben cursussen
waarvoor de ruimte gereserveerd is, en daaropvolgend het werken aan en
voor cursussen, scripties en verslagen.

### Adverteren

De loginschermen van de PC’s in de PC-cursuszalen bieden naast de
officiële mededelingen plaats voor 7 advertenties van 400x280 pixels.
Deze kunnen gemaild worden naar inlogschermen\@science.ru.nl. De huidige
advertenties kunnen ook [hier](/nl/howto/loginschermen/) bekeken worden.

### Openingstijden

Er is een [sleutelprocedure](/nl/howto/sleutelprocedure-terminalkamers/)
voor het openen en sluiten van de ruimtes.

