---
author: petervc
date: '2019-09-06T16:18:30Z'
keywords: []
lang: nl
tags:
- internet
title: Domeinnaam registratie
wiki_id: '721'
---
C&CZ heeft een partner-account bij domeinnaam registrar
[QDC](http://www.qdc.nl) verkregen, waarmee we een `.nl`, `.com`, `.org`
of `.eu` domeinnaam snel en eenvoudig kunnen aanmaken. Veel andere
extensies zijn ook mogelijk, alhoewel de kosten daarvan in enkele
gevallen aanzienlijk zijn. Het (technisch) beheer van de domeinnaam
wordt door C&CZ gedaan (via onze eigen Internet domain name servers).
Hiermee wordt de Radboud Universiteit Nijmegen houder van de domeinnaam
in plaats van bv. een medewerker van een afdeling.

Voor afdelingen van FNWI zullen in principe de (jaarlijkse) kosten van
de registratie van een domeinnaam niet worden doorbelast.

-   Voor nieuwe domeinen: stuur een mail naar postmaster\@science.ru.nl
    met de benodigde informatie.
-   Voor bestaande domeinen: het is mogelijk om bestaande domeinen te
    verhuizen (naar C&CZ/QDC) maar dit vereist wat extra papierwerk,
    neem hiervoor contact op met C&CZ systeembeheer of stuur een mail
    naar postmaster\@science.ru.nl.
-   Gesigneerde [SSL Certificaten](/nl/howto/ssl-certificaten/) voor deze
    domeinnamen kunnen ook via C&CZ worden aangevraagd d.m.v. een mail
    naar postmaster.
