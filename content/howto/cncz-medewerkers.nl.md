---
author: petervc
date: '2022-06-28T15:11:11Z'
keywords: []
lang: nl
tags:
- overcncz
title: CNCZ Medewerkers
wiki_id: '116'
---
-   Afdelingshoofd
    -   {{< author "bbellink" >}}
-   Secretariaat
    -   {{< author "alinssen" >}},
        [secretariaat](/nl/howto/secretariaat/)
-   Systeembeheer en -ontwikkeling
    -   {{< author "remcoa" >}}
    -   {{< author "petervc" >}}
    -   {{< author "bram" >}}
    -   {{< author "miek" >}}
    -   {{< author "wim" >}}
    -   {{< author "ericl" >}}
    -   {{< author "simon" >}}
    -   {{< author "polman" >}}
    -   {{< author "visser" >}}
-   Applicatieontwikkeling
    -   {{< author "remcoa" >}}
    -   {{< author "alexander" >}}
    -   {{< author "fmelssen" >}}
-   Operations/Helpdesk/Werkplekondersteuning/zaalbeheer
    -   {{< author "bjorn" >}}
    -   {{< author "john" >}}
    -   {{< author "stefan" >}}
    -   {{< author "bertw" >}}
    -   {{< author "dominic" >}}

Er is ook een lijst van [oud-medewerkers](/nl/howto/oud-medewerkers/).
