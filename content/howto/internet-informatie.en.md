---
author: remcoa
date: '2008-10-13T11:24:09Z'
keywords: []
lang: en
tags:
- internet
title: Internet informatie
wiki_id: '64'
---
## Webdesign

-   [Web Style Guide](http://www.webstyleguide.com/)

## HTML

A number of HTML tutorials, simple (at the top) to extensive (at the
bottom)

-   [Beginning HTML at
    htmlgoodies.com](http://htmlgoodies.earthweb.com/)\
    [Introduction to HTML: Table of
    Contents](http://www.cwru.edu/help/introHTML/toc.html)\
    [WDVL: HTML - The HyperText Markup
    Language](http://www.wdvl.com/Authoring/HTML/)\
    [<http://www.looksmart.com/eus1/eus53832/eus155852/eus53906/eus65717/eus71942/eus537159/r?l&>;
    LookSmart - Advanced HTML Tutorials]\
    [HTML 3.2 Quick
    Reference](http://www.ukans.edu/~grobe/html-3.2-quick-ref.html)\
    [Index DOT Html: THE Advanced HTML
    Reference](http://www.blooberry.com/indexdot/html/index.html)\
    [HTML handleiding](http://www.handleidinghtml.nl)

## Colors

Colours are important for a website, always
use ‘browser-safe’ colours:

-   [Webmonkey \| Reference: Color
    Codes](http://hotwired.lycos.com/webmonkey/reference/color_codes/)\
    [Hex and Word Colors
    Codes](http://htmlgoodies.earthweb.com/tutors/colors.html)\
    [Browser-safe Colour
    Tool](http://phoenix.herts.ac.uk/pub/R.M.Young/colour/safe-colours.html)

## CSS

-   [w3schools CSS tutorial](http://www.w3schools.com/css/)\
    [House of
    Style](http://www.westciv.com/style_master/house/index.html)\
    [WDG CSS tutorial](http://www.htmlhelp.com/reference/css/)\
    [Webmonkey CSS
    informatie](http://hotwired.lycos.com/webmonkey/authoring/stylesheets/)\
    [CSS information bij glish.com](http://glish.com/css/)\
    [Index DOT CSS](http://www.blooberry.com/indexdot/css/index.html)

## Javascript

-   ; [Javascript
    informatie](http://www.hitmill.com/programming/js/javascript.html)

## PHP

-   ; [De PHP site](http://www.php.net/)\
    ; [Het Zend Framework](http://framework.zend.com/)\
    ; [Zend](http://www.zend.com/)\
    ; [Pear](http://pear.php.net/)\
    ; [Hotscripts](http://www.hotscripts.com/)
