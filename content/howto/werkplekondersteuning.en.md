---
author: petervc
date: '2022-06-28T14:51:55Z'
keywords: []
lang: en
tags:
- overcncz
title: Werkplekondersteuning
wiki_id: '128'
---
Operations/helpdesk
is located in room HG00.051.

## Who are they?

-   {{< author "dominic" >}}
-   {{< author "bjorn" >}}
-   {{< author "john" >}}
-   {{< author "stefan" >}}

## What do they do?

-   User
    support for students and staff
-   [Installatie en onderhoud van de studentenvoorzieningen, met name
    de [terminalkamers en openbare
    werkplekken](/en/howto/terminalkamers/)][Installation and
    maintenance of hardware available for students, especially the
    [terminal rooms and public workstations](/en/howto/terminalkamers/)]
-   General support of computer equipment in
    research departments
-   Operational management of the
    server rooms
-   Assistance for, or advice with respect
    to,
    -   [het [installeren van PC’s](/en/howto/installeren-werkplek/) en
        netwerksoftware][ [installing
        PC’s](/en/howto/installeren-werkplek/) and network software]
    -   [ [reparaties van PC’s en printers](/en/howto/reparaties/)][
        [repairing PC’s and printers](/en/howto/reparaties/)]
