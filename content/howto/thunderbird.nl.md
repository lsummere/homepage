---
author: bram
date: '2022-09-19T16:07:13Z'
keywords: []
lang: nl
tags:
- email
title: Thunderbird
wiki_id: '22'
---
## Een Email account instellen.

Let op: Bij een *zelf geïnstalleerde* PC moet u bij het installeren van
Thunderbird aan het eind **Launch Mozilla Thunderbird now** uitvinken en
klik op **Finish**.
{{< figure src="/img/old/tb-no-bw-install.jpg" title="Laatste installatie dialoogvenster" >}}

### Profiel aanmaken

Let Op! Heeft U al eerder Thunderbird *geïnstalleerd* of *opgestart*,
zonder eerst een profiel aan te maken, lees dan eerst
[Thunderbird\#Thunderbird\_opgestart\_zonder\_eerst\_een\_profiel\_aan\_te\_maken](/nl/howto/thunderbird#thunderbird-opgestart-zonder-eerst-een-profiel-aan-te-maken/)

  ---------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------
  Start de profiel manager op, ga naar **Start -\>     {{< figure src="/img/old/tb-profile-manager-leeg.jpg" title="Profile Manager" >}}
  Programs -\> Mozilla Thunderbird -\> Profile         
  Manager**.                                           

  En klik op **Create Profile…**.                      {{< figure src="/img/old/tb-create-profile-wizard.jpg" title="Create Profile Wizard" >}}

  Klik op **Next**. Vul bij **Enter new profile        {{< figure src="/img/old/tb-completing-create-profile-wizard.jpg" title="Completing 'Create Profile Wizard'" >}}
  name:** eventueel een beschrijvende naam in (zolang  
  het invulveld maar niet leeg is).                    

  Klik op **Choose Folder…** om een map aan te maken   {{< figure src="/img/old/tb-create-profile-choose-folder.jpg" title="Choose Folder - 'Create Profile Wizard'" >}}
  waarin de instellingen, adresboek e.d. (en als de    
  “Local Folders” gebruikt worden ook mail) in bewaard 
  gaat worden. Selecteer de home directory (U: schijf) 
  onder **My Computer**                                

  Klik op **Make New Folder**, en geef deze een naam,  {{< figure src="/img/old/tb-finishing-create-profile-wizard.jpg" title="Finishing - 'Create Profile Wizard'" >}}
  b.v. **thunderbird**, en klik vervolgends op **OK**. 
  Controleer of onder **Your user settings,            
  preferences, bookmarks and mail will be stored in:** 
  iets staat als **H:\\thunderbird** of de door U      
  gekozen nieuwe directory op de H: schijf. Klik op    
  **Finish**, er is nu een profiel aangemaakt.         
  ---------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------

### Thunderbird voor de 1e keer starten

  ------------------------------------------------- -------------------------------------------------------------------------------------------------------
  Start Thunderbird door op de profielnaam te       {{< figure src="/img/old/tb-start-user-profile.jpg" title="Start Thunderbird via User Profiles" >}}
  dubbelklikken. In het vervolg kan thunderbird ook 
  via het dektop icoon gestart worden. Het 1e keer  
  starten van Thunderbird kan *enkele minuten*      
  duren, heb geduld - dit duurt alleen de 1e keer   
  zo lang.                                          

  ------------------------------------------------- -------------------------------------------------------------------------------------------------------

### Account Wizard doorlopen

  ----------------------------------------------------- ---------------------------------------------------------------------------------------------------------------
  Thunderbird start nu met de Account Wizard.           {{< figure src="/img/old/tb-account-wizard.jpg" title="Account Wizard" >}}

  Selecteer **Email account** en klik op **Next**. Vul  {{< figure src="/img/old/tb-account-wizard-identity.jpg" title="Account Wizard - Identity" >}}
  bij **Your Name** Uw naam in zoals U wilt dat andere  
  deze bij “Van:” of “From:” te zien krijgen. Vul bij   
  **Email address** uw email address is (of een van uw  
  aliassen) zoals vermeld bij de DHZ website achter:    
  “Email adres: / Mail address:” of een van uw aliassen 
  (zoals bij DHZ als “alias” vermeld).                  

  Klik op **Next**. Selecteer bij **Select the type of  {{< figure src="/img/old/tb-account-wizard-server-info.jpg" title="Account Wizard - Server Information" >}}
  incoming server you are using** het keuzebolletje     
  **IMAP**. Vul bij **Incoming Server** in wat bij DHZ  
  staat achter: “Mail verzamelen op: / Imap/pop         
  mailserver:”. Vul bij **Outgoing Server** in:         
  **smtp.science.ru.nl**                                

  Klik op **Next** Vul bij **Incoming User Name** in    {{< figure src="/img/old/tb-account-wizard-user-names.jpg" title="Account Wizard - User Names" >}}
  wat bij DHZ staat achter “Gebruikersnaam / Username”. 
  Vul bij **Outgoing User Name** in wat bij DHZ staat   
  achter “Gebruikersnaam / Username”.                   

  Klik op **Next**. Laat bij **Account Name** de        {{< figure src="/img/old/tb-account-wizard-account-name.jpg" title="Account Wizard - Account Name" >}}
  ingevulde waarde staan - Uw email adres - of vul een  
  beschrijvende naam in.                                

  Klik op **Next**. Controleer de samenvatting van de   {{< figure src="/img/old/tb-account-wizard-finish.jpg" title="Account Wizard - Finishing" >}}
  instellingen die gemaakt zijn.                        

  Klik op **Finish**. Het onderstaande alert-venster    {{< figure src="/img/old/tb-account-wizard-after-finish-alert.jpg" title="Account Wizard - Alert" >}}
  verschijnt. Dit heeft *niet* te maken met “Maximum    
  number of connections”, maar met nog niet correct     
  ingestelde instellingen, waardoor er nog geen         
  verbinding met de mailserver gemaakt kan worden. Klik 
  op **OK**.                                            
  ----------------------------------------------------- ---------------------------------------------------------------------------------------------------------------

### Opties instellen

  ------------------------------------------------------ ----------------------------------------------------------------------------------------------------------------------
  Het hoofdvenster van Thunderbird verschijnt. Ga via de {{< figure src="/img/old/tb-options-composition-general.jpg" title="Options -\> Composition -\> General" >}}
  menubalk naar **Tools -\> Options…**. Selecteer        
  bovenin het Options venster **Composition**, ga naar   
  het tabblad **General**. Veel mensen vinden het        
  prettiger om als ze een bericht forwarden of een       
  ge-forward bericht ontvangen, het door-te-sturen       
  bericht in de body (is de inhoud) van het bericht      
  staat i.p.v. als attachment.                           

  Verander bij **Forward messages** via de drop-down     {{< figure src="/img/old/tb-options-composition-addressing.jpg" title="Options -\> Composition -\> Addressing" >}}
  **As Attachment** in **Inline**. Ga naar het tabblad   
  **Addressing**. Meestal is het handiger om             
  emailadressen van mensen waarheen U mail stuurt        
  gescheiden te houden van de persoonlijke adresboek,    
  voor het overzicht. Verander bij **Automatically add   
  outgoing email addresses to my** via de drop-down      
  **Personal Address Book** in **Collected Addresses**.  
  ------------------------------------------------------ ----------------------------------------------------------------------------------------------------------------------

### Account settings instellen

```{=html}
<table>
```
```{=html}
<tbody>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Ga via de menubalk naar **Tools -\> Account
Settings…**.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-overview.jpg" title="Account Settings" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik op **Server Settings** onder het
zojuist aangemaakte account. Zet in de **Security
Settings** box het **Use secure
connection** keuzebolletje op
**SSL**.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-server-settings.jpg" title="Account Settings -&gt; Server Settings" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings -\> Server Settings
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik op **Advanced…** Vink
**Show only subscribed folders** uit.
Wilt u uw mappen niet in de map inbox hebben staan vul dan
**INBOX.** (let op de punt) bij
**IMAP server directory** in.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-advanced.jpg" title="&quot;Advanced&quot; Account Settings" alt="" />`{=html}
```{=html}
<figcaption>
```
“Advanced” Account Settings
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik op **OK**. Klik op
**Copies & Folders** onder het zojuist
aangemaakte account.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-copies-and-folders-default.jpg" title="Account Settings -&gt; Copies &amp; Folders" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings -\> Copies & Folders
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
De standaardinstellingen zijn meestal goed. Als U bijvoorbeeld al een
andere **Sent** folder heeft, en wilt
blijven gebruiken kan per item, via het
**Other** keuzebolletje een andere folder
onder Uw accountnaam gekozen worden. Voorbeeld hoe standaardmap te
veranderen…
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-copies-and-folders-alternate.jpg" title="Account Settings -&gt; Copies &amp; Folders" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings -\> Copies & Folders
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik op **Composition & Addressing**onder
het zojuist aangemaakte account. Veelal is het prettiger om het antwoord
op een reply boven het originele bericht te zetten. Verander hiervoor
bij **Automatically quote the original message when
replying** achter
**Then** in de drop-down
**start my reply below the quote** in
**start my reply above the quote**.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-composition-and-addressing.jpg" title="Account Settings -&gt; Composition &amp; Addressing" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings -\> Composition & Addressing
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Als U emails in “platte tekst” op wilt maken, vink dan
**Compose messages in HTML format** uit.
Selecteer **Outgoing Server (SMTP)**.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-outgoing-server.jpg" title="Account Settings -&gt; Outgoing Server (SMTP)" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings -\> Outgoing Server (SMTP)
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik op **Edit…**. Selecteer het
keuzebolletje bij **TLS, if available**.
Verander eventueel het poortnummer, bij
**Port** in
**587**, zie de
`<a href="email_authsmtp" title="wikilink">`{=html}Instellen van
Authenticated SMTP`</a>`{=html} handleiding voor uitleg hierover. Vul
eventueel een **Description** in. Dit kan
handig zijn als er meerdere smtp servers ingesteld worden.
```{=html}
</p>
```
```{=html}
<p>
```
Voor nieuwere versies van Tunderbird bij gebruik poort 587: Kies
“connection security” “STARTTLS” en zorg dat “use secure authentication”
uitgevinkt staat.
```{=html}
</p>
```
```{=html}
<p>
```
Klik het **Account Settings** venster met
**OK** dicht.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="tb-account-settings-outgoing-server-smtp-server.jpg" title="Account Settings -&gt; Outgoing Server (SMTP) -&gt; SMTP Server" alt="" />`{=html}
```{=html}
<figcaption>
```
Account Settings -\> Outgoing Server (SMTP) -\> SMTP Server
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
</tbody>
```
```{=html}
</table>
```
### Accountinstellingen controleren.

  ---------------------------------------------------------- -------------------------------------------------------------------------------------------
  Selecteer onder de accountnaam **Inbox**, en klik op **Get {{< figure src="/img/old/tb-get-first-mail.jpg" title="Eerste mail binnenhalen." >}}
  Mail**.                                                    

  Vul waar gevraagd het wachtwoord in, en klik op **OK**.    {{< figure src="/img/old/tb-testmail-binnen.jpg" title="Overzicht met een mailtje." >}}
  Bij **Inbox** staat nu een plusteken. Klik hierop om deze  
  uit te vouwen. Er zouden nu ten minste 2 mappen, met de    
  naam “Virus” en “Spam” moeten staan. Klik op **Write**,    
  vul bij **To:** Uw e-mailadres in, als **Subject:** b.v.   
  test. Klik nu op **Send**. Klik weer op **Get Mail**. Als  
  alles goed is heeft U nu het testmailtje in Uw Inbox.      
  ---------------------------------------------------------- -------------------------------------------------------------------------------------------

## Thunderbird opgestart zonder eerst een profiel aan te maken

### Er is nog niets gedaan in Thunderbird - alleen een keer opgestart, account wizard niet doorlopen

Als er nog niets met Thunderbird gedaan is, en U hier *absoluut zeker*
van bent kunt U het profiel veilig verwijderen. Volg dan de volgende
stappen.

  ----------------------------------------- -----------------------------------------------------------------------------------------------------
  Ga naar de Thunderbird profile manager    {{< figure src="/img/old/tb-profile-manager-verwijderen.jpg" title="Profile Manager" >}}
  Klik op **Start -\> Programs -\> Mozilla  
  Thunderbird -\> Profile Manager**.        

  Klik op **Delete Profile…** Klik op       {{< figure src="/img/old/tb-profile-manager-verwijderen-ookfiles.jpg" title="Profile Manager" >}}
  **Delete Files** Klik op **Exit**.        
  ----------------------------------------- -----------------------------------------------------------------------------------------------------

U kun nu zonder problemen de handleiding voor het instellen van een
Email account volgen.

### Er zitten al gegevens in Thunderbird die behouden of verhuisd moeten worden

Als U het profiel wilt verplaatsen naar een goede locatie, volg dan de
volgende stappen.

  ------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------
  Zoek eerst het profiel op en lees de [\#3        
  uitleg] Verplaats de profile directory naar een  
  goede locatie. B.v. van **C:\\Documents and       
  Settings\\loginnaam\\Application                  
  Data\\Thunderbird\\Profiles\\uy1yi71r.default**   
  naar **H:\\thunderbird**.                         

  Ga naar de Thunderbird profile manager Klik op    {{< figure src="/img/old/tb-profile-manager-verwijderen.jpg" title="Profile Manager" >}}
  **Start -\> Programs -\> Mozilla Thunderbird -\>  
  Profile Manager**.                                

  Klik op **Delete Profile…** Als U het             {{< figure src="/img/old/tb-profile-manager-verwijderen-nietfiles.jpg" title="Profile Manager" >}}
  onderstaande venster krijgt, klik dan op **Don’t  {{< figure src="/img/old/tb-profile-manager-leeg.jpg" title="Profile Manager" >}}
  Delete Files** - en controleer of de profiel      
  directory goed verplaatst is.                     

  En klik op **Create Profile…**.                   {{< figure src="/img/old/tb-create-profile-wizard.jpg" title="Create Profile Wizard" >}}

  Klik op **Next**. Vul bij **Enter new profile     {{< figure src="/img/old/tb-completing-create-profile-wizard.jpg" title="Completing 'Create Profile Wizard'" >}}
  name:** eventueel een beschrijvende naam in       
  (zolang het invulveld maar niet leeg is).         

  Klik op **Choose Folder…** om de map te kiezen    {{< figure src="/img/old/tb-create-profile-choose-folder.jpg" title="Choose Folder - 'Create Profile Wizard'" >}}
  waarheen het profiel verplaatst is - b.v.         
  **H:\\thunderbird**. Blader naar de map toe.      

  Selecteer de map, en klik vervolgends op **OK**.  {{< figure src="/img/old/tb-finishing-create-profile-wizard.jpg" title="Finishing - 'Create Profile Wizard'" >}}
  Controleer of onder **Your user settings,         
  preferences, bookmarks and mail will be stored    
  in:** de goede map geselecteerd is. Klik op       
  **Finish**.                                       

  Thunderbird starten Start Thunderbird door op de  {{< figure src="/img/old/tb-start-user-profile.jpg" title="Start Thunderbird via User Profiles" >}}
  profielnaam te dubbelklikken. In het vervolg kan  
  thunderbird ook via het dektop icoon gestart      
  worden.                                           
  ------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------

## Profielen

Er zijn 2 soorten profielen die op deze pagina genoemd worden. Dit is
het Windows gebruikersprofiel en het Thunderbird profiel.

### Het Windows gebruikersprofiel

Dit zijn de Windows instellingen die op een server worden gebackupt
indien U een [beheerde werkplek PC](/nl/howto/hardware-bwpc/) heeft. Het
Windows gebruikersprofiel staat op de systeempartitie (meestal de **C:
schijf**) in een subdirectory van **Documents and Settings** genoemd
naar Uw **loginnaam**. Zie het onderstaande voorbeeld.

{{< figure src="/img/old/tb-documents-and-settings.jpg" title="Documents and Settings" >}}

*Als er in deze pagina verder nog verwezen wordt naar “profiel” wordt,
indien niet verder aangegeven het Thunderbird profiel bedoeld.*

### Het Thunderbird profiel

Dit is de verzameling instellingen en gegevens van Thunderbird
opgeslagen in een directory. Gegevens die zoal hierin staan zijn
contactpersonen, van welke server mail opgehaalt moet worden, eventueel
lokaal opgeslagen emails, enz. Een profiel wordt standaard aangemaakt in
Uw Windows profiel. Dit is voor thuisgebruik goed, maar Uw lokale mail
en instellingen worden niet ge-backupped! In het geval van een [beheerde
werkplek](/nl/howto/hardware-bwpc/) wordt dit ook op deze lokatie
ge-backupped - maar de ruimte is hier zeer beperkt. Daarom moet Uw
Thunderbird profiel in principe op een netwerkschijf staan die
gebackupped wordt. In dit de verdere FAQ’s wordt ervanuitgegaan dat de
ge-backuppede lokatie waar U Uw Thunderbird profiel wilt -of- heeft
opgeslagen Uw **homedirectory** is.

Om de achter de lokatie te komen waar **nu** uw profiel opgeslagen is,
doe het volgende:

  ------------------------------------------------------------ --------------------------------------------------------------------------------------------------------------
  Ga naar de Thunderbird profile manager, **Start -\> Programs {{< figure src="/img/old/tb-profile-manager-leeg-geenselectie.jpg" title="Profile Manager" >}}
  -\> Mozilla Thunderbird -\> Profile Manager**.               

  Is deze, zoals op de afbeelding leeg, dan heeft U *nog geen  {{< figure src="/img/old/tb-profile-manager-default.jpg" title="Profile Manager - Met één account" >}}
  Thunderbird profiel*. U kun dan zonder problemen de          
  handleiding voor het instellen van een Email account volgen. 
  De onderstaande punten hoeven dan *niet* doorlopen te        
  worden. Is er wel een profiel naam aanwezig, zoals in de     
  onderstaande afbeelding - sluit de profile manager door op   
  **Exit** te klikken en ga dan verder met de onderstaande     
  stappen.                                                     

  Zorg dat verborgen bestanden zichtbaar zijn. Open **My       {{< figure src="/img/old/tb-folder-options.jpg" title="Tools -\> Folder Options..." >}}
  Computer** op het bureaublad. Ga via het menu naar **Tools   
  -\> Folder Options…**, selecteer het **View** tabblad.       

  Selecteer bij **Hidden files and folders** het keuzebolletje 
  **Show hidden files and folders**. Deze optie kan waar       
  teruggezet worden wanneer het bekijken / verhuizen /         
  verwijderen van het profiel klaar is. Klik op **OK**.        

  Bekijk het profiles.ini bestand Ga naar Uw “Windows          {{< figure src="/img/old/tb-profiles-ini.jpg" title="profiles.ini" >}}
  gebruikersprofiel directory”, **Application Data** en        
  vervolgends **Thunderbird**. B.v. “**C:\\Documents and       
  Settings\\loginnaam\\Application Data\\Thunderbird**”. In    
  deze directory staat een **profiles.ini**. Dit bestand       
  verwijst naar de locatie waar het profiel opgeslagen is.     
  Open het bestand door hierop te dubbelklikken. In dit        
  bestand staan twee regels die nu relevant zijn. Achter       
  **IsRelative** zal of **0** of **1** staan. Bij **Path** zal 
  een relatief - bij een IsRelative waarde van 1, of absoluut  
  pad - bij een IsRelative waarde van 0, staan.                

  **IsRelative** = 1 **Path** waarde is **Profiles/\<directory {{< figure src="/img/old/tb-profiles-ini-content-wrong.jpg" title="Inhoud profiles.ini relatief pad" >}}
  naam\>** waarbij “directorynaam” staat voor een willekeurige 
  letters-met-cijfers combinatie met “.default” erachter.      
  Hieronder een voorbeeld waarbij het profiel onder het        
  Windows-profiel staat - dit is, voor niet-thuissituaties,    
  niet goed! Het **Path** is hier                              
  **Profiles/uy1yi71r.default**. De locatie van het profiel is 
  de directory waarin het **profiles.ini** bestand zich bevind 
  met eraanvast de waarde van **Path**. De locatie van het     
  profiel is bij dit voorbeeld **C:\\Documents and             
  Settings\\loginnaam\\Application                             
  Data\\Thunderbird\\Profiles\\uy1yi71r.default**              

  **IsRelative** = 0 **Path** waarde is het volledige pad naar {{< figure src="/img/old/tb-profiles-ini-content-correct.jpg" title="Inhoud profiles.ini absoluut pad" >}}
  de profiel directory In het volgende voorbeeld staat het     
  profiel op de homedirectory. De **Path** regel heeft de      
  waarde **H:\\thunderbird**. De locatie van het profiel is de 
  waarde van **Path**, in dit voorbeeld **H:\\thunderbird**    
  ------------------------------------------------------------ --------------------------------------------------------------------------------------------------------------

## Hoe stel ik een LDAP adresboek in?

U kunt het Science LDAP adresboek toevoegen door de onderstaande stappen
uit te voeren.

  --------------------------------------------------------- ----------------------------------------------------------------------------------------------------
  Activeer en open de directory servers lijst Ga via het    {{< figure src="/img/old/tb-ldap-adresboek-vinkaan.jpg" title="Directory Server aanvinken" >}}
  menu naar **Tools -\> Options…**. Selecteer bovenin het   
  Options venster **Composition**, ga naar het tabblad      
  **Addressing**. Vink **Directory Server** aan, en klik op 
  **Edit Directories…**.                                    

  Voeg een nieuwe toe                                       {{< figure src="/img/old/tb-ldap-adresboek-lijst.jpg" title="Directory Servers lijst" >}}

  Klik op **Add**. Vul bij het **Directory Server           {{< figure src="/img/old/tb-ldap-adresboek-toevoegen.jpg" title="Directory Server toevoegen" >}}
  Properties** venster, bij het tabblad **General** in:     
  **Name** een beschrijvende naam, b.v.:                    
  **Science-Adresboek** **Hostname** :                      
  **ldap.science.ru.nl** **Base DN** : **o=addressbook**    
  **Port number** heeft als defaultwaarde al **389** -      
  hoeft niet veranderd te worden. En klik alle openstaande  
  optie vensters met **OK** dicht.                          
  --------------------------------------------------------- ----------------------------------------------------------------------------------------------------

### Gebruik van het Science-Adresboek

Het LDAP adresboek kunt U op 2 plaatsen gebruiken.

#### LDAP Adresboek vanuit “Address Book” gebruiken

Klik in het hoofdvenster op de knop **Address Book**

{{< figure src="/img/old/tb-ldap-adresboek-resultaat-book.jpg" title="LDAP Adresboek vanuitAddress Book gebruiken" >}}

Aan de linkerkant onder ziet U een lijst met beschikbare adresboeken.
Hier staat nu het **Science-Adresboek** bij, klik hierop. Als U nu in
het zoekvanster een stuk van de naam of het emailadres invult, ziet U
het venster eronder vullen - tot maximaal 100 matches - met items die
hieraan voldoen.

#### LDAP Adresboek vanuit een “Compose” venster gebruiken

Klik in het hoofdvenster op de knop **Write**

{{< figure src="/img/old/tb-ldap-adresboek-resultaat-newmail.jpg" title="LDAP Adresboek vanuit eenCompose venster gebruiken" >}}

Er verschijnt een **Compose** venster. Klik op de **Contacts** knop en
selecteer bij **Address Book** het **Science-Adresboek**. Als U nu in
het zoekvanster een stuk van de naam of het emailadres invult, ziet U
het venster eronder vullen - tot maximaal 100 matches - met items die
hieraan voldoen.

### Opmerkingen

Niet alle adresboek items hebben ook een email adres (b.v. receptie) hou
hier rekening mee! Als U wilt weten wat er bij een item aan attributen
aanwezig zijn kunt U rechts klikken op een item, en **Properties**
selecteren.

{{< figure src="/img/old/tb-ldap-adresboek-resultaat-properties.jpg" title="Contacts card Properties" >}}

Vanwege de beveiliging kunnen er **geen items veranderd worden** in het
LDAP adresboek, wel kan er een kopie gemaakt worden in een lokaal
adresboek door het betreffende item uit het “Science-Adresboek” te
slepen naar b.v. het “Personal Address Book.” Het LDAP adresboek is
**alleen bereikbaar** voor machines vanaf het RU-netwerk.

## Tips / trucs

[Thunderbird tips](http://email.about.com/od/mozillathunderbirdtips/)

## Add-ons

Add-ons zijn stukjes code die kleine verbeteringen of nieuwe
mogelijkheiden toevoegen aan Thunderbird. Zie de [Thunderbird
Add-ons](/nl/howto/thunderbird-add-ons/) pagina.

Hoe de font grootte in From en To regels in ThunderBird aanpassen: Zie
Engelse versie.

[Categorie:Thunderbird](/nl/tags/thunderbird)
