---
author: remcoa
date: '2022-10-06T15:13:58Z'
keywords: []
lang: nl
tags:
- internet
title: TLS Certificaten
wiki_id: '68'
---
# TLS certificaten

Iedere service die beschikbaar is via TLS (https), moet een TLS
certificaat hebben, zo ook elke webserver met versleutelde of
“beveiligde” inhoud. Een TLS
(Transport Layer Security, zie [RFC 8446](https://www.rfc-editor.org/rfc/rfc8446))
certificaat is een
elektronisch getekende garantie dat een bepaalde server daadwerkelijk de
server is die deze claimt te zijn. Ze worden hoofdzakelijk (maar niet
alleen) gebruikt om webpagina’s te geven via een versleutelde
verbinding. Een certificaat is getekend door een Certificate Authority
(CA), deze verzekert de integriteit van het certificaat.

Een aantal Certificate Authorities wordt standaard vertrouwd door TLS
clients (inclusief web-browsers), o.a. Let's Encrypt, Verisign, Thawte en Terena. Dat
wil zeggen dat certificaten door een van deze getekend zonder meer
vertrouwd worden zonder tussenkomst van de gebruiker. Alle C&CZ servers
en webapplicaties zijn voorzien van door Terena (via SURFdiensten)
getekende certificaten.

# Certificaten verkrijgen

Omdat een TLS Certificaat gebruikt wordt als **bewijs** dat de website
of server valide is, kan niet zomaar iedereen een gesigneerd certificaat
aanvragen voor een willekeurige domeinnaam. De Certificate Authority
controleert dat de aanvrager de rechtmatige eigenaar is van de
domeinnaam waarvoor het certificaat wordt aangevraagd. Domeinnamen die
via C&CZ worden [geregistreerd](/nl/howto/domeinnaam-registratie/) komen
op naam van de Radboud Universiteit te staan. Voor zulke domeinnamen kan
C&CZ ook een TLS Certificaat aanvragen.

# Heartbleed OpenSSL lek

Op 7 april 2014 werd bekend dat er enkele jaren een kwetsbaarheid (lek)
heeft gezeten in een aantal versies van
[OpenSSL](https://www.openssl.org/). OpenSSL wordt gebruikt voor het
versleutelen van netwerkverkeer. Door dit lek kon een aanvaller de
geheime sleutel van een service ophalen, waarmee versleuteld verkeer
ontsleuteld kon worden. Daarnaast kon door dit lek een aanvaller het
complete geheugen van de service lezen, waarin gevoelige informatie als
wachtwoorden kon staan.

Na het bekend worden van dit [Heartbleed OpenSSL
lek](http://heartbleed.com/) zijn alle kwetsbare C&CZ services
automatisch gerepareerd. Daarna zijn op donderdag 10 april nieuwe
certificaten ingezet. De oude certificaten worden [ongeldig
verklaard](http://nl.wikipedia.org/wiki/Certificate_revocation_list).
Indien men de onderstaande C&CZ services gebruikt heeft, is het
verstandig om het Science wachtwoord op de [DHZ
website](https://dhz.science.ru.nl) aan te passen. Het oude wachtwoord
zou namelijk bij een aanvaller bekend kunnen zijn geworden.

De lijst van kwetsbare C&CZ services waar medewerkers of studenten van
FNWI gebruik van kunnen hebben gemaakt is:

-   Mail gebruikers:
    -   squirrel.science.ru.nl: De oude Science webmailservice was
        kwetsbaar sinds 29 januari 2014.
    -   roundcube.science.ru.nl:De nieuwe Science
        webmailservice.
    -   autoconfig.science.ru.nl: De website voor automatische
        configuratie van mailprogramma’s als Thunderbird en
        Outlook.

-   Docenten: dossiers.science.ru.nl
