---
author: simon
date: '2021-02-05T17:02:21Z'
keywords: []
lang: en
tags: []
title: Vpnsec Linux install
wiki_id: '1005'
---
## Installation with NetworkManager on Ubuntu 20.04.1 LTS

Networkmanager is now capable of generating a new vpn connection without
manually creating the config file. Start with installing the necessary
packages:

    $ sudo apt install strongswan-nm network-manager-strongswan libstrongswan-standard-plugins  strongswan-libcharon \
                       libcharon-extauth-plugins libstrongswan-extra-plugins strongswan-charon libcharon-extra-plugins

Reboot after the installation. Via Settings -\> Network you can then add
a VPN connection by clicking on the +. Choose a name for the connection
profile, fill in vpnsec.science.ru.nl as Server address. Choose as
Client Authentication EAP (Username/Password) and fill in your science
account for Identity and Username. By clicking on the ? after Password
you can select the policy for the password. Finally in the Options
select ‘Request an inner IP address’ and ‘Enforce UDP encapsulation’

Apply and then you can switch on the VPN through the NetworkManager
applet in the toolbar.

### Plasma (KDE)

Pretty much the same as the general instructions, “Server” may be called
“Gateway” heet, you don’t need to specify a key or certificate, I assume
this is for key-based authentication.

To make an actual VPN connection, go to the system tray and select VPN
from the network icon and click connect.

## Installation with NetworkManager on Ubuntu 16.04.1 LTS

[Ubuntu 16.04]{style="color:#FF0000"}: There is a known bug people are trying to fix,
see:  [msg4923789](https://bugs.launchpad.net/bugs/1570352).

With Ubuntu 16.04.1 LTS it is now possible to use ipsec. However some
manual configuration has to be performed, editting the connection using
the VPN applet is not possible,\
\* Install required software:

    $ sudo apt install strongswan-nm network-manager-strongswan libstrongswan-standard-plugins strongswan-plugin-eap-mschapv2 

-   Copy the contents below to
    /etc/NetworkManager/system-connections/VPN-science:

    [connection]
    id=vpnsec.science.ru.nl
    uuid=1e0ab541-e051-41c2-819f-a005b296c53b
    type=vpn
    autoconnect=false
    permissions=
    secondaries=
    timestamp=1465384551

    [vpn]
    virtual=yes
    encap=yes
    address=vpnsec.science.ru.nl
    user=USERNAME
    method=eap
    password-flags=0
    ipcomp=no
    service-type=org.freedesktop.NetworkManager.strongswan

    [vpn-secrets]
    password=

    [ipv4]
    dns-search=
    method=auto

    [ipv6]
    addr-gen-mode=stable-privacy
    dns-search=
    ip6-privacy=0
    method=auto

-   Change USERNAME to your science login.
-   Change the permissions to `-rw-------` (owner rw permisions, group
    and other no permissions):

      chmod u=rw,go= /etc/NetworkManager/system-connections/VPN-science
      ls -la /etc/NetworkManager/system-connections/

-   Reboot the machine before trying to use the VPN connection.

Starting the connection is identical to Ubuntu 14.04. (When starting the
VPN connection, your science password is requested)\
**NB: The password will be stored in plain text in the file mentioned
above!**

## Installation with NetworkManager on Ubuntu 14.04 LTS

This procedure assumes using **`NetworkManager`**.
See
[below](#.5binstallatie_zonder_networkmanager.5d.5binstallation_without_networkmanager.5d)
for a manual procedure.

Install the required software:

    $ sudo apt-get install network-manager-strongswan strongswan-plugin-eap-mschapv2
    The following NEW packages will be installed:
      libstrongswan{a} network-manager-strongswan strongswan-ike{a} strongswan-nm{a}
      strongswan-plugin-eap-mschapv2 strongswan-plugin-openssl{a} 
    ...

    $ sudo service network-manager stop
    network-manager stop/waiting
    $ sudo service network-manager start
    network-manager start/running, process 29031

Configuration:

Select the NetworkManager applet and after that
**`Edit Connections...`**

[400px](/en/howto/bestand:vpnsec-linux-2.png/)

Click **`Add`**,
select IPsec/IKEv2 in the section
**`VPN`**, click
**`Create`**

[400px](/en/howto/bestand:vpnsec-linux-3.png/)

Enter data
at:**`Connection name`**,
**`Address`**
(**`vpnsec.science.ru.nl`**),
loginname, etc. and check the marks where needed.

[400px](/en/howto/bestand:vpnsec-linux-4.png/)

Save: (**`Save`** [and]
**`Close`**)

[400px](/en/howto/bestand:vpnsec-linux-5.png/)

Start the **`VPN`**.
Select the NetworkManager applet,
next **`VPN Connections`** and finally the connection
created.

[400px](/en/howto/bestand:vpnsec-linux-6.png/)

### Known problems

If the VPN connection has been established, but `ping ns1.science.ru.nl`
doesn’t work, while `ping 131.174.224.4` does work, then probably
`dnsmasq` is the culprit. This can be solved bij disabling the dnsmasq
DNS cache, as is described in [Ask Ubuntu: DNS problem when connected to
a
VPN](http://askubuntu.com/questions/320921/having-dns-issues-when-connected-to-a-vpn-in-ubuntu-13-04):

    First make sure that there are no lines beginning with nameserver in any files in /etc/resolvconf/resolv.conf.d.
    If /etc/resolvconf/resolv.conf.d/tail is a symbolic link to target original, make it point to /dev/null.

    Second, disconnect from the VPN. Edit /etc/NetworkManager/NetworkManager.conf

    $ sudo gedit /etc/NetworkManager/NetworkManager.conf

    and comment out

    dns=dnsmasq

    (i.e., add a # so that it looks like the following)

    #dns=dnsmasq

    and then

    sudo service network-manager restart

## Installation with NetworkManager on Debian Jessie

Unfortunately, the strongswan-network-manager package did not make it to
Debian Jessie (and as of writing, is not in jessie-backports either).
Nevertheless, backporting the current version of the package appears to
work fine. The steps involved in achieving this were taken almost
verbatim from [this
guide](http://sebastiangibb.de/debian/2015/09/19/university-cambridge-vpn-debian-jessie.html).

First you have to install the debian packaging tools:

    sudo apt-get install packaging-dev debian-keyring devscripts equivs

Afterwards you have to download the debian source package:

    dget -x http://http.debian.net/debian/pool/main/n/network-manager-strongswan/network-manager-strongswan_1.3.1-1.dsc

Install potentially missing dependencies:

    cd network-manager-strongswan-1.3.1/
    sudo mk-build-deps --install --remove

Add a backport revision number:

    dch --local ~bpo80+ --distribution jessie-backports "Rebuild for jessie-backports."

Build the package (without package signing)

    dpkg-buildpackage -us -uc

Install the newly built package:

    sudo dpkg -i ../network-manager-strongswan_1.3.1-1~bpo80+1_amd64.deb

Install potentially missing dependencies, just to be sure:

    sudo apt-get install -f

Install additional plugins (most notably the eap-mschapv2 plugin):

    sudo apt-get install libcharon-extra-plugins

Restart network manager

    sudo systemctl restart network-manager.service

## Installation without NetworkManager

*Note: Below some remarks on doing this for Fedora Core
(FC23).*

Install strongswan, including the curl, eap-identity, eap-mschapv2 and
eap-md5 (required for eap-mschapv2) plugins. When you compile strongswan
from source, make sure to pass the right parameters to the
**`configure`** script.

    $ ./configure --enable-curl --enable-md5 --enable-openssl --enable-xauth-eap --enable-eap-md5 --enable-eap-gtc --enable-eap-tls --enable-eap-ttls --enable-eap-peap --enable-eap-mschapv2 --enable-eap-identify
    $ make
    $ sudo make install

You can test which plugins are loaded with
**`sudo ipsec statusall`** or
**`sudo ipsec listplugins`**. If necessary you can
load plugins manually by editing
**`strongswan.conf`**.

Make sure your **`ipsec.conf`**, probably located
in /etc or in /usr/local/etc, looks like this:

    config setup
        strictcrlpolicy=yes

    conn %default
        ikelifetime=60m
        keylife=20m
        rekeymargin=3m
        keyingtries=1
        keyexchange=ikev2

    conn science
        left=%defaultroute
        leftfirewall=yes
        leftsourceip=%config
        leftauth=eap-mschapv2
        leftid=mysciencelogin        <-- edit this
        eap_identity=mysciencelogin  <-- edit this
        right=vpnsec.science.ru.nl
        rightauth=pubkey
        rightid=@vpnsec.science.ru.nl
        rightsubnet=0.0.0.0/0
        forceencaps=yes
        auto=start

And your **`ipsec.secrets`** as follows, where you
enter your own Science account name and password. Watch out that this
file cannot be read by everyone, as it contains your password! It is
typically owned by **`root:root`** with
**`-rw-------`** (600) permissions.

    mysciencelogin : EAP "mypassword"

Everything should work now:

    $ sudo ipsec start
    $ sudo ipsec up science

It can be necessary to put the root certificate in the right folder
manually:

    $ sudo ln -s /etc/ca-certificates/extracted/cadir/DigiCert_Assured_ID_Root_CA.pem [/usr/local]/etc/ipsec.d/cacerts/DigiCert_Assured_ID_Root_CA.pem

## Fedora (FC23)

Work in progress… [Gebruiker:Arjen], FC23, 4/3/2016:

On Fedora (tested on FC23), replace command
**`ipsec`** by
**`strongswan`** in the instructions above.

To get the newest SELinux policies, you need to issue:

    dnf update --enablerepo=updates-testing selinux-policy

Edit `/etc/strongswan/strongswan.d/charon/resolve.conf` to include a
line:

    file = /etc/resolv.conf

To resolve errors about certificate CN=TERENA SSL CA 3 not being
trusted:

Navigate to [1](https://pki.cesnet.cz/en/ch-tcs-ssl-ca-3-crt-crl.html)
Download `DigiCert_Assured_ID_Root_CA.crt` and `TERENA_SSL_CA_3.crt`.

Next, extract the `.pem` files, copy these to the strongswan config
directories, and reread the certificates, like this:

    openssl x509 -inform DES -in DigiCert_Assured_ID_Root_CA.crt -out DigiCert_Assured_ID_Root_CA.pem -text
    openssl x509 -inform DES -in TERENA_SSL_CA_3.crt -out TERENA_SSL_CA_3.pem -text

    cp DigiCert_Assured_ID_Root_CA.pem /etc/strongswan/ipsec.d/cacerts
    cp TERENA_SSL_CA_3.pem /etc/strongswan/ipsec.d/certs

    strongswan rereadall

To overcome problems with DNS, I had to stop service \`libvirtd\`. Yet
to be resolved.
