---
author: petervc
date: '2019-06-25T12:40:26Z'
keywords: []
ShowToc: true
lang: nl
tags:
- internet
title: Vpn linux
wiki_id: '72'
---
# PPTP VPN turned off

Use [VPNsec](/nl/howto/vpnsec-linux-install/) instead of PPPT!

## Het opzetten van een PPTP VPN connectie in Linux

Om een PPTP VPN op te zetten in Linux zijn een aantal onderdelen nodig.
U moet minimaal beschikken over:

-   Lees **science.ru.nl** ipv **sci.kun.nl**. Plaatjes zijn gemaakt
    voor de DNS rename!!!
-   Een internet connectie
-   Een provider met GRE ondersteuning (standaard)
-   Een kabel/ADSL router (mits van toepassing) met GRE ondersteuning
    (standaard)
-   Een gebruikersnaam en wachtwoord

Zie voorlopig [deze
website](http://pptpclient.sourceforge.net/documentation.phtml) en/of
[hier](http://www.cuhk.edu.hk/itsc/network/vpn/fedora.html) voor een
engelse handleiding

## PPTP VPN in openSuSE 11.1

-   Installeer het pakket “pptp” met YaST
-   Wordt root met het “su” commando
-   Maak een ppp0 interface met het volgende commando:

`/usr/sbin/pptpsetup --create radboud --server vpn-srv.science.ru.nl --username USERNAME --encrypt --start`

-   Voor iedere website die je via VPN wilt bezoeken geef je een “route”
    commando.

B.v., voor toegang tot www.sciencedirect.com:

`/sbin/route add -net www.sciencedirect.com netmask 255.255.255.255 dev ppp0`

### Notes

-   In /etc/ppp/options staat default “idle 600”. Een VPN connectie
    wordt verbroken als ze 600 seconden niet wordt gebruikt
-   Met /sbin/ifconfig kun je zien het ppp0 interface bestaat
-   Gebruik /usr/sbin/traceroute www.sciencedirect.com om te zien of VPN
    gebruikt wordt voor toegang tot de betreffende site
-   Om al de verbindingen via vpn te laten lopen zie:

<http://pptpclient.sourceforge.net/routing.phtml>

## PPTP VPN in openSuSE 11.2

-   Klik op netwerkmanager in de systeembalk en vervolgens op ‘manage
    connecties’.
-   Open het tabblad VPN.
-   Add\>pptp
-   Vul een connectie naam in, bijv. Radboud.
-   Vul in bij Gateway: vpn-srv.science.ru.nl.
-   Gebruikersnaam en wachtwoord zijn je standaard voor het
    science.ru.nl domein.
-   (De instelling van het IP adressen tabblad moet zijn ‘Configure:
    Automatic (VPN)’.)
-   Klik op ‘geavanceerd’ en vink aan ‘gebruik MPPE encryptie’.
-   Klik overal op ‘ok’. Nu is de vpn verbinding geconfigureerd en staat
    deze ook in het lijstje van je netwerkmanager in de systeembalk.

## PPTP VPN in openSuSE 11.4 (tested for gnome)
