---
author: bram
date: '2022-09-19T16:27:01Z'
keywords: []
lang: en
tags:
- hardware
title: Installeren werkplek
wiki_id: '40'
---
## Installing a PC

When installing a new PC you have several options.

-   If the PC is not connected to the network, C&CZ can offer little or
    no help to install your PC. You can [borrow](/en/tags/software)
    DVD/CD’s with a Windows Operating system and application software
    like MS-Office, etc from C&CZ .
-   If the PC is connected to the network via a (PXE enabled) network
    interface, a fully automated installation of Windows with standard
    applications is possible. In this case your PC becomes a [beheerde
    werkplek PC](/en/howto/hardware-bwpc/).\
    Also it is possible to install, fully automated, Ubuntu Linux with
    standard applications on the PC.\
    In both cases contact C&CZ to register your PC.
