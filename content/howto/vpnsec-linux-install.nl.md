---
author: simon
date: '2021-02-05T17:02:21Z'
keywords: []
lang: nl
tags: []
title: Vpnsec Linux install
wiki_id: '1005'
---
## Installatie met NetworkManager op Ubuntu 20.04.1 LTS

Networkmanager kan inmiddels een vpn connectie aanmaken zonder
handmatige aanpassingen. Start met het installeren van de vereiste
pakketten:

    $ sudo apt install strongswan-nm network-manager-strongswan libstrongswan-standard-plugins  strongswan-libcharon \
                       libcharon-extauth-plugins libstrongswan-extra-plugins strongswan-charon libcharon-extra-plugins

Reboot na installatie. Vervolgens kun je via Settings-\> Network een VPN
toevoegen door op + te klikken. Kies een naam voor het profiel wat je nu
aanmaakt, vul in vpnsec.science.ru.nl als Server Address Kies Client
Authentication EAP (Username/Password) en vul bij Identity en Username
je science loginname in Door op het ? te klikken achter Password kun je
kiezen voor de gewenste password opslag policy. Vink tenslotte in het
Options blokje ‘Request an inner IP address’ en ‘Enforce UDP
encapsulation’ aan.

Apply en daarna kun je de VPN connectie inschakelen.

### Plasma (KDE)

In principe gelijk aan de algemene instructies, let op de de “Server”
mogelijk “Gateway” heet, je hoeft geen certificate of key in te vullen,
ik neem aan dat dat voor key based authentication is.

Voor het maken van verbinding kies je in de tray voor het netwerk ikoon
en daarbinnen verbinden met de nieuwe VPN.

## Installatie met NetworkManager op Ubuntu 16.04.1 LTS

[Ubuntu 16.04]{style="color:#FF0000"}: Er is een bekende bug waaraan
gewerkt wordt, zie:  [msg4923789](https://bugs.launchpad.net/bugs/1570352).

Met Ubuntu 16.04.1 LTS is het nu mogelijk om ipsec te gebruiken. Hiertoe
moet je wel enig handwerk verrichten, het editten van de connectie
middels de VPN applet is nog niet mogelijk.\
\* Installeer de vereiste software:

    $ sudo apt install strongswan-nm network-manager-strongswan libstrongswan-standard-plugins strongswan-plugin-eap-mschapv2 

-   Copier (als root/sudo) de tekst hieronder naar
    /etc/NetworkManager/system-connections/VPN-science:

    [connection]
    id=vpnsec.science.ru.nl
    uuid=1e0ab541-e051-41c2-819f-a005b296c53b
    type=vpn
    autoconnect=false
    permissions=
    secondaries=
    timestamp=1465384551

    [vpn]
    virtual=yes
    encap=yes
    address=vpnsec.science.ru.nl
    user=USERNAME
    method=eap
    password-flags=0
    ipcomp=no
    service-type=org.freedesktop.NetworkManager.strongswan

    [vpn-secrets]
    password=

    [ipv4]
    dns-search=
    method=auto

    [ipv6]
    addr-gen-mode=stable-privacy
    dns-search=
    ip6-privacy=0
    method=auto

-   Verander USERNAME in het bestand naar je science login.
-   Verander de rechten naar -rw——- (owner rw rechten, group en other
    geen rechten):

      chmod u=rw,go= /etc/NetworkManager/system-connections/VPN-science
      ls -la /etc/NetworkManager/system-connections/

-   Reboot de machine alvorens gebruik te willen maken van de VPN
    connection.

Het opstarten van de connectie is conform Ubuntu 14.04. (Bij het starten
wordt om je science wachtwoord gevraagd)\
**NB: Het wachtwoord wordt leesbaar opgeslagen in het bovengenoemde
bestand!**

## Installatie met NetworkManager op Ubuntu 14.04 LTS

Deze procedure gaat uit van gebruik van Ubuntu 14.04 en
**`NetworkManager`**. Zie
[hieronder](#.5binstallatie_zonder_networkmanager.5d.5binstallation_without_networkmanager.5d)
voor een handmatige procedure.

Installeer de benodigde software:

    $ sudo apt-get install network-manager-strongswan strongswan-plugin-eap-mschapv2
    The following NEW packages will be installed:
      libstrongswan{a} network-manager-strongswan strongswan-ike{a} strongswan-nm{a}
      strongswan-plugin-eap-mschapv2 strongswan-plugin-openssl{a} 
    ...

    $ sudo service network-manager stop
    network-manager stop/waiting
    $ sudo service network-manager start
    network-manager start/running, process 29031

Configuratie:

Selecteer het NetworkManager applet en
vervolgens
**`Edit Connections...`**

[400px](/nl/howto/bestand:vpnsec-linux-2.png/)

Klik **`Add`**,
selecteer IPsec/IKEv2 in de sectie
**`VPN`**, klik
**`Create`**

[400px](/nl/howto/bestand:vpnsec-linux-3.png/)

Voer gegevens in bij:**`Connection name`**,
**`Address`**
(**`vpnsec.science.ru.nl`**),
gebruikersnaam, etc. en zet de betreffende
vinkjes.

[400px](/nl/howto/bestand:vpnsec-linux-4.png/)

Sla op: (**`Save`** ===

Als de VPN-verbinding gemaakt is, maar `ping ns1.science.ru.nl` werkt
niet, terwijl `ping 131.174.224.4` wel werkt, dan kan de reden zijn dat
`dnsmasq` dit veroorzaakt. Dat kan opgelost worden door de DNS-cache van
dnsmasq niet te gebruiken, zie [Ask Ubuntu: DNS problem when connected
to a
VPN](http://askubuntu.com/questions/320921/having-dns-issues-when-connected-to-a-vpn-in-ubuntu-13-04):

    First make sure that there are no lines beginning with nameserver in any files in /etc/resolvconf/resolv.conf.d.
    If /etc/resolvconf/resolv.conf.d/tail is a symbolic link to target original, make it point to /dev/null.

    Second, disconnect from the VPN. Edit /etc/NetworkManager/NetworkManager.conf

    $ sudo gedit /etc/NetworkManager/NetworkManager.conf

    and comment out

    dns=dnsmasq

    (i.e., add a # so that it looks like the following)

    #dns=dnsmasq

    and then

    sudo service network-manager restart

## Installatie met NetworkManager op Debian Jessie

Helaas is het strongswan-network-manager package niet in Debian Jessie
opgenomen (en zit het op moment van schrijven ook niet in
jessie-backports). Desalniettemin lijkt een backport van de huidige
versie van het package goed te werken. De stappen om dit te bereiken
zijn bijna letterlijk overgenomen van [this
guide](http://sebastiangibb.de/debian/2015/09/19/university-cambridge-vpn-debian-jessie.html).

Allereerst moet je de debian packaging tools installeren:

    sudo apt-get install packaging-dev debian-keyring devscripts equivs

Daarna moet je het debian source package downloaden:

    dget -x http://http.debian.net/debian/pool/main/n/network-manager-strongswan/network-manager-strongswan_1.3.1-1.dsc

Installeer potentieel ontbrekende dependencies

    cd network-manager-strongswan-1.3.1/
    sudo mk-build-deps --install --remove

Voeg een revisienummer voor de backport toe:

    dch --local ~bpo80+ --distribution jessie-backports "Rebuild for jessie-backports."

Bouw het package (zonder handtekening)

    dpkg-buildpackage -us -uc

Installeer het zojuist gebouwde package:

    sudo dpkg -i ../network-manager-strongswan_1.3.1-1~bpo80+1_amd64.deb

Installeer potentieel ontbrekende dependencies, voor de zekerheid:

    sudo apt-get install -f

Installeer aanvullende plugins (met name de eap-mschapv2 plugin):

    sudo apt-get install libcharon-extra-plugins

Herstart network manager

    sudo systemctl restart network-manager.service

## Installatie zonder NetworkManager

*PS: Onderaan extra opmerkingen voor deze stappen onder
Fedora Core (FC23).*

Installeer strongswan, inclusief de curl, eap-identity, eap-mschapv2 en
eap-md5 (benodigd voor eap-mschapv2) plugins. Wanneer je strongswan zelf
compileert, zorg er dan voor dat je de benodigde plugins meegeeft aan
het **`configure`**-script.

    $ ./configure --enable-curl --enable-md5 --enable-openssl --enable-xauth-eap --enable-eap-md5 --enable-eap-gtc --enable-eap-tls --enable-eap-ttls --enable-eap-peap --enable-eap-mschapv2 --enable-eap-identify
    $ make
    $ sudo make install

Je kunt testen welke plugins geactiveerd zijn met
**`sudo ipsec statusall`** of
**`sudo ipsec listplugins`**. Indien nodig kun je
plugins handmatig laden vanuit
**`strongswan.conf`**.

Zorg ervoor dat je **`ipsec.conf`**, waarschijnlijk
te vinden in /etc of in /usr/local/etc, er als volgt uitziet:

    config setup
        strictcrlpolicy=yes

    conn %default
        ikelifetime=60m
        keylife=20m
        rekeymargin=3m
        keyingtries=1
        keyexchange=ikev2

    conn science
        left=%defaultroute
        leftfirewall=yes
        leftsourceip=%config
        leftauth=eap-mschapv2
        leftid=mysciencelogin        <-- edit this
        eap_identity=mysciencelogin  <-- edit this
        right=vpnsec.science.ru.nl
        rightauth=pubkey
        rightid=@vpnsec.science.ru.nl
        rightsubnet=0.0.0.0/0
        forceencaps=yes
        auto=start

En je **`ipsec.secrets`** als volgt, waarbij je je
eigen Science-gebruikersnaam en-wachtwoord invult. Let erop dat dit
bestand niet door iedereen te lezen moet zijn, want je wachtwoord staat
erin! Het is typisch eigendom van **`root:root`**
met **`-rw-------`** (600) permissies.

    mysciencelogin : EAP "mypassword"

Alles zou nu moeten werken:

    $ sudo ipsec start
    $ sudo ipsec up science

Het kan nodig zijn om het root-certificaat handmatig in de juiste map te
zetten:

    $ sudo ln -s /etc/ca-certificates/extracted/cadir/DigiCert_Assured_ID_Root_CA.pem [/usr/local]/etc/ipsec.d/cacerts/DigiCert_Assured_ID_Root_CA.pem

## Fedora (FC23)

Work in progress… [Gebruiker:Arjen], FC23, 4/3/2016:

Vervang op Fedora (getest met FC23) **`ipsec`**
door **`strongswan`**.

Je hebt de nieuwste SELinux policies nodig, doe daarom:

    dnf update --enablerepo=updates-testing selinux-policy

Edit `/etc/strongswan/strongswan.d/charon/resolve.conf` zodat het deze
regel bevat:

    file = /etc/resolv.conf

Als je fouten krijgt met certificaat CN=TERENA niet trusted etc., doe
dan als volgt. Ga naar
[1](https://pki.cesnet.cz/en/ch-tcs-ssl-ca-3-crt-crl.html) Download
`DigiCert_Assured_ID_Root_CA.crt` en `TERENA_SSL_CA_3.crt`.

Extraheer de `.pem` files, copieer deze naar de relevante strongswan
config directories, en reread de certificates:

    openssl x509 -inform DES -in DigiCert_Assured_ID_Root_CA.crt -out DigiCert_Assured_ID_Root_CA.pem -text
    openssl x509 -inform DES -in TERENA_SSL_CA_3.crt -out TERENA_SSL_CA_3.pem -text

    cp DigiCert_Assured_ID_Root_CA.pem /etc/strongswan/ipsec.d/cacerts
    cp TERENA_SSL_CA_3.pem /etc/strongswan/ipsec.d/certs

    strongswan rereadall

Zoals ook hierboven beschreven, houd ik problement met DNS. Stoppen van
service \`libvirtd\` lost het op, maar een echte oplossing moet ik nog
vinden.
