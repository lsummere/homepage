---
author: petervc
date: '2019-06-25T12:40:26Z'
keywords: []
ShowToc: true
lang: nl
tags:
- internet
title: Vpn linux
wiki_id: '72'
---
# PPTP VPN turned off

Use [VPNsec](/en/howto/vpnsec-linux-install/) instead of PPPT!

## Configuring a PPTP VPN connection in Linux

To configure a PPTP VPN connection in Linux you need at least the
following:

-   Read **science.ru.nl** instead of **sci.kun.nl**. Pictures are
    before the DNS rename!!!
-   An internet connection
-   A provider with GRE support (default)
-   A cabel/ADSL router (if applicable) with GRE support (default)
-   A username and password

See [1](http://pptpclient.sourceforge.net/documentation.phtml) and/or
[2](http://www.cuhk.edu.hk/itsc/network/vpn/fedora.html) for
installation instructions for the various Linux distributions.

## PPTP VPN in openSuSE 11.1

-   Install the “pptp” package using YaST
-   Become root using the “su” command
-   Create a ppp0 interface with the following command:

`/usr/sbin/pptpsetup --create radboud --server vpn-srv.science.ru.nl --username USERNAME --encrypt --start`

-   For each website that you want to access through VPN give a “route”
    command. E.g., for access to www.sciencedirect.com:

`/sbin/route add -net www.sciencedirect.com netmask 255.255.255.255 dev ppp0`

### Notes

-   The configuration file /etc/ppp/options contains by default “idle
    600”. A VPN connection is disconnected if it is idle for 600 seconds
-   Use /sbin/ifconfig to check wether the ppp0 interface exists
-   Use /usr/sbin/traceroute www.sciencedirect.com to check wether VPN
    is used for access to this site
-   To use VPN for all connections see:

<http://pptpclient.sourceforge.net/routing.phtml>

## PPTP VPN in openSuSE 11.2

-   Click on the network manager in in the systemtray and select ‘manage
    connections’.
-   Open the tab that says ‘VPN’ and click add \> pptp.
-   Fill in a connection name, for example ‘Radboud’.
-   Gateway is vpn-srv.science.ru.nl.
-   Username and password are your standard for the science.ru.nl
    domain.
-   (The IP address tab should say ‘Configure: Automatic (VPN)’.)
-   Click the ‘advanced’ button and enable ‘use MPPE encryption’.
-   Click ‘ok’ everywhere to close the menus. The vpn connection is now
    configured and should be available in the

list of the network manager in the system tray.

## PPTP VPN in openSuSE 11.4 (tested for gnome)

-   Make sure the NetworkManager is running. If there is no
    “NetworkManager notification” in the panel start it:
    -   The NetworkManager must be started by root, open a terminal and
        enter:

`sudo /usr/sbin/NetworkManager`

To start it automatically upon booting use the setup tool YaST and
select:

`System`\
`- /etc/sysconfig Editor`\
`-- Network`\
`--- General`\
`---- NETWORKMANAGER YES`

Messages from the NetworkManager can be found in
`/var/log/NetworkManager`

-   Click on the Network Manager in the panel and select

`VPN Connections`\
`- Configure VPN`

-   In the “VPN” tab click “Add”
-   Select “Point-to-Point Tunneling Protocol” and click “Create”
-   Choose a connection name, for example ‘Science Radboud’.
-   Enter “Gateway:” `vpn-srv.science.ru.nl`
-   Optionally provide “User name:” (Science account, not the one of
    your RU account).
-   You may also already provide your “Password:”.
-   Select: Use POint-to-Point encryption (MPPE)
-   Click ‘ok’
