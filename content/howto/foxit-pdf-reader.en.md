---
author: arranc
date: '2022-09-12T06:31:56Z'
keywords: []
lang: en
tags: []
title: FoxIt PDF Reader
wiki_id: '674'
---
The program [FoxIt PDF
Reader](http://www.foxitsoftware.com/pdf/rd_intro.php) can be used as an
alternative to Adobe Reader, it is a free program to view or print PDF
files. FoxIt can be found on the [S disk](/en/howto/s-schijf/) in the
directory “FoxIt-PDF-Reader”, which also contains a user manual in PDF
format.

One reason to sometimes not use Adobe Reader is that Adobe Reader 8
produces PostScript that the HP4250/HP4350 printers can’t handle, not
even after a firmware upgrade. Therefore C&CZ switched to using the
(slower) PCL-driver for almost all HP4250/HP4350 printers.

Some users may experience some errors with FoxIt if it’s set as the
default PDF reader on computers other than Managed PC’s. If a Network
Drive hasn’t been made to the S-Disk, or if it hasn’t been logged into
yet, you will receive an error. To solve this, in the case of the
missing Network Drive, make one to the S-Disk and open it. Otherwise,
open it and login.
