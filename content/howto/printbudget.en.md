---
author: petervc
date: '2016-08-17T11:34:41Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
- contactpersonen
title: Printbudget
wiki_id: '33'
---
To use the [C&CZ printers](/en/howto/printers-en-printen/) the loginname
must be present in a budgetgroup for the specific group of printers.
Next to that, the budget must be sufficient to print at least 1 page.
The budget is automatically being lowered by the costs of print jobs. On
the [Do It Yourself website](http://diy.science.ru.nl) one can choose
the default budget to print from. There one can also view the current
amounts of all budgets that can be used and view a summary of the last
twenty print jobs.

To start a new budgetgroup, one has to stop by C&CZ [system
administration](/en/howto/overcncz-wieiswie-systeembeheer/).

C&CZ sends an email when the printbudget has become too low. For a
personal budget the [mail](/en/howto/emailcodes/) is sent to the personal
email address. For a group budget the [mail](/en/howto/emailcodes/) is
sent to the owners of the budget group. Send a mail to postmaster if you
want to be warned earlier. Please include the name of the budgetgroup
and the value of the printbudget at which you want to receive a mail.

Putting money on an existing budget group can be done in a few ways:

-   for groups with a budget number, by sending mail to
    postmaster\@science.ru.nl mentioning the amount, the budget number
    and the budget group.
-   if necessary with cash at C&CZ [system
    administration](/en/howto/overcncz-wieiswie-systeembeheer/).
