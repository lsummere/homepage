---
author: polman
date: '2009-05-08T13:40:04Z'
keywords: []
lang: en
tags:
- internet
- wiki
title: Mediawiki Extensie Installeren
wiki_id: '688'
---
Installing a MediaWiki extension

Checkout the extension Extension-name by
way of:

     svn co http://svn.wikimedia.org/svnroot/mediawiki/trunk/extensions/Extension-name 

Copy the transferred files
to the extensions directory

Remember to copy a .htaccess file with RewriteEngine Off into the new
directory
