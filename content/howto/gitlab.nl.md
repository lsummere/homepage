---
author: bram
date: '2021-12-03T16:33:36Z'
keywords: []
tags: [ science login, gitlab ]
title: GitLab
wiki_id: '945'
cover:
  image: img/2021/gitlab.png
---
## Over GitLab

Quote van de [GitLab website](https://about.gitlab.com/features/)

> GitLab is an incredibly powerful open source code collaboration
> platform, git repository manager, issue tracker and code reviewer.

Onze GitLab dienst is direct vanaf internet beschikbaar. Je hebt geen [VPN](/nl/howto/vpn)
 verbinding nodig om het te gebruiken.


## Inloggen

Navigeer naar:

<http://gitlab.science.ru.nl>

Daar zie je twee manieren om in te loggen:

- **Science login** - voor studenten en medewerkers van de Science Faculteit. Gebruik hiervoor je [Science login](/en/howto/login/).
- **Standard** - voor externen.

## Science login

Hoewel iedereen met een [Science Login](/en/howto/login/) op GitLab kan inloggen, kun je in GitLab alleen personen 'zien' die al een keer zijn ingelogd. Houd hier rekening mee als je projectleden in GitLab aan een project toevoegt.

## Externe gebruikers

Omdat bij veel projecten ook niet RU-medewerkers of studenten betrokken zijn, is het mogelijk om 
voor deze personen een GitLab-login aan te vragen. Dit kan per mail naar [Postmaster](/nl/howto/contact).
Vermeld hierbij de volgende details:

| item         | beschrijving                                                                                                                                                                       |
| :----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| login naam   | We laten deze loginnaam met een underscore `_` beginnen om geen conflict met bestaande of toekomstrige science loginnamen te krijgen.                                              |
| Naam         | Voor en Achternaam van de gebruiker. Onder deze naam kun je de gebruiker in GitLab terug vinden                                                                                    |
| Email addres | GitLab gebruikt dit adres om een geautomatiseerde mail te versturen naar de nieuwe gebruiker. In deze mail staan verdere instructies om een eigen wachtwoord in te kunnen stellen. |


Zodra [Postmaster](/en/howto/contact) het account heeft aangemaakt, kun je het deze aan projecten toevoegen. Externe gebruikers hebbne standaard niet de mogelijkheid om zelf projecten aan te maken. Mocht dat nodig zijn, dan is dat in overleg mogelijk.

## Git en grote bestanden

GitLab heeft LFS (Large File Storage) aan staan. Git LFS maakt het mogelijk
om grote bestanden onder versiebeheer te hebben, zonder dat ze in de git-repository
zijn opgeslagen. Kijk in de handleiding op de [Git LFS project website](https://git-lfs.github.com/) voor meer informatie.

## Documentatie

- [Git documentatie](http://git-scm.com/documentation)
- [Algemene GitLab documentatie](http://doc.gitlab.com/)

## Changelog
- 2022-10-17 feature-flags voor de [Debian API](https://docs.gitlab.com/ee/user/packages/debian_repository/#enable-the-debian-api) aangezet.
- 2021-12-03 [GitLab pages](/nl/news/2021-12-07_gitlab-pages-installed) staat nu aan.
- 2021-10-21 de Docker Container Registry aangezet. Zie de officiele [GitLab
 documentatie](https://gitlab.science.ru.nl/help/user/packages/container_registry/index.md) voor meer info.
- 2020-06-26 `gitlab.pep.cs.ru.nl` is verhuisd naar een nieuwe Ubuntu 20.04 server. Dit zijn de ssh fingerprints voor `gitlab.pep.cs.ru.nl`:

``` text
256 SHA256:bVYa+loFqV0Hdv3sPukp40zf1LdQd4PNBOSJa6oRnyE root@gitlab9 (ECDSA)
256 SHA256:j4pDiX2SSKGHjoZ1OQXgrKpIvLxrCnckZCtYWapHFcM root@gitlab9 (ED25519)
3072 SHA256:QIdVcGdaEhyYXcUVtc2FIa1R9F/1KUekR+7IV23c0LI root@gitlab9 (RSA)
```

- 2019-01-25 GitLab geupdate naar versie 11.7.0 en tevens naar een nieuwe Ubuntu 18.04 verhuisd. Je kunt een melding krijgen bij het het pushen of pullen van code. De huidige ssh-fingerprints voor `gitlab.science.ru.nl` zijn:

``` text
SHA256:7G/4WD/tUE8H0k9MkBznlcx+ZWgUxnn3KazKm1XQwRk root@gitlab (ED25519)
SHA256:NgZ9HciXlwG5JNPSFbem7bBbDbhqaNAO7JHag8rwi/I root@gitlab (RSA)
SHA256:BSSXi19WgCSpZu5AQkUTtwm+5zAMioQJpFE3oxBrIMQ root@gitlab (ECDSA)
```

- 2016-09-13 Reply-mail is ingesteld. Het is mogelijk om op door gitlab-verstuurde notificaties te antwoorden.
- 2016-07-18 [GitLab LFS](/en/howto/gitlab#git-and-managing-large-files/) (Large File Storage) aangezet.

## Onderhoudsvenster

We voeren GitLab updates gwoonlijk uit op vrijdag ochtenden Tijdens de upgrade kan het zijn dat GitLab even onbereikbaar is.
