---
author: wim
date: '2008-11-13T11:41:57Z'
keywords: []
lang: en
tags:
- internet
title: Vpn windows98
wiki_id: '74'
---
## Het opzetten van een PPTP VPN in Windows 95/98/98SE

Om een PPTP vpn op te zetten in Windows 95/98/98SE zijn een aantal
onderdelen nodig. U moet minimaal beschikken over:

-   Lees **science.ru.nl** ipv **sci.kun.nl**. Plaatjes zijn gemaakt
    voor de DNS rename!!!
-   Een internet connectie
-   Een provider met GRE ondersteuning (standaard)
-   Een kabel/ADSL router (mits van toepassing) met GRE ondersteuning
    (standaard)
-   Een gebruikersnaam en wachtwoord
-   De update voor de Windows versie

Deze Procedure is onder te verdelen in 3 onderdelen:

-   [Installatie](#installatie)
-   [Configuratie](#configuratie)
-   [Vragen en problemen](/en/howto/vpn-windowsxp#vragen-en-problemen/)

### Installatie

Eerst opent u het configuratiescherm. Bij een standaard installatie is
die te vinden onder:

**Start** -\> **Settings** -\> **Control Panel**. Selecteer **Add/Remove
Programs**.

Klik het tabblad **Windows setup**. Selecteer **Communication** en
vervolgens **Details**.

Selecteer **Dail-Up Networking** en **Virtual Private Networking**. Klik
**OK** en vervolgens nog een keer **OK**.

Herstart de computer eventueel als hierom gevraagd wordt. Installeer een
van de benodigde updates voor

[Windows95](http://download.microsoft.com/download/win95/Update/17648/W95/EN-US/dun14-95.exe),
[Windows98](http://download.microsoft.com/download/win98/Update/17648/W98/EN-US/dun14-98.exe)
of
[windows98SE](http://download.microsoft.com/download/win98SE/Update/17648/W98/EN-US/dun14-SE.exe).

Als deze voltooid dient u de computer opnieuw op te starten.

### Configuratie

Dubbelklik **My Computer**. Vervolgens dubbelklik op **Dail-Up
Networking** icoon. In het geval dat dit de eerste keer is zal er gelijk
een nieuwe verbinding worden gemaakt. Zo niet dan dient u te
dubbelklikken op **Make New Connection**.

{{< figure src="/img/old/pptp\_win9x-1.gif" title="pptp\_win9x-1.gif" >}}

Vervolgens klikt u **Next**. Geef een toepasselijke naam voor de VPN
verbinding. Bijvoorbeeld **B-FAC VPN connectie**. Selecteer als device
de Microsoft VPN Adapter.

{{< figure src="/img/old/pptp\_win9x-2.gif" title="pptp\_win9x-2.gif" >}}

Klik op **Next**. Vul nu de hostname of het IP adres van de VPN server
in.

{{< figure src="/img/old/pptp\_win9x-3.gif" title="pptp\_win9x-3.gif" >}}

Klik op **Next** en vervolgens **Finish**. Dubbelklik nu **My Computer**
weer en selecteer **Dail-up Networking**.Selecteer nu de connectie die u
zojuist heeft aangemaakt. Vervolgens selecteert u **File** -\>
**Properties**. Het volgende scherm zou nu moeten verschijnen.

{{< figure src="/img/old/pptp\_win9x-4.gif" title="pptp\_win9x-4.gif" >}}

Klik nu op het tabblad **Server Types**. De volgende opties moeten hier
aan staan.

-   Enable sofware compression
-   Required encrypted password
-   Require data encryption

Alle andere opties moet uit staan.

{{< figure src="/img/old/pptp\_win9x-5.gif" title="pptp\_win9x-5.gif" >}}

Klik nu op **TCP/IP Settings**. Vink de optie **Use default gateway on
remote network** uit wanneer u alleen verbinding maakt naar de
universiteit. Maakt u echter verbinding naar niet universitaire sites,
waarvoor het belangrijk is dat de verbinding vanuit het universitaire
netwerk lijkt te komen, dan moet deze optie juist aan staan!

{{< figure src="/img/old/pptp\_win9x-6.gif" title="pptp\_win9x-6.gif" >}}

Sluit alle vensters af met **OK**. Nu dubbelklik op de verbinding.

{{< figure src="/img/old/pptp\_win9x-7.gif" title="pptp\_win9x-7.gif" >}}

Vul hier u login gegevens in en klik op connect. Binnen een minuut zou
de verbinding tot stand moeten komen.
