---
author: petervc
date: '2022-11-02T09:49:45+01:00'
keywords: []
lang: en
tags:
- overcncz
title: Systeemontwikkeling
wiki_id: '132'
---
## System Development and Administration

System development and administration is located in room HG03.055.

## Who are they?

-   {{< author "remcoa" >}}\
    {{< author "petervc" >}}\
    {{< author "bram" >}}\
    {{< author "wim" >}}\
    {{< author "ericl" >}}\
    {{< author "fmelssen" >}}\
    {{< author "simon" >}}\
    {{< author "polman" >}}\
    {{< author "visser" >}}\
    {{< author "miek" >}}\
    {{< author "alexander" >}}

## What do they do?

System Administration takes care of

-   Management of the [servers of the faculty and of various departments](/en/howto/hardware-servers/).
-   Back-up and restore of the data on the servers.
-   Management of work places in the departments and in the [PC-rooms and publicly available working places](/en/howto/terminalkamers/):
    Managed PC’s with MS-Windows and/or Ubuntu Linux.

For all other computers (hundreds of MS-Windows PCs and tens of Linux
PCs and MACs, not managed by C&CZ) it is understood that the owner
(department) takes care of the systems management. For these systems,
C&CZ system administration can try to answer questions.

-   Support for education renewal and digital provision of information.
-   Internet-number administration (*nameserver*, DNS/DHCP) for the
    Faculty of Science.
