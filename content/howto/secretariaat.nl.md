---
author: petervc
date: '2022-06-28T11:28:42Z'
keywords: []
lang: nl
tags:
- overcncz
title: Secretariaat
wiki_id: '534'
---
## Wie zijn zij?

-   {{< author "alinssen" >}}

## Wat doet zij?

Het secretariaat zorgt voor:

-   Afdelings administratie.
-   Functioneel beheer van het tijdregistratiesysteem.
-   Licenties [Matlab en toolboxen](/nl/howto/matlab/).
-   Licenties [Maple](/nl/howto/maple/), Labview,
    [Mathematica](/nl/howto/mathematica/).
