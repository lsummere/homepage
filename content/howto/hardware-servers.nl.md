---
author: petervc
date: '2022-06-28T15:15:31Z'
keywords: []
lang: nl
tags:
- hardware
title: Hardware servers
wiki_id: '51'
---
## Servers en Werkplekken

C&CZ beheert ca. 900 Linux workstations en servers, en ca. 900 servers
en werkplekken met MS-Windows, voornamelijk in het domein
`science.ru.nl`. Dual boot machines (Linux/Windows) worden hierbij
dubbel geteld. De ca. 350 servers zijn verdeeld over drie datacenters,
ca. 150 hebben een snelle netwerkaansluiting (25 Gb/s).

### Werkplekken

C&CZ beheert voor afdelingen werkplekken met Ubuntu Linux of MS-Windows.
Ook zijn er in de PC-kamers algemeen toegankelijke werkplekken waarvan
iedereen met een [Science login](/nl/howto/studenten-login/) gebruik kan
maken, maar ook alle RU-medewerkers en RU-studenten en gasten met een
tijdelijke login. Vrijwel alles is dual-boot, de gebruiker kan kiezen
uit Ubuntu Linux en MS-Windows.

### Linux loginservers

Er zijn enkele [secure shell](/nl/howto/ssh/) servers beschikbaar voor
studenten en medewerkers van FNWI (iedereen met een [Science
account](/nl/howto/login/)).

Deze zijn voor algemeen gebruik: iedereen mag ze gebruiken zoals men
wil, \*zolang het geen overlast voor anderen oplevert\*.

De standaard loginserver is:

`lilo.science.ru.nl`

De naam van de alternatieve loginserver is:

`stitch.science.ru.nl`

Op dit moment wijst lilo naar een machine met de naam lilo7 (Ubuntu
20.04). Stitch wijst naar lilo6 (Ubuntu 18.04).

De ssh vingerafdrukken van de lilo’s zijn:

`1024 SHA256:JVE0FA1fFGtzbuqP1D6cW8V3fkmqxdCL5H3xRMsiJxc   lilo6 (DSA)`\
`2048 SHA256:c+kPQDMuzf+1uKIJt8RIFOUume0Z1g2UjUOpxXXyCuY   lilo6 (RSA)`\
` 256 SHA256:dRvKM7qcQF8wGZZofYT3/JbSGUXA3l3nadzzqrHvWhE   lilo6 (ECDSA)`\
` 256 SHA256:5PBNahtMibQTmjQgDlBM4OVEJ4fo9vJv/4JTJ1XfXB8   lilo6 (ED25519)`\
`1024 MD5:e7:2c:39:51:18:7a:17:26:c8:55:a1:f8:61:5d:fa:d8  lilo6 (DSA)`\
`2048 MD5:f9:8a:5f:16:7b:2b:6c:42:26:78:c5:bc:a3:94:ed:33  lilo6 (RSA)`\
` 256 MD5:d6:2a:8b:7a:7d:ab:f6:4b:99:5c:9f:be:1e:98:ae:0e  lilo6 (ECDSA)`\
` 256 MD5:39:6c:c9:da:a6:3b:3b:48:0e:a0:26:b5:7a:ea:a6:89  lilo6 (ED25519)`

` 256  SHA256:si3g2elo5m6TShx3PjX0+vF50pZ8NK/iXz/ESB+ZeP0  lilo7 (ECDSA)`\
` 256  SHA256:wHypnsKnhOMcagDli8xSxp2AnRYXAIfs+EVNB1/LdiA  lilo7 (ED25519)`\
`3072  SHA256:Ce6uHmTO4693gFgFzSROlkSY2urZLpKm2i2Ibc9tQ6A  lilo7 (RSA)`\
` 256  MD5:61:aa:8b:f7:d8:fb:a0:41:a9:ab:3b:33:89:7a:71:fd lilo7 (ECDSA)`\
` 256  MD5:47:0d:11:c8:65:4e:40:24:14:c0:6f:86:55:79:56:dc lilo7 (ED25519)`\
`3072  MD5:2c:54:2a:7b:74:3b:8a:87:ba:80:b8:76:13:c2:eb:d4 lilo7 (RSA)`

` 256  SHA256:yVihq2mnLULOCBFdvO2RWBps33hdE6PmaIGeBXX44wY  lilo8 (ECDSA)`\
` 256  SHA256:gsvoEP5djMC5GsXJJUYktYq3TjRtNQPKo0/psULHBYc  lilo8 (ED25519)`

Om te voorkomen dat je bij iedere wisseling van hardware of wijziging
van het lilo of stitch alias, je te maken krijgt met de ssh waarschuwing

`@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`\
`@       WARNING: POSSIBLE DNS SPOOFING DETECTED!          @`\
`@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`\
`The ECDSA host key for lilo.science.ru.nl has changed`

kun je in je lokale .ssh/known\_hosts file een regel opnemen waarmee je
in 1x alle door ons getekende host keys vertrouwd.

`@cert-authority *.science.ru.nl ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHpJveyOrLKFRDsbiW/29OadbCbkmUaIXnWbhVwtytbpftAc7Stj2RYa8yBmgfdm82T/UBVu1tLbeeCYQI8UlCvbAALMx+I60ux+iEGVdDBgIOjeu6LuY12pksVlXy/nKc59+m3AdMXfGHA8cI/O8eFosQLJ+dck7SBcvTT4lPhEcSQxg==`

##### ISP’s met beperkte Internet-toegang

Als je achter een firewall zit die geen uitgaand verkeer op de standaard
ssh poort 22 toestaat, dan is ook poort 80 en 443 voor ssh naar
lilo/stitch te gebruiken, omdat ze ook op die poorten een verbinding
toestaan.

### Reken-servers/clusters

C&CZ beheert een aantal rekenclusters, waarbij afdelingen hun eigen
compute nodes aangeschaft hebben. In een enkel geval heeft een afdeling
(Astrofysica) een apart cluster gemaakt door ook hun eigen headnode
(master) aan te schaffen. Het grootste `cn`-cluster heef ook een klein
aantal door C&CZ aangeschafte nodes, waar FNWI-afdelingen na overleg met
C&CZ gebruik van kunnen maken. Voor afdelingen van FNWI is de housing en
het beheer van clusternodes gratis. Het `cn`-cluster is heterogeen. Alle
nodes gebruiken [SLURM](/nl/howto/slurm/) als clustersoftware, een deel
draait nog ubuntu 18.04, een deel ubuntu 20.04. Per februari 2022 telde
het cn-cluster 123 nodes met 4700 cores en 30 TB RAM. Diverse
clusternodes hebben een GPU met CUDA cores. De [CPU-load van de
cn-clusternodes](https://cricket.science.ru.nl/grapher.cgi?target=%2Fclusternodes%2Foverview)
wordt automatisch bijgehouden.

Het coma-cluster van Sterrenkunde met (per februari 2022) 1048 cores,
8780 GB RAM, 600 TB fileserver storage, GPU’s is een apart cluster. Voor
de RU-afdeling [Centre for Language Studies](http://www.ru.nl/cls/)
wordt een apart rekenclusters beheerd.

Soms worden ook de PC’s in de
[terminalkamers](/nl/howto/sleutelprocedure-terminalkamers/) buiten de
openingstijden gebruikt als rekencluster.

### Mail-server

Op welke mail-server de mail voor een bepaalde gebruiker afgeleverd
wordt (en dus te lezen is), kan die gebruiker zijn op de [Doe-Het-Zelf
website](http://dhz.science.ru.nl/). Mail is toegankelijk via IMAP over
SSL. POP3 wordt ondersteund, maar niet aangeraden. De alias
`smtp.science.ru.nl` wijst naar de machine waar mail afgeleverd kan
worden met het SMTP-protocol. *N.B.*: Wanneer men via een andere
(niet-RU) Internet-provider aangesloten is, zal onze mailserver alleen
mail voor onze (RU) gebruikers accepteren en veel andere mail weigeren
(“Relaying Denied!”). Men kan dit voorkomen door de smtp-server van de
Internet-provider te gebruiken of door gebruik te maken van
[Authenticated SMTP](/nl/howto/email-authsmtp/).

### WWW-servers

Er zijn verschillende WWW-servers met [websites](/nl/howto/website/) en
web-applicaties. Een beschrijving van de [setup van de Apache
webservers](/nl/howto/apachesetup/) is beschikbaar.

### GitLab- servers

De [GitLab](/nl/howto/gitlab/) service maakt het mogelijk om gezamenlijk
broncode te ontwikkelen, git repositories te beheren, issues te traken
en broncode te reviewen.

De ssh vingerafdrukken van gitlab zijn:

` 256 SHA256:BSSXi19WgCSpZu5AQkUTtwm+5zAMioQJpFE3oxBrIMQ root@gitlab (ECDSA)`\
` 256 SHA256:7G/4WD/tUE8H0k9MkBznlcx+ZWgUxnn3KazKm1XQwRk root@gitlab (ED25519)`\
`2048 SHA256:NgZ9HciXlwG5JNPSFbem7bBbDbhqaNAO7JHag8rwi/I root@gitlab (RSA)`

gitlab.pep.cs.ru.nl fingerprints:

` 256 SHA256:pBFiBobAQTozB0xeeMCp3Du2PFtX7YIXw6XRFJSAGH0 root@gitlab (ECDSA)`\
` 256 SHA256:iWfwGvGZ/hdFImBzzG5Bu/y5gqhUkLOdSTYmzl1+k2w root@gitlab (ED25519)`\
`2048 SHA256:lWWFUfhwODG0bBuQJUR8ymlVfqpNHrXk0xbFAoDpTIg root@gitlab (RSA)`

### Printer-server

Een server is betrokken bij het aansturen van printers. Zie de
[printerpagina](/nl/howto/hardware-printers/) om te zien welke server
geadviseerd wordt in welke situatie.

### Disk-servers

Deze servers delen verschillende typen files uit zoals home-directories,
applicatie-software en schijven voor afdelingen en groepen. Alle moderne
servers hebben alleen maar redundante disks, soms door “mirroring”, soms
door een “RAID-array”. Dat betekent dat uitval van 1 enkele schijf geen
overlast voor gebruikers veroorzaakt.

Zie voor mee informatie, bijvoorbeeld over [huren van
schijfruimte](/nl/howto/diskruimte/) door afdelingen, de
[diskruimte](/nl/howto/diskruimte/)-pagina.

### VPN-server

Om vanaf overal op het Internet toegang te hebben tot servers en
services van het campusnetwerk is er een [VPN](/nl/howto/vpn/)-service.

### FTP-server

Om het aanbieden van grote bestanden aan anderen eenvoudig te maken, is
er een FTP-service. De service is zowel toegankelijk via [het originele
FTP-protocol](ftp://ftp.science.ru.nl/pub) als via [het HTTPS-protocol
van het web](https://ftp.science.ru.nl). Iedereen met een
Science-account die [via postmaster](/nl/howto/contact/) een map
(directory) op deze server heeft gekregen, kan op verschillende manieren
daar bestanden naartoe kopiëren:

-   via een loginserver als `lilo` op
    `/vol/ftp/pub/`*uweigenmap*.
-   door de netwerkschijf aan te koppelen: `\\ftp.science.ru.nl\ftp`

Door het gebruik van een onleesbare bovenliggende map met een geheime
map daaronder, kan men ook enigszins gevoelige data overdragen.

### License-servers

Voor allerlei software pakketten is er een license server, die het
mogelijk maakt om op alle werkplekken de software te gebruiken, waarbij
het totale gebruik binnen het totaal aantal gekochte licenties blijft.
Veel van deze licenties werken op basis van
[FlexNet](http://www.flexerasoftware.com/producer/products/software-monetization/flexnet-licensing/)
van Flexera.

### Backup-servers

Van vrijwel alle schijven die onder beheer van C&CZ staan, worden
regelmatig [backups](/nl/howto/backup/) gemaakt, zodat bij kleine of
grote calamiteiten de data niet verloren gaan.

