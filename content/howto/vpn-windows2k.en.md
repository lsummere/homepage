---
author: wim
date: '2008-11-13T11:47:44Z'
keywords: []
lang: en
tags:
- internet
title: Vpn windows2k
wiki_id: '73'
---
## Configuring a PPTP VPN connection in Windows 2000

Om een PPTP VPN op te zetten in Windows 2000 zijn een aantal onderdelen
nodig. U moet minimaal beschikken over:

-   Lees **science.ru.nl** ipv **sci.kun.nl**. Plaatjes zijn gemaakt
    voor de DNS rename!!!
-   Een internet connectie
-   Een provider met GRE ondersteuning (standaard)
-   Een kabel/ADSL router (mits van toepassing) met GRE ondersteuning
    (standaard)
-   Een gebruikersnaam en wachtwoord

Deze Procedure is onder te verdelen in 3 onderdelen:

-   [Installatie](#installatie)
-   [Configuratie](#configuratie)
-   [Vragen en Problemen](#vragen_en_problemen)

### Installatie

Eerst opent u het configuratiescherm. Bij een standaard installatie is
dit te vinden onder:

**Start** -\> **Settings / Instellingen** -\> **Control Panel /
Configuratiescherm**

Dubbelkik op **Network and Dial-Up Connections / Netwerk- en
inbelverbindingen**.

Klik op **Make New Connection / Nieuwe verbinding maken**. Het volgende
scherm zal verschijnen.

{{< figure src="/img/old/pptp\_eng\_1.jpg" title="pptp\_eng\_1.jpg" >}}

Vervolgens klikt u **Next / Volgende**. Uit het volgende menu kiest u
**Connect to a private network through Internet / Verbinding maken met
een particulier netwerk via het Internet**.

{{< figure src="/img/old/pptp\_eng\_2.jpg" title="pptp\_eng\_2.jpg" >}}

Klikt op **Next / Volgende**. In het volgende scherm kan worden gekozen
om eerst een andere connectie tot stand te brengen. Dit is nodig indien
u dient in te bellen. Dit kan voor ADSL nodig zijn bij sommige providers

{{< figure src="/img/old/pptp\_eng\_3.jpg" title="pptp\_eng\_3.jpg" >}}

Klik op **Next / Volgende**. Vervolgens moeten we de VPN server ingeven.
Dit kan een IP adres of een hostname zijn.

{{< figure src="/img/old/pptp\_eng\_4.jpg" title="pptp\_eng\_4.jpg" >}}

Klik **Next / Volgende** om verder te gaan. Hierna kan een keuze worden
gemaakt of het voor alle gebruikers is of alleen voor de ingelogde
gebruiker

{{< figure src="/img/old/pptp\_eng\_5.jpg" title="pptp\_eng\_5.jpg" >}}

Vervolgens klikt u weer **Next / Volgende**. In het laatste scherm kunt
u de connectie een naam geven. Geef dit een passende naam zoals **B-FAC
vpn connection**. Ook is het aan te bevelen de optie **Add shortcut to
my desktop** aan te vinken zodat in het vervolg makkelijk de connectie
gemaakt kan worden

{{< figure src="/img/old/pptp\_eng\_6.jpg" title="pptp\_eng\_6.jpg" >}}

Klik nu **Finish / Voltooien** om de installatie te voltooien. Nu klikt
u op het icoon op u desktop met de naar die u aan de VPN connectie heeft
gegeven. Als u geen snelkoppeling op u desktop heeft staan kunt u de
connectie benaderen via: **Start** -\> **Settings / Instellingen** -\>
**Control Panel / Configuratiescherm**, **Network and dail-up
connections / Netwerk- en inbelverbindingen** en vervolgens de vpn
verbinding kiezen. Het volgende scherm zal zich openen.

{{< figure src="/img/old/pptp\_eng\_7.jpg" title="pptp\_eng\_7.jpg" >}}

klik op opties om de **Properties / Eigenschappen** van de verbinding te
bekijken. Vervolgens klikt u op het tabblad **Security**. In dit tabblad
zou het volgende aan moeten staan :

-   Typical (Recommended settings)
-   Require secured password
-   Requite Data Encryption

{{< figure src="/img/old/pptp\_eng\_8.jpg" title="pptp\_eng\_8.jpg" >}}

Vervolgens gaat u naar het tabblad **Networking**. Zet hier het type op
PPTP.

{{< figure src="/img/old/pptp\_eng\_9.jpg" title="pptp\_eng\_9.jpg" >}}

Vervolgens dubbelklikt u op **Internet Protocol(TCP/IP)** en vervolgens
**Advanced**. In het tabblad **General** vinkt u de optie **Use default
gateway on remote network** uit wanneer u alleen verbinding maakt naar
de universiteit. Maakt u echter verbinding naar niet universitaire
sites, waarvoor het belangrijk is dat de verbinding vanuit het
universitaire netwerk lijkt te komen, dan moet deze optie juist aan
staan!

{{< figure src="/img/old/pptp\_eng\_10.jpg" title="pptp\_eng\_10.jpg" >}}

{{< figure src="/img/old/pptp\_eng\_11.jpg" title="pptp\_eng\_11.jpg" >}}

Vervolgens de vensters sluiten met **OK** tot u weer bij het volgende
scherm komt.

{{< figure src="/img/old/pptp\_eng\_7.jpg" title="pptp\_eng\_7.jpg" >}}

Vul vervolgens u gebruikersnaam en wachtwoord in en klikt u **Connect**.
In het geval dat alles correct is zal naar enkele seconde de verbinding
tot stand gebracht zijn.

### Vragen en problemen

**Vraag**: Ik krijg een foutmelding over “Your credentials have failed
remote network authentication”.

**Antwoord**:

-   U gebruikersnaam of wachtwoord is onjuist. Neem contact op met
    systeembeheer.
-   U bevind zich in een domein en het domein wordt toegevoegd aan u
    gebruikersnaam. Kijk of u het domein kunt verwijderen of zonodig
    neem contact op met systeembeheer voor een aanpassing.

**Vraag**: Error 734: The PPP link control protocol was terminated.

**Antwoord**: U gebruikt mogelijk een te lage of geen encryptie op de
verbinding. Zie de installatieprocedure voor de correcte instellingen.

Vraag: Error 769: The specified destination is not reachable. Antwoord:
De hostname of het IP adres van de machine waar nu naar connect zijn
onjuist. Controleer de instellingen en indien nodig neem contact op met
het systeembeheer.

**Vraag**: Error 678: There was no answer.

**Antwoord**: de hostname of het IP addres zijn onjuist, Controleer de
instellingen.

**Vraag**: Error 619: the specified port is not connected.

**Antwoord**:

1.  Er is iets fout gegaan met de connectie, herstart windows en probeer
    het opnieuw.
2.  U draait een firewall die GRE pakketten blokkeert.
3.  Uw Service provider blokkeert GRE pakketten.
4.  Uw ADSL/Kabel router blokkeert GRE pakketten.

**Vraag**: Error 741: The local computer does not support the required
data encryption type.

**Antwoord**: Er wordt gebruikt gemaakt van een andere methode als dat
de server toestaat. Controleer instelling.

**Vraag**: Als ik de VPN connectie start werkt mijn internet niet meer.

**Antwoord**: U heeft de gateway naar het remote netwerk aan laten
staan. Controleer de instelling.

**Vraag**: Soms wordt mijn verbinding verbroken.

**Antwoord**: Raadpleeg systeembeheer, het is mogelijk dat iemand anders
gebruik maakt van u account.

**Vraag**: Werkt PPTP ook over een NAT verbinding ?

**Antwoord**: Ja, echter in sommige gevallen kan het zo zijn dat de
router geen GRE pakketten doorlaat. Er zijn situaties waarbij het niet
mogelijk is met meer dan een PC te connected als NAT wordt gebruikt.

**Vraag**: Werkt Xwin32 over de VPN verbinding ?

**Antwoord**: Het is mogelijk om Xwin32 te gebruiken. Het is te
gebruiken alsof het op het normale B-FAC netwerk is. Echter moet wel
rekening worden gehouden met de snelheid van de verbinding die vaak vele
malen langzamer is.

**Vraag**: Er gebeuren vreemde dingen op mijn computer, zijn er logs van
connecties ?

*’Antwoord*: Ja, er worden logs bijgehouden van welke connecties er naar
de VPN tunnels gaan. Mocht er iets vreemd aan de hand zijn dan kunt u
contact opnemen met systeembeheer. Zorg wel dat u over voldoende
gegevens beschikt zoals : tijdstip, gebruikersnaam, IP adres van u
verbinding. Tevens worden er logs bijgehouden van de connecties van de
VPN naar het KUN netwerk. Het is niet mogelijk om vanuit buiten het KUN
Netwerk de VPN verbinding te benaderen en visaversa.

**Vraag**: Kan ik vanuit het KUN netwerk mijn computer thuis benaderen ?

**Antwoord**: Ja, mits u het IP adres of domeinnaam weet die u is
toegewezen.
