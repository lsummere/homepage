---
author: remcoa
date: '2022-10-06T15:12:05Z'
keywords: []
lang: nl
tags:
- internet
title: Vpn mac
wiki_id: '805'
---
# PPTP VPN is now turned off!

as of december 1st, pptp vpn is no longer available.

## OUD: Het opzetten van een PPTP VPN connectie in OS X

Informatie over de nieuwe [Science VPN](/nl/howto/vpn/) staat op een
andere plaats, hieronder staat alleen informatie over hoe een VPN
connectie naar de \*\*OUDE\*\* Science VPN server wordt geconfigureerd
en opgezet onder OS X 10.6 (Snow Leopard). Onder OS X 10.5 (Leopard) is
de procedure vrijwel identiek. Onder OS X 10.4 (Tiger) moet een aparte
VPN applicatie (standaard beschikbaar in de folder Toepassingen) worden
gebruikt.

-   Open Systeemvoorkeuren;
-   Kies “Netwerk” configuratie;
-   Klik op de + linksonder om een nieuw interface te definieren;

{{< figure src="/img/old/vpn-2-define-interface.png" title="VPN-2-Define-Interface.png" >}}

-   Kies Interface: VPN, VPN-type: PPTP;
-   Kies een geschikte naam voor de nieuwe voorziening, b.v. “Science
    VPN”;
-   Klik “Maak aan” en keer terug naar het Netwerk configuratiescherm;
-   In de interface-lijst links staat een nieuw VPN interface, selecteer
    deze;

{{< figure src="/img/old/vpn-3-configure.png" title="VPN-3-Configure.png" >}}

-   Vul het VPN serveradres in: vpn.science.ru.nl;
-   Vul de Science accountnaam in;
-   Vink “Toon VPN-status in menubalk” aan;
-   Druk op “Pas toe” om de wijzigingen op te slaan;

{{< figure src="/img/old/vpn-4-connect.png" title="VPN-4-Connect.png" >}}

-   Start de VPN connectie via het VPN icoon in de menubalk bovenaan het
    scherm;

{{< figure src="/img/old/vpn-5-authenticate.png" title="VPN-5-Authenticate.png" >}}

-   Geef het wachtwoord in het pop-up authenticatie-scherm;

{{< figure src="/img/old/vpn-6-connected.png" title="VPN-6-Connected.png" >}}

-   Het Netwerk configuratie-scherm laat zien dat de VPN verbinding
    actief is.

## Troubleshooting

Als de VPN verbinding niet tot stand komt, zijn mogelijke oorzaken:

-   Er is geen werkende Internet-verbinding, controleer of browsen
    werkt.
-   De VPN-connectie is niet goed geconfigureerd, controleer alle
    instellingen.
-   De science loginnaam / wachtwoord combinatie is onjuist, probeer in
    te loggen op [dhz.science.ru.nl](https://dhz.science.ru.nl/) om ze
    te controleren.
-   De VPN server is tijdelijk niet beschikbaar, vraag na bij C&CZ.
-   De provider laat geen GRE-verkeer toe (onwaarschijnlijk).
-   De kabel/ADSL router ondersteunt geen GRE-verkeer
    (onwaarschijnlijk).
