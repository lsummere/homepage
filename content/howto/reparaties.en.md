---
author: petervc
date: '2022-06-28T14:59:09Z'
keywords: []
lang: en
tags:
- storingen
- hardware
- contactpersonen
title: Reparaties
wiki_id: '103'
---
The repair procedure depends on the firm that sold the machine. Whether
the repair is free of charge or not, depends on the precise terms of
warranty (and whether the repair is within the period of warranty or
not).

1.  Normally, PC’s sold by [standard PC supplier Dustin](/en/howto/huisleverancier-pc's/)
    have a warranty period of 3 years. But: be aware that this does not hold for all machines. Read
    the [website of Dustin (formerly CentralPoint)](http://www.dustin.nl/) carefully when ordering.
2.  For other suppliers and products, a different warranty period may be
    applicable.

-   Repairs can be ordered through C&CZ: **within as well as outside of
    the period of warranty**. We can also advise when a defect is
    suspected.
-   The supplier or C&CZ will inform you about the day of repair. You
    have to make sure that the technician of the supplier has access to
    the apparatus on that day.

-   Ordering a repair can be done through [email, telephone or a visit to
    our office](/en/howto/contact/).

------------------------------------------------------------------------

N.B.1 The user accepts possible costs which result from this order. An
estimate of € 0 does not imply automatically that there will be no
costs. This also holds for repairs within the warranty period.

N.B.2 In case the costs for repair exceed € 125,- an e-mail with a
specified report and the repair number will be sent to the owner. The
owner must, subsequently, inform the company whether the repair has to
be carried out or not.
