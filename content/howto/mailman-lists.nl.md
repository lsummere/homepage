---
title: Mailman lists
author: bram
date: 2022-10-15
keywords: []
tags:
- email
aliases:
- mailman
---
Mailman biedt configureerbare en modereerbare [mailman
mailinglists](http://mailman.science.ru.nl/),
bijvoorbeeld de lijst van [FNWI-medewerkers](http://mailman.science.ru.nl/mailman/listinfo/fnwi-medewerkers) of die van
[FNWI-studenten](http://mailman.science.ru.nl/mailman/listinfo/fnwi-studenten). Meestal kan men zichzelf aanmelden of afmelden van zo'n mailman mailinglist. Elke mailman mailinglist heeft een beheerder, die berichten goedkeurt of afkeurt. Voor mailman mailinglistbeheerders: Om spam te filteren kan men bij de `Privacy options ...` onder de `Spam filters` als `Spam filter regexp:` opnemen:

```
X-Spam-Score:.*\*\*\*\*\*
```

en daarna de radio-button `Discard` aanzetten en helemaal onderaan op
`Submit Your Changes` klikken.

Daarnaast is het verstandig om alleen mail te accepteren van leden van
de lijst (al dan niet gemodereerd) en een expliciete lijst van te
accepteren afzenders en alle andere mail automatisch weg te gooien. Dat
scheelt een enorme hoeveelheid spam voor de moderator. Dit doe je door
onder `Privacy Options ...` door te klikken op `Sender filters` en
dan vervolgens `Action to take for postings from non-members for which no explicit action is defined.` te kiezen voor `Discard` en iets lager bij `Should messages from non-members, which are automatically discarded, be forwarded to the list moderator?` te kiezen voor `No` en vervolgens onderaan op `Submit Your Changes` klikken.
