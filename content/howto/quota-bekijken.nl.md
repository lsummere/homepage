---
author: polman
date: '2018-08-06T11:30:18Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
title: Quota bekijken
wiki_id: '26'
---
Je persoonlijke quota bepaalt alleen hoeveel ruimte je eigen data mag
innemen en hoeveel files/folders je mag hebben (bv. op je eigen
U:-schijf/home-directory). Ook andere disks kunnen quota hebben. De mail
servers gebruiken ook quota om het diskruimtegebruik in ieders mailbox
te limiteren.

## Windows

Op Windows werkplekken waar de home-directory als U:-schijf aangekoppeld
is, kan met door met de rechtermuisknop op de schijf te klikken en dan
de `Eigenschappen` (`Properties`) te bekijken, zien hoeveel ruimte (in
kbytes) er in gebruik is en nog beschikbaar is binnen de persoonlijke
quota.

## Unix

Op Linux werkplekken kan men het volgende commando gebruiken om te
bepalen wat je quota op dit moment is:

` quota`

Je ziet nu regels zoals:

`Disk quotas for mijnlogin (uid 207):`\
`Filesystem        usage   quota   limit    timeleft  files  quota  limit    timeleft`\
`/home/mijnlogin  491585  500000  600000              24901  50000  60000`

-   usage: Dit is de hoeveelheid diskruimte in KB (kilobytes) die je op
    dit moment in gebruik hebt.\
    quota: Dit is de hoeveelheid diskruimte die je mag gebruiken.\
    Onder Unix mag je tijdelijk (7 dagen) meer gebruiken dan wat quota
    aangeeft. Onder Windows is dit een harde grens.
-   limit: Dit is de hoeveelheid diskruimte die je maximaal kunt
    gebruiken.\
    timeleft : De tijd die je nog hebt om onder de quota grens te
    komen.\
    Als je te lang wacht (er verschijnt EXPIRED) dan kun je geen
    bestanden meer maken voordat je genoeg opgeruimd hebt (weer onder je
    quota zit). Als je in deze situatie een file schrijft (nieuw of
    oud), bestaat het risico dat er data verloren gaat.
-   files : Dit is de hoeveelheid bestanden die je op dit moment in
    gebruik hebt.\
    quota : Dit is de hoeveelheid bestanden die mag gebruiken.\
    Onder Unix mag je tijdelijk (7 dagen) meer gebruiken dan wat quota
    aangeeft. Onder Windows is dit weer een harde grens.
-   limit : Dit is de hoeveelheid bestanden die je maximaal kunt
    gebruiken.\
    timeleft : De tijd die je nog hebt om onder de quota grens te
    komen.\
    Als je te lang wacht (er verschijnt EXPIRED) dan kun je geen
    bestanden meer maken voordat je genoeg opgeruimd hebt (weer onder je
    quota zit).

## Bepalen waar je diskruimte voor gebruikt wordt

Ga op een Unix loginserver eerst naar je eigen home directory

` cd`\
` pwd`

Het `pwd` commando behoort iets te laten zien als `/home/mijnlogin`.

Typ nu het volgende commando in:

` du -sh * .??* | sort -h`

Er verschijnt een lijst van files en folders, gesorteerd op grootte, met
daarbij de hoeveelheid ruimte in MB (megabyte) die ingenomen wordt.

Open op een Windows systeem in de verkenner de H: schijf en bekijk het
diskruimtegebruik per folder door met rechts te klikken en te kiezen
voor `Eigenschappen` (`Properties`).

## Mail

De mail quota en het huidige gebruik kunnen eenvoudig bekeken worden via
de [Roundcube webmail service](http://roundcube.science.ru.nl). In de
linkeronderhoek staat het percentage dat van de quota in gebruik is.
Daarnaast bij de map-acties kan een lijst van mappen getoond worden,
waarbij een optie is om de map-grootte te laten zien.
