---
author: wim
date: '2021-09-24T10:55:58Z'
keywords: []
lang: en
tags:
- internet
title: Vpn
wiki_id: '71'
---
## VPN (Virtual Private Network) connection

A [RU-central VPN service
EduVPN](https://www.ru.nl/ict-uk/staff/working-off-campus/vpn-virtual-private-network/)
can be used with [RU-account and RU-password](http://www.ru.nl/idmuk/).

C&CZ also manages a VPN server, which makes it possible for all users to
gain secure access to the network with their [Science username and
password](/en/howto/studenten-login/).

The computer at home (or anywhere on the Internet) becomes part of the
campus network. In this way users can get access to services that are
normally only accessible from computers on campus. The most common of
such services are [connecting to disk shares](/en/howto/diskruimte/) or
to special servers.\
For the use of the [University library](http://www.ru.nl/ub) one does
not need VPN, because the library has a proxy website, that can be used
from anywhere on the Internet after logging in with your [RU-account and
RU-password](http://www.ru.nl/idmuk/).

## VPNSec

General:

-   VPN-server/gateway: **vpnsec.science.ru.nl**,
    based on
    [IPsec](https://wikipedia.org/wiki/IPsec).

### VPNsec setup Windows 10

-   **Windows**: Just
    add a \*new\* VPN with server vpnsec.science.ru.nl, that’s all.
    Detailed instructions:

From Windows Settings, take the following route:

-   Networks and Internet
-   VPN
-   Add a VPN connection
-   Fill in
    -   

  ------------------------ ------------------------ -------------------
  VPN provider             Windows (built-in)       default choice
  Connection name          Science-VPNsec           choose something
  Server name or address   vpnsec.science.ru.nl     provide as stated
  VPN type                 Automatic                default choice
  Type of sign-in info     User name and password   default choice
  User name (optional)     Science account          optional
  Password (optional)      Science password         optional
  ------------------------ ------------------------ -------------------

-   Save

### VPNsec setup Windows 7/8

-   **Windows**: Just add a \*new\* VPN with server
    vpnsec.science.ru.nl, that’s all. Detailed instructions are below.

From the windows Control panel, take the following route:

-   Network and Internet
-   Network and Sharing Center
-   Setup a new connection or network
-   Connect to a workspace -\> Next
-   Use my Internet connection (VPN)
-   Fill in the Internet address: vpnsec.science.ru.nl
-   and connection name, for example: Science VPNsec
-   You’ll be prompted for your Science username and password.

### VPNsec setup macOS

-   open “System Preferences”

-   open “Network” preferences, after which you get the dialog shown in
    Picture 1 below.

    **Picture 1**[none\|left\|500px](/en/howto/file:vpnmac1.png/)

Then execute the following steps:

1.  click on the “+” in left bottom to add a new network configuration\
    In the popup window fill in the following info:\
    {{< figure src="/img/old/vpnmac2.png" title="vpnmac2.png" >}}\
    press the “Create” button. Note: you are free to choose a string
    value for the “Service Name”. Probably you can best name it
    “VPNsec”.
2.  Select the new "VPN (IKE2) configuration on the left column (see
    picture 1 above)
3.  Fill in hostname of VPN on the form on the right side (see picture 1
    above)
4.  Press the “Authentication Settings..” button and fill in their your
    science credentials. (see picture 1 above)
5.  Press the “Apply” button (see picture 1 above)

### VPNsec setup iOS

-   **iOS** (iPhone/iPad):

Deleting the old VPN profile via the config app following the routine
above for macOS works, users have let us know.

Alternative: Download and install
[VPNsec-iOS.mobileconfig](/download/old/vpnsec-ios.mobileconfig) to your
iPhone/iPad. Tested on iPad with iOS 9, according to documentation iOS 8
should work too, but is yet untested.

### VPNsec setup Android

-   **Android**: Install the
    [strongSwan](https://play.google.com/store/apps/details?id=org.strongswan.android)
    app with “IKEv2 EAP (Username/Password)”. **NB: some
    special characters in the password should be escaped using a
    “\\”**.

### VPNsec setup Linux

-   **Linux**: [VPNsec Linux installation and
    configuration](/en/howto/vpnsec-linux-install/)
-   **Ubuntu 16.04**: There is a known bug people are trying to fix,
    see[msg4923789](https://bugs.launchpad.net/bugs/1570352).
    A work around currently exists, requiring some
    manual configuration. See: [VPNsec Linux installation and
    configuration](/en/howto/vpnsec-linux-install/). Or use the [OpenVPN](/en/howto/vpn/) service.

## OpenVPN for Linux

For e.g.
Linux users that have trouble getting VPNsec working on their systems,
C&CZ provides an OpenVPN service.

### Setting up OpenVPN on Linux

Make sure you have the
openvpn package installed. For Debian based distributions (like Ubuntu),
run:

`sudo apt-get install openvpn`

Next, download the
openvpn configuration file:

`wget `<https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-ca-science.ovpn>

### Starting OpenVPN on Linux

Startup the OpenVPN tunnel as
follows:

`sudo openvpn openvpn-ca-science.ovpn`

You’ll be asked for your science login name and
password. Hit Control+C to terminate the OpenVPN connection.

### All traffic through OpenVPN

Use OpenVPN’s
**–redirect-gateway autolocal** option (or put it in the config file as
**redirect-gateway autolocal**)

## OpenVPN for macOS

This starts with the choice of OpenVPN client software: The OpenVPN
protocol is not one that is built into macOS. Therefore a client program
is required that can handle capturing the traffic you wish to send
through the OpenVPN tunnel, and encrypting it and passing it to the
OpenVPN server. And of course, the reverse, to decrypt the return
traffic. See
[OpenVPN.net](https://openvpn.net/vpn-server-resources/connecting-to-access-server-with-macos/)
for several options.

Next, download the
openvpn configuration file:

`           `<https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-science.ovpn>

## SSH SOCKS-Proxy to access journals (linux)

There is a convenient alternative to VPN or the UB proxy website
described above to access online journals from anywhere. With
[SSH](http://en.wikipedia.org/wiki/Secure_Shell) one can start a so
called SOCKS Proxy-server, which can be used by web-browsers.

-   Login to your Science account with ssh:

` ssh -D 8942 lilo.science.ru.nl       # (or any other login-server)`\
` (Enter password if required)`

If your Science username (e.g. “peter”) is different on your local
username use:

` ssh -D 8942 peter@lilo.science.ru.nl`

The -D flag starts “dynamic” application-level portforwarding. The port
number (here 8942) can be any number above 1024 and below 65536. If a
port is already in use by another process try a different number.

-   Tell the web browser to use the server. In Firefox:

` * Edit - Preferences - Advanced - Settings`\
` * Select "Manual proxy configuration"`\
` * SOCKS Host: localhost      Port: 8942`\
` * Select SOCKS v5`\
` * OK`

Chromium and Google Chrome can be called from the command line with the
proxyserver option:

` chromium-browser --proxyserver="socks5://localhost:8942"`

If you now go to a journal website i.e., [J. Chem.
Phys.](http://scitation.aip.org), you should see “Your access is
provided by: Universiteitsbibliotheek” and you should have the same
access as from within the Radboud University domain.

### Run ssh in the background

With these flags:

` ssh -f -N -D port user@lilo.science.ru.nl`

ssh will run in the background (-f) and only setup the proxy server but
not actually logon (-N).

### Troubleshooting

The “netstat” command may be used to troubleshoot problems:

` netstat -at`

will show all active and non-active tcp sockets. In the above example
you should see something like:

` MYPC:/home/peter $ netstat -at`\
` Active Internet connections (servers and established)`\
` Proto Recv-Q Send-Q Local Address           Foreign Address         State      `\
` tcp        0      0 localhost:smtp          *:*                     LISTEN      `\
` tcp        0      0 localhost:8942          *:*                     LISTEN      `\
` tcp        0      0 *:ssh                   *:*                     LISTEN      `\
` tcp        0      0 localhost:ipp           *:*                     LISTEN      `\
` tcp        0      0 peter.home:36953        postvak.science.r:imaps ESTABLISHED`\
` tcp        0      0 peter.home:36808        lilo3.science.ru.nl:ssh ESTABLISHED`\
` tcp        0      0 localhost:smtp          *:*                     LISTEN      `\
` tcp        0      0 localhost:8942          *:*                     LISTEN`
