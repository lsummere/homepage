---
author: lsummere
date: '2022-05-19T12:05:09Z'
keywords: []
lang: nl
tags:
- software
- labs
title: Labservant
wiki_id: '1062'
---
# General

# Account

*The procedure below only works for employees with an U-number. For
access with an E-number account please contact
labservant\@science.ru.nl*\
All employees and guest who need to access to labservant can obtain an
account using the following steps:

1.  Request labservant access via the secretariat of your department,
    they can grand labservant access via RBS.
2.  After labservant access is granted in RBS, login to labservant using
    your RU account (https://labservant.science.ru.nl)
3.  During your first logon an account is created based on your personal
    data in RBS
4.  After a successful login (you should see the Labservant home
    screen), the lab-manager of your department can grant you the
    authorizations needed.

When the message “Logged in but no verified check” is displayed after
logon, labservant asses was not granted in RBS by your department’s
secretariat

# Lab-managers

Each organisational unit or cluster of units has one or more labservant
lab-managers who can grant labservant authorizations to department
members and can manage the department’s storage locations and budget
numbers. The labmanager is the first in line to contact regarding any
labservant questions or issues.

| **Institute/Service**                              | **Department**                        | **Lab Manager(s)**                                                     |
|----------------------------------------------------|---------------------------------------|------------------------------------------------------------------------|
| **Donders Centre for Neuroscience**
| Donders Centre                                     | Biophysics                            | ............                                                           |
| Donders Centre                                     | Molecular Neurobiology                | ............                                                           |
|                                                    |                                       |                 Nick van Bakel                                         |
|                                                    |                                       |                                   Céline Sijlmans                      |
| Donders Centre                                     | Neuroinformatics                      | ............                                                           |
|                                                    |                                       |                 Liz van den Brand                                      |
| Donders Centre                                     | Neurophysiology                       | ............                                                           |
|                                                    |                                       |                 Debbie Tilburg-Ouwens                                  |
|                                                    |                                       |                                          Marie-Louise Beenen           |
|
| *High Field Magnet Laboratory & FELIX*
| HFML/FELIX                                         | FELIX Facility                        | Jonathan Martens                                                       |
|                                                    |                                       |                     Britta Redlich                                     |
| HFML/FELIX                                         | High Field Magnet Laboratory          | Hans Engelkamp                                                         |
|
| *Institute for Molecules and Materials*
| IMM                                                | Analytical Chemistry & Chemometrics   | Joris Meurs                                                            |
| IMM                                                | Applied Materials Science             | Peter Mulder                                                           |
|                                                    |                                       |                 Maarten van Eerden                                     |
|                                                    |                                       |                                       Natasha Gruginskie               |
| IMM                                                | Biomolecular Chemistry                | Els van Genne                                                          |
| IMM                                                | Molecular and Laser Physics           | Andre van Roij                                                         |
|                                                    |                                       |                   Chris Berkhout                                       |
|                                                    |                                       |                                     Michel Balster                     |
| IMM                                                | Molecular Chemistry Cluster           | Samuel Bosma                                                           |
|                                                    |                                       |                 Jan Dommerholt                                         |
|                                                    |                                       |                                   Theo Peters                          |
| IMM                                                | NMR research Facility                 | Ruud Aspers                                                            |
|                                                    |                                       |                Frank Nelissen                                          |
| IMM                                                | Scanning Probe Microscopy             | Henning von Allwörden                                                  |
| IMM                                                | Solid State Chemistry                 | Erik de Ronde                                                          |
|                                                    |                                       |                  Paul Tinnemans                                        |
| IMM                                                | Spectroscopy of Solids and Interfaces | Chris Berkhout                                                         |
|
| *Institute for Water and Wetland Research*
| IWWR                                               | Animal Ecology and Physiology         | Wim Atsma                                                              |
|                                                    |                                       |              Jan Zethof                                                |
|                                                    | Ecology                               | Germa Verheggen-Kleinheerenbrink                                       |
|                                                    |                                       |                                     Niels Wagamakers                   |
|                                                    |                                       |                                                         Peter Cruijsen |
| IWWR                                               | Experimental Plant sciences           | Daan van den Brink                                                     |
| IWWR                                               | IWWR Radio Lab                        | Wim Atsma                                                              |
|                                                    |                                       |              Jan Zethof                                                |
| IWWR                                               | Microbiology                          | Katinka van de Pas-Schoonen                                            |
|                                                    |                                       |                                Rob de Graaf                            |
|
| *Educational Institutes*
| OBW                                                | Education Labs Biosciences            | Lydia Ketelaars                                                        |
|                                                    |                                       |                    Guus Middelbeek                                     |
| OMW                                                | Education Labs Molecular Sciences     | Tom Bloemberg                                                          |
|                                                    |                                       | Luuk van Summeren                                                      |
| ONS                                                | Education Labs Physics                |                                                                        |
|
| *Radboud Institute for Molcular Life Sciences*
| RIMLS                                              | Molecular Biology                     | Anita Kaan                                                             |
| RIMLS                                              | Molecular Developmental Biology       | Siebe van Genesen                                                      |
| RIMLS                                              | Proteomics and Chromatin Biology      | Pascal Jansen                                                          |
|
| *Supporting Services*
| GI                                                 | General Instrumentation               | Sebastian Krosse                                                       |
| IHZ                                                | Logistics                             | Maikel Diebels                                                         |
|                                                    |                                       |  Lars Heijnen                                                          |
|                                                    |                                       |  Jeffrey Manders                                                       |
| TeCe                                               | Techno Centre                         |                                                                        |

