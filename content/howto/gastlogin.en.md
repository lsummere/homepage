---
author: petervc
date: '2015-07-27T12:00:14Z'
keywords: []
lang: en
tags: []
title: Gastlogin
wiki_id: '993'
---
If you want to give one or more guests access to the [wireless
network](/en/howto/netwerk-draadloos/) or the pcs in the [computer
labs](/en/howto/terminalkamers/), there are several possibilities, see
below. For more long-term logins for employees, students, or guests, see
the [page about logins](/en/howto/login/).

-   Only needed today? One or a few logins for today: visit the [Library
    of Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/) .
-   Only wifi needed? Employees of Science-departments can request
    [Eduroam Visitor Access accounts](https://eva.eduroam.nl/inloggen),
    that give visitors access to a separate part of the wireless
    network. through the [Library of
    Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/) or
    [C&CZ-Operations](/en/howto/werkplekondersteuning/), tel. 20000.
-   In all other cases: contact [postmaster](/en/howto/contact/) at least
    a few working days before the logins are needed with information
    about:
    -   How many logins?
    -   For which purpose? If the pc’s in the [computer
        labs](/en/howto/terminalkamers/) are used, is special software
        needed?
    -   Until which date are the logins needed? After that date they
        will be removed.
    -   You will receive a mail with a pdf attachment, that can be
        printed on 8x3 A4 sticker paper.
