---
author: caspar
date: '2009-01-12T09:19:22Z'
keywords: []
lang: en
tags: []
title: Site bijwerken
wiki_id: '189'
---
### Nog niet ingedeeld

-   [:Categorie:Hardware](/en/tags/hardware)
-   [:Categorie:Software](/en/tags/software)

### Bert

-   [Telefonie gidsen](/en/howto/telefonie-gidsen/)
-   [Doorgeven van wijzigingen](/en/howto/doorgeven-van-wijzigingen/)
-   [Abonnement en
    gesprekskosten](/en/howto/abonnement-en-gesprekskosten/)
-   [GSM / FAX / DECT](/en/howto/gsm-/-fax-/-dect/)

### Theo

-   [Hardware budgetten](/en/howto/hardware-budgetten/)

### Remco

-   [:Categorie:Procedures](/en/tags/procedures)
-   [quota aanpassen](/en/howto/quota-aanpassen/)
-   [:Categorie:Faq](/en/tags/faq)
-   [Faq email](/en/howto/faq-email/)
-   [Faq quota bekijken](/en/howto/faq-quota-bekijken/)

### kan weg

-   [Overcncz helpdesk storing](/en/howto/overcncz-helpdesk-storing/)
-   [Hardware terminalkamers
    tkxp](/en/howto/hardware-terminalkamers-tkxp/)
-   [Hardware inbindapparaat](/en/howto/hardware-inbindapparaat/)
-   [Software ms ip](/en/howto/software-ms-ip/)
-   [Netwerk mutaties](/en/howto/netwerk-mutaties/)
-   [Netwerk ethergids](/en/howto/netwerk-ethergids/)
-   [Medewerkers sleutel](/en/howto/medewerkers-sleutel/)
