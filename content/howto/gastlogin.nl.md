---
author: petervc
date: '2015-07-27T12:00:14Z'
keywords: []
lang: nl
tags: []
title: Gastlogin
wiki_id: '993'
---
Wanneer men een of meerdere gasten tijdelijk toegang wil geven tot het
[draadloze netwerk](/nl/howto/netwerk-draadloos/) of tot de PC’s in de
[terminalkamers](/nl/howto/terminalkamers/), dan zijn er diverse
mogelijkheden, zie hieronder. Voor langduriger logins voor medewerkers,
studenten of gasten, zie de [pagina over logins](/nl/howto/login/).

-   Alleen vandaag nodig? Een of enkele logins voor vandaag: te
    verkrijgen bij de [Library of
    Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/).
-   Alleen wifi nodig? Medewerkers van O&O afdelingen binnen FNWI kunnen
    [Eduroam Visitor Access accounts](https://eva.eduroam.nl/inloggen),
    die gasten toegang geven tot een apart deel van het draadloze
    netwerk, aanvragen bij de [Library of
    Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/) of
    bij [C&CZ-Werkplekondersteuning](/nl/howto/werkplekondersteuning/),
    tel. 20000.
-   In alle andere gevallen: benader uiterlijk enkele werkdagen vooraf
    [postmaster](/nl/howto/contact/) met informatie over:
    -   Hoeveel logins?
    -   Voor welk doel? Als ook gebruik van pc’s in de
        [terminalkamers](/nl/howto/terminalkamers/) gemaakt wordt, is er
        dan speciale programmatuur nodig?
    -   Tot wanneer zijn de logins nodig? Daarna worden ze verwijderd.
    -   U krijgt een mail met een pdf bijlage, die geprint kan worden op
        8x3 A4 stickervellen.
