---
author: petervc
date: '2017-08-17T13:19:21Z'
keywords: []
lang: nl
tags:
- medewerkers
title: Medewerkers huisstijl
wiki_id: '81'
---
## RU Huisstijl Informatie

Algemene informatie is te vinden op de [RU huisstijl
site](http://www.ru.nl/huisstijl/).

In september 2004 is de nieuwe RU-huisstijl ingevoerd waarbij de naam en
logo van de universiteit is veranderd en dus ook de lay-out van alle
drukwerk. Voor MS-Word-toepassingen zijn centraal, in opdracht van de
Eenheid Communicatie, sjablonen en handleidingen ontwikkeld. Het gebruik
daarvan wordt hieronder besproken. Door C&CZ is een [RU huisstijl
LaTeX-briefstijl](/nl/howto/medewerkers-latexbrief/) ontwikkeld. Binnen
ICIS zijn templates ontwikkeld voor [RU-huisstijl
LaTeX-presentaties](http://www.ru.nl/icis/research/presentation/), die
ook op de C&CZ Linux systemen beschikbaar zijn in
/usr/local/share/texmf/tex/latex/rustyle/.

### RU Briefstijl voor Word, nieuwe versie (4.1) van december 2005

Koppel op de volgende de netwerkshare \\\\software-srv\\software aan:

-   Verkenner -\> Extra -\> Netwerk verbinding maken. Station: S (of een
    andere beschikbare drive letter). Map: \\\\software-srv\\software.
    Vink aan “Opnieuw verbinding maken bij inloggen”.

Open de zojuist gemaakte netwerkshare..

In de directory RU-Huisstijl bevinden zich drie handleidingen
aangeleverd door het ISC(vroeger UCI): Een Installatie handleiding, een
Gebruikershandleiding en een Conversiehandleiding. Deze laatste is
alleen van belang als je met een eerdere versie van de sjablonen hebt
gewerkt.

Lees beide handleidingen door, met name de gebruikershandleiding geeft
uitleg over de verschillende sjablonen die aangeboden worden. Hieronder
volgt kort hoe je de sjablonen voor jezelf werkend kunt krijgen.

In de directory RU-Huisstijl staan 9 .dot files en twee .ini bestanden.

`RU_Badge.dot            RU_Etiket_Large.dot     RU_Settings.ini`\
`RU_Brief.dot            RU_Fax.dot              RU_VisiteKaartje.dot`\
`RU_COG.dot              RU_Huisstijl.dot        RU_WithCompliments.dot`\
`RU_Brief_MM.dot         RU_User_Settings.ini`

Om de huisstijl voor de eigen afdeling te kunnen gebruiken, moeten de
bestanden gekopieerd worden naar een voor de gebruiker zelf schrijfbare
directory. Dit kan zijn je eigen homedirectory (H-schijf), een voor de
eigen afdeling schrijfbare netwerkdisk of een directory op de lokale PC.

Eenmalige actie (zie in de Installatiehandleiding, Instellingen per
gebruiker, pagina 3): Open in Word een leeg document, selecteer in de
menubalk achtereenvolgens de menuopties: Extra -\> Sjablonen en
invoegtoepassingen -\> Toevoegen. Blader naar de directory waar je
RU\_Huisstijl.dot naar toe hebt gekopieerd. Selecteer het bestand
RU\_Huisstijl.dot.

**Word 2010:** Ga naar Bestand -\> Opties -\> Invoegtoepassingen;
selecteer *Word-invoegtoepassingen* in het drop-downmenu ‘Beheren’ en
klik op ‘Start’. Klik in het pop-upvenster ‘Sjablonen en
invoegtoepassingen op ’Toevoegen’; blader naar de directory waar je
RU\_Huisstijl.dot naar toe hebt gekopieerd. Selecteer het bestand
RU\_Huisstijl.dot.

Vervolgens moet je Word afsluiten. Deze stap zorgt ervoor dat de
Huisstijl sjablonen het bestand RU\_Settings.ini kunnen vinden waarin
adresgegevens etc. worden opgeslagen. Het is essentieel dat
RU\_Settings.ini in de directory staat waar RU\_Huisstijl.dot staat.

Mocht deze stap niet lukken dan zal dat waarschijnlijk komen omdat de
Macro Veiligheidsinstellingen het gebruik van ongetekende macro’s niet
toestaat. Open dan in Word menuoptie Extra -\> Macro -\> Veiligheid en
stel deze in op Gemiddeld. Dan zal er iedere keer als je een
sjabloonbestand gebruikt een dialoog verschijnen die je expliciet vraagt
of je de macros wilt aanzetten ja of nee.

Nieuw in deze versie is dat de macro’s getekend zijn middels een
certificaat. Dat biedt de mogelijkheid dit certificaat voorgoed toe te
voegen aan de lijst van vertrouwde certificaten zodat je daarna de
Macrobeveiliging weer op hoog kunt zetten en dat je de sjablonen
voortaan kunt gebruiken zonder dat je telkens toestemming moet geven. Ga
daartoe als volgt te werk: in de beveiligingswaarschuwing-dialoog klik
je op “Details” en in de dialoog die dan verschijnt klik je op
“Certificaat weergeven”, vervolgens kies je “Certificaat installeren”.
Klik vervolgens een paar keer op “Volgende” in de “wizard Certificaat
importeren”. Kies tenslotte in de Macrobeveiliging-dialoog op deze bron
altijd vertrouwen en daarna kun je de macrobeveiliging weer op hoog
zetten.

Voer vervolgens stap 4 van de Installatiehandleiding uit, open
RU\_COG.dot (**Word 2010:** klik rechtsboven op ‘Invoegtoepassingen’) en
kies uit het menu RU Huisstijl de optie Edit User Ini Folder, vul in het
dialoogvenster het volledige pad in naar waar je de huisstijl bestanden
hebt gekopieerd. Klik op wijzigen en sluit af. Daarna kun je de
adresgegevens van de afdeling of dienst invullen. Deze worden dan
bewaard in het bestand RU\_User\_Settings.ini in de zojuist opgegeven
directory.

Voor de [RU huisstijl met Office2007](/nl/howto/huisstijl-office2007/) is
een aparte pagina.

Vanaf hier kun je verder de Gebruikershandleiding volgen. De
persoonlijke profielen die je maakt, worden opgeslagen in
RU\_User\_Settings.ini, wees dus voorzichtig met dit bestand.
