---
author: wim
date: '2021-09-24T10:55:58Z'
keywords: []
lang: nl
tags:
- internet
title: Vpn
wiki_id: '71'
---
## VPN (Virtual Private Network) verbinding

Er is een [RU-centrale VPN-service
EduVPN](https://www.ru.nl/ict/medewerkers/off-campus-werken/vpn-virtual-private-network/),
waar men met [RU-account en RU-wachtwoord](http://www.ru.nl/wachtwoord)
gebruik van kan maken.

C&CZ beschikt ook over een VPN server, waarmee alle gebruikers met hun
[Science-login en wachtwoord](/nl/howto/studenten-login/) een beveiligde
toegang tot het netwerk kunnen krijgen.

De werkplek thuis (of ergens anders op het Internet) wordt daarmee
gezien als onderdeel van het campusnetwerk. Op deze manier kan men
toegang krijgen tot faciliteiten die alleen vanaf het campusnetwerk
toegankelijk zijn. Men kan hierbij denken aan [het aankoppelen van
netwerkschijven](/nl/howto/diskruimte/) of toegang tot speciale servers.\
Voor het gebruik van de [UB (bibliotheek)](http://www.ru.nl/ub) is de
VPN niet nodig, want de UB gebruikt een proxy website, die na inloggen
met [RU-account en RU-wachtwoord](http://www.ru.nl/wachtwoord) toegang
vanuit het hele Internet mogelijk maakt.

## VPNSec

Algemeen:

-   VPN-server/gateway: vpnsec.science.ru.nl,
    op basis van [IPsec](https://wikipedia.org/wiki/IPsec).

### VPNsec setup Windows 10

-   Maak een *nieuwe* VPN aan, met server vpnsec.science.ru.nl, dat is alles. Uitgebreide instructies:

Vanuit Windows Configuratiescherm, ga naar:

-   Netwerk en Internet
-   Netwerkcentrum
-   Een nieuwe verbinding of een nieuw netwerk instellen
-   Verbinding met een bedrijfsnetwerk maken -\> Volgende
-   Ik wil een nieuwe verbinding maken
-   Mijn Internetverbinding (VPN) gebruiken
-   Geef het Internet-adres voor de verbinding op. Internet-adres:
    vpnsec.science.ru.nl
-   Naam van deze verbinding: Science VPNsec (of iets anders) en klik
    “Maken”.

-   Gebruiken: klik in de taakbalk op het vliegtuig/Internet-icoon en
    klik op “Science VPNsec” en “Verbinding maken”.
-   Vul de Science loginnaam en het bijbehorende wachtwoord in. Daarna:
    “Verbonden”.


### VPNsec setup macOS

-   open “System Preferences”

-   open “Network” preferences, after which you get the dialog shown in
    Picture 1 below.

    **Picture 1**[none\|left\|500px](/nl/howto/file:vpnmac1.png/)

Then execute the following steps:

1.  click on the “+” in left bottom to add a new network configuration\
    In the popup window fill in the following info:\
    {{< figure src="/img/old/vpnmac2.png" title="vpnmac2.png" >}}\
    press the “Create” button. Note: you are free to choose a string
    value for the “Service Name”. Probably you can best name it
    “VPNsec”.
2.  Select the new "VPN (IKE2) configuration on the left column (see
    picture 1 above)
3.  Fill in hostname of VPN on the form on the right side (see picture 1
    above)
4.  Press the “Authentication Settings..” button and fill in their your
    science credentials. (see picture 1 above)
5.  Press the “Apply” button (see picture 1 above)

### VPNsec setup iOS

-   **iOS** (iPhone/iPad):

Het oude VPN-profiel via de config-app verwijderen en de instructie
hierboven voor macOS zo goed mogelijk volgen, blijkt voor veel mensen te
werken.

Alternatief: Download en installeer
[VPNsec-iOS.mobileconfig](/download/old/vpnsec-ios.mobileconfig) op je
iPhone/iPad. Getest op iPad met iOS 9, volgens documentatie werkt iOS 8
ook, maar ongetest.

### VPNsec setup Android

-   **Android**: Installeer de
    [strongSwan](https://play.google.com/store/apps/details?id=org.strongswan.android)
    app met “IKEv2 EAP (Username/Password)”. **NB: sommige
    tekens in een wachtwoord moeten worden ge-escaped met een
    “\\”**.

### VPNsec setup Linux

-   **Linux**: [VPNsec Linux installatie en
    configuratie](/nl/howto/vpnsec-linux-install/)
-   **Ubuntu 16.04**: Er is een bekende bug waaraan gewerkt wordt,
    zie: [msg4923789](https://bugs.launchpad.net/bugs/1570352).
    Inmiddels is er een ‘work around’. deze vraagt echter wat
    handwerk. Zie: [VPNsec Linux installation and
    configuration](/nl/howto/vpnsec-linux-install/). Of gebruik
    de [OpenVPN](/nl/howto/vpn/) service.

## OpenVPN voor Linux

Voor o.a. Linux gebruikers die moeite hebben om de VPNsec service
werkend te krijgen, biedt C&CZ een OpenVPN service aan.

### Opzetten van OpenVPN op Linux

Zorg dat je het package openvpn geïnstalleerd hebt. Voor op Debian
gebaseerde distributies zoals Ubuntu, gebruik:

`sudo apt-get install openvpn`

Download daarna het openvpn configuratiebestand:

`wget `<https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-ca-science.ovpn>

### Start OpenVPN op Linux

Start de OpenVPN tunnel als volgt:

`sudo openvpn openvpn-ca-science.ovpn`

Er zal gevraagd worden naar de Science loginnaam an
wachtwoord. De verbinding kan verbroken worden door Control+C te
tikken.

### Al het Internet verkeer door OpenVPN

Gebruik OpenVPN’s **–redirect-gateway autolocal** optie (of zet het in
het config bestand als **redirect-gateway autolocal**)

## OpenVPN macOS

Dit begint met de keus van OpenVPN client software: Het OpenVPN protocol
zit niet in macOS. Daarom is client software nodig, die het verkeer af
kan handelen dat door de OpenVPN tunnel moet, dat verkeer kan kan
versleutelen en doorsturen naar de OpenVPN server. En natuurlijk moet
die client software ook het verkeer dat terugkomt kunnen ontsleutelen.
Zie
[OpenVPN.net](https://openvpn.net/vpn-server-resources/connecting-to-access-server-with-macos/)
voor verschillende mogelijkheden.

Download daarna het openvpn configuratiebestand:

`           `<https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-science.ovpn>

## SSH SOCKS-Proxy to access journals (linux)

There is a convenient alternative to VPN or the UB proxy website
described above to access online journals from anywhere. With
[SSH](http://en.wikipedia.org/wiki/Secure_Shell) one can start a so
called SOCKS Proxy-server, which can be used by web-browsers.

-   Login to your Science account with ssh:

` ssh -D 8942 lilo.science.ru.nl       # (or any other login-server)`\
` (Enter password if required)`

If your Science username (e.g. “peter”) is different on your local
username use:

` ssh -D 8942 peter@lilo.science.ru.nl`

The -D flag starts “dynamic” application-level portforwarding. The port
number (here 8942) can be any number above 1024 and below 65536. If a
port is already in use by another process try a different number.

-   Tell the web browser to use the server. In Firefox:

` * Edit - Preferences - Advanced - Settings`\
` * Select "Manual proxy configuration"`\
` * SOCKS Host: localhost      Port: 8942`\
` * Select SOCKS v5`\
` * OK`

Chromium and Google Chrome can be called from the command line with the
proxyserver option:

` chromium-browser --proxyserver="socks5://localhost:8942"`

If you now go to a journal website i.e., [J. Chem.
Phys.](http://scitation.aip.org), you should see “Your access is
provided by: Universiteitsbibliotheek” and you should have the same
access as from within the Radboud University domain.

### Run ssh in the background

With these flags:

` ssh -f -N -D port user@lilo.science.ru.nl`

ssh will run in the background (-f) and only setup the proxy server but
not actually logon (-N).

### Troubleshooting

The “netstat” command may be used to troubleshoot problems:

` netstat -at`

will show all active and non-active tcp sockets. In the above example
you should see something like:

` MYPC:/home/peter $ netstat -at`\
` Active Internet connections (servers and established)`\
` Proto Recv-Q Send-Q Local Address           Foreign Address         State      `\
` tcp        0      0 localhost:smtp          *:*                     LISTEN      `\
` tcp        0      0 localhost:8942          *:*                     LISTEN      `\
` tcp        0      0 *:ssh                   *:*                     LISTEN      `\
` tcp        0      0 localhost:ipp           *:*                     LISTEN      `\
` tcp        0      0 peter.home:36953        postvak.science.r:imaps ESTABLISHED`\
` tcp        0      0 peter.home:36808        lilo3.science.ru.nl:ssh ESTABLISHED`\
` tcp        0      0 localhost:smtp          *:*                     LISTEN      `\
` tcp        0      0 localhost:8942          *:*                     LISTEN`
