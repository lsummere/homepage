---
author: wim
date: '2010-04-09T08:27:49Z'
keywords: []
lang: en
tags:
- internet
title: Vpn F-Secure
wiki_id: '726'
---
Ad-hoc translated from Dutch. Needs additional attention….

Adjust F-Secure:

Open F-Secure from **traybar**.

Select **Settings**. A new window appears.

Select **Network connections**, select **Firewall**. Clik **Add**.

Enter for example **GRE** in **Name** field. Click **Next**.

Choose **Any IP address**. Click **Next**.

Scroll to **GRE / Cisco Generic Routing Encapsulation Tunnel** and
select this item.

Now things become somewhat unclear, but click on the **?** icon in this
rule and Select **\<-\>**. Click **Next**.

Select **No warning**. Clik **Next**.

Click **Finish**.
