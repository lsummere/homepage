---
author: petervc
date: '2021-12-05T23:34:06Z'
keywords: []
lang: en
tags:
- software
title: Mathematica
wiki_id: '150'
---
[Mathematica](http://www.wolfram.com/products/mathematica/) is a mathematical software package made
by [Wolfram Research](http://www.wolfram.com/).

[ned] C&CZ betaalt de netwerk-licentie gekocht voor het kleinschalig
gelijktijdig gebruik van Mathematica binnen FNWI. Bij [C&CZ
systeembeheer](/en/howto/overcncz-wieiswie-systeembeheer/) zijn de
installatie- en licentie-informatie te krijgen. [/ned] [eng] C&CZ
pays for the network license for the concurrent use (by a small number
of people) of Mathematica within the Faculty of Science. One can get the
installation and license information from [C&CZ system
administration](/en/howto/overcncz-wieiswie-systeembeheer/). [/eng]

The use of Mathematica is logged by the license server.

On the Linux PCs that are managed by C&CZ, Mathematica can be found in
/vol/mathematica. On MS-Windows PC’s Mathematica can be found on the
S-disk (software).
