---
author: wim
date: '2010-04-09T08:27:49Z'
keywords: []
lang: nl
tags:
- internet
title: Vpn F-Secure
wiki_id: '726'
---
Aanpassen F-Secure:

Open F-Secure vanuit **traybar**.

Selecteer **Instellingen**. Een nieuw window verschijnt.

Selecteer **Netwerkverbindingen**, selecteer **Firewall**. Klik
**Toevoegen**.

Vul bij **Naam** bijvoorbeeld **GRE** in. Klik **Volgende**.

Kies **Elk IP adres**. Klik **Volgende**.

Scroll naar **GRE / Cisco Generic Routing Encapsulation Tunnel** en
selecteer dit item.

Nu wordt het wat onduidelijk, maar klik op het **?** icoontje in de
regel en Selecteer **\<-\>**. Klik **Volgende**.

Selecteer **Geen waarschuwing**. Klik **Volgende**.

Klik **Voltooien**.
