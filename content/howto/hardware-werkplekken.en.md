---
author: petervc
date: '2012-08-17T14:33:34Z'
keywords: []
lang: en
tags:
- hardware
title: Hardware werkplekken
wiki_id: '580'
---
## PC workstation

Everyone interested in buying a PC will be interested in reading the
information about the [default supplier of
PCs](/en/howto/huisleverancier-pc's/). C&CZ can help with the
[installation](/en/howto/installeren-werkplek/) or do this completely.
One can choose between [MS-Windows](/en/howto/windows-beheerde-werkplek/)
and [Ubuntu Linux](/en/howto/unix/). Information about the [repair
procedure](/en/howto/reparaties/) is also available.
