---
author: arranc
date: '2022-09-08T12:36:19Z'
keywords: []
lang: nl
tags:
- storage
- ceph
title: Data Opslag
wiki_id: '48'
---
# Data Opslag

De diskruimte op [C&CZ servers](/nl/howto/hardware-servers/) kan door
gebruikers benaderd worden vanaf allerlei [C&CZ servers en
werkplekken](/nl/howto/hardware-servers/), maar ook vanaf andere
werkplekken of zelfs vanaf thuis met WinSCP of via [VPN](/nl/howto/vpn/). Van
vrijwel alle schijven die onder beheer van C&CZ staan, worden regelmatig
[backups](/nl/howto/backup/) gemaakt, zodat bij kleine of grote
calamiteiten de data niet verloren gaan.

## Home directories

-   Elke gebruiker met een [Science login](/nl/howto/login/)
    heeft (recht op) een eigen stuk diskruimte van enkele Gigabytes op
    een server. Die diskruimte wordt onder Unix/Linux de
    “home-directory” en onder Windows de “H- of U-schijf” genoemd. De
    plaats van de homedirectory (op welke server) kan men zien door in
    te loggen op de [Doe-Het-Zelf website](https://dhz.science.ru.nl).

### Naamgeving
| naamgeving                          | voorbeeld                                                                    |
| :---------------------------------- | :--------------------------------------------------------------------------- |
| Loginnaam                           | `guest204`                                                                   |
| Windows pad                         | `\\home1.science.ru.nl\guest204`  (check op [DHZ](https://dhz.scince.ru.nl)) |
| MacOS / Linux pad                   | `smb://home1.science.ru.nl/guest204`                                         |
| Linux (door C&CZ beheerde systemen) | `/home/guest204`                                                             |


### Toegangsrechten

Lang geleden was de (Unix) homedirectory van een gebruiker, op enkele
afgeschermde delen na, leesbaar voor alle andere gebruikers van de
server. Tegenwoordig is de homedirectory van een gebruiker alleen
toegankelijk voor de gebruiker zelf. De gebruiker kan zelf de
toegangsrechten wijzigen. C&CZ controleert op homedirectories die voor
iedereen schrijfbaar zijn.

### Toegang via NFS

Een homedirectory (U: schijf) kan op Linux aangekoppeld worden via
[NFS/Kerberos (English only)](/nl/howto/mount-homedisk/).

## Functionaliteit en prijs van netwerkschijven

### RAID server schijven

Aparte schijfruimte (diskruimte) voor groepen/afdelingen/projecten: er
zijn enkele fileservers met [RAID
opslag](http://nl.wikipedia.org/wiki/Redundant_Array_of_Independent_Disks)
waarop partities gehuurd kunnen worden voor een periode van 3 jaar. De
prijs voor nieuwe schijven of verlenging van oude schijven is per juli
2018 intern FNWI:

| grootte                                | incl. backup      | zonder backup    |
| :------------------------------------- | :---------------- | :--------------- |
| ca. 200 GB                             | € 40 per jaar     | € 10 per jaar    |
| ca. 400 GB                             | € 80 per jaar     | € 20 per jaar    |
| > 400 GB up to 1 TB (no daily backup?) | € ??? per TB/jaar | € 50 per TB/jaar |
| > 1 TB (no backup?)                    | N/A               | Zie Ceph storage |


Hoewel zelfs de goedkoopste versie aanmerkelijk duurder is dan het
aanschaffen van 1 harde schijf voor 1 PC, is het vaak zinvol vanwege de
betrouwbaarheid (redundante schijven, backup, onderhoudscontract) en
bedrijfszekerheid (stabiele server). Een of meer mappen op zo’n partitie
kunnen op Windows PCs beschikbaar worden als een schijf, door het maken
van een netwerkverbinding. Op Unix/Linux computers kunnen ze via NFS
aangekoppeld worden. De mogelijkheid om te lezen en/of te schrijven in
zo’n map kan beperkt worden tot een groep logins. Die groep kan door de
afdeling zelf beheerd worden via de [Doe-Het-Zelf
website](/nl/howto/dhz/).

C&CZ heeft op die servers een service contract afgesloten en beschikt
over reserve-apparatuur, waardoor een storing relatief snel opgelost kan
worden. Omdat de schijven in een RAID-set opgenomen zijn, veroorzaakt
het uitvallen van 1 of zelfs 2 schijven, geen storing voor gebruikers.
Van de partities worden ook (dagelijks en incrementeel)
[backups](/nl/howto/backup/) gemaakt, zodat zelfs bij uitval van de hele
serverruimte de data nog (ooit) teruggezet kunnen worden.

### Ceph Storage

Vanaf november 2019 kunnen we bijna oneindige storage aanbieden voor
FNWI middels ons Ceph storage cluster. Door hoe Ceph werkt is er een
afweging tussen snelheid en redundantie. Met de extra redundantie opties
kunnen we zelfs betere beschikbaarheid bieden dan bij RAID-6 systemen.
De fysieke opslag servers zijn verdeeld over 3 locaties (datacenters).
**NB Ceph volumes hebben geen backups, de volumes zijn doorgaans te
groot om te kunnen backuppen.**

#### Keuzes in redundantie

Ceph heeft de mogelijkheid om data op verschillende manieren op te slaan
(per “pool” te kiezen), standaard worden blokken 3 maal opgeslagen,
zodat bij verlies van ́é́én blok er nog twee kopiëen over blijven.
Inmiddels is het ook zo dat de 3copy pool blijft werken bij uitval van
een heel datacenter. In het begin hadden we twee locaties, waardoor we
voor betere betrouwbaarheid een 4copy pool hebben gemaakt, maar die
voegt met drie locaties weinig toe aan de betrouwbaarheid.

Naast het opslaan van kopiëen kent Ceph ook nog “Erasure Coding” (EC)
als vorm van redundantie. Het voordeel van deze manier is dat je minder
redundantie overhead hebt, door het gebruik van een algoritme zoals
bijvoorbeeld RAID-6 toe te passen. Het nadeel van EC is dat de overhead
voor kleine bestanden erg groot is. Er zijn verschillende EC pools;
EC8+3: goedkoop, maar bij vernietiging van 1 heel datacenter is alle
data weg (heel onwaarschijnlijk dat dat gebeurt), bij tijdelijke uitval
van een datacenter is de data veilig, maar even niet beschikbaar. De
EC5+4 pool blijft beschikbaar en schrijfbaar bij uitval van 1 datacenter
en bij vernietiging van een datacenter is de data nog veilig.

**Ceph Erasure coding heeft een grote overhead bij kleine files, de
prijzen in de tabel hieronder zijn gebaseerd op de optimale overhead,
die pas benaderd wordt met files groter dan 4 megabytes.**

| Pool                 | waarom                           | prijs per TB* per jaar zonder backup |
| :------------------- | :------------------------------- | ------------------------------------ |
| Erasure coding 8+3** | goedkoop                         | € 50 (was 45)                        |
| Erasure coding 5+4   | goedkoop + extra redundantie     | € 60                                 |
| 3 copy               | snellere r+w                     | € 100                                |
| 4 copy***            | snellere r+w + extra redundantie | € 135                                |

> **\* 1TB** is 1.000.000.000.000 bytes
>
> **\*\* EC8+3 prijs** is van 45 naar 50 euro gegaan per 1 januari 2022, dit
> is vanwege het gebruik, waarbij blijkt dat er toch erg veel kleine files
> opgeslagen zijn, wat in Ceph tot flinke overhead zorgt.
>
> **\*\*\* 4copy**. De 4copy pool bleef beschikbaar toen we 2 datacenters
> hadden, als een van de twee datacenters uitviel, de 3copy pool blijft nu
> ook beschikbaar bij het uitvallen van een heel datacenter, daarmee is
> het voordeel van de 4copy pool grotendeels weg. De 4copy pool heeft wel
> het voordeel van een extra kopie, maar dat maakt niet uit bij het
> uitvallen van een heel datacenter.

De Ceph storage kan in overleg worden aangeboden als Windows/Samba
share, NFS share en als S3 object store. Object store is fundamenteel
anders dan een normaal bestandssysteem, dus als S3 gewenst is, kan de de
data niet ook als Windows share of NFS share gebruikt worden.

De performance eigenschappen van Ceph zijn anders dan normale netwerk
opslag op losse servers; de schrijfsnelheden zijn over het algemeen
groter dan de leessnelheid en nog meer dan bij traditionele opslag zijn
kleine bestanden funest voor de doorvoersnelheid.

## Naamgeving voor netwerkschijven

| Naming                              | Example                                       |
| :---------------------------------- | :-------------------------------------------- |
| Share naam                          | `sharename`                                   |
| Windows pad                         | `\\sharename-srv.science.ru.nl\sharename`     |
| MacOS / Linux pad                   | `smb://sharename-srv.science.ru.nl/sharename` |
| Linux (door C&CZ beheerde systemen) | `/vol/sharename`                              |


### Toegangsrechten

De meeste gedeelde schijven zijn lees- en schrijfbaar voor een bepaalde
groep gebruikers. Welke accounts er lid zijn van deze groep, kan door de
eigenaren van de groep bijgehouden worden op de [Doe-Het-Zelf
website](/nl/howto/dhz/).

### Aanvragen

Bij een aanvraag van een of meer netwerkschijven zal vermeld moeten
worden:

- gewenste naam/namen van de schijf/schijven
- gewenste grootte (max ca. 500GB bij backup)
- evt. gewenste backup-schema’s om kosten te beperken (Daily/Monthly/Yearly)
- Science loginnaam van een eigenaar
- evt. Science loginnaam van een lid
- kostenplaats of projectcode voor de kosten voor de eerste drie jaar.

## Tijdelijke gedeelde schijfruimte

Het komt wel eens voor dat je een of meer grote bestanden (meer dan
enkele tientallen MB) naar iemand binnen de faculteit wilt sturen, mail
is daarvoor ongeschikt. Om dit makkelijker te maken is er een
netwerkschijf beschikbaar waar je tijdelijk grote bestanden kunt
plaatsen die dan door iemand anders van deze locatie gekopiëerd kunnen
worden. Bedenk dat dit nadrukkelijk bedoeld is voor tijdelijke opslag,
er worden geen backups van gemaakt en de disk wordt iedere dag
schoongemaakt (bestanden ouder dan 21 dagen worden verwijderd).

Deze share kan ook gebruikt worden om tijdelijk bestanden te bewaren
alleen leesbaar voor jezelf. Dit kan door de share onder een andere naam
te koppelen.

Merk op dat ook in dat geval oude bestanden verwijderd worden.

Zorg ervoor dat de tijdstempels van bestanden zijn bijgewerkt wanneer u
een bestand naar deze share kopieert. Sommige kopieerprogramma’s (zoals
rsync) behouden de originele tijdstempels en oudere bestanden worden dan
verwijderd. Om tijdstempels bij te werken, kunt u de volgende opdracht
gebruiken:

```
find . -exec touch {} +
```

Maak alsjeblieft eerst een sub-map aan met b.v. je Science-loginnaam,
zet de tijdelijke bestanden vervolgens in die map.

Voor bestanden samen kleiner dan 250GB is ook
[Surfdrive](/nl/howto/nieuws/) een alternatief. Voor het versturen van
bestanden tot 500GB is [SURFfilesender](https://www.surffilesender.nl/)
geschikt.

### Naamgeving van tijdelijke opslag
| Naming                              |                                                                               |
| :---------------------------------- | :---------------------------------------------------------------------------- |
| Share name                          | `temp`                                                                        |
| Windows pad                         | `\\temp-srv.science.ru.nl\share` or `\\temp-srv.science.ru.nl\onlyme`         |
| MacOS / Linux pad                   | `smb://temp-srv.science.ru.nl/share` or `smb://temp-srv.science.ru.nl/onlyme` |
| Linux (door C&CZ beheerde systemen) | `/vol/temp`                                                                   |


### Toegangsrechten

- Leesbaar voor alle gebruikers: `share`
- Alleen zelf lezen: `onlyme`
