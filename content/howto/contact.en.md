---
author: bram
date: '2022-11-02T10:08:04+01:00'
keywords: ['contact']
lang: en
tags: ['faq']
title: Contact
wiki_id: '816'
weight: 1000
aliases:
- helpdesk
---
For all ICT related questions and problems within FNWI please contact
C&CZ.

## Helpdesk & [Sysadmin](../systeemontwikkeling)
| phone                               | mail
| ------------------------------------|-----
| [+31 24 36 20000](tel:+31243620000) | helpdesk@science.ru.nl
| [+31 24 36 53535](tel:+31243653535) | postmaster@science.ru.nl

## Visiting address

We are located in room **HG03.055**. The helpdesk is available during office hours (8:30 - 17:00).
During that time the office is staffed.

C&CZ is located in the Huygens building, 3rd floor, next to the entrance
of wing number 5. Big orange wall stickers spell “C&CZ” at the entrance.
The room number is HG03.055, which can be decoded as Huygens (HG), 3rd
floor (03), central street (.0), roughly in the middle (055). In the
Huygens building wing numbers run from 1 to 8, increasing from south to
north. Room numbers increase in the same direction. The central street
is numbered as wing 0.

> Full address:
> Huygens Building, HG03.055, Heyendaalseweg 135, 6525 AJ  Nijmegen
>
> Postal address:
> Radboud University Nijmegen, Faculty of Science, C&CZ, P.O. Box 9010, 6500  GL Nijmegen

The page about [how to reach the campus](https://www.ru.nl/en/contact/getting-here) also lists all parkings.

# Other helpdesks

## ILS Helpdesk

Contact the ILS Helpdesk for questions about:
- ILS managed PC's
- [Printing](https://www.ru.nl/facilitairbedrijf/printen/printen/)
- `@ru.nl` mail addresses
- Desk and cell phones
- Network outlets

| phone                              | mail              | web
| ---------------------------------- | ----------------- | ---
| [+31 24 36 22222](tel:+31243622222)| icthelpdesk@ru.nl | https://www.ru.nl/ict-uk/

Students with questions about their Radboud-account or `@ru.nl` address, please
contact [Student Affairs](https://www.ru.nl/en/departments/academic-affairs-departments/student-affairs) directly.
