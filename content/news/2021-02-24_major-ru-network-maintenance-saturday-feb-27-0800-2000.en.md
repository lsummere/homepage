---
author: petervc
date: 2021-02-24 14:07:00
tags:
- medewerkers
- studenten
title: Major RU network maintenance Saturday Feb. 27 08:00-20:00
---
The ISC [announced](https://www.ru.nl/systeem-meldingen/) that Saturday
February 27 08:00-20:00 major RU network maintenance work will be
carried out. This will mean that all RU services will be unavailable
several times for at most an hour. This concerns all RU services
including those of FNWI/C&CZ: e-mail, VPN, wifi, BASS, OSIRIS,
Brightspace, Syllabus+, Corsa, etc.
