---
author: petervc
date: 2013-07-02 12:05:00
tags:
- studenten
- medewerkers
- docenten
title: Mailproblemen na weggeven wachtwoord aan phishers
---
Horde webmail bleek voor de zoveelste keer misbruikt te worden voor
spam. Weer had een naïeve gebruiker het Science-wachtwoord aan
phishers/spammers gegeven, waardoor dit mogelijk werd. Nadat eerst horde
stopgezet is, is vannacht de login van deze gebruiker afgezet en horde
weer herstart. Dinsdagochtend bleek deze kortdurende spam-outbreak toch
reden geweest te zijn voor de beheerders van hotmail.com om onze
uitgaande mailserver op hun zwarte lijst te zetten. Daarom hebben we
dinsdagochtend deze server een ander IP-adres gegeven en Microsoft
verzocht het oude nummer van de zwarte lijst te halen.
