---
author: petervc
date: 2013-07-02 00:06:00
tags:
- studenten
- medewerkers
- docenten
title: Nieuwe multifunctionele printers overal
---
Vanaf 1 juli 2013 worden op allerlei plaatsen in FNWI-gebouwen
multifunctionele printers/copiers/scanners van het merk Konica Minolta
neergezet ter vervanging van de Ricoh’s. Lees op de
[KM-pagina](/nl/howto/km/) alles over de vervanging.
