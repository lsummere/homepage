---
author: petervc
date: 2017-07-26 17:30:00
tags:
- medewerkers
title: Exchange account/mail-address Givenname.Surname at ru.nl for FNWI staff from
  August 1
---
Starting August 1, all FNWI employees who do not have an account yet in
[Exchange, the RU central mail and
calendar](http://www.ru.nl/ict-uk/staff/mail-calendar/), will get such
an account and with that an extra mail address, of the form
Givenname.Surname\@ru.nl. For anyone who already has an Exchange
account, with a mail address ending in e.g. @ru.nl @donders.ru.nl
@fnwi.ru.nl, nothing changes.

In Exchange one can share calendars with eachother, which makes making
appointments easier. All new Exchange accounts will get a “redirect” to
the current Science address. As a result, you will not miss an email:
you only get an extra address, that redirects all new mail to the
existing address.

Do you want to use Exchange? The “redirect” can be turned off by logging
in to <https://mail.ru.nl> with your U-number preceded by “RU\\” and
your RU password. After that, you can remove the check box in “Options
-\> Create an Inbox Rule” at “redirect all incoming”. Sending new
Science mail to Exchange can be turned on by visiting the
[Do-It-Yourself Site](https://dhz.science.ru.nl) and “forward” to the
@ru.nl mail address.

Take Note 1: Thunderbird users should install the add-on Lightning to be
able to see and process Exchange calendar requests.

Take Note 2: if there is a redirect line in Exchange, Exchange will not
forward mails with sender address the same as the forwarding address, in
order to prevent mail loops.

If you have questions, please [contact C&CZ](/en/howto/contact/).
