---
author: petervc
date: 2010-07-16 16:49:00
title: 'Personnel changes: Jos Alsters, Pauline Berens'
---
November 1, 2010, {{< author "petervc" >}} will go back to an
administrator/developer function, the new head of C&CZ will be
{{< author "caspar" >}}.

Jos Alsters has left C&CZ as of July 15, 2010. He has started his own
ICT company [Itsal.nl](http://www.itsal.nl), " IT Services from Open
Standards". C&CZ intends to hire Jos for some assignments.

On the same day {{< author "pberens" >}} started at OWC (Education
Centre) and C&CZ as IT-administrator/developer for Science education.
