---
author: simon
date: 2017-05-01 10:30:00
tags:
- medewerkers
- docenten
- studenten
title: 'Linux Login servers: default Ubuntu 16.04'
---
The support for Ubuntu 12.04 LTS has ended on April 25th, 2017.
Therefore we will turn off lilo3 with this version of Ubuntu on May 1st,
2017. The default login server (lilo.science.ru.nl) will be redirected
to lilo5 with Ubuntu 16.04 LTS and lilo4.science.ru.nl with Ubuntu 14.04
LTS remains available until support for this version ends in 2019.

Signatures lilo5:

2048 SHA256:VK8+B+OuT+n8JSjoy9uWYw5COb+cZ+01S/pWiAteEtA lilo5 (RSA) 256
SHA256:NeO+rdPufkFYEgLs6YMWPvMJzOBK9pNlxdjMJzK3OBY lilo5 (ECDSA) 256
SHA256:zViYANQchkRs6QNPSSoxOkVyA8Ixe7aZCvzcSQEZK7s lilo5 (ED25519)
