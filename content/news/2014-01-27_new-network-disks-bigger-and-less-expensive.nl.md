---
author: petervc
date: 2014-01-27 16:48:00
tags:
- medewerkers
- studenten
title: Nieuwe netwerkschijven groter en prijsgunstiger
---
Door de aanschaf van een nieuwe file server, kan C&CZ de prijzen voor
nieuwe [netwerkschijven](/nl/howto/diskruimte/) verlagen met 50%. Vanwege
de onlangs aangekondigde [nieuwe backupservice](/nl/howto/nieuws/) zijn
nu ook grotere schijven (400 GB) mogelijk. Daarnaast is er de nieuwe
optie van een niet-gebackupte netwerkschijf, die een factor vier
goedkoper is. Zo’n schijf kan b.v. gebruikt worden om kopieën van een
locale schijf op te maken. De overeenkomsten voor het huren van
schijfruimte worden net als eerder voor 3 jaar afgesloten. Bestaande
overeenkomsten worden niet veranderd.
