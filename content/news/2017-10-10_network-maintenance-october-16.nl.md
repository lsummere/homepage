---
author: mkup
date: 2017-10-10 11:30:00
tags:
- studenten
- medewerkers
- docenten
title: Netwerkonderhoud op 16 oktober
---
Op maandagochtend 16 oktober om 05:00 uur worden drie netwerkswitches
van nieuwe software voorzien. Aangesloten apparaten (ook ip-telefoons en
draadloze toegangspunten) zullen op drie locaties gedurende ca 20
minuten hun netwerk verliezen:

`- Mercator 1 eerste verdieping (ICIS, alle apparatuur)`
`- Huygens vleugel 4 (alle apparatuur, ook in leeszalen Library of Science)`
`- Huygens vleugel 5 (ca. de helft van de apparatuur)`
