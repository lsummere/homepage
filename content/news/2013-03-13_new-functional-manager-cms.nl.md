---
author: caspar
date: 2013-03-13 18:11:00
tags:
- medewerkers
- docenten
title: Nieuwe functioneel beheerder CMS
---
Peter Klok was jarenlang de steun en toeverlaat (functioneel beheerder
in ICT-jargon) voor iedereen bij FNWI die iets deed met websites in het
RU Content Management Systeem ([CMS](/nl/howto/cms/)). Nu Peter met
pensioen is, is deze taak overgenomen door C&CZ. Dit betekent dat men
met alle beheer- en ondersteuningsvragen over facultaire websites, wat
de onderliggende techniek ook is, bij C&CZ terecht kan. In de praktijk
wordt de CMS ondersteuning primair uitgevoerd door Wim G.H.M. Janssen,
ICT-specialist bij het instituut IMAPP. C&CZ (in het bijzonder Fred
Melssen) zal als achterwacht voor Wim optreden.
