---
author: petervc
date: 2017-06-22 17:43:00
tags:
- medewerkers
- docenten
- studenten
title: C&CZ reachable via Whatsapp/Telegram/Signal
---
If you do not have cellphone coverage, but do have an Internet
connection, then you can [reach C&CZ](/en/howto/contact/) also via
Whatsapp/Telegram/Signal on
{{< figure src="/img/old/telephone.gif" title="telefoon" >}} +31 6 15
35 26 77. The direct reason for this is that not in all wings of the
Huygens building all mobile carriers have good coverage. However, the
wireless Eduroam network has good coverae everywhere, which allows all
staff and students to call C&CZ, independent of their mobile provider.
Gradually, more providers will support [Wi-Fi
calling](https://www.cnet.com/news/what-you-need-to-know-about-wifi-calling/),
which eliminates the need for an app like Whatsapp/Telegram/Signal for
this purpose. Currently only Vodafone supports Wi-Fi calling, KPN has
plans to introduce this in 2017.
