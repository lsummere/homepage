---
author: erik_joost_visser
date: 2015-11-25 00:00:00
tags:
- medewerkers
- docenten
- studenten
title: Watch out\! Scammers send mail resembling a picture sent from a smartphone
---
Early this week, many employees received an email with the subject
“img”, as entire content “Sent from my Lenovo” and as [attachment
malware with a name like “IMG\_0112201135\_2015
JPEG.cab”](http://www.myce.com/news/malware-spreads-by-cab-e-mail-attachments-to-evade-ziprar-filters-74635/).
A “.cab” file is a compressed archive, similar to a zip file. Because
people on Windows PCs usually do not see file extensions, this seems to
be a picture (JPEG) to them, but it is a “….JPEG.exe”. Double clicking
on the fake picture infects the computer by executing the “.exe”.
Therefore C&CZ changed the [MIMEDefang
filter](http://www.mimedefang.org/) on the [Science
mailserver](/en/howto/:category:email/) to remove “.cab” attachments from
mails, as has been done for years with other dangerous extensions like
“.exe”. When a user fell for this malware all kinds of files were
encrypted by the malware and the user was notified about a ransom that
had to be paid to decrypt the files. This could be fixed by reinstalling
the PC and restoring a [backup](/en/howto/backup/) of before the
encryption. As of today, this malware is also detected by F-Secure.
Idea: change your Windows settings [not to hide file
extensions](http://www.pcadvisor.co.uk/how-to/software/how-show-or-hide-file-extensions-3341794/).

16:22
