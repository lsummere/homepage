---
author: petervc
date: 2007-11-20 12:55:00
title: Eerste student en medewerker passen C&CZ wiki aan
---
De handleiding voor het [installeren van het draadloze netwerk onder
Linux](/nl/howto/netwerk-draadloos-handleidinglinux/) is door student
Leon Swinkels aangepast met details voor Ubuntu Gutsy Gibbon en door
medewerker Gerrit Groenenboom voor Suse 10.1.
