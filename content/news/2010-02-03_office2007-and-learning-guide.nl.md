---
author: polman
date: 2010-02-03 18:12:00
title: Office2007 en Learning Guide
---
Afdelingen die graag overstappen van Microsoft Office XP naar 2007 op
hun [Windows beheerde werkplekken](/nl/howto/windows-beheerde-werkplek/),
kunnen hiervoor een verzoek bij C&CZ indienen. Om gebruikers te helpen
bij de migratie, heeft de RU een Nederlandstalige [Learning
Guide](http://sito.science.ru.nl/LearningGuide/Publications/start/default.htm)
aangeschaft, die vanaf het campusnetwerk geraadpleegd kan worden. De
gids bevat ook informatie over Windows7.
