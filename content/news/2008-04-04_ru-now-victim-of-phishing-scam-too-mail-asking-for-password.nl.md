---
author: petervc
date: 2008-04-04 13:08:00
title: Ook RU nu slachtoffer van phishing scam (mail waarin om wachtwoord gevraagd
  wordt)
---
Ook veel RU-medewerkers hebben nu een mail ontvangen, die op het eerste
gezicht afkomstig leek te zijn van een RU Helpdesk, waarin om reactie
met loginnaam en wachtwoord werd gevraagd, omdat anders de mail
afgesloten zou worden. Het antwoord van een onnadenkende naieve
gebruiker ging weer naar een “@hotmail.co.uk” mail-adres. C&CZ hecht
eraan te benadrukken dat \*geen enkele officiele instantie\* u ooit per
email of telefonisch om uw wachtwoord zal vragen, net zomin als om uw
pincode van uw bankrekening. Dit is een vrijwel exacte kopie van het op
29 januari door C&CZ gemelde incident bij de TU Delft. Als iemand toch
zo onverstandig geweest is om loginnaam en wachtwoord te mailen, dan is
het verstandig om snel het wachtwoord te veranderen. C&CZ zal mensen
benaderen die via een C&CZ mailserver gereageerd hebben en heeft tevens
mail naar het betreffende “@hotmail.co.uk” mail-adres nu geblokkeerd op
de C&CZ smtp-servers.
