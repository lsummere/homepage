---
author: mkup
date: 2018-11-22 15:04:00
tags:
- medewerkers
- docenten
- studenten
title: 'RU network maintenance: Monday evening/night November 26 to 27 22:00-03:00
  hours'
---
Monday evening November 26, there will be maintenance on central RU
network equipment: some routers will get a software upgrade. From 22:00
hours, parts of the RU network and central RU-services will not be
available. Within Huygens and other FNWI buildings the internet and
wireless network will not be available from 23:30-00:30 hours. Please
think which processes depend on connectivity and prepare for this
possible outage if necessary. Future maintenance windows can be seen on
the [ISC website](http://www.ru.nl/systeem-meldingen/).
