---
author: theon
date: 2010-03-12 19:15:00
title: Old pc's for sale to Science employees and students
---
In January, all 5 year old pc’s in [pc rooms and study
landscape](/en/howto/terminalkamers/) have been replaced by new ones.
Departments of the Science Faculty have already had the opportunity to
express their interest in these old pc’s. C&CZ still has a few dozen
left, which are now offered to individual employees and students of the
Science Faculty, almost without any guarantee. When interested, contact
C&CZ ([Helpdesk](mailto:Helpdesk@science.ru.nl), Theo Neuij/Mathieu
Bouwens, 20000). Specifications: Dell Optiplex SX280; Pentium 4 dual
core processor 2.8 GHz, 1 GB RAM, 80 GB hard disc, including (attached)
17" TFT-flatscreen, keyboard and mouse.
