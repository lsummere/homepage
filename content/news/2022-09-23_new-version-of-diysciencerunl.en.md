---
author: remcoa
date: 2022-09-23 13:05:00
tags:
- medewerkers
- studenten
title: New version of DIY.science.ru.nl
cover:
  image: img/2022/dhz.png
---
Today the new version of the C&CZ self service site
[DIY](https://diy.science.ru.nl) went live. This version also has a
completely rewritten back-end, which allows new functionality to be
added in the near future. The main new functionality that is available
now, is the possibility of logging in with the Radboud ID (employee
number or student number) in order to set a forgotten Science password.
In case of questions contact [C&CZ](/en/howto/contact/).
