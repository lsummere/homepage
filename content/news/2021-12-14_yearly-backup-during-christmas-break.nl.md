---
author: visser
date: 2021-12-14 14:57:00
tags:
- studenten
- medewerkers
- docenten
title: Jaarlijkse backup in kerstvakantie
---
Zoals elk jaar zal [tijdens de kerstvakantie een volledige backup op
tape](https://wiki.cncz.science.ru.nl/index.php?title=Backup&setlang=nl#.5BMaandelijkse_en_jaarlijkse_backup_naar.5D.5BMonthly_and_yearly_backup_to.5D_tape)
van heel veel servers/bestandssystemen gemaakt worden, die lang bewaard
wordt. Dit zal ervoor zorgen dat servers in deze periode soms trager
reageren.
