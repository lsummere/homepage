---
author: petervc
date: 2014-10-15 17:11:00
tags:
- studenten
- medewerkers
- docenten
title: E-mailadressen en wachtwoorden buitgemaakt
---
De
[CERT-RU](http://www.ru.nl/informatiebeveiliging/ru-professionals/cert-ru-0/)
heeft een
[waarschuwingsmail](http://www.ru.nl/systeem-meldingen/?id=69&lang=nl&tg=0&f=0)
gestuurd naar de 600 ru.nl E-mailadressen die voorkwamen in de [lijst
van 1,3 miljoen buitgemaakte Nederlandse
accounts](http://tweakers.net/nieuws/99018/ruim-miljoen-nederlandse-e-mailadressen-en-wachtwoorden-buitgemaakt.html).
Dat ru.nl E-mailadres is blijkbaar gebruikt als registratie op een of
andere website, die gehackt is. Wachtwoorden die op zo’n gehackte site
gebruikt zijn, moet je nu bekend veronderstellen bij Internetcriminelen.
Daarom moet je dat wachtwoord nergens meer gebruiken. Verder wordt
iedereen aangeraden om verschillende wachtwoorden te gebruiken voor
verschillende websites/applicaties. Vaak hoef je die niet eens te
onthouden, omdat ze eenvoudig gereset kunnen worden. In andere gevallen
kun je een wachtwoordkluis als
[KeePass](http://keepass.info/download.html) gebruiken, zowel voor
Windows, Apple, Android als Linux.
