---
author: petervc
date: 2016-06-22 14:48:00
tags:
- medewerkers
- docenten
- studenten
title: "Printers on ground floor will be switched to Péage"
---
After the [test of Péage on two MFP’s](/en/howto/nieuws/), we expect to
switch all MFP’s on the ground floor and mezzanine of the Huygens
building from the C&CZ print system to the [RU wide
Péage](http://www.ru.nl/peage) early August. The other KMs follow
shortly thereafter. C&CZ will maintain FNWI-specific instructions at
[the C&CZ Péage page](/en/howto/peage/).
