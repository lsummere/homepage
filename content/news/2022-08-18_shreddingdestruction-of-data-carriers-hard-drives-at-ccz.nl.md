---
author: petervc
date: 2022-08-18 11:29:00
tags:
- studenten
- medewerkers
- docenten
title: Versnipperen/vernietigen van datadragers/schijven bij C&CZ
cover:
  image: img/2022/shredded-harddrives.jpg
---
De afdeling IHZ beheert een contract met een hierin gespecialiseerd
extern bedrijf voor het op locatie vernietigen/versnipperen van
datadragers. De beveiligde metalen container die hierbij als tijdelijke
opslag gebruikt wordt, is met ingang van het nieuwe collegejaar bij C&CZ
te vinden. FNWI-medewerkers en -studenten kunnen zelf bij C&CZ
datadragers met gevoelige gegevens in de container deponeren. De C&CZ
helpdesk kan helpen bij het herkennen en verwijderen van datadragers uit
desktops, laptops etc.
