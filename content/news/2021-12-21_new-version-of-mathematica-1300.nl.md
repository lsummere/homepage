---
author: polman
date: 2021-12-21 15:15:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van Mathematica (13.0.0)
---
Er is een nieuwe versie (13.0.0) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden. De installatiebestanden voor Windows, Linux en macOS zijn op
de [Install](/nl/howto/install-share/)-schijf te vinden, ook voor oudere
versies. Licentie- en installatiegegevens zijn bij
[C&CZ](/nl/howto/contact/) te verkrijgen.
