---
author: caspar
date: 2009-12-23 16:48:00
title: Backup tape robots upgraded
---
C&CZ has upgraded its two 5 year old LTO2 (200 Gb/tape uncompressed)
8-Tape Library units to two LTO4 (800 Gb/tape uncompressed) 24-Tape
Library units. We named them *R2D2* and *C3PO*. This upgrade not only
increases the backup capacity by a factor of 12, which is necessary
because more and more data needs to be [backupped](/en/howto/backup/). It
also speeds up the backup and restore times because the drives are
faster and the manual operational overhead is reduced.

In the near future we will be looking for a new destination for our old
but still trustworthy LTO2 units. If you are interested please mail to
postmaster at science.ru.nl.
