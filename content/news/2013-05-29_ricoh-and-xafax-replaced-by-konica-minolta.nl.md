---
author: petervc
date: 2013-05-29 14:21:00
tags:
- medewerkers
- docenten
- studenten
title: Ricoh en Xafax vervanging door Konica Minolta
---
Over enkele weken zullen we o.a. op plaatsen waar nu nog
[Ricoh](/nl/howto/ricoh/)’s staan, een multifunctional (MFP) van de
nieuwe leverancier [Konica Minolta (KM)](/nl/howto/km/) bijplaatsen. Op
de meeste plaatsen zal de Ricoh na een korte overstaptijd verwijderd
worden. Op enkele plaatsen zal nog een Ricoh met Xafax-paal tot juli
beschikbaar blijven, zodat men oude Xafax-kaartjes op kan maken. Omdat
het RU-brede
[Peage](http://www.ru.nl/gdi/voorzieningen/printen-scannen/peage-0/)-project
voor medewerkers erg uitloopt, worden de KM’s op dezelfde manier
aangestuurd als de Ricoh’s. Het enige verschil is dat de Xafax-palen
verdwijnen omdat het contract met Xafax stopt en niet verlengd kan
worden. Kopiëren kan daarom op de KM’s alleen na het intoetsen van een
code. Er zal met afdelingen overlegd worden over de code en op welke
kostenplaats de kopieerkosten geboekt kunnen worden. Wie niet over de
code beschikt, maar wel over printbudget, kan scannen en daarna printen
om iets te kopiëren.
