---
author: petervc
date: 2022-05-24 16:10:00
tags:
- medewerkers
- studenten
title: Test of Ubuntu 22.04 on Linux login server lilo8
cover:
  image: img/2022/ubuntu2204.png
---
As a test of the new version of Ubuntu, 22.04 LTS, we have installed a
new login server `lilo8.science.ru.nl`. In a few months it will replace
the four year old login server `lilo6`. The name `lilo`, that always
points to the newest/fastest login server, will then be moved from the
two year old `lilo7` to the new `lilo8`. The new `lilo8` is a Dell
PowerEdge R6515 with 1 AMD 7502P 2.5GHz 32-core processor and 64GB RAM.
Because we enabled hyper-threading, it is showing 64 processors. For
users who want to check the identity of this new server before supplying
their Science-password to the new server, see [the section about our
loginservers](/en/howto/hardware-servers/). Please [report all
problems](/en/howto/contact/) with this new version of Ubuntu Linux.
