---
author: stefan
date: 2015-08-24 16:16:00
tags:
- medewerkers
- studenten
title: Grootformaat stickers printen
---
Het printen van grootformaat stickers is nu mogelijk op de posterprinter
“[Kamerbreed](/nl/howto/kamerbreed/)”. Het was al mogelijk om posters te
printen op fotopapier en op canvas. De prijs voor het printen van
stickers is € 30 per A0.
