---
author: petervc
date: 2012-01-23 12:40:00
tags:
- studenten
- medewerkers
title: Printer jansteen2 vervangen door escher (HP 9050)
---
De oude zwart-wit printer in de Library of Science, met de naam
`jansteen2` is, omdat er storingen waren, vervangen door een nieuwe
zwart-wit laserprinter met de naam `escher`. Escher is een HP LaserJet
9050.
