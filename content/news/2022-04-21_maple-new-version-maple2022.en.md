---
author: petervc
date: 2022-04-21 21:39:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2022'
cover:
  image: img/2022/maple.png
---
The latest version of [Maple](/en/howto/maple/), Maple2022, for
Windows/macOS/Linux can be found on the
[Install](/en/howto/install-share/) network share and has been installed
on C&CZ managed Linux computers. License codes can be requested from
C&CZ helpdesk or postmaster.
