---
author: mkup
date: 2014-02-25 12:06:00
tags:
- medewerkers
- studenten
title: Aanpassingen Eduroam en bedraad netwerk
---
Naar aanleiding van diverse klachten over de [Eduroam
dekking](http://www.ru.nl/draadloos) in het Huygensgebouw, is een plan
gemaakt, waarbij drie wireless access-points verplaatst en negen stuks
bijgeplaatst worden. De verwachting is dat dit per eind april
gerealiseerd zal zijn.

De geplande [netwerkaanpassingen buiten
werktijd](/nl/howto/recente-storingen/) aan het bedrade netwerk, om het
netwerk geschikt te maken voor [IP telefonie](/nl/tags/telefonie/), zijn
even uitgesteld, omdat bleek dat het netwerk na de aanpassingen minder
stabiel was dan voorheen.

C&CZ verzoekt iedereen om problemen met het draadloze en bedrade netwerk
te melden via netmaster\@science.ru.nl.
