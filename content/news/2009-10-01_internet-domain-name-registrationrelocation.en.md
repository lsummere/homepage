---
author: caspar
date: 2009-10-01 16:58:00
title: Internet domain name registration/relocation
---
C&CZ has recently obtained a partner account at domain name registrar
[QDC](http://www.qdc.nl), allowing us to register a `.nl`, `.com`,
`.org` or `.eu` domain name quickly and easily. The (technical)
administration of such a domain name is controlled by C&CZ (the C&CZ
Internet domain name servers). In this way Radboud University Nijmegen
becomes the owner of the domain name instead of an individual employee
of a department). Relocation of existing domain names to the registrar
QDC is also possible, but will involve some additional paperwork, see
the [domain name registration page](/en/howto/domeinnaam-registratie/)
for additional details.
