---
author: petervc
date: 2016-12-08 18:03:00
tags:
- studenten
title: 'Studieverenigingen: printbudget naar Peage?'
---
Als groepen van studenten die nog C&CZ-printbudget hebben, voor half
december bij [C&CZ](/nl/howto/contact/) aangeven naar welke Peage
accounts ze het overgeboekt willen hebben, dan kan het nog in 2016
afgerond worden. Daarna kan het alleen nog met een eenmalige
betaalopdracht naar een bankrekening, wat minder efficiënt is. Alle FNWI
Konica Minolta MFP’s die doorbelast werden via het
C&CZ-printbudgetsysteem, zijn overgezet naar Peage. Het
C&CZ-printbudgetsysteem wordt alleen nog gebruikt voor de HP-printer
escher (Library of Science), de posterprinter kamerbreed en de [C&CZ
3D-printservice](/nl/howto/printers-en-printen/).
