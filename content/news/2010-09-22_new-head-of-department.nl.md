---
author: petervc
date: 2010-09-22 15:44:00
title: Nieuw afdelingshoofd
---
Per 1 november 2010 zal {{< author "petervc" >}} terug gaan naar een
technische functie als beheerder/ontwikkelaar, hij wordt opgevolgd als
afdelingshoofd C&CZ door {{< author "caspar" >}}.
