---
author: wim
date: 2016-08-08 12:13:00
tags:
- medewerkers
- studenten
title: "C&CZ Printsysteem wordt vervangen door Péage"
---
Péage is het print/scan/copy systeem van de Radboud Universiteit voor
medewerkers en studenten. Algemene informatie is te vinden op de [Péage
website](http://www.ru.nl/peage).

Vanaf 4 augustus wordt het [C&CZ print- en budget
systeem](/nl/howto/printers-en-printen/) gefaseerd vervangen door het
campus brede [Péage print systeem](http://www.ru.nl/peage). U kunt Péage
vanaf nu instellen en het ‘Follow-me’ printen uitproberen op reeds
gemigreerde printers.

Op de [C&CZ Péage pagina](/nl/howto/peage/) geeft C&CZ informatie die
specifiek is voor medewerkers en studenten van FNWI.
