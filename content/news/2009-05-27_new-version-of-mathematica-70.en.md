---
author: polman
date: 2009-05-27 16:54:00
title: New version of Mathematica (7.0)
---
A new version of [Mathematica](/en/howto/mathematica/) (7.0) has been
installed. The CDs can be borrowed by departments that take part in the
license.
