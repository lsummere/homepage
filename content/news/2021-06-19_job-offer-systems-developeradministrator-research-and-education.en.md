---
author: bbellink
date: 2021-06-19 23:50:00
title: Job offer systems developer/administrator research and education
---
C&CZ is looking for a new colleague: systems developer/administrator
research and education. For more info see [the RU job offer
page](https://www.ru.nl/werken-bij/vacature/details-vacature/?recid=1156671&doel=embed&taal=nl).
