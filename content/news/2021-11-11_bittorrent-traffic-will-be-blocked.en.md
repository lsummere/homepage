---
author: simon
date: 2021-11-11 09:59:00
tags:
- studenten
- medewerkers
- docenten
title: BitTorrent traffic will be blocked
---
Because [BitTorrent](https://en.wikipedia.org/wiki/BitTorrent) is
considered to be a security risk (malware/reputation damage) and the
traffic primarily consists of illegal downloads, the RU Security
Operations Center (SOC) proposed to block this type of traffic from the
RU network. Should this cause problems for research or education, we
would like to hear from you, so we can notify the SOC and perhaps
request an exception.
