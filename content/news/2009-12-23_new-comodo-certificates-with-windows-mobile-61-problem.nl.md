---
author: polman
date: 2009-12-23 17:40:00
title: Nieuwe Comodo-certificaten met Windows Mobile 6.1 probleem
---
De [in mei aangekondigde](/nl/howto/nieuws/) Comodo SSL-certificaten
worden nu langzamerhand op de RU in gebruik genomen. De verwachting dat
het “popup-vrije” certificaten zouden zijn, is niet helemaal gehaald:
gebruikers met een smartphone met Windows Mobile 6.1 wordt geadviseerd
om de [aanwezigheid van het Comodo SSL root-certificate te
controleren](http://www.ru.nl/uci/diensten_voor/medewerkers/wireless_ru/verificatie_comodo/).
Indien dit root-certificate niet aanwezig is, kan in de toekomst geen
gebruik gemaakt worden van het beveiligde draadloze netwerk of
beveiligde RU-websites.
