---
author: petervc
date: 2008-01-17 15:22:00
title: Wireless network settings for mobile devices
---
For smartphones or PDA’s with Windows Mobile we now have an
[installation manual](/en/howto/netwerk-draadloos-wm6handleiding/) to get
connected to the wireless network. In order to connect the iPhone or
iPod Touch to the RU campus wireless network, Apple would have to come
with a newer software update than version 1.1.3 of January 2008,
containing support for WPA-Enterprise 802.1x.
