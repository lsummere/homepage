---
author: wim
date: 2008-09-03 11:53:00
title: H:-schijf van studenten verhuisd en vergroot
---
Vanwege de nieuwe [terminalkamer](/nl/howto/terminalkamers/) TK023 met
dual-boot Windows/Linux PC’s zijn alle H:-schijven (homedirectories
onder Linux/Unix) van studentenlogins verhuisd naar een nieuwe server.
Dat gaf ook de mogelijkheid om de [quota](/nl/howto/quota-bekijken/) op
die schijf te verhogen naar 1 GB per student.
