---
author: petervc
date: 2015-06-01 14:04:00
tags:
- medewerkers
- docenten
- studenten
title: Let op\! Oplichters versturen e-mail uit naam van Ziggo
---
Al een paar weken ontvangen mensen e-mails die van kabelmaatschappij
Ziggo afkomstig lijken te zijn, maar die in werkelijkheid phishing mails
zijn, gestuurd door Internetcriminelen. Het onderwerp van de mail is “Uw
betalingstermijn is verstreken”. In de mail wordt gesuggereerd dat de
ontvanger nog een factuur open heeft staan en wordt gedreigd met
incassokosten. De bedoeling van zo’n fraudeur is om de ontvanger op een
link te laten klikken of een bijlage te laten openen. Voor meer info
over deze specifieke phishing mail, zie
[dichtbij.nl](http://www.dichtbij.nl/eindhoven/112/artikel/4027753/let-op-oplichters-versturen-email-uit-naam-van-ziggo.aspx).
Dit keer zijn het valse e-mails uit naam van Ziggo, maar er zijn meer
oplichters actief. Een handige website die informatie verstrekt over
oplichtingspraktijken is [fraudehelpdesk.nl](http://fraudehelpdesk.nl).
