---
author: petervc
date: 2014-10-10 13:33:00
tags:
- medewerkers
title: Veilig netwerk voor kwetsbare pc's aan apparatuur
---
Het komt regelmatig voor dat pc’s aan dure meetapparatuur geen software
updates krijgen, omdat het risico te groot is dat er dan een probleem
met de apparatuur ontstaat. In die gevallen kan [C&CZ
netwerkbeheer](/nl/howto/netwerkbeheer/) helpen, door de apparatuur te
isoleren op een afgeschermd apart netwerk, met specifieke regels voor
welk verkeer van en naar dat stukje netwerk toegestaan is. Het is
mogelijk om data te schrijven naar een netwerkschijf, die vanaf andere
delen van het netwerk gelezen (maar niet geschreven) kan worden. Ook kan
bv. verkeer naar de C&CZ printer-server toegestaan zijn, zodat geen
locale printer nodig is. Afdelingen als Microbiologie, HFML en FELIX
maken al lange tijd gebruik van deze afscherming. Een recente
kwetsbaarheid, die een groot risico is voor Linux-systemen met
achterstallige software, is
[Shellshock](http://en.wikipedia.org/wiki/Shellshock_%28software_bug%29).
Windows-systemen zullen erg kwetsbaar zijn als ze niet de Microsoft
patches hebben die op [Patch
Tuesday](http://en.wikipedia.org/wiki/Patch_Tuesday) gepubliceerd
worden.
