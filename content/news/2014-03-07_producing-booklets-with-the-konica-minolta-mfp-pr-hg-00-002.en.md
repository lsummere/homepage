---
author: wim
date: 2014-03-07 12:12:00
tags:
- medewerkers
- studenten
title: Producing booklets with the Konica Minolta MFP pr-hg-00-002
---
In the [Konica Minolta MFP](/en/howto/copiers/) pr-hg-00-002 a punch unit
and a saddle-unit that can fold have been placed. This makes it possible
to produce complete booklets. One can punch 2 or 4 holes. At maximum 20
sheets can be saddle-stitched and folded, so one can produce an A4
booklet from A3 sheets or an A5 booklet from A4 sheets.
