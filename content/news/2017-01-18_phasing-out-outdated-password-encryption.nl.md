---
author: wim
date: 2017-01-18 14:37:00
tags:
- medewerkers
- studenten
title: Uitfaseren verouderde wachtwoordversleuteling
---
Uit beveiligingsoverwegingen gaat C&CZ een verouderde, inmiddels als
onveilig beschouwde, wachtwoordversleuteling van Science accounts
uitfaseren. Het gaat hierbij om accounts waarvan sinds 1 september 2014
het wachtwoord niet is aangepast. De aanpassing vindt midden februari
2017 plaats.

Indien u meerdere jaren het wachtwoord van uw Science account niet hebt
aangepast, kan het gebeuren dat bepaalde diensten nog gebruik maken van
de oude versleuteling. U kunt hierbij denken aan wachtwoorden om in te
loggen op Linux machines, Eduroam-toegang middels uw science account of
de Science [VPN](/nl/howto/vpn/) server. Om toch gebruik te kunnen
blijven maken van die diensten, moet de oude versleuteling vervangen
worden. Dit kan door het opnieuw instellen van uw Science wachtwoord op
de [Doe het Zelf site](https://dhz.science.ru.nl). Onder het Menu item
‘wachtwoord’ kunt u het wachtwoord instellen. Op diezelfde pagina kunt u
tevens zien wanneer voor het laatst uw wachtwoord is aangepast.
