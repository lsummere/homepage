---
author: petervc
date: 2021-04-20 11:22:00
tags:
- medewerkers
- docenten
- studenten
title: 'Mailman lists sender address change: from sender to list address'
---
[DMARC](https://en.wikipedia.org/wiki/DMARC) is increasingly being used
as a spamfilter, checking whether the sender ought to send mail via this
route. To comply with this for mail sent through
[Mailman](https://wiki.cncz.science.ru.nl/index.php?title=Categorie%3AEmail&setlang=en#.5Bnl.5DMailing_lijsten.5B.2Fnl.5D.5Ben.5DMailing_lists.5B.2Fen.5D),
we will change the From: sender address to the list address. The
current:

`> From: Some User <some.user@somewhere.domain>`
`> Sender: "listname" <listname-bounces@science.ru.nl>`

will be changed to:

`> From: Some User via listname <listname@science.ru.nl>`
`> Reply-To: Some User <some.user@somewhere.domain>`
`> Sender: "listname" <listname-bounces@science.ru.nl>`

If you filter or forward mail automatically, you might need to change
the filter.
