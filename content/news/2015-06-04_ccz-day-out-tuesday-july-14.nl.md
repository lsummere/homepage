---
author: petervc
date: 2015-06-04 15:47:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: dinsdag 14 juli'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor dinsdag 14 juli. Voor
bereikbaarheid in geval van ernstige storingen wordt gezorgd.
