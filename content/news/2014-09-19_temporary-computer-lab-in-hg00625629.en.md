---
author: john
date: 2014-09-19 13:10:00
tags:
- medewerkers
- studenten
title: Temporary computer lab in HG00.625/629
---
Due to the increased number of students, a new [computer
lab](/en/howto/terminalkamers/) will be realized in HG00.625/629. This
space becomes available through the relocation of
[FEZ](http://www.ru.nl/fnwi/fez/) to Mercator 1, fourth floor. Because
no decision has been made about the final layout of wing 6, a temporary
facility will be realized. There will be 42 student and one instructor
PC, type [HP EliteOne 800
G1](http://store.hp.com/NetherlandsStore/Merch/Product.aspx?id=H5U26ET&opt=ABH&sel=PBDT)
(All-in-One, i5-4570S Core, 2.9GHz, 8GB, 23 inch widescreen LCD monitor,
sound), dual boot with Windows and Linux (Ubuntu). This computer lab
should be ready early November.
