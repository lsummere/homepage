---
author: petervc
date: 2020-03-27 11:07:00
tags:
- medewerkers
- studenten
title: New version of Mathematica (12.1.0)
---
A new version of [Mathematica](/en/howto/mathematica/) (12.1.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`. The installation files for Windows and
Linux can be found on the [Install](/en/howto/install-share/)-disk, also
for older versions of Mathematica. Departments that take part in the
license can request installation and license info from C&CZ.
