---
author: simon
date: 2019-12-17 16:15:00
tags:
- medewerkers
- studenten
- ceph
title: 'Nieuwe opslag voor data: Ceph'
---
Sinds kort heeft C&CZ een extra keus voor de opslag van data, naast de
traditionele RAID-opslag op individuele fileservers:
[Ceph](https://wiki.cncz.science.ru.nl/index.php?title=Diskruimte&setlang=nl#Ceph_Storage).
De voornaamste voordelen van
[Ceph](https://en.wikipedia.org/wiki/Ceph_(software)) zijn dat het
vrijwel onbeperkt schaalbaar is en geen single-point-of-failure heeft,
maar juist “self-healing” en “self-managing” is. Binnen de Ceph-storage
zijn nog verschillende keuzes mogelijk, de duurdere zijn zelfs bestand
tegen de uitval van een heel datacenter. De Ceph-opslag kan ook
aangeboden worden als
[S3-storage](https://en.wikipedia.org/wiki/Amazon_S3). Vanwege de
cluster-opzet kan de performance van Ceph-storage verrassend zijn:
schrijven is i.h.a. sneller dan lezen, vooral bij veel kleine bestanden
kan Ceph veel trager zijn dan de traditionele storage. Wie wil proberen
of Ceph voordelen heeft. kan [contact opnemen met
C&CZ](/nl/howto/contact/).
