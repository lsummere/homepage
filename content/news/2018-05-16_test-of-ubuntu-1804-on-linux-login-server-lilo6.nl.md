---
author: wim
date: 2018-05-16 14:16:00
tags:
- medewerkers
- studenten
title: Test van Ubuntu 18.04 met Linux loginserver lilo6
---
Als test voor de nieuwe versie van Ubuntu, 18.04 LTS, hebben we een
nieuwe loginserver beschikbaar, onder de naam `lilo6.science.ru.nl`.
Over enige tijd zal deze de vijf jaar oude loginserver `lilo4`
vervangen. De naam `lilo`, die altijd naar de nieuwste/snelste
loginserver wijst, zal pas dan omgezet worden van de twee jaar oude
`lilo5` naar de nieuwe `lilo6`. De nieuwe `lilo6` is een Dell PowerEdge
R440 met 2 stuks Xeon Silver 4110 CPU @2.10GHz 8-core processoren en
64GB RAM. Met hyperthreading aan lijkt dit dus een 32-processor machine.
Voor wie de identiteit van deze server wil controleren alvorens het
Science wachtwoord aan de nieuwe server te geven, zie [de sectie over
onze loginservers](/nl/howto/hardware-servers/). Alle problemen met deze
nieuwe versie van Ubuntu Linux [graag melden](/nl/howto/contact/).
