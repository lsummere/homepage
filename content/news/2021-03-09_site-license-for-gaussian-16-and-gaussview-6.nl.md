---
author: polman
date: 2021-03-09 23:20:00
tags:
- medewerkers
- studenten
title: Sitelicentie voor Gaussian 16 en GaussView 6
---
Organische Chemie heeft een site licentie aangeschaft voor het gebruik
van [Gaussian 16](https://gaussian.com/gaussian16/) en [GaussView
6](https://gaussian.com/gaussview6/) en de GMMX Module op Linux. Deze
software is op het
[cn-cluster](https://wiki.cncz.science.ru.nl/Hardware_servers#.5BReken-.5D.5BCompute_.5Dservers.2Fclusters)
geïnstalleerd. Afdelingen die hiervan gebruik willen maken, kunnen
contact opnemen met Prof. Jana Roithova of met
[Postmaster](/nl/howto/contact/).
