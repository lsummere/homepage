---
author: polman
date: 2008-06-03 13:34:00
title: Nieuwe versie van Mathematica (6.0.2.1)
---
Er is een nieuwe versie (6.0.2.1) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd. De CD’s zijn te leen
voor afdelingen die meebetalen aan de licentie.
