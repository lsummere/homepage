---
author: petervc
date: 2017-06-22 17:43:00
tags:
- medewerkers
- docenten
- studenten
title: C&CZ bereikbaar via Whatsapp/Telegram/Signal
---
Wanneer men geen GSM-dekking, maar wel een Internet-verbinding heeft,
kan men [C&CZ ook bereiken](/nl/howto/contact/) via
Whatsapp/Telegram/Signal op
{{< figure src="/img/old/telephone.gif" title="telefoon" >}} +31 6 15
35 26 77. De directe aanleiding is dat niet in alle vleugels van het
Huygensgebouw alle mobiele providers goede dekking hebben. Wel is overal
Eduroam wifi aanwezig, waardoor alle medewerkers en studenten C&CZ
kunnen bellen, onafhankelijk van hun mobiele provider. Langzamerhand
gaan ook meer providers
[wifi-bellen](https://www.androidplanet.nl/nieuws/bellen-via-wifi/)
ondersteunen, waardoor de noodzaak van een app als
Whatsapp/Telegram/Signal voor dit doel wegvalt. Op dit moment
ondersteunt alleen Vodafone wifi-bellen, KPN heeft plannen om dit in
2017 te gaan introduceren.
