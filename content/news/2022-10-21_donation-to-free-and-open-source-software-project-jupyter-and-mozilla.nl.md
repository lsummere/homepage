---
title: "Donatie aan vrije en open source software: Project Jupyter en Mozilla"
author: petervc
date: 2022-10-21
tags:
- medewerkers
- studenten
cover:
  image: img/2022/donation-to-free-and-open-source-software-project-jupyter-and-mozilla.png
---
C&CZ maakt voor het merendeel van de services gebruik van [vrije
software](https://nl.wikipedia.org/wiki/Vrije_software) en
[opensourcesoftware](https://nl.wikipedia.org/wiki/Opensourcesoftware).
Daarom is al enige tijd geleden besloten dat de
C&CZ-medewerkers elk jaar stemmen welke projecten een donatie van
C&CZ krijgen. Dit jaar is de keus gevallen op
[Project Jupyter](https://jupyter.org/about) als nieuwe begunstigde.
Project Jupyter is een non-profit, open-source project, geboren uit het IPython Project in 2014 toen
het zich ontwikkelde om interactieve data science en wetenschappelijke berekeningen te ondersteunen
in alle programmeertalen. C&CZ maakt hier gebruik van in de [JupyterHub](https://cncz.science.ru.nl/nl/howto/jupyterhub/) voor FNWI onderwijs.
Als tweede begunstigde is net als vorig jaar [de Mozilla
Foundation](https://foundation.mozilla.org/nl/) gekozen. Veel mensen zullen de
webbrowser [Mozilla
Firefox](https://nl.wikipedia.org/wiki/Mozilla_Firefox) en de mailclient
[Mozilla Thunderbird](https://nl.wikipedia.org/wiki/Mozilla_Thunderbird)
kennen en er misschien elke dag urenlang gebruik van maken. Maar [de
Mozilla Foundation doet nog veel
meer](https://foundation.mozilla.org/nl/what-we-do/) om te proberen te
zorgen dat het internet een publieke hulpbron blijft, open en
toegankelijk voor iedereen.
