---
author: petervc
date: 2016-01-25 18:18:00
tags:
- medewerkers
- studenten
title: Word zelf geen bron van een datalek\!
---
Om zelf geen bron van een datalek met persoonsgegevens te worden, is het
verstandig om na te denken welke persoonsgegevens u van anderen
(collega’s, studenten, …) heeft. Als u ze niet echt nodig heeft, is
verwijderen de veiligste keus. Versleuteling van persoonsgegevens, b.v.
met de gratis software [7zip](http://www.7-zip.org/) (open source) of
[VeraCrypt](https://veracrypt.codeplex.com/), die beide voor
Windows/Linux/MacOSX beschikbaar zijn, is een goede keus, zeker als u de
persoonsgegevens op een mobiele datadrager heeft, zoals een laptop of
een USB-stick. Het bestandsysysteem van smartphones/tablets/laptops kan
vaak ook versleuteld worden
([Windows](http://www.tomsguide.com/us/encrypt-files-windows,news-18314.html),
[Android](http://www.howtogeek.com/141953/how-to-encrypt-your-android-phone-and-why-you-might-want-to/),
[iPad/iPhone](http://searchmobilecomputing.techtarget.com/tip/How-iOS-encryption-and-data-protection-work)),
daarnaast is het vaak mogelijk deze bij verlies of diefstal op afstand
te wissen
([Android](https://support.google.com/accounts/answer/6160500?hl=en),
[iPad/iPhone](https://support.apple.com/kb/ph2701?locale=nl_NL)).
Meldingen van een datalek kunt u doen bij [C&CZ](/nl/howto/contact/),
maar ook 24/7 bij de [ILS helpdesk](/en/howto/contact/@ils-helpdesk).
