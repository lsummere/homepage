---
author: theon
date: 2009-09-28 14:05:00
title: Studentenprinterwijzigingen
---
Naast de printer `jansteen` is een tweede HP4350 zwart-wit laserprinter
`jansteen2` geplaatst. Dit maakt printen eenvoudiger voor studenten: als
er een probleem met een van deze printers is, kan men eenvoudig de
andere kiezen. Reparaties van deze printers kunnen lang duren omdat er
geen onderhoudscontract meer voor is. De HP4350 printers `monet` en
`chagall` zijn verwijderd.
