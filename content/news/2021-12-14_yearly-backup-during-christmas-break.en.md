---
author: visser
date: 2021-12-14 14:57:00
tags:
- studenten
- medewerkers
- docenten
title: Yearly backup during Christmas break
---
Just like every year during the Christmas break, in the next weeks a
[full backup of many servers/filesystems onto
tape](https://wiki.cncz.science.ru.nl/index.php?title=Backup&setlang=en#.5BMaandelijkse_en_jaarlijkse_backup_naar.5D.5BMonthly_and_yearly_backup_to.5D_tape)
will be made, that is preserved a long time. This will have the effect
that servers will sometimes react slower during this period.
