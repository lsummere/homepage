---
author: petervc
date: 2021-12-06 00:24:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2021.2 and license continued'
---
The C&CZ administered “4 concurrent user” license for Maple has been
continued until December 31, 2022. The latest version of
[Maple](/en/howto/maple/), Maple2021.2, for Windows/macOS/Linux can be
found on the [Install](/en/howto/install-share/) network share and has
been installed on C&CZ managed Linux computers. License codes can be
requested from C&CZ helpdesk or postmaster.
