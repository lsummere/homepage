---
author: polman
date: 2009-12-23 17:40:00
title: New Comodo certificates with Windows Mobile 6.1 problem
---
The new Comodo certificates, that were [announced in
May](/en/howto/nieuws/), are gradually being used by RU-servers. The
expectation that these certificates would be “popup free”, has not been
completely fulfilled: users with a Windows Mobile 6.1 smartphone are
advised to [check the presence of the Comodo SSL root
certificate](http://www.ru.nl/uci/diensten_voor/medewerkers/wireless_ru/verificatie_comodo/).
Without this root certificate, usage of the secure wireless network or
secure RU-websites will become impossible.
