---
author: petervc
date: 2008-01-17 15:22:00
title: Draadloze netwerkinstellingen voor mobiele apparaten
---
Voor smartphones of PDA’s met Windows Mobile is er nu een
[handleiding](/nl/howto/netwerk-draadloos-wm6handleiding/) om het
draadloze netwerk te gebruiken. Om met de iPhone of iPod Touch van het
RU draadloze netwerk gebruik te maken zou Apple met een nieuwere
software update dan versie 1.1.3 van januari 2008 moeten komen, met
WPA-Enterprise 802.1x ondersteuning.
