---
author: sommel
date: 2008-05-06 10:50:00
title: Nieuwe multi-functionele Ricoh kopieerapparaten
---
Alle Oce kopieerapparaten zijn nu vervangen door
[Ricoh](/nl/howto/ricoh/) apparaten die ook kunnen
[printen](/nl/howto/printers-en-printen/) en
[scannen](/nl/howto/scanners/). Tevens zijn de oude printers *dali* en
*oersted* uitgefaseerd en is de printer *chagall* verplaatst van
HG00.089 naar HG00.201 (C). Binnenkort zal de printer *picasso* ook
worden uitgezet.
