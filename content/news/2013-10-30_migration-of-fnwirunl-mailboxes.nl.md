---
author: petervc
date: 2013-10-30 17:42:00
tags:
- medewerkers
- docenten
title: Migratie van @fnwi.ru.nl mailboxen
---
Het [ICT Service Center](http://www.ru.nl/isc) (ISC) gaat alle mailboxen
met een `@fnwi.ru.nl` adres op de avond van 20 november migreren van de
oude Exchange 2003 servers naar de nieuwe Exchange 2010 servers. De
aanpassingen op door het ISC beheerde werkplekken zullen automatisch
door het ISC gedaan worden. Voor thuiswerkplekken, smartphones, tablets
en laptops, door C&CZ beheerde werkplekken en werkplekken in eigen
beheer moeten de instellingen handmatig aangepast worden. Vaak zal het
voldoende zijn om de servernaam `outlookweb.ru.nl` te vervangen door
`mail.ru.nl`. In alle gevallen kan C&CZ assistentie verlenen.
