---
author: petervc
date: 2015-01-15 00:04:00
tags:
- medewerkers
- docenten
title: 'SURFdrive: safer than Dropbox'
---
The [ISC](http://www.ru.nl/isc) has
[announced](http://www.ru.nl/ictsecurity/news/index/@978261/surfdrive-saving/)
that [SURFdrive](https://surfdrive.surf.nl/) is available for RU
employees. This gives a maximum of 100 GB of Dropbox-like storage for
work-related files, that should not have the security classification
critical or sensitive. Data is stored within Dutch borders, the users
remain the owners of their own data, and SURF itself provides no
information to third parties, which makes SURFdrive more secure than
Dropbox. [Earlier](/en/howto/nieuws-archief/) SURF made
[SURFfilesender](https://www.surffilesender.nl/) available, with which
you can send larger files more secure than with alternatives like
[WeTransfer](https://www.wetransfer.com).
