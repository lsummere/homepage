---
author: mkup
date: 2014-03-27 14:15:00
tags:
- medewerkers
- studenten
title: Aanpassingen Eduroam 27 maart
---
Het [ISC](http://www.ru.nl/isc) heeft ons meegedeeld dat donderdagavond
27 maart ’s avonds instellingen van de controllers van het [Eduroam
draadloze netwerk](/nl/howto/netwerk-draadloos/) aangepast worden om
problemen op te lossen. Graag [melden bij
netmaster\@science.ru.nl](/nl/howto/contact/) als deze aanpassingen
problemen opgelost hebben of juist problemen lijken te veroorzaken.

Dit staat los van het [eerder aangekondigde](/nl/howto/nieuws/)
bijplaatsen en verplaatsen van wireless access points om per eind april
de dekking in het Huygensgebouw verbeterd te hebben.

C&CZ verzoekt iedereen om problemen met het draadloze en bedrade netwerk
te melden via netmaster\@science.ru.nl.
