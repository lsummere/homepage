---
author: petervc
date: 2008-03-01 00:03:00
title: Oce copiers vervangen door Ricoh copiers/printers/scanners
---
De Oce kopieerapparaten worden vervangen door Ricoh apparaten die ook
kunnen printen en scannen. Er is een
[Ricoh-overzichtspagina](/nl/howto/ricoh/), maar natuurlijk staan ze ook
vermeld op de pagina’s over de [kopieerapparaten](/nl/howto/copiers/), de
[netwerkprinters](/nl/howto/printers-en-printen/) en de
[scanners](/nl/howto/scanners/).
