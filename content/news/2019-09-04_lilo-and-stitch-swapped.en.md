---
author: simon
date: 2019-09-04 16:33:00
tags:
- medewerkers
- studenten
title: Lilo and Stitch swapped
---
As was announced earlier, the linux login (ssh) servers
(lilo5.science.ru.nl and lilo6.science.ru.nl) have swapped their
nicknames, which makes Ubuntu 18.04 the standard instead of Ubuntu
16.04. The name lilo.science.ru.nl now refers to lilo6.science.ru.nl and
stitch.science.ru.nl now refers to lilo5.science.ru.nl. If you get
complaints about changed finger-prints, you know why. See also [the page
about servers](/en/howto/hardware-servers/).
