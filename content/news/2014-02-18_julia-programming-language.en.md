---
author: bram
date: 2014-02-18 10:44:00
tags:
- medewerkers
- studenten
title: Julia programming language
---
The [Julia programming
language](http://en.wikipedia.org/wiki/Julia_%28programming_language%29)
is available as of today on the [Linux login
servers](/en/howto/hardware-servers/). Julia is a high-level dynamic
programming language designed to address the requirements of
high-performance numerical and scientific computing. The most notable
aspect of Julia is its performance, which often comes within a factor of
two of fully optimized C code. If you want Julia to be available on
other computers too, please [contact C&CZ](/en/howto/contact/).
