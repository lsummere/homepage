---
author: petervc
date: 2008-08-20 17:15:00
title: Surfnet Usenet News service gestopt
---
Zoals [in maart al aangekondigd](/nl/howto/nieuws-archief/), heeft
Surfnet de Usenet News service gestopt. Het [UCI nieuwsartikel
hierover](http://www.ru.nl/uci/actueel/nieuws/usenet_news/) somt een
aantal alternatieven op.
