---
author: wim
date: 2012-02-21 11:29:00
tags:
- studenten
- medewerkers
- docenten
title: SPSS19 everywhere, older versions outdated
---
We have installed
[SPSS19](http://www-01.ibm.com/software/analytics/spss/products/statistics/)
on all C&CZ Managed Windows and Linux pc’s. The license for older
versions of SPSS is terminated as of July, 2012. For installation on
privately managed computers of employees and students of the Science
Faculty, you can find the software on the
[Install](/en/howto/install-share/) network share, also for Mac.
