---
author: mkup
date: 2015-03-17 09:56:00
tags:
- medewerkers
- studenten
title: Renewal WiFi network Huygens and Mercator 1
---
During the period between March 23th and April 20th 2015, the WiFi
network (Eduroam and RU-guest) will be renewed in the Huygens building,
its surroundings and in Mercator 1 (floors 0-4). All existing access
points are going to be replaced by a new type and the number of access
points will be expanded to increase the bandwidth, coverage and
capacity. The work involved will be performed by JS Network Solutions,
commissioned by the ISC and under the direction of the UVB. Although the
employees of JS will work mainly in the corridor areas, access to some
offices and/or labs is necessary, of course after consulting the
residents, secretariats and lab-managers. The work will start in Huygens
wing 1 towards wing 8 and will finish in Mercator 1. The renewal of the
WiFi network in the other faculty buildings is expected to take place in
2016. For further information, please contact Marcel Kuppens.
