---
author: bram
date: 2012-02-28 16:53:00
tags:
- studenten
title: Which computer lab has free PCs?
---
Since recently, the website [welke.tk](http://welke.tk) shows which
computer lab has free pc’s. Advantages of this site over the existing
[current usage
page](http://www.ru.nl/studenten/voorzieningen/pc-voorzieningen_0/faculteiten/actuele_bezetting/)
are that it is realtime and that one can immediately see whether a lab
is or will be reserved for lectures. The website
[welke.tk](http://welke.tk) can be viewed on smartphones too. The
website has been created by Bas Westerbaan in cooperation with C&CZ, the
design was made by Judith van Stegeren, Ruben Nijveld helped with mobile
support. The source of the reserved hours is
[Ruuster.nl](http://beta.ruuster.nl).
