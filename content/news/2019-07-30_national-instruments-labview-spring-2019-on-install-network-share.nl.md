---
author: petervc
date: 2019-07-30 14:54:00
tags:
- medewerkers
- docenten
title: National Instruments LabVIEW Spring 2019 op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, is de zojuist binnengekomen versie “Spring 2019”
op de [Install](/nl/howto/install-share/)-netwerkschijf gezet.
Installatiemedia zijn ook te leen. Licentiecodes zijn bij C&CZ helpdesk
of postmaster te verkrijgen.
