---
author: polman
date: 2017-02-21 23:53:00
tags:
- medewerkers
- studenten
title: New license period for Mathematica coming up
---
The current license period for [Mathematica](http://www.wolfram.com)
will end soon, departments interested in using
[Mathematica](/en/howto/mathematica/) in the next contract period of 3
years can [contact](/en/howto/contact/) C&CZ.
