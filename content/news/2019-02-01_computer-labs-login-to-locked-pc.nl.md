---
author: wim
date: 2019-02-01 12:28:00
tags:
- medewerkers
- studenten
title: 'PC-cursuszalen: log in op gelockte pc'
---
Vanaf nu is het mogelijk om een pc in [de pc-cursuszalen, bibliotheek en
studielandschap](/nl/howto/terminalkamers/) in te loggen als een andere
gebruiker de pc gelockt heeft. De hoop is dat dit enigszins bijdraagt om
het tekort aan werkplekken te verminderen. Toen deze pc’s nog Windows7
draaiden, kon men de ingelogde gebruiker na 15 minuten inactiviteit
netjes uitloggen zonder de pc te herstarten, maar deze software werkt
niet meer met Windows10. Als een andere gebruiker inloggen gaat door
linksonder te klikken op “Switch User” op het vergrendelscherm.
