---
author: polman
date: 2016-02-19 17:41:00
tags:
- medewerkers
- studenten
title: 'Science SLURM compute cluster: using idle nodes'
---
The [SLURM configuration](/en/howto/slurm/) of the
[cn-cluster](/en/howto/hardware-servers/) has been changed. This gives
jobs of owners high priority in their own partition (on their own nodes)
and it makes it possible for all users to use all nodes with low
priority. Next to that, all users can submit jobs to run on the C&CZ
purchased node, with shorter running jobs getting higher priority.
