---
author: petervc
date: 2018-05-17 12:34:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van OriginLabPro (2018b, 9.5)
---
Er is een nieuwe versie (2018b, 9.5) van
[OriginLabPro](/nl/howto/originlab/), software voor wetenschappelijke
grafieken en data-analyse,beschikbaar op de
[Install](/nl/howto/install-share/)-schijf. Zie [de website van
OriginLab](https://www.originlab.com/2018b) voor alle info over de
nieuwe versie. De licentieserver ondersteunt deze deze versie.
Afdelingen die meebetalen aan de licentie kunnen bij C&CZ de licentie-
en installatie-informatie krijgen, ook voor standalone gebruik.
Installatie op door C&CZ beheerde PC’s wordt gepland. Afdelingen die
OriginLab willen gaan gebruiken, kunnen zich melden bij
[C&CZ](/nl/howto/contact/).
