---
author: petervc
date: 2010-05-21 12:37:00
title: Reduce mail from \*-studenten-\* mailing lists
---
Student representatives reported to C&CZ that some students would like
to reduce the amount of mails they receive from the [Mailman mailing
lists](/en/tags/email). The mailing lists for students are the umbrella
list
[fnwi-studenten](http://mailman.science.ru.nl/mailman/listinfo/fnwi-studenten),
and the lists per discipline and year that make up the umbrella list.
Therefore C&CZ changed the configuration of the -studenten- mailing
lists, so digests (summaries) for the -students- mailing lists are only
mailed once a week. Of course this only has an effect for students who
have selected to prefer to be mailed a digest instead of receiving every
mail separately. For all other [Mailman
mailinglists](http://mailman.science.ru.nl/) a digest is mailed once a
day. The digest option can be selected per user per mailing list, e.g.
on the -studenten-discipline/year mailing list website, through “change
your subscription options”.
