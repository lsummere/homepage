---
author: wim
date: 2018-05-16 14:16:00
tags:
- medewerkers
- studenten
title: Test of Ubuntu 18.04 on Linux login server lilo6
---
As a test of the new version of Ubuntu, 18.04 LTS, we have installed a
new login server `lilo6.science.ru.nl`. In a few months it will replace
the five year old login server `lilo4`. The name `lilo`, that always
points to the newest/fastest login server, will then be moved from the
two year old `lilo5` to the new `lilo6`. The new `lilo6` is a Dell
PowerEdge R440 with 2 Xeon Silver 4110 CPU @2.10GHz 8-core processors
and 64GB RAM. Because we enabled hyper-threading, it is showing 32
processors. For users who want to check the identity of this new server
before supplying their Science-password to the new server, see [the
section about our loginservers](/en/howto/hardware-servers/). Please
[report all problems](/en/howto/contact/) with this new version of Ubuntu
Linux.
