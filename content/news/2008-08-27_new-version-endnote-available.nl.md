---
author: petervc
date: 2008-08-27 10:37:00
title: Nieuwe versies Endnote beschikbaar
---
Een CD van Endnote versie X2 is beschikbaar voor MS-Windows, versie X1
voor Macintosh. Het kan voor Windows geïnstalleerd worden vanaf de
[install](http://www.cncz.science.ru.nl/software/installscience) netwerk
schijf. De CD’s kunnen ook door medewerkers en studenten geleend worden,
ook voor thuisgebruik. Over enkele weken zal het ook de [Windows-XP
Beheerde Werkplek PCs](/nl/howto/windows-beheerde-werkplek/) beschikbaar
zijn.
