---
author: bram
date: 2014-05-23 17:25:00
tags:
- medewerkers
- studenten
title: Printer queues can be viewed on Do It Yourself page
---
Viewing the combined queue of all printers is now possible on the [DIY
page](/en/howto/dhz/) of C&CZ. Until now, this was only possible using
the command [lpq](http://manned.org/lpq/c2ebe2ea?v=a) on the
C&CZ-managed Ubuntu Linux login servers and workstations. Now this is
possible from all machines using a web browser.
