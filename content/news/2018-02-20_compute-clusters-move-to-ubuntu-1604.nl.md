---
author: polman
date: 2018-02-20 14:35:00
tags:
- medewerkers
- studenten
title: Rekenclusters naar Ubuntu 16.04
---
C&CZ heeft een nieuwe [head
node](https://superuser.com/questions/400927/specific-difference-between-head-node-and-gate-way-to-a-cluster)
voor het [cn-cluster](/nl/howto/hardware-servers/) ingericht met Ubuntu
16.04 en een nieuwe versie van SLURM. Dat maakt het mogelijk om alle
cn-clusternodes te upgraden naar deze nieuwe versies. Ook andere
clusters (coma, neuroinf, mlp) hebben al deze upgrade gehad. Het door
C&CZ beheerde cn-rekencluster bestaat op dit moment uit 60 nodes met
samen 1560 cores en 8 TB RAM. Het coma-cluster van Sterrenkunde, met 920
cores, 600 TB fileserver storage, GPU’s en snelle
[Infiniband](http://nl.wikipedia.org/wiki/InfiniBand) interconnectie is
een apart cluster. Voor meer info, zie de
[cn-rekencluster](/nl/howto/hardware-servers/) pagina.
