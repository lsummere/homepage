---
author: wim
date: 2009-06-16 15:44:00
title: McAfee stopt\! Stap over op F-Secure
---
De datum van het [eerder aangekondigde](/nl/howto/nieuws/) stoppen van de
[McAfee](http://www.mcafee.com) licentie, 1 september 2009, komt
naderbij! Ook op de [Windows Beheerde
Werkplekken](/nl/howto/hardware-bwpc/) wordt McAfee stapsgewijs vervangen
door [F-Secure](http://www.f-secure.com), beginnend met [PC-zalen en
studielandschap](/nl/howto/terminalkamers/). Gebruik door medewerkers en
studenten is ook thuis toegestaan. De F-Secure Client Security software
is op de
[install](http://www.cncz.science.ru.nl/software/installscience)
netwerkschijf te vinden. Licentiecodes kunnen bij C&CZ verkregen worden.
Een goede beveiliging is meer dan een virusscanner, zie bv. de [RU
ICT-beveiliging website](http://www.ru.nl/ict-beveiliging) over het
beveiligen en schoonmaken van een pc.
