---
author: keesk
date: 2008-12-09 16:38:00
title: "Tracelab: chemicaliën boekhouding en bestelling"
---
Vanwege de steeds strengere regelgeving ten aanzien van bestelling en
administratie van chemicaliën is het de bedoeling om per 1 januari 2009
alleen nog [Tracelab](/nl/howto/tracelab/) te gebruiken om chemicaliën te
bestellen en dus bestellingen die direct via [BASS
Finlog](/nl/howto/bass/) worden gedaan te weigeren. Dit geldt voor alle
eenheden van FNWI (ook in de Researchtoren) en voor alle gebruikers die
in de gebouwen van FNWI gehuisvest zijn (dus ook spin-off bedrijven).
