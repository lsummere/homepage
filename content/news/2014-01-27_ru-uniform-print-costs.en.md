---
author: petervc
date: 2014-01-27 16:48:00
tags:
- medewerkers
- studenten
title: RU uniform print costs
---
As of February 1, C&CZ will change the
[[http://wiki.science.ru.nl/cncz/Printers\_en\_printen?setlang=en\#.5BKosten.5D.5BCosts.5D](/en/howto/printers-en-printen/){.uri}
print costs], in agreement with the RU Executive Board decision to make
print costs uniform within Radboud University. The price for black&white
A4 will increase 5.7%. Costs for A4 color, A3 black&white and A3 color
will be lowered by 18%, 54% and 31% respectively. Because we hope that
the C&CZ print system will be replaced by the RU wide
[Peage](/en/howto/peage/) system in the near future, we will not
immediately prioritize to adapt the C&CZ print system to make
single-sided more expensive than double-sided, as is now the case
elsewhere on campus.
