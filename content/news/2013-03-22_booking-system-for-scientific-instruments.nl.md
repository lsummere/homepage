---
author: fmelssen
date: 2013-03-22 14:15:00
tags:
- medewerkers
- docenten
title: Reserveringssysteem voor wetenschappelijke instrumenten
---
In het kader van een samenwerkingsverband tussen FNWI en het UMCN heeft
C&CZ een web-applicatie gebouwd t.b.v. het reserveren van
wetenschappelijke instrumenten. Het gaat hierbij om het instrumentarium
van het [Microscopic Imaging
Centre](http://www.umcn.nl/Research/Departments/cellbiology/Pages/Microscopic%20Image%20Center.aspx)
(MIC), het [Gemeenschappelijk
Instrumentarium](http://www.ru.nl/fnwi/gi/) (GI) en [Organische
Chemie](http://www.ru.nl/imm/research-facilities/research-groups/). De
web-applicatie is te vinden op <https://bookings.science.ru.nl>. Omdat
de door C&CZ ontwikkelde applicatie generiek toepasbaar is, zal deze
binnenkort ook ingezet worden voor het uitleensysteem van
[laptops](/nl/howto/laptop-pool/) via de Library of Science. Indien u een
boekings/reserveringsvraagstuk heeft dat mogelijk opgelost kan worden
met deze applicatie, neem dan s.v.p. [contact op met
C&CZ](/nl/howto/contact/).
