---
author: petervc
date: 2022-04-21 21:39:00
tags:
- studenten
- medewerkers
- docenten
title: 'Nieuwe versie Maple: Maple2022'
cover:
  image: img/2022/maple.png
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2022, is voor
Windows/macOS/Linux te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf en is op door C&CZ
beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen.
