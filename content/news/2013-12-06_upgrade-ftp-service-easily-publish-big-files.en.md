---
author: polman
date: 2013-12-06 18:10:00
tags:
- medewerkers
- docenten
title: 'Upgrade FTP service: easily publish big files'
---
The Science FTP service has been moved to a new server. It can be
accessed through [the original FTP
protocol](ftp://ftp.science.ru.nl/pub) as well as through [the web HTTP
protocol](http://ftp.science.ru.nl). It can be the easiest way to make
big files available to others. On the [server
page](/en/howto/hardware-servers/) one can read how to use the service.
