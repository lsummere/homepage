---
author: petervc
date: 2008-04-02 18:18:00
title: Tijdelijke rekenserver kan enkele weken getest worden
---
Er is een test-machine beschikbaar om te zien of 4-core Intel
processoren geschikt zijn als uitbreiding van het
[cn-cluster](/nl/howto/hardware-servers/). De machine blijft nog wel een
paar weken staan. Als het goed is, kan iedereen met een Science-login er
met ssh op inloggen.

Naam: t4150.science.ru.nl Type: Sun Fire X4150 Processoren: 2 Intel
E5345 CPU’s met ieder 4 cores van 2.33GHz. Memory: 8GB OS: Fedora 7
