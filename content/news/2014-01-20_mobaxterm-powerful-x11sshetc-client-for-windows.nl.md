---
author: petervc
date: 2014-01-20 16:56:00
tags:
- medewerkers
- docenten
- studenten
title: 'MobaXterm: krachtige X11/SSH/enz.-client voor Windows'
---
[MobaXterm](http://mobaxterm.mobatek.net/) is nu beschikbaar op de
[S-schijf](/nl/howto/s-schijf/). Het is een krachtig en eenvoudig
bruikbaar alternatief voor X11/SSH programmatuur als Xming, Xwin32,
WinSCP en Putty. Van de MobaXterm website: “MobaXterm is een verbeterde
terminal voor Windows met een X11 server, een SSH client met tabbladen
en bevat een aantal andere netwerktools voor remote computing (VNC, RDP,
telnet, rlogin). MobaXterm brengt alle essentiële Unix-commando’s naar
het Windows bureaublad, in een enkele exe-bestand dat direct gebruikt
kan worden.” Ook de OpenGL-ondersteuning kan een reden zijn om MobaXterm
te gaan gebruiken. Bij professioneel gebruik dient men een [licentie
voor de Professional
Edition](http://mobaxterm.mobatek.net/download.html) te overwegen.
Eerder werd al gemeld dat [de ondersteuning van X-win32
stopt](/nl/howto/nieuws/) per 31 januari 2014. Indien men nog niet
overgestapt is, is MobaXterm aan te raden.
