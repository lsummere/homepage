---
author: petervc
date: 2012-01-23 12:40:00
tags:
- studenten
- medewerkers
title: Printer jansteen2 replaced by escher (HP 9050)
---
The b/w printer in the Library of Science, that had the name
`jansteen2`, has been replaced by a new b/w laserprinter called
`esxcher`. Escher is an HP LaserJet 9050.
