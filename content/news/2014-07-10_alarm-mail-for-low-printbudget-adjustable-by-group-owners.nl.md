---
author: bram
date: 2014-07-10 17:49:00
tags:
- medewerkers
- studenten
title: Alarmmail voor laag printbudget door groepseigenaren in te stellen
---
De hoogte van het [printbudget](/nl/howto/printbudget/) van een groep,
waarbij automatisch een alarmmail naar de eigenaren gestuurd moet
worden, is vanaf vandaag aan te passen door die eigenaren op de
[Doe-Het-Zelf website](/nl/howto/dhz/).
