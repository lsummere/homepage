---
author: petervc
date: 2008-04-03 18:57:00
title: MS Office 2007 Multi-Language Pack available
---
The CD and 2 dual-layer DVDs (13 GB in total) of the MS Office 2007
Multi-Language Pack can be [borrowed](/en/howto/microsoft-windows/) for
campus use.
