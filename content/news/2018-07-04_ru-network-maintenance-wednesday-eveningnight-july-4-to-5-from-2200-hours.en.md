---
author: mkup
date: 2018-07-04 14:39:00
tags:
- medewerkers
- docenten
- studenten
title: 'RU network maintenance: Wednesday evening/night July 4 to 5 from 22:00 hours'
---
Wednesday evening July 4, there will be maintenance on central RU
network equipment. From 22:00 hours, central RU applications and network
will not be available most of the time. For telephones no disturbance is
expected. For the C&CZ services there is a possibility of disruptions:
no Internet connection for some time. Please think which processes
depend on connectivity to RU central services and prepare for this
possible outage if necessary. Future maintenance windows can be seen on
the [ISC website](http://www.ru.nl/systeem-meldingen/).
