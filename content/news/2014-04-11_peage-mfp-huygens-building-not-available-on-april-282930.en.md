---
author: petervc
date: 2014-04-11 15:20:00
tags:
- studenten
- medewerkers
title: Peage MFP Huygens building not available on April 28/29/30
---
April 28/29/30, the [Peage](/en/howto/peage/) multifunctional
(printer/copier/scanner) and the Peage top-up unit near the restaurant
in the Huygens building cannot be used. The [ISC](http://www.ru.nl/isc)
will investigate then if Peage can also be made available for employees
instead of only for students. On April 28, the MFP will be moved to the
test location, on April 30, it will be moved back.

If you don’t want to read the details below: it is advised to change the
Science password on the [DIY website](http://diy.science.ru.nl). The
same holds for the RU password and other passwords for Internet
services.

April 7, 2014, a two-year-old vulnerability was announced in several
versions of [OpenSSL](https://www.openssl.org/). OpenSSL is used for
encrypting network traffic. Because of this vulnerability, an attacker
could have retrieved the secret key of a service, with which traffic
could be decrypted. In addition to this, due to this vulnerability an
attacker could read the memory of the service, thereby retrieving
sensitive information like passwords.

After the announcement of this [Heart Bleed OpenSSL
leak](http://heartbleed.com/) all vulnerable C&CZ services were
automatically repaired. On Thursday, April 10th, we have deployed new
certificates for these services. The old certificates will be
[revoked](http://nl.wikipedia.org/wiki/Certificate_revocation_list). If
one has used one of these C&CZ services, it is wise to change the
Science password on the [DIY website](http://diy.science.ru.nl). The old
password could have become known to an attacker. For the full list of
C&CZ services that were vulnerable and references to vulnerable external
services, see [the C&CZ certificate page](/en/howto/ssl-certificaten/).
The most used ones are:

-   Mail users:
-   squirrel.science.ru.nl: The old Science webmail service was
    vulnerable since January 29, 2014.
-   roundcube.science.ru.nl: The new Science webmail service.
-   autoconfig.science.ru.nl: The website for automatic configuration of
    mail clients like Thunderbird and Outlook.
-   Lecturers: dossiers.science.ru.nl and eduview.science.ru.nl
-   MySQL database owners: phpmyadmin.science.ru.nl
-   FNWI news letter editors: newsroom.science.ru.nl
