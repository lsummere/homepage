---
author: petervc
date: 2007-12-18 15:59:00
title: "OpenOffice 2.3.1 voor Solaris geïnstalleerd"
---
De nieuwste versie van het kantoorpakket
[OpenOffice.org](http://www.openoffice.org) (2.3.1) is geïnstalleerd
voor Solaris. Tik `ooffice` om het te gebruiken.
