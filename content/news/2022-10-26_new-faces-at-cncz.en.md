---
title: New faces at C&CZ
author: petervc
date: 2022-10-26
tags:
- medewerkers
- studenten
cover:
  image: img/2022/new-faces-at-c-cz.png
aliases:
- 2022-10-26_new-faces-at-c-cz
---
C&CZ is pleased that recent vacancies have been filled. A year ago
[Eric Lieffers](https://www.ru.nl/en/people/lieffers-h) joined us
as systems developer / sysadmin. For years, Eric worked as sysadmin at the Faculty of Social Sciences and at ISC/ILS.
Last month, [Miek Gieben](https://www.ru.nl/en/people/gieben-r) joined the systems developer / sysadmin team and
[Alexander Greefhorst](https://www.ru.nl/en/people/greefhorst-a) joined as developer.
Miek studied Computer Science in Nijmegen and
has now returned to Nijmegen after having worked for a.o. SIDN, Google and Goldman Sachs,
most recent years as Site Reliability Engineer. Alexander is an experienced backend developer,
specialized in complex data structures, he has a background in linguistics. 
