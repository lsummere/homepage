---
author: polman
date: 2010-09-22 19:44:00
tags:
- studenten
- docenten
title: Save settings on B-FAC PC's
---
If you want to save your user settings when logging in on the [PC’s in
study landscape, computer labs, etc.](/en/howto/terminalkamers/), you
have to login to the [Do-It-Yourself website](/en/howto/dhz/) and check
in Profile: “Roaming profile for B-FAC domain”. This makes logging in a
bit slower, because all user settings have to be copied from a server.
The advantage is that all user settings are saved when logging out. An
example of such a user setting is the trust of all “ru.nl” sites in the
[NoScript extension for Firefox](http://noscript.net/), that is
necessary to use e.g. [Blackboard](http://www.ru.nl/blackboard/). New
logins from now on will get a roaming profile in all domains.
