---
author: petervc
date: 2008-04-03 18:18:00
title: Matlab nieuwe versie (R2008a)
---
Met ingang van maart 2008 is er een nieuwe versie (7.6, R2008a) van
[Matlab](/nl/howto/matlab/) beschikbaar op alle door C&CZ beheerde PCs
met Linux of Windows. Voor de Solaris9 Suns is R2007a de laatste versie.
Voor meer info over deze release, zie [de Mathworks
website](http://www.mathworks.com/products/new_products/latest_features.html)
