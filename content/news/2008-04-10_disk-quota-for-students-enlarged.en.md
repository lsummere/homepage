---
author: petervc
date: 2008-04-10 11:31:00
title: Disk quota for students enlarged
---
The [standard disk quota for students](/en/howto/studenten/) have been
enlarged to 400 MB. Students can thus save more files on their
homedirectory (H: disk) which, hopefully, lets them store less files on
their Desktop (or anywhere else in their profile). A large profile makes
log-in and -out slow.
