---
author: simon
date: 2019-02-25 18:08:00
tags:
- medewerkers
- docenten
- studenten
title: Linux login server lilo4 decommissioned
---
The support for Ubuntu 14.04 LTS ends on April 25th, 2019. Therefore we
will turn off lilo4 with this version of Ubuntu on May 1st, 2019. The
default login server (lilo.science.ru.nl) will be redirected to lilo6
with Ubuntu 18.04 LTS after the summer break. The current default
loginserver lilo5.science.ru.nl with Ubuntu 16.04 LTS will remains
available until support for this version ends in 2021. The [signatures
of the C&CZ
loginservers](https://wiki.cncz.science.ru.nl/Hardware_servers#Linux_.5Bloginservers.5D.5Blogin_servers.5D)
can be checked.
