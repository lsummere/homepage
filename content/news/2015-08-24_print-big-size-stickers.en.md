---
author: stefan
date: 2015-08-24 16:16:00
tags:
- medewerkers
- studenten
title: Print big size stickers
---
Printing big size stickers is now possible on the poster printer
“[Kamerbreed](/en/howto/kamerbreed/)”. It was already possible to print
on photo paper and on canvas. The price for printing stickers is € 30
per A0.
