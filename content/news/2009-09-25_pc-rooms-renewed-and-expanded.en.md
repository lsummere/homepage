---
author: petervc
date: 2009-09-25 16:31:00
title: PC-rooms renewed and expanded
---
The more than five year old pc’s in [PC-rooms](/en/howto/terminalkamers/)
HG00.029 en HG02.702 have been replaced by new ones. At the same time
the number of pc’s has been increased by 27 to accommodate the larger
number of students. Both rooms are dual-boot: every pc can be used with
Microsoft Windows or with Fedora Linux. By waiting for the lower prices
of the [standard supplier](/en/howto/huisleverancier-pc's/), that were a
result of the European tender of pc’s, thousands of euro’s could be
saved. The plan is to replace the other about hundred pc’s that were
bought for the pc-rooms in 2004, at the time of the completion of the
first phase of the Huygens building, at the end of 2009.
