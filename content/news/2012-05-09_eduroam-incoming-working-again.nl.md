---
author: petervc
date: 2012-05-09 18:14:00
tags:
- studenten
- medewerkers
- docenten
title: Eduroam inkomend werkt weer
---
Van netwerkbeheer [UCI](http://www.ru.nl/uci) hebben we gehoord dat in
de maand april
[Eduroam-inkomend](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/eduroam/)
vanaf een andere instelling niet werkte. De reden zou een wijziging
geweest zijn van de manier waarop Eduroam-requests bij het UCI
binnenkwamen. Dit is eind april opgelost. Authenticatie kan, zoals
uitgelegd op onze pagina over het [draadloze
netwerk](/nl/howto/netwerk-draadloos/) zowel met U-nummer als met
Science-loginnaam.
