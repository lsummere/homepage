---
author: petervc
date: 2013-11-25 17:42:00
tags:
- medewerkers
- docenten
title: Microsoft support for Windows XP ends, please change a.s.a.p\!
---
From April 8, 2014 [Microsoft will no longer support Windows
XP](http://www.microsoft.com/en-us/windows/enterprise/endofsupport.aspx).
Therefore C&CZ advises to change to a different operating system before
that date. For PC’s within the Faculty of Science, one can change to
C&CZ managed [Windows 7](/en/howto/windows-beheerde-werkplek/) and/or
Ubuntu 12.04. On a Windows7 managed PC, a department can have local
administrator (localadmin) rights. One can also ask C&CZ to [only
install a PC with Windows 7 and/or Ubuntu
12.04](/en/howto/windows-beheerde-werkplek/). Please [contact the C&CZ
helpdesk](/en/howto/contact/) if you want to make use of this. Please
note that if a PC doesn’t have the [required
hardware](/en/howto/windows-beheerde-werkplek/), one will have to buy new
hardware, e.g. upgrade memory or buy a new PC.
