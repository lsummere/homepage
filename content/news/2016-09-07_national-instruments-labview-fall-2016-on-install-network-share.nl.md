---
author: petervc
date: 2016-09-07 18:18:00
tags:
- medewerkers
- docenten
title: National Instruments LabVIEW Fall 2016 op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, is de zojuist binnengekomen versie “Fall 2016”
op de [Install](/nl/howto/install-share/)-netwerkschijf gezet.
Installatiemedia zijn ook te leen. Licentiecodes zijn bij C&CZ helpdesk
of postmaster te verkrijgen.
