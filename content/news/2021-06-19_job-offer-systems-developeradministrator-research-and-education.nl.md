---
author: bbellink
date: 2021-06-19 23:50:00
title: Vacature systeemontwikkelaar/beheerder onderwijs en onderzoek
---
C&CZ is op zoek naar een nieuwe collega: systeemontwikkelaar/beheerder
onderwijs en onderzoek. Voor meer info zie [de RU
vacaturesite](https://www.ru.nl/werken-bij/vacature/details-vacature/?recid=1156671&doel=embed&taal=nl).
