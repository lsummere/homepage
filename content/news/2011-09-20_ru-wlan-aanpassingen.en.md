---
author: mkup
date: 2011-09-20 15:51:00
tags:
- studenten
- medewerkers
- docenten
title: Ru-wlan aanpassingen
---
The [ru-wlan](http://www.ru.nl/draadloos) wireless network can now also
be used with a Science account, using
*loginname*`@science.ru.nl`. Starting October
3, 2011, the [UCI](http://www.ru.nl/uci) is switching off direct
peer-to-peer traffic between wireless client, at first outside the
Huygens building. This will improve the security of the network.
