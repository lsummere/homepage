---
author: simon
date: 2019-12-17 16:15:00
tags:
- medewerkers
- studenten
- ceph
title: 'New data storage: Ceph'
---
Recently C&CZ presented an extra choice for data storage, next to the
traditional RAID storage on individual fileservers:
[Ceph](https://wiki.cncz.science.ru.nl/index.php?title=Diskruimte&setlang=en#Ceph_Storage).
The main advantages of
[Ceph](https://en.wikipedia.org/wiki/Ceph_(software)) are, that is
scales almost without limits and does not have a single point of
failure, but instead is “self-healing” and “self-managing”. Within Ceph
storage, choices can be made, the more expensive ones have no problem
when a complete datacenter goes down. The Ceph storage can also be used
as [S3-storage](https://en.wikipedia.org/wiki/Amazon_S3). Because of the
cluster setup, performance of Ceph storage can be surprising: writing is
in general faster than reading, especially with many small files, Ceph
can be much slower than traditional storage. If you want to try whether
Ceph suits you, please [contact C&CZ](/en/howto/contact/).
