---
author: john
date: 2013-03-15 14:45:00
tags:
- medewerkers
- docenten
title: Veilige afvoer ICT-apparatuur en datadragers
---
Voor de afvoer van ICT-apparatuur (PC’s, laptops, servers, etc.) en
datadragers (harddisks, tapes) met potentieel gevoelige data, is nu een
veilig afvoerkanaal beschikbaar. Deze spullen kunnen aan een medewerker
van het [Logistiek
Centrum](http://www.ru.nl/fnwi/ihz/logistiek/logistiek_centrum/) worden
aangeboden. Het is nu dus niet meer noodzakelijk om eerst zelf harddisks
uit systemen te (laten) verwijderen.
