---
author: petervc
date: 2017-08-19 00:44:00
tags:
- studenten
title: Temporarily free F-Secure security software for RU students
---
From August 21 thru September 21 2017 students of Radboud University may
download F-Secure SAFE free of charge, for their
pc/laptop/smartphone/tablet/iMac/Macbook. For more info, visit the [ISC
student software page](http://www.ru.nl/ict/studenten/software/).
