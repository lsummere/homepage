---
author: stefan
date: 2013-11-20 14:07:00
tags:
- medewerkers
- studenten
title: Poster printer "kamerbreed" repaired
---
The poster printer “[Kamerbreed](/en/howto/kamerbreed/)” is up and
running again. Because spare parts were hard to get repair has taken a
long time.
