---
author: caspar
date: 2011-10-06 12:43:00
tags:
- studenten
- medewerkers
- docenten
title: Update on the transfer of PC's to GDI
---
The [transfer to GDI of the PC’s](/en/howto/gdi/) in the Personnel
Department to GDI is almost done and we have reached an agreement on the
way in which the PC’s in the computer labs, the Library of Science, and
the ‘studielandschap’ will be transferred.
