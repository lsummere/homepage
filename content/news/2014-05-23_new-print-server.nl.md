---
author: wim
date: 2014-05-23 17:14:00
tags:
- medewerkers
- studenten
title: Nieuwe printserver
---
De aansturing van vrijwel alle printers is verhuisd naar een nieuwe
server. Indien men nog printers aankoppelt van de oude “printer-srv”,
zal men een mail krijgen met het verzoek om over te gaan naar de nieuwe
server, zoals vermeld op [de
printerpagina](/nl/howto/printers-en-printen/). Alle problemen [graag
melden](/nl/howto/contact/).
