---
author: petervc
date: 2022-03-22 12:16:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2022a beschikbaar
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2022a, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
hebben. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op alle door
C&CZ beheerde Linux machines is R2022a beschikbaar, een oudere versie
(/opt/matlab-R2021b/bin/matlab) is nog tijdelijk te gebruiken. Op de
door C&CZ beheerde Windows-machines zal Matlab tijdens het semester niet
van versie veranderen om versieafhankelijkheden bij lopende colleges te
voorkomen.
