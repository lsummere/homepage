---
author: john
date: 2013-12-13 22:19:00
tags:
- medewerkers
- docenten
title: BASS Java 7 problem
---
The [ISC](http://www.ru.nl/isc) informed us that after the installation
of patches installed in [BASS](/en/howto/bass/) during the weekend of
December 1, the possibility to use a recent version of Java (version 7)
inadvertently disappeared. The ISC is looking for a solution, but it is
not clear yet when this issue will be resolved.
