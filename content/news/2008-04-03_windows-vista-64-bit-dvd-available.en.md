---
author: petervc
date: 2008-04-03 18:54:00
title: Windows Vista 64-bit DVD available
---
The 64-bit version of MS Vista Enterprise upgrade can be
[borrowed](/en/howto/microsoft-windows/) for campus use, although C&CZ
advises not to use this version due to possible problems with drivers,
network printers etc.
