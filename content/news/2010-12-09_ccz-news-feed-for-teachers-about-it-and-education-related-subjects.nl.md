---
author: pberens
date: 2010-12-09 11:04:00
tags:
- docenten
title: C&CZ RSS Feed voor docenten over ICT en onderwijs
---
[http://wiki.science.ru.nl/cncz/index.php?title=Nieuws&action=feed&lang=nl&feed=rss&tags=docenten](/nl/howto/nieuws/){.uri}

De feed bevat nieuws voor FNWI-docenten over ICT en onderwijs in het
algemeen en E-learning en Blackboard in het bijzonder.
