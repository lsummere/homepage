---
author: bbellink
date: 2018-11-27 11:22:00
tags:
- medewerkers
title: C&CZ draagt telefoonbeheer over aan ISC
---
Telefonie is sinds de overgang naar
[IPT](https://wiki.cncz.science.ru.nl/Categorie:Telefonie#.5BIP_Telefonie_.28IPT.29.5D.5BIP_Telephony_.28IPT.29.5D)
verworden tot een applicatie op het netwerk, waarmee ook het werk
aanzienlijk is verminderd. Daarom draagt C&CZ per 1 januari 2019 het
beheer van de telefoonaansluitingen van FNWI over aan het [RU-centrale
telefoonbeheer van het ISC](http://www.ru.nl/telefonie), die het dan
voor de hele RU zal doen. Voor FNWI-netwerkzaken kunt u wel nog steeds
terecht bij [C&CZ](/nl/howto/contact/).
