---
author: petervc
date: 2012-09-27 11:33:00
title: 'Phishing mail: Update Your Web Mailbox'
---
Recently a number of employees of the Faculty of Science received a
phishing email with the subject “Update Your Web Mailbox !!!” This email
is supposedly sent by “Science.Ru.Nl Administrator” and tries to lure
the recipients into giving away their login credentials. It is not the
first time we see a phishing attack specifically aimed at Science-users
and it will most probably not be the last time either. The mail contains
a link to a shortened URL under <http://goo.gl/> . If you fall for this
and click on the link it leads you to a (now deleted) [Google
Docs](https://docs.google.com/) site. If you are gullible enough to log
in with your Science (or RU) credentials, then these become known to
Internet criminals. This means that your account and all of your files
and email are accessible by those criminals. This can go undetected for
a long time until the user credentials are abused in such a way that the
effects becomes visible. Usually the criminals try to use the user
credentials to send spam via our mail servers, as
[recently](/en/howto/recente-storingen/) happened. This is something we
notice; in such a case C&CZ will disable the user account a.s.a.p. to
prevent further abuse of the account and to prevent our mail servers
from ending up on black lists.
