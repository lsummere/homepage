---
author: petervc
date: 2013-01-03 14:27:00
tags:
- studenten
- medewerkers
title: MS Office 2013 on Install network share
---
The most recent version of [Microsoft
Office](http://office.microsoft.com), 2013, is available for MS-Windows.
It can be found on the [Install](/en/howto/install-share/) network share.
The license permits use on university computers. License codes can be
requested from C&CZ helpdesk or postmaster. One can also order the DVD’s
on [Surfspot](http://www.surfspot.nl).
