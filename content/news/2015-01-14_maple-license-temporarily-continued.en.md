---
author: petervc
date: 2015-01-14 23:37:00
tags:
- studenten
- medewerkers
- docenten
title: Maple license temporarily continued
---
The license of [Maple](/en/howto/maple/) has been temporarily continued
with a 5 user license. This gives [IMAPP](http://www.ru.nl/imapp/) and
[C&CZ](/) some time to find other departments that are willing to
contribute financially to such a license. This would cost 900 € per year
excl. VAT. Interested? Please contact [C&CZ](/en/howto/contact/).
