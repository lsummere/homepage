---
author: petervc
date: 2008-08-20 17:15:00
title: Surfnet Usenet News service ended
---
As was [announced in March](/en/howto/nieuws-archief/), Surfnet ended the
Usenet News service. The [UCI news article on this
subject](http://www.ru.nl/uci/actueel/nieuws/usenet_news/) lists a
number of alternatives.
