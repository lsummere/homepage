---
author: mkup
date: 2013-01-17 10:59:00
tags:
- studenten
- medewerkers
title: New Radius server for ru-wlan and eduroam (wireless)
---
On Monday, January 28th 2013 at 8:00 am, one of the servers that is
being used by the wireless network of the RU, will be replaced. This
replacement will affect you as a user of the wireless networks ru-wlan
and eduroam. Further info can be found on our [Service interruptions
page](/en/howto/recente-storingen/).
