---
author: petervc
date: 2014-06-20 12:25:00
tags:
- medewerkers
title: Oude financieel systeem FNWI na acht jaar uitgezet
---
Bij de invoering van [BASS](/nl/howto/bass/) in 2006 is besloten om de
gegevens uit het toenmalige financiële systeem van FNWI, Airbase, niet
over te nemen. Vanwege belastingverplichtingen en om het mogelijk te
houden om details van oude inkooporders in te zien, heeft C&CZ deze
systemen tot in 2014 in de lucht moeten houden. Nu deze systemen
uitgefaseerd zijn, kan ook de domeinnaam `dbf.kun.nl`, die nog herinnert
aan het Directoraat B-Faculteiten van de Katholieke Universiteit
Nijmegen, verwijderd worden.
