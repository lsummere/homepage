---
author: caspar
date: 2009-01-16 14:39:00
title: Afdelingscontactpersonen i.p.v. eerstelijns ondersteuners
---
De titel “eerstelijnsondersteuner” voor de
aanspreekpunten/contactpersonen voor C&CZ in een vakgroep/afdeling bleek
in overleg met anderen op de campus vaak verkeerd geinterpreteerd te
worden, omdat in het algemeen computerondersteuningsgroepen zichzelf als
eerstelijns- en/of tweedelijnsondersteuners betitelden. Daarom wordt op
de C&CZ wiki nu de term
“[afdelingscontactpersoon](/nl/howto/:category:contactpersonen/)” (en
afgeleide termen) gehanteerd.
