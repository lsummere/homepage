---
author: petervc
date: 2011-12-05 11:10:00
tags:
- studenten
- medewerkers
- docenten
title: Reageer nooit op mail met vraag naar wachtwoord\!
---
Een paar dagen geleden heeft een van onze gebruikers geantwoord op een
mail die zogenaamd van “support” kwam en vroeg naar loginnaam en
wachtwoord. Het gevolg was dat even later dit account misbruikt werd om
via onze mailservers honderduizenden spam-mails de wereld in te sturen.
Daarom is onze uitgaande mailserver op zwarte lijsten beland, van o.a.
Windows Live Hotmail. Daarom hebben we vanochtend het IP-nummer van deze
machine aangepast, wat de snelste manier is om de server van de lijst af
te krijgen. Les: Reageer nooit op een mail die vraagt naar loginnaam en
wachtwoord! Dat geeft veel overlast voor ons en andere gebruikers.
