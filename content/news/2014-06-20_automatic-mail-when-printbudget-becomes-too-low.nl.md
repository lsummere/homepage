---
author: polman
date: 2014-06-20 10:59:00
tags:
- medewerkers
- studenten
title: Automatische mail bij te laag printbudget
---
Wanneer het [printbudget](/nl/howto/printbudget/) onvoldoende is, worden
printjobs niet meer verwerkt. Omdat men niet altijd snel weet dat dit de
oorzaak is, gaat C&CZ een mail sturen wanneer het printbudget te laag
wordt. Bij een persoonlijk budget wordt [de mail](/nl/howto/emailcodes/)
naar het persoonlijke mail-adres gestuurd. Bij een groepsbudget wordt
[de mail](/nl/howto/emailcodes/) naar de eigenaren van de budgetgroep
gestuurd. Stuur een mail naar postmaster als u al eerder gewaarschuwd
wilt worden. Vermeld hierbij de naam van de budgetgroep en de hoogte van
het printbudget waarbij u een mail wilt krijgen.
