---
author: petervc
date: 2009-02-06 13:15:00
title: Software licenses for employees and students of RU
---
Because there are many licenses for software, with different usage rules
for employees, students, on campus and at home, the
[UCI](http://www.ru.nl/uci) published a (Dutch) [news article about
ordering
software](http://www.ru.nl/uci/actueel/nieuws/software_bestellen/). A
lot of information about software (licenses) can also be found through
the [C&CZ software page](/en/tags/software), e.g. about [software for
computers running Microsoft Windows](/en/howto/microsoft-windows/).
