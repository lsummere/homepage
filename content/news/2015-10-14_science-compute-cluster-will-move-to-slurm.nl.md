---
author: polman
date: 2015-10-14 15:45:00
tags:
- medewerkers
- studenten
title: Science rekencluster gaat over op SLURM
---
De door C&CZ beheerde [rekenclusters](/nl/howto/hardware-servers/) zullen
gefaseerd overgaan op het gebruik van [SLURM](/nl/howto/slurm/) als
clustersoftware. Daarmee wordt dan
[Torque](http://www.adaptivecomputing.com/products/open-source/torque/)
/ [MAUI](http://www.adaptivecomputing.com/products/open-source/maui/) en
het nog oudere [Sun/Oracle
GridEngine](http://en.wikipedia.org/wiki/Oracle_Grid_Engine)
uitgefaseerd. Met grote dank aan Pim Schellart van Sterrenkunde die [de
documentatie](/nl/howto/slurm/) verzorgd heeft.
