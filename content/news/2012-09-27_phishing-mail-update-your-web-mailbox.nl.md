---
author: petervc
date: 2012-09-27 11:33:00
title: 'Phishing mail: Update Your Web Mailbox'
---
Diverse medewerkers van FNWI hebben de afgelopen dagen een phishing mail
ontvangen met als onderwerp “Update Your Web Mailbox !!!” De mail is
ondertekend door “Science.Ru.Nl Administrator” en probeert de ontvanger
ertoe te verleiden zijn logingegevens prijs te geven. Het is niet de
eerste keer dat zo’n phishing attack specifiek gericht is op
Science-gebruikers en zal vast niet de laatste keer zijn. In de mail
staat een link naar een verkorte URL onder <http://goo.gl/> . Wanneer je
erin trapt en op de link klikt, dan kom je op een (inmiddels
verwijderde) [Google Docs](https://docs.google.com/) site terecht. Als
je dan onnozel genoeg bent om in te loggen met Science (of RU)
logingegevens, dan zijn die op dat moment bekend bij
internet-criminelen. D.w.z. dat zij toegang hebben tot je account en
daarmee al je bestanden en mail. Dit kan lang onopgemerkt blijven totdat
er op een zichtbare manier misbruik van de logingegevens wordt gemaakt!
Meestal zal men met de logingegevens proberen om spam te versturen via
onze mailservers, zoals [onlangs](/nl/howto/recente-storingen/) gebeurde.
Dit valt op; C&CZ zal in zo’n geval de gecompromitteerde login z.s.m.
uitzetten, om verder misbruik te voorkomen en te voorkomen dat onze
mailservers op zwarte lijsten komen.
