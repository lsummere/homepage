---
author: mkup
date: 2018-09-25 10:23:00
tags:
- medewerkers
- docenten
- studenten
title: 'RU network maintenance: Monday evening/night October 1 to 2 22:00-03:00 hours'
---
Monday evening October 1, there will be maintenance on central RU
network equipment: all routers get a software upgrade. From 22:00 hours,
parts of the RU network will not be available, a.o. wireless and
Internet access. The network in Huygens and other FNWI buildings will
not be available from 01:00-02:00 hours. Also C&CZ services will be
disrupted. Please think which processes depend on connectivity to RU
central services and prepare for this possible outage if necessary.
Future maintenance windows can be seen on the [ISC
website](http://www.ru.nl/systeem-meldingen/).
