---
author: bram
date: 2021-12-07 15:29:00
tags:
- studenten
- medewerkers
- docenten
title: GitLab Pages installed
---
C&CZ recently installed a GitLab Pages server. With GitLab Pages, you
can publish static websites directly from a repository in
[GitLab](/en/howto/gitlab/). To publish a website with Pages, you can use
any static site generator, like Hugo, Harp, Mkdocs, or Hexo. You can
also publish any website written directly in plain HTML, CSS, and
JavaScript. More info and documentation in the [GitLab Pages
documentation](https://gitlab.science.ru.nl/help/user/project/pages/index.md)
