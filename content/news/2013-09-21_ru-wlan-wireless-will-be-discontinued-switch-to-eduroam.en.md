---
author: mkup
date: 2013-09-21 16:01:00
tags:
- studenten
- medewerkers
- docenten
title: Ru-wlan wireless will be discontinued, switch to Eduroam
---
In all buildings of the RU the [Eduroam wireless
network](/en/howto/netwerk-draadloos/) is now available. Therefore the
wireless network ru-wlan will be discontinued on November 1, 2013.
Please switch to Eduroam as soon as possible. One can logon with *U-* or
*S-number*\@ru.nl and RU-password or with
*science-username*\@science.ru.nl and the Science password. Note:
Laptops with Windows 8 sometimes do not connect automatically to
Eduroam. If this is the case choose “Microsoft: Protected EAP (PEAP)” at
the prompt “Choose network authentication” during configuration.
Wireless network [configuration manuals](http://www.ru.nl/draadloos/)
for various devices and operating systems are available on the [IT
Servicecenter](http://www.ru.nl/isc) website.
