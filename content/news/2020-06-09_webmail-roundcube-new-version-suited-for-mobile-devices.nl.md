---
author: polman
date: 2020-06-09 23:05:00
tags:
- medewerkers
- docenten
- studenten
title: 'Webmail: Roundcube nieuwe versie geschikt voor mobiel'
---
De webmail-omgeving [Roundcube](https://roundcube.science.ru.nl), heeft
een grote update gekregen, met ook een andere gebruikersinterface,
geschikt voor smartphones. Ook het hersturen/bouncen van mails is nu
mogelijk. Voor alle nieuwe functionaliteit, zie [de website van het
project](https://roundcube.net/). Roundcube heeft allerlei
mogelijkheden, zoals het beheren van het delen van mailmappen met andere
gebruikers en het filteren van mail direct bij binnenkomst op de server.
Zie voor meer info onze [mail pagina](/nl/tags/email/).
