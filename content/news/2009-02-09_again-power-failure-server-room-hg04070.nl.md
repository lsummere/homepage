---
author: _josal
date: 2009-02-09 12:31:00
title: Opnieuw stroomstoring serverruimte HG04.070
---
Op zaterdag 31 januari 2009, ’s nachts rond 03:00 uur is
brandbeveiliging aan de stroomvoorziening van de computerruimte HG04.070
wederom geactiveerd, waardoor de stroomvoorziening volledig afgeschakeld
werd. Ook deze keer was er geen sensor die een te hoge temperatuur heeft
gedetecteerd.

Er is geen extra informatie beschikbaar gekomen die een meer definitieve
aanwijzing kan geven over de oorzaak van de laatste ‘Noodstop’ van de
UPS!

Om een nieuw onterecht brandalarm te voorkomen, kan alléén één (1)
tijdelijk geplaatste, mechanische temperatuur-schakelaar de ‘Noodstop’
veroorzaken.

Het brand-beveiliging-schakelsysteem met de 4 temperatuur sensoren is
afgekoppeld van de UPS-noodstop. Het systeem is wel werkend en nog
steeds aangesloten op het gebouw-bewaking-systeem.

Er is ontdekt dat bij elke toegangsdeur een brand-sleutel-schakelaar
aanwezig was, direct aangesloten op ‘Noodstop’. Deze schakelaars werden
niet door het gebouwsysteem bewaakt. De schakelaars zijn afgekoppeld van
de ‘Noodstop’ en worden nu wél middels het gebouwsysteem bewaakt.

Met deze opzet wordt bereikt:

-   dat er nog steeds een werkende brandbeveiliging is, met een tragere
    sensor op één (1) plaats (nabij de hot-spot).
-   dat een storing in het oude circuit of in de deurschakelaars wordt
    gedetecteerd, maar zeker géén noodstop veroorzaakt.
-   dat het ook duidelijk is wanneer een probleem in de UPS de
    ‘Noodstop’ veroorzaakt.

Afhankelijk van de toekomstige alarmen zullen nieuwe gegevens
beschikbaar komen die inzicht geven in de problematiek.

Bij uitblijven van enig alarm, is afgesproken dat de huidige inrichting
tenminste twee maanden in bedrijf blijft.
