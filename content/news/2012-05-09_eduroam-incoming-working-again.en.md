---
author: petervc
date: 2012-05-09 18:14:00
tags:
- studenten
- medewerkers
- docenten
title: Eduroam incoming working again
---
The [UCI](http://www.ru.nl/uci) network administrators told us that in
the month April
[Eduroam-incoming](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/eduroam/)
from a different Eduroam institution did not work. Reason for this was a
change in the way Eduroam-requests entered the UCI. This has been
corrected at the end of April. Authenticating to Eduroam, as explained
on our [wireless network](/en/howto/netwerk-draadloos/) page, is possible
with U-number and with Science loginname.
