---
author: wim
date: 2015-11-24 12:25:00
tags:
- medewerkers
- studenten
title: 'Nieuwe VPN: vpnsec.science.ru.nl'
---
Per direct is een nieuwe [VPN](/nl/howto/vpn/)-service beschikbaar via de
server vpnsec.science.ru.nl, de service is gebaseerd op
[IPsec](https://nl.wikipedia.org/wiki/IPsec). De bedoeling is dat alle
gebruikers voor 1 maart 2016 overgestapt zijn naar de nieuwe VPN. De
oude VPN, op basis van
[PPTP](https://nl.wikipedia.org/wiki/Point-to-Point_Tunneling_Protocol),
kan dan uitgezet worden.
