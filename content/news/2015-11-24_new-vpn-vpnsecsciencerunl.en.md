---
author: wim
date: 2015-11-24 12:25:00
tags:
- medewerkers
- studenten
title: 'New VPN: vpnsec.science.ru.nl'
---
A new [VPN](/en/howto/vpn/) service, with server vpnsec.science,ru.nl,
based on [IPsec](https://en.wikipedia.org/wiki/IPsec) is available. We
intend to have all users moved over to the new VPN before March 1, 2016
and then terminate the old VPN based on
[PPTP](https://en.wikipedia.org/wiki/Point-to-Point_Tunneling_Protocol).
