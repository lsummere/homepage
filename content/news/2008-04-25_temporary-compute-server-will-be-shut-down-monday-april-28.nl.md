---
author: petervc
date: 2008-04-25 12:23:00
title: Tijdelijke rekenserver wordt maandag 28 april uitgezet
---
De “t4150” test-machine met 4-core Intel processoren (Sun Fire X4150,
E5345 CPU’s) als mogelijke uitbreiding van het
[cn-cluster](/nl/howto/hardware-servers/) wordt maandag 28 april
uitgezet. Er zal nog een hardware-RAID test en een eenvoudige
herinstallatie plaatsvinden, daarna wordt de machine terug naar Sun
verzonden.
