---
author: wim
date: 2008-03-03 14:47:00
title: Samba updates met problemen?
---
C&CZ zal de komende weken Samba upgraden op diverse servers. Deze
upgrades kunnen enige overlast veroorzaken bij het aankoppelen van
netwerkschijven op uw Windows PC. Bij [Upgrade Samba
3.0.28](/nl/howto/upgrade-samba-3.0.28/) kunt u lezen hoe u de problemen
zelf kunt oplossen.
