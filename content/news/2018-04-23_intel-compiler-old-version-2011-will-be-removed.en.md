---
author: petervc
date: 2018-04-23 16:28:00
tags:
- medewerkers
- studenten
title: 'Intel compiler: old version (2011) will be removed'
---
In 2014 C&CZ bought two licences for concurrent use of the 2014 version
of the [Intel Cluster Studio for
Linux](http://software.intel.com/en-us/articles/intel-cluster-studio-xe/)
which can be found in `/vol/opt/intelcompilers`. Therefore the old
(2011) version will be reoved on May 1, 2018, after which the old
license server can be removed.
