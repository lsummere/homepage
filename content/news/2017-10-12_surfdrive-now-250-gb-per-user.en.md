---
author: petervc
date: 2017-10-12 15:35:00
tags:
- medewerkers
- docenten
title: 'SURFdrive: now 250 GB per user'
---
In [SURF
News](https://www.surf.nl/en/news/2017/10/from-now-on-up-to-250-gigabytes-of-storage-on-surfdrive.html)
we read that as of October 9, the storage space for users of
[SURFdrive](https://www.surfdrive.nl/) has been expanded from 100 GB to
250 GB. SURFdrive is a Dropbox-like storage for work-related files, that
should not have the security classification critical or sensitive. Data
is stored within Dutch borders, the users remain the owners of their own
data, and SURF itself provides no information to third parties, which
makes SURFdrive more secure than Dropbox.
