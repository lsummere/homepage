---
author: petervc
date: 2012-01-27 13:11:00
tags:
- studenten
- medewerkers
- docenten
title: Maple 15
---
A new version (15) of [Maple](/en/howto/maple/) has been installed on
Linux. Soon it will also be available on the [Windows-XP Managed
PCs](/en/howto/windows-beheerde-werkplek/). It can be installed on
Windows (64-bit of 32-bit), Mac or Linux (64-bit or 32-bit) from the
[Install](/en/howto/install-share/) network disk. The license codes can
be requested from postmaster. Maple is also licensed for home use by
employees and students.
