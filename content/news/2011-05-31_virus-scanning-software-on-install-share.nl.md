---
author: petervc
date: 2011-05-31 14:37:00
tags:
- medewerkers
- docenten
title: Virus scanner software op install schijf
---
Tijdens de ICT beveiligingscampagne werden USB-sticks met
virusscanner-software en speelkaarten van [Cybersave
Yourself](http://www.surfnet.nl/nl/Thema/cybersafe/Pages/Default.aspx)
uitgedeeld. De [virusscanner software van de
USB-stick](/nl/howto/ict-beveiligingscampagne/) is naar de [Install
share](/nl/howto/install-share/) gekopiëerd.
