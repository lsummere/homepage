---
author: john
date: 2013-12-13 22:19:00
tags:
- medewerkers
- docenten
title: BASS probleem met Java 7
---
Het [ISC](http://www.ru.nl/isc) heeft ons meegedeeld dat bij de
invoering van technische patches voor [BASS](/nl/howto/bass/) in het
weekend van 1 december, de mogelijkheid om te werken met een recente
Java versie (versie 7) abusievelijk is weggevallen. Er wordt gewerkt aan
een oplossing, maar het is nog onduidelijk wanneer die er zal zijn.
