---
author: caspar
date: 2010-11-20 21:05:00
title: Nieuwe mail/agenda-service Share
---
Het UCI heeft een nieuwe mail- en agenda-service voor de RU ontwikkeld.
De nieuwe service heet Share en is gebaseerd op het open standards
product Zimbra. Alle RU studenten en een deel van de RU medewerkers
maakt al gebruik van Share. Voordat Share voor alle FNWI-medewerkers
ingevoerd kan worden, moeten de FNWI Exchange accounts gemigreerd zijn.
Dat kan pas wanneer een aantal tekortkomingen in Share is opgelost. Er
is nog geen planning voor de invoering van Share bij FNWI te geven.
