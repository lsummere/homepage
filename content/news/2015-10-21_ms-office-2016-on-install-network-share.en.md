---
author: wim
date: 2015-10-21 16:05:00
tags:
- medewerkers
- docenten
title: MS Office 2016 on Install network share
---
The most recent version of [Microsoft
Office](http://office.microsoft.com), 2016, is available on the
[Install](/en/howto/install-share/) network share. The license permits
installation of this version on university computers. License codes can
be requested from C&CZ helpdesk or postmaster. For privately owned
computers you can order this software relatively cheap at
[Surfspot](http://www.surfspot.nl).
