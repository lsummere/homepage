---
author: polman
date: 2008-02-20 11:53:00
title: Nieuwe licentie periode Mathematica aanstaande
---
De huidige licentie periode voor [Mathematica](http://www.wolfram.com)
loopt binnenkort af, afdelingen die geinteresseerd zijn in het gebruik
van [Mathematica](/nl/howto/mathematica/) voor de nieuwe 3-jarige
contractperiode kunnen contact opnemen met C&CZ.
