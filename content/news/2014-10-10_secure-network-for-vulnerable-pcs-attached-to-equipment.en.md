---
author: petervc
date: 2014-10-10 13:33:00
tags:
- medewerkers
title: Secure network for vulnerable pcs attached to equipment
---
Often PCs attached to expensive measuring equipment do not get software
updates, because the risk is too high that afterwards there is a problem
with the equipment. In those cases [C&CZ network
administration](/en/howto/netwerkbeheer/) can help by isolating the PCs
on a separate shielded network, with specific rules for the network
traffic allowed to and from that network segment. It is possible to
write data to a network drive, that can be read (but not written) from
other locations. Also traffic to the C&CZ printer server can be allowed,
so no local printer is necessary. Departments such as Microbiology, HFML
and FELIX have been using this shield for years. A recent vulnerability
that is a huge risk for for Linux systems without software updates, is
[Shellshock](http://en.wikipedia.org/wiki/Shellshock_%28software_bug%29).
Windows systems will be very vulnerable unless they have the Microsoft
patches that are published on [Patch
Tuesday](http://en.wikipedia.org/wiki/Patch_Tuesday).
