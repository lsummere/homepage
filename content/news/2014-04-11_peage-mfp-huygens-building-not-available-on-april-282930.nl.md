---
author: petervc
date: 2014-04-11 15:20:00
tags:
- studenten
- medewerkers
title: Peage MFP Huygensgebouw 28/29/30 april niet beschikbaar
---
De [Peage](/nl/howto/peage/)-multifunctional
(printer/kopieerapparaat/scanner) en de Peage-betaalautomaat bij het
restaurant in het Huygensgebouw zullen op 28/29/30 april niet gebruikt
kunnen worden. Het [ISC](http://www.ru.nl/isc) gaat dan onderzoeken of
Peage ook geschikt gemaakt kan worden voor medewerkers i.p.v. alleen
maar voor studenten. Op 28 april wordt de MFP verhuisd naar een
testlocatie, op 30 april komt hij terug.

Wie niet alle details hieronder wil lezen: Het wordt aangeraden om het
Science wachtwoord op de [DHZ website](http://dhz.science.ru.nl) aan te
passen. Hetzelfde geldt voor het RU wachtwoord en wachtwoorden van
andere Internet services.

Op 7 april 2014 werd bekend dat er enkele jaren een kwetsbaarheid (lek)
heeft gezeten in een aantal versies van
[OpenSSL](https://www.openssl.org/). OpenSSL wordt gebruikt voor het
versleutelen van netwerkverkeer. Door dit lek kon een aanvaller de
geheime sleutel van een service ophalen, waarmee versleuteld verkeer
ontsleuteld kon worden. Daarnaast kon door dit lek een aanvaller het
complete geheugen van de service lezen, waarin gevoelige informatie als
wachtwoorden kon staan.

Na het bekend worden van dit [Heartbleed OpenSSL
lek](http://heartbleed.com/) zijn alle kwetsbare C&CZ services
automatisch gerepareerd. Daarna zijn op donderdag 10 april nieuwe
certificaten ingezet. De oude certificaten worden [ongeldig
verklaard](http://nl.wikipedia.org/wiki/Certificate_revocation_list).
Indien men een van deze C&CZ services gebruikt heeft, is het verstandig
om het Science wachtwoord op de [DHZ website](http://dhz.science.ru.nl)
aan te passen. Het oude wachtwoord zou namelijk bij een aanvaller bekend
kunnen zijn geworden. Voor een volledig overzicht van C&CZ services die
kwetsbaar waren en verwijzingen naar lijsten van kwetsbare externe
services, zie [de C&CZ certificatenpagina](/nl/howto/ssl-certificaten/).
De meest gebruikte zijn:

-   Mailgebruikers:
-   squirrel.science.ru.nl: De oude Science webmailservice was kwetsbaar
    sinds 29 januari 2014.
-   roundcube.science.ru.nl: De nieuwe Science webmailservice.
-   autoconfig.science.ru.nl: De website voor automatische configuratie
    van mailprogramma’s als Thunderbird en Outlook.
-   Docenten: dossiers.science.ru.nl en eduview.science.ru.nl
-   MySQL database eigenaren: phpmyadmin.science.ru.nl
-   FNWI nieuwsbrief editors: newsroom.science.ru.nl
