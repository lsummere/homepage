---
author: petervc
date: 2015-01-20 12:24:00
tags:
- studenten
- medewerkers
title: Adobe Creative Cloud software
---
Adobe Creative Cloud is [the successor to Creative
Suite](http://www.cnet.com/news/adobe-kills-creative-suite-goes-subscription-only/).
The [Adobe Creative Cloud
software](https://www.adobe.com/nl/creativecloud/catalog/desktop.html)
is available on the [Install](/en/howto/install-share/) network share.
License codes are available from C&CZ helpdesk or postmaster. According
to the site license, this Adobe Creative Cloud for Enterprise (Specified
Apps Only Non-Video) may be installed on all computers owned by the RU
without additional costs. For home use of this software, staff and
students can visit [SurfSpot](http://www.surfspot.nl). The software
consists of the following components: Photoshop, Illustrator, InDesign,
Dreamweaver, Muse Pro, Acrobat Pro, Bridge, Encore, Fireworks, Flash
Builder Premium, InCopy, Media Encoder, Edge Animate, Edge Code
(preview) and Edge Reflow (preview).

For a supplement of € 15.68 ex VAT per year per installation, one has
the Adobe Institution Bundle (Add-On), with the packages: Photoshop
Elements, Premiere Elements, Captivate and Presenter.

For a supplement of € 31.35 ex VAT per year per installation, one has
the Upsell Adobe Creative Cloud Enterprise (Add-On), with the packages:
After Effects, Premiere Pro, Audition, Flash Professional, Lightroom,
Prelude and SpeedGrade.
