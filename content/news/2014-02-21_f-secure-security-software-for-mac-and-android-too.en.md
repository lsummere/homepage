---
author: petervc
date: 2014-02-21 16:38:00
tags:
- medewerkers
- studenten
title: 'F-Secure security software: for Mac and Android too'
---
On self-managed computers that have been purchased with RU funds or
through external funding for the benefit of Radboud University, you are
allowed to install the RU paid version of F-Secure. There is a [version
for MS Windows 7 and
8](http://www.radboudnet.nl/informatiebeveiliging/secure/download-secure/)
and there is a [Mac
version](http://www.radboudnet.nl/informatiebeveiliging/secure/aanvraagformulier/).

The price of F-Secure for privately owned computers (Windows PCs, Macs
en Android) has been lowered for employees as well as for students to €
2 per year. At [Surfspot](http://www.surfspot.nl) search for Secure. Of
course, you can install different security software instead, like the
free [Panda for Windows
8](http://www.pandasecurity.com/netherlands/windows8/), the free [Avast
voor Windows 7](http://www.avast.com/en-us/index) or possibly the free
[Microsoft Security
Essentials](http://windows.microsoft.com/en-us/windows/security-essentials-download)
for Windows, that is only seldom recommended.
