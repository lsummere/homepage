---
author: petervc
date: 2010-12-20 13:46:00
title: NL Language Pack for MS Office 2010 UK
---
The [Language Packs](http://office.microsoft.com/en-us/language/) for
the 32- en 64-bits version of [MS Office Professional Plus
2010](http://office.microsoft.com/en-gb/professional-plus/) are
available for computers owned by the Faculty of Science. The
[CD’s](/en/howto/microsoft-windows/) can be borrowed and are available on
the [install](/en/tags/software) disc.
