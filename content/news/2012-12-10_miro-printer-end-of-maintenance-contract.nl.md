---
author: petervc
date: 2012-12-10 13:14:00
tags:
- studenten
- medewerkers
- docenten
title: 'Miro printer: einde onderhoudscontract'
---
Jarenlang gaven sommige gebruikers de voorkeur aan de Xerox Phaser
kleuren-[printer](/nl/howto/printers-en-printen/) miro, vanwege de andere
(was-)kleurentechniek dan de Ricoh Aficio
[laserprinters](/nl/howto/printers-en-printen/). Het onderhoudscontract
van miro kan echter niet verlengd worden. Dat betekent niet dat we de
printer nu al stoppen, maar wel dat bij een grote storing in 2013 de
printer afgevoerd zal worden. Het is overigens dat bedoeling dat in de
eerste helft van 2013 op de RU overal nieuwe multifunctionals neergezet
worden.
