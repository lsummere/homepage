---
author: mkup
date: 2016-05-23 16:39:00
tags:
- medewerkers
- studenten
title: Discontinuation of authenticated network outlets in Library of Science and
  on mezzanine
---
From late 2013, a number of authenticated network outlets were available
in the reading room of the [Library of
Science](http://www.ru.nl/library/library/library-locations/library-science/)
and on the mezzanine above the restaurant. Since the improvement of the
wireless network ([eduroam)](http://www.ru.nl/ict-uk/staff/wifi/) in
2015, these outlets have hardly been used. Therefore C&CZ and the ISC
have decided to deactivate them as of Monday June 6, 2016. After that,
the associated
[Qmanage](http://www.quarantainenet.nl/oplossingen/detectie/) server
will be shut down.
