---
author: polman
date: 2009-03-24 13:50:00
title: Address book of Faculty of Science in Outlook/Thunderbird/Squirrel
---
It is very easy to find all mail addresses of staff and students in
Outlook, Thunderbird and Squirrel using the [LDAP address
book](/en/howto/ldap-adresboek/).
