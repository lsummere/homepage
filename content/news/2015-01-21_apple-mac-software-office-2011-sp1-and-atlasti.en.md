---
author: petervc
date: 2015-01-21 13:12:00
tags:
- studenten
- medewerkers
title: 'Apple Mac-software: Office 2011 SP1 and ATLAS.ti'
---
For Apple Macs there is a new (SP1) version of the Microsoft Office 2011
software and a first version of the ATLAS.ti Qualitative Data Analysis
software available on the [Install](/en/howto/install-share/) network
share. License codes are available from C&CZ helpdesk or postmaster.
According to the site license, this software may be installed on all
computers owned by the RU without additional costs. For home use of this
software, staff and students can visit
[SurfSpot](http://www.surfspot.nl).
