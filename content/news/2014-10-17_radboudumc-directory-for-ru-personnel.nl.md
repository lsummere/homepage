---
author: bertw
date: 2014-10-17 14:14:00
tags:
- medewerkers
- docenten
title: RadboudUMC-telefoongids voor RU-medewerkers
---
Voor medewerkers van de RU (met een U-nummer en RU-wachtwoord) is [de
RadboudUMC-telefoongids](https://www.extern.umcn.nl/ru_umcn_tel/ru.html)
te raadplegen op het RadboudUMC intranet. Er is ook een [overzicht van
gidsen op de campus](/nl/howto/telefoon-en-e-mail-gidsen/).
