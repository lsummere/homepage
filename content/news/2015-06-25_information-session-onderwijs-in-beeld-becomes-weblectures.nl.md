---
author: caspar
date: 2015-06-25 15:13:00
tags:
- medewerkers
- docenten
- studenten
title: 'Voorlichtingsbijeenkomst: "Onderwijs in Beeld" wordt "Weblectures"'
---
Maandag 29 juni, van 12:30 tot 13:30 (stipt), in LIN1 (Linnaeusgebouw).

Hierbij nodigen we u graag uit voor een voorlichtingsbijeenkomst over
het nieuwe weblecture systeem dat per september 2015 bij FNWI in gebruik
wordt genomen. Deze nieuwe service van de Radboud Universiteit vervangt
het huidige Onderwijs in Beeld. In een presentatie zoomen we in op wat
dit voor u betekent: wat verandert er voor docenten en studenten en wat
zijn de nieuwe mogelijkheden als u een college laat opnemen.

Wij hopen u te zien op maandag 29 juni.

Zie ook [de informatiesite over
weblectures](http://www.radboudnet.nl/weblectures) (log in met uw
u-nummer).

Met vriendelijke groeten, Anna Galema (Functioneel beheerder
weblectures) en Peter van der Wolk (Coördinator weblectures
Mediatechniek)
