---
author: sommel
date: 2009-11-19 14:53:00
title: Linux printprobleem en -wijziging
---
De [FSR](http://www.ru.nl/fnwi/fsr/) rapporteerde een beveiligingslek in
het printen vanaf Fedora Linux, dat het mogelijk maakte om van andermans
budget te printen. Als noodoplossing moet men op een Linux machine,
zoals `lilo` en `stitch`, het wachtwoord intikken bij het printen naar
een [gebudgetteerde printer](/nl/howto/printers-en-printen/). Zie de
[print informatie](/nl/howto/printers-en-printen/) voor het printen naar
gebudgetteerde printers vanuit een applicatie die niet om een wachtwoord
vraagt. Een definitieve oplossing, het invoeren van een centrale
[CUPS](http://www.cups.org/)-server, was op korte termijn niet te
realiseren. Met dank aan de FSR voor het rapporteren van het
beveiligingslek.
