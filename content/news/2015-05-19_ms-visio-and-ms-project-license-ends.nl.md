---
author: wim
date: 2015-05-19 17:12:00
tags:
- medewerkers
- docenten
- studenten
title: MS Visio en MS Project licentie stopt
---
MS [Visio](https://products.office.com/nl-nl/visio/flowchart-software)
en MS
[Project](https://products.office.com/en-us/project/project-and-portfolio-management-software)
zijn per januari 2015 geen onderdeel meer van de [eerder
aangekondigde](/nl/howto/nieuws/) nieuwe overeenkomst 2015-2017 tussen
SURFmarket en Microsoft. Voor zowel Visio als Project geldt een ‘grace
period’ van 1 jaar, wat inhoudt dat reeds geïnstalleerde versies van
deze producten tot eind 2015 zonder extra kosten mogen worden gebruikt.
Om verrassingen per 1 januari 2016 te voorkomen, zal C&CZ al in de
zomervakantie, eind juli, Visio en Project van de door C&CZ [Beheerde
Werkplekken](/nl/howto/windows-beheerde-werkplek/) verwijderen.
