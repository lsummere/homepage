---
author: mkup
date: 2013-01-17 10:59:00
tags:
- studenten
- medewerkers
title: Nieuwe Radius server voor ru-wlan en eduroam (draadloos)
---
Op maandagochtend 28 januari 2013 om 8.00 uur zal door het UCI één van
de servers waarvan het draadloze netwerk van de RU gebruik maakt, worden
vervangen. Deze vervanging zal enige gevolgen hebben voor gebruikers van
de draadloze netwerken ru-wlan en eduroam. Nadere info vindt u op onze
[Storingspagina](/nl/howto/recente-storingen/).
