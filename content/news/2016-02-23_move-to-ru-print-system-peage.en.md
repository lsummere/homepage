---
author: wim
date: 2016-02-23 17:25:00
tags:
- medewerkers
- studenten
title: Move to RU print system Peage
---
C&CZ and the [ISC](http://www.ru.nl/isc) are preparing the replacement
of the C&CZ printing system by the RU-wide
[Peage](http://www.ru.nl/facilitairbedrijf/printen/printen-studenten/printen-campus/)
printing system, that can be used by students some years now. Once staff
and guests can also use Peage, the C&CZ printing system can be phased
out. The move is expected to take place from April. Peage benefits
include: follow-me-printing and Scan-to-Me scanning. A disadvantage is
that one can not have different budget groups. Charging employees for
the costs will be done through the account (kostenplaats) of the chief
appointment. The Konica Minolta printer in the restaurant of the Huygens
building is a Peage printer already. If Peage is further implemented, we
will start with the Konica Minolta printers on the ground floor of the
Huygens building. After the ground floor, we will continue with the
other floors and finally with the buildings around Huygens.
