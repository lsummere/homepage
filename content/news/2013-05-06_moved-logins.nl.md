---
author: bram
date: 2013-05-06 17:35:00
tags:
- medewerkers
- docenten
title: Logins verhuisd
---
gemigreerd naar modernere fileservers. U kunt hierdoor problemen
ondervinden met het verbinden van uw [eigen
schijf](/nl/howto/diskruimte#home-directories/) vanaf een niet door C&CZ
beheerde werkplek, bijvoorbeeld vanaf thuis via VPN. Raadpleeg in dat
geval [de Doe-Het-Zelf website](https://dhz.science.ru.nl) voor de
actuele locatie van uw “Eigen schijf” (home directory).
