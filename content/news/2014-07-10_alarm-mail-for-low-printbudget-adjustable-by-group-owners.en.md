---
author: bram
date: 2014-07-10 17:49:00
tags:
- medewerkers
- studenten
title: Alarm mail for low printbudget adjustable by group owners
---
All [print budget](/en/howto/printbudget/) owners of a budgetgroup can
now adjust the value of the budget at which automatically an alarm mail
is sent to these owners. They can do that at the [Do-It-Yourself
website](/en/howto/dhz/).
