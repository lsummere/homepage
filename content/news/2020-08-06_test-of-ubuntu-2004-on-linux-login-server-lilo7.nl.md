---
author: simon
date: 2020-08-06 13:41:00
tags:
- medewerkers
- studenten
title: Test van Ubuntu 20.04 met Linux loginserver lilo7
---
Als test voor de nieuwe versie van Ubuntu, 20.04 LTS, hebben we een
nieuwe loginserver beschikbaar, onder de naam `lilo7.science.ru.nl`.
Over enige tijd zal deze de vier jaar oude loginserver `lilo5`
vervangen. De naam `lilo`, die altijd naar de nieuwste/snelste
loginserver wijst, zal pas dan omgezet worden van de tweeënhalf jaar
oude `lilo6` naar de nieuwe `lilo7`. De nieuwe `lilo7` is een Dell
PowerEdge R7515 met 1 AMD 7502P 2.5GHz 32-core processor en 64GB RAM.
Met hyperthreading aan lijkt dit dus een 64-processor machine. Voor wie
de identiteit van deze server wil controleren alvorens het Science
wachtwoord aan de nieuwe server te geven, zie [de sectie over onze
loginservers](/nl/howto/hardware-servers/). Alle problemen met deze
nieuwe versie van Ubuntu Linux [graag melden](/nl/howto/contact/).
