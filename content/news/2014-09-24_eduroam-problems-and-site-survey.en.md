---
author: mkup
date: 2014-09-24 10:35:00
tags:
- medewerkers
- studenten
title: Eduroam problems and site survey
---
There are [again](/en/howto/nieuws-archief/) complaints about the
wireless network [Eduroam](http://www.ru.nl/draadloos), with
authentication as well as with coverage. In case of problems with RU
authentication (U123456\@ru.nl, s123456\@ru.nl, …) one can try the
alternative scienceloginname\@science.ru.nl. To improve coverage in the
Huygens building, Ronald Versteeg of [JS Network
Solutions](http://www.js-networksolutions.nl/) will be measuring in the
entire Huygens building for the next four weeks. For restricted access
areas he will consult with [C&CZ Network
admins](/en/howto/netwerkbeheer/) and/or the appropriate department.The
[ISC network department](http://www.ru.nl/isc) will base improvement
plans on the results of that site survey.

C&CZ invites everyone to report problems with the wireless and wired
network to netmaster\@science.ru.nl.
