---
author: petervc
date: 2013-05-29 14:21:00
tags:
- medewerkers
- docenten
- studenten
title: Ricoh and Xafax replaced by Konica Minolta
---
In a few weeks we will place new [Konica Minolta (KM)](/en/howto/km/)
multifunctionals (MFP’s) next to the [Ricoh](/en/howto/ricoh/)’s and some
other places. After a short period, in which one can switch from Ricoh
to KM, we will remove most of the Ricoh’s. Until July we will have a
Ricoh with Xafax-pole in a few places, so one can empty the Xafax-cards.
Because the RU-wide
[Peage](http://www.ru.nl/gdi/voorzieningen/printen-scannen/peage-%28-english%29-0/)-project
is still not in operation for employees, the new KM’s can be used like
the Ricoh’s. The only difference is that the Xafax-poles will be removed
because the contract with Xafax ends and cannot be continued. Copying on
th KM’s therefore will be only be possible after entering a code. We
will contact departments about the code and about the
budget/kostenplaats from which the copy costs can be reimbursed. If you
do not have a code, copying can be accomplished by scanning and
printing.
