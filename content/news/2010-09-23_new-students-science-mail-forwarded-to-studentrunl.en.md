---
author: polman
date: 2010-09-23 18:23:00
tags:
- studenten
title: 'New students: Science-mail forwarded to @student.ru.nl'
---
In the first weeks of the new college year, mail sent to new students
@student.science.ru.nl, has been forwarded to their
[Studielink](http://www.studielink.nl) address, because no
[@student.ru.nl](http://share.ru.nl) address was available. Today, this
forward has been changed to the @student.ru.nl address, because Biology
otherwise could not send mail to all new students. If a new student
wants to change the forwarding address, that can be done on the
[Do-It-Yourself website](http://diy.science.ru.nl).
