---
author: bertw
date: 2012-09-12 14:11:00
tags:
- studenten
- medewerkers
title: Mobile telephony in and around the Huygens building
---
Mobile enterprise telephony with Vodafone Wireless Office (VWO) is
offered as a service as of today. The indoor coverage of the
[Vodafone](http://www.vodafone.nl) mobile network in all of the Huygens
building is good enough to go forward with VWO. There are plans to
improve the coverage in the cellars and the buildings around the Huygens
building (Logistiek Centrum, NMR, HFML, FEL and Nanolab). The earlier
expected connection of KPN and T-mobile to the [Distributed Antenna
System](http://en.wikipedia.org/wiki/Distributed_Antenna_System) that
was installed for this, is still uncertain. Our [telephony
page](/en/tags/telefonie/) contains all info about VWO (mobile calling
and being reachable with RI-internal 5-digit numbers, also outside of
the campus. You can also find info about IP-telephony there.
IP-telephony (desktop) and VWO (cell-phone) are the future of
RU-telephony.
