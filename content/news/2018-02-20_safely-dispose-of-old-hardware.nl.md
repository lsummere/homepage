---
author: stefan
date: 2018-02-20 14:50:00
tags:
- medewerkers
title: Oude hardware veilig verwijderen
---
Bij het [Logistiek
Centrum](https://www.radboudnet.nl/fnwi/fnwi/ihz-algemeen/ihz-logistiek/)
van IHZ is een afgesloten ruimte waar oude hardware veilig afgevoerd kan
worden. Dit is van belang voor datadragers om datalekken te voorkomen.
Deponeer a.u.b. oude hardware die data bevat niet in een container waar
een voorbijganger in kan grabbelen.
