---
author: stefan
date: 2017-05-24 17:22:00
tags:
- medewerkers
- studenten
title: Poster printer service kamerbreed more reliable
---
The [A0 photo printer kamerbreed](/en/howto/kamerbreed/), that C&CZ
acquired free of charge in May 2013, printed many posters for staff and
students. Lately there were several breakdowns, that took quite long to
repair. That’s why we decided to buy a new one, with warranty contract.
In case of calamities we can switch to the old one. We expect that that
makes the poster print service a guaranteed service for the next years,
where we will not have to redirect customers to the [RU
copyshop](http://www.ru.nl/facilitairbedrijf/winkels/copyshop/).
