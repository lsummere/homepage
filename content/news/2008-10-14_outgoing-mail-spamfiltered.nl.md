---
author: josal
date: 2008-10-14 17:40:00
title: Uitgaande mail op spam gefilterd
---
Vanaf vandaag wordt mail die door het [Science
spamfilter](/nl/howto/email-spam/) als spam herkend wordt, *niet meer
naar externe mailservers verstuurd*, maar in een centrale
quarantaine-map bewaard. Deze wijziging van het gedrag van de [Science
mailservers](/nl/howto/hardware-servers/) is noodzakelijk omdat die
servers anders op een zwarte lijst terecht komen. Het via de
[Doe-Het-Zelf website](http://dhz.science.ru.nl) instellen van
[doorsturen/forwarden van inkomende mail](/nl/tags/email) levert dus
vanaf nu een *gefilterde mailstroom* op.
