---
author: bram
date: 2016-04-26 17:00:00
tags:
- studenten
- medewerkers
title: 'Phasing out svn service: use GitLab'
---
As of june 1st, we will shut down our [Subversion](/en/howto/subversion/)
server. Only a small number of users have active repositories on the
subversion server. [GitLab](/en/howto/gitlab/) is offered as an
alternative service. Users of svn.science.ru.nl and svn.cs.ru.nl are
requested to move their repositories. Instructions on how to proceed can
be found on our [Subversion](/en/howto/subversion/) page.
