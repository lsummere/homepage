---
author: simon
date: 2019-02-26 10:43:00
tags:
- medewerkers
- docenten
- studenten
title: 'eLabFTW: open source electronic lab notebook en database'
---
Recently, [eLabFTW](https://elabftw.science.ru.nl) has been made
available for departments in FNWI. This is a free, secure, modern and
compliant open source electronic lab notebook and database, with which
different teams (research groups) can efficiently track their
experiments, but also manage their lab with a powerful and flexible
database. Employees of departments that already use eLabFTW (within and
around Molecular Biology) can register themselves at
[eLabFTW](https://elabftw.science.ru.nl), new departments must first
[contact Postmaster](/en/howto/contact/) in order to make a new team. See
[www.eLabFTW.net](https://www.elabftw.net/) for more info.
