---
author: polman
date: 2014-01-16 17:58:00
tags:
- medewerkers
- docenten
- studenten
title: Automatic configuration of mail clients
---
When installing a mail client it is now possible to configure all
settings (login name, IMAP and SMTP server and parameters) automatically
for most of the C&CZ mail domains. In general you just need to fill in
your main mail address. For details, see [the mail
service](/en/tags/email) page. Using a webmail service like
[Roundcube](http://roundcube.science.ru.nl/) is even simpler, there are
no settings to fill in.
