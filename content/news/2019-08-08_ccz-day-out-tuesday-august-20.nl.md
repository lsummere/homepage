---
author: petervc
date: 2019-08-08 12:42:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: dinsdag 20 augustus'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor dinsdag 20 augustus.
Voor bereikbaarheid in geval van ernstige storingen wordt gezorgd.
