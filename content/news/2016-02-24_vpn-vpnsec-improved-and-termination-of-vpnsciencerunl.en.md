---
author: wim
date: 2016-02-24 14:35:00
tags:
- medewerkers
- studenten
title: 'VPN: vpnsec improved and termination of vpn.science.ru.nl'
---
The configuration and [installation information](/en/howto/vpn/) of the
new VPN (the IPsec based vpnsec.science.ru.nl) has been improved, in
particular for OS X. The
[Vpnsec-macosx.mobileconfig](/en/howto/images/6/61/vpnsec-macosx.mobileconfig/)
has been signed with a [certificate that can be
verified](https://www.digicert.com/intranet-server-security.htm). The
new VPN can now be used with all devices that we tested. All users
should move to the new VPN before May 1, 2016. After that, we will
terminate the old VPN, that is based on
[PPTP](https://nl.wikipedia.org/wiki/Point-to-Point_Tunneling_Protocol).
