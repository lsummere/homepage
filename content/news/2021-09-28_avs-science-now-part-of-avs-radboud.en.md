---
author: bbellink
date: 2021-09-28 13:02:00
tags:
- medewerkers
- docenten
- studenten
title: AVS Science now part of AVS Radboud
---
As of October 1, the [Science
AVS](https://www.ru.nl/science/about-the-faculty/organisation/supporting-services/audio-visual-service/)
will be merged with [Radboud
AVS](https://www.ru.nl/fb/english/services/media-technology/). This
reorganization is a result of the improvement programme of facility
management and real estate, with bundling of similar services on campus.
This bundling creates a campus-wide AV service that will be controlled
in its entirety by [Campus & Facilities](https://www.ru.nl/fb/english/).
For FNWI users, the existing contact options (email, telephone) and
availability of local support will be continued for the time being in
accordance with the existing situation. There will be further
communication about any changes in the future. The AVD Faculty of
Science resorted under C&CZ since January 2018.
