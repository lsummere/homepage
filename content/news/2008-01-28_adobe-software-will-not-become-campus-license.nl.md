---
author: petervc
date: 2008-01-28 14:21:00
title: Adobe software wordt geen campuslicentie\!
---
Vanmiddag meldde [SURFdiensten](http://www.surfdiensten.nl) dat niet
genoeg onderwijsinstellingen wilden meedoen met de voorgestelde
meerjarige overeenkomst voor [Adobe](http://www.adobe.com) produkten.
Hierdoor zijn mensen die de komende tijd Adobe-produkten willen
aanschaffen aangewezen op stukslicenties.
