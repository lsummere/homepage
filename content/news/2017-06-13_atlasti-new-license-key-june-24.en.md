---
author: petervc
date: 2017-06-13 12:40:00
tags:
- studenten
- medewerkers
title: ATLAS.ti new license key June 24
---
The [ISC](http://www.ru.nl/isc) license manager let us know that the new
license key of ATLAS.ti [will be
available](http://www.radboudnet.nl/ictservicecentrum/codes/licentiecode-atlas/)
starting June 24.
