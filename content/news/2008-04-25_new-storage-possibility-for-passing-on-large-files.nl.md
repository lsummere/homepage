---
author: polman
date: 2008-04-25 12:56:00
title: Nieuwe opslagmogelijkheid voor het doorgeven van grote bestanden
---
`Om het lokaal doorgeven van grote bestanden (waar mail minder geschikt voor is) te vereenvoudigen is er een nieuwe`

netwerk share gemaakt, zie [TempDisk](/nl/howto/tempdisk/).
