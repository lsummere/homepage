---
author: _josal
date: 2009-01-21 13:49:00
title: Power failure server room HG04.070
---
January 20, around 10:45h, the power supply fire protection unit in the
server room HG04.070 was activated, which caused an immediate and
complete power down of all servers in that room.

Through bypassing the protection unit, power could be switched on again,
first partially and fully functional again about 12:00h.

Then the fixing hardware and software problems resulting from the
immediate shutdown could be started. This took several hours for some
systems. Work on the student file server lasted until the morning of
January 21.

Around 18:00h the protection unit was activated again with a larger
margin for the room temperature.
