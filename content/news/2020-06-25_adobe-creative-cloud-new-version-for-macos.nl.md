---
author: john
date: 2020-06-25 17:25:00
tags:
- studenten
- medewerkers
- docenten
title: Adobe Creative Cloud nieuwe versie voor macOS
---
Van de Adobe [Creative Cloud
software](https://www.adobe.com/nl/creativecloud.html) (Acrobat,
Illustrator, Photoshop etc., nu ook Premiere Pro en Audition) is voor
macOS, inclusief macOS 10.15 Catalina, een nieuwe versie beschikbaar.
Deze mag gebruikt worden op apparaten die eigendom van de RU zijn. De
installatie gaat via de Adobe Creative Cloud Desktop app, waarmee je na
inloggen met een Adobe account, de via de RU-licentie beschikbare
Adobe-applicaties kunt uitkiezen. Gedetailleerde instructies voor
installatie kunnen via [de C&CZ helpdesk](/nl/howto/contact/) verkregen
worden.
