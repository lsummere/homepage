---
author: polman
date: 2009-05-27 12:07:00
title: Mathematica Player
---
In alle terminalkamers is Mathematica Player geïnstalleerd, hiermee zijn
Mathematica notebooks af te spelen, zie de
[Wolfram](http://www.wolfram.com/products/player/) website voor meer
informatie.
