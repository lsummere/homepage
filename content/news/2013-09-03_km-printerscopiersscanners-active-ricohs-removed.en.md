---
author: petervc
date: 2013-09-03 14:06:00
tags:
- studenten
- medewerkers
- docenten
title: KM printers/copiers/scanners active, Ricohs removed
---
All [Konica Minolta MFP’s](/en/howto/km/) are in production now, all
Ricohs have been switched off. KM has changed the settings of a few
printers to print only b/w, preventing accidental color pages. We advise
everyone to try to set the printer configuration on your desktop to
print default in b/w. [Printing](/en/howto/printers-en-printen/) and
[scanning](/en/howto/scanners/) works roughly the same as with the
Ricohs. Employees of the Faculty of Science can [contact
C&CZ](/en/howto/contact/) to get a PIN code for copying. Students can
make copies by using the [Peage
MFP](http://www.ru.nl/ictservicecentrum/studenten/peage-%28-english%29/)
near the restaurant, or make copies by scanning and printing afterwards.
