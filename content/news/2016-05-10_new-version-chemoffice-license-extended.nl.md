---
author: polman
date: 2016-05-10 13:54:00
tags:
- studenten
- medewerkers
title: Nieuwe versie ChemOffice, verlenging licentie
---
De licentie van [ChemBioOffice](/nl/howto/chembiooffice/) is verlengd tot
mei 2019, tevens is er een nieuwe versie beschikbaar ChemOffice 2015.
Deze versie zal de oude versie uit 2013 vervangen op alle door C&CZ
beheerde pc’s.
