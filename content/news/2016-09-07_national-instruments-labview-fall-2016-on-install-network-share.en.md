---
author: petervc
date: 2016-09-07 18:18:00
tags:
- medewerkers
- docenten
title: National Instruments LabVIEW Fall 2016 on Install network share
---
To make it easier to install [NI LabVIEW](http://www.ni.com/labview/)
for departments that participate in the license, the newly arrived “Fall
2016” version has been copied to the [Install](/en/howto/install-share/)
network share. Install media can also be borrowed. License codes can be
obtained from C&CZ helpdesk or postmaster.
