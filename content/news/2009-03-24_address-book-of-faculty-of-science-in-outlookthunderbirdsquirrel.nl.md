---
author: polman
date: 2009-03-24 13:50:00
title: Adresboek FNWI in Outlook/Thunderbird/Squirrel
---
Het is erg makkelijk om alle mail adressen van medewerkers en studenten
te vinden in Outlook,Thunderbird of Squirrel door gebruik te maken van
het [LDAP adresboek](/nl/howto/ldap-adresboek/).
