---
author: mkup
date: 2013-03-05 11:02:00
tags:
- studenten
- medewerkers
- docenten
title: More IP-numbers for ru-wlan and Science (wireless)
---
Monday, March 4th 2013 at 18:00 hours, the number of IP numbers that is
available in the FNWI buildings for ru-wlan and Science was doubled, [as
was announced shortly before](/en/howto/recente-storingen/). Because
ru-wlan moved to a new range, users of ru-wlan lost connectivity for at
most 15 minutes. There was already a plan to replace ru-wlan and Science
within the FNWI buildings by the RU-wide Eduroam and ru-wlan, but the
wireless network usage had grown so fast that we could not wait for this
plan to be realized. The week before, some wireless users at times could
not even get an IP address, although the lease time had been brought
down to 30 minutes. Therefore this temporary measure became necessary on
such short notice. Because there are more IP numbers available now, more
users can now[switch from Science to ru-wlan
wireless](/en/howto/netwerk-draadloos/), in preparation for the
realization of the plan mentioned above.
