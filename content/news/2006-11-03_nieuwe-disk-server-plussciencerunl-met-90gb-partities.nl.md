---
author: polman
date: 2006-11-03 15:11:00
title: Nieuwe disk-server plus.science.ru.nl met 90GB partities
---
Er is weer een nieuwe
[disk-server](/nl/howto/hardware-servers#disk.5b-.5d.5b-.5dservers/)
plus.science.ru.nl met door FNWI-afdelingen te huren 90GB partities van
een RAID-array.
