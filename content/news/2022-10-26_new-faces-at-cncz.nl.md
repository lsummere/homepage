---
title: Nieuwe gezichten bij C&CZ
author: petervc
date: 2022-10-26
tags:
- medewerkers
- studenten
cover:
  image: img/2022/new-faces-at-c-cz.png
aliases:
- 2022-10-26_new-faces-at-c-cz
---
C&CZ is erg tevreden dat recente vacatures vervuld zijn. Een jaar geleden kwam
[Eric Lieffers](https://www.ru.nl/personen/lieffers-h)
als systeemontwikkelaar/systeembeheerder. Eric heeft jaren als systeembeheerder bij FSW en bij ISC/ILS gewerkt.
Vorige maand kwam
[Miek Gieben](https://www.ru.nl/personen/gieben-r)
het team systeemontwikkeling/systeembeheer versterken en
[Alexander Greefhorst](https://www.ru.nl/personen/greefhorst-a)
trad toe als ontwikkelaar. Miek studeerde Informatica in Nijmegen en
is nu terug in Nijmegen na onder andere gewerkt te hebben voor SIDN, Google en Goldman Sachs,
de laatste jaren als Site Reliability Engineer. Alexander is een ervaren backend ontwikkelaar,
gespecialiseerd in complexe datastructuren, hij heeft een achtergrond in taalkunde. 
