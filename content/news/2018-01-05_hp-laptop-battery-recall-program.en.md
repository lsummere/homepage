---
author: stefan
date: 2018-01-05 17:40:00
tags:
- medewerkers
- docenten
title: HP laptop battery recall program
---
HP announced a recall program for laptop batteries that may overheat and
pose a fire hazard. The batteries were sold in the period between
December 2015 and December 2017. If you bought an HP laptop or battery
in that period, it is wise to [[check whether the battery should be
replaced](https://batteryprogram687.ext.hp.com/).
