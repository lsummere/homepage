.PHONY: test
test:
	bin/check_frontmatter
	bin/check_lang
	echo $$(git rev-parse HEAD)
	CI_COMMIT_SHA=$$(git rev-parse HEAD) hugo server --verbose --navigateToChanged --buildFuture

.PHONY: mirror
mirror:
	CI_COMMIT_SHA=$$(git rev-parse HEAD) hugo server -b home.cncz.nl -d /tmp/xxx
