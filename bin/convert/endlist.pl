#!/usr/bin/perl

$html = "off";
while(<>) {
    if ( /```{=html}/ ) {
        $html = "on";
        next
    }
    if ($html eq "on") {
        if (/<!-- end list -->/) {
            $html = "suppress";
            next
        }
    }

    if ($html eq "suppress") {
        $html = "off";
        next
    }

    if ($html eq "on") {
        print "```{=html}\n";
        $html = "off"
    }


    print $_;
}
